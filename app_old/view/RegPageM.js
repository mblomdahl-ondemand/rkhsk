Ext.define('RKHSK.view.RegPageM', {
  //extend: 'Ext.viewport.Viewport',
  extend: 'Ext.TabPanel',
  alias: 'widget.regpagem',
  requires: [
    'Ext.form.Panel',
    'Ext.TitleBar',
    'Ext.form.FieldSet',
    'Ext.field.Radio',
    'Ext.field.CheckBox',
    'Ext.field.Select',
    'Ext.field.Email',
    'RKHSK.view.cmp.PersonalIdFieldM',
    'RKHSK.view.cmp.NameFieldM',
    'RKHSK.view.cmp.AdmittedSemesterFieldM',
    'RKHSK.view.cmp.PaymentKeyFieldM',
    'RKHSK.view.cmp.StreetAddressFieldM',
    'RKHSK.view.cmp.StreetNumberFieldM',
    'RKHSK.view.cmp.StreetEntranceFieldM',
    'RKHSK.view.cmp.StreetApartmentFieldM',
    'RKHSK.view.cmp.ZipCodeFieldM',
    'RKHSK.view.cmp.CountryFieldM',
    'RKHSK.view.cmp.CityFieldM',
    'RKHSK.view.cmp.PhoneFieldM',
    'RKHSK.view.cmp.EmailFieldM'
  ],
  defaults: {
    styleHtmlContent: true
  },
  //form = Ext.Viewport.add(formBase);
  
  //autoRender: true,
  //height: 300,
  //width: 300,
  id: 'regPageM',
  //autoScroll: true,
  fullscreen: true,
  //layout: 'fit',
  config: {
    //style: 'background: #f6f6f6; background-image: url(resources/images/logo-bg.png);',
    fullscreen: true,
    //layout: 'card',
    //autoRender: true,
    //scrollable: 'vertical',
    items: [
      {
        title: 'Medlemsansökan',
        xtype: 'formpanel',
        autoRender: true,
        id: 'regForm',
        items: [
          {
            margin: '0 -17 0 -17',
            styleHtmlContent: true,
            //xtype: 'label',
            cls: 'rm-label',
            minHeight: 400,
            html: [
              '<div style="background-image: url(resources/images/logo-bg.png); background-size: 100%; background-repeat: no-repeat; background-position: -17px 0; max-width: 480px; width: 100%; min-height: 400px; position: absolute; margin: auto; z-index: -1;"></div>',
              '<h4><b>RKH-SK: Medlemsregistrering<br>http://regga.rkh-sk.se</b></h4>',
              '<p>När du skickat in din medlemsanmälan får du ett e-postmeddelande till adressen du angivit. Meddelandet innehåller information om hur du betalar in medlemsavgiften och dina personliga inloggningsupggifter.</p>',
              '<p>Med hjälp av din personliga kod kan du återvända till denna sida för att se status för ditt ärende och få svar på frågor som <i>Har min inbetalning registrerats av studentkåren än?</i> och <i>Är mina uppgifter överförda till SSSB och Mecenat?</i></p>',
              '<p>Medlemsanmälningar för vårterminen tas emot från och med 1 januari. För höstterminen öppnar anmälningen den 1 augusti.</p>',
              '<p>Obs: Genom att registrera dig som medlem i Röda Korsets Högskolas Studentkår [RKH-SK] accepterar du att dina personuppgifter behandlas i RKH-SK:s medlemsregister.</p>'
            ].join(''),
            //height: 300,
          },
          {
            xtype: 'fieldset',
            title: '1. Medlemsansökan avser ...',
            instructions: 'Ange vilken termin din ansökan om medlemskap avser.<br>(* markerar obligatoriska fält.)',
            defaults: {
              required  : false,
              labelAlign: 'left',
              labelWidth: '50%'
            },
            items: [
              {
                xtype: 'radiofield',
                id: 'lastApplicationFor1',
                label: 'Termin VT12',
                name: 'lastApplicationFor',
                value: 'VT12',
                required: true,
                checked: true
              },
              {
                xtype: 'radiofield',
                id: 'lastApplicationFor2',
                label: 'Termin HT12',
                name: 'lastApplicationFor',
                value: 'HT12',
                disabled: true
              }
            ]
          },
          {
            xtype: 'fieldset',
            title: '2. Årskurs & personuppgifter',
            instructions: 'Ange årskurs vid RKH och dina personuppgifter.<br>(* markerar obligatoriska fält.)',
            defaults: {
              required  : true,
              labelAlign: 'left',
              labelWidth: '40%',
              clearIcon: true
            },
            items: [
              {
                xtype: 'admittedsemesterfield',
                name: 'admittedSemester',
                id: 'admittedSemester',
                label: 'Årskurs',
                store: 'AdmissionSemesterM',
                displayField: 'name',
                placeHolder: 'Ange årskurs',
                valueField: 'value'
              },
              {
                xtype: 'personalidfield',
                id: 'personalId',
                label: 'Personnr.',
                placeHolder: 'YYMMDD-NNNN',
                name: 'personalId'
              },
              {
                xtype: 'namefield',
                id: 'firstName',
                label: 'Förnamn',
                placeHolder: 'Ange förnamn',
                name: 'firstName'
              },
              {
                xtype: 'namefield',
                id: 'lastName',
                label: 'Efternamn',
                placeHolder: 'Ange efternamn',
                name: 'lastName'
              }
            ]
          },
          {
            xtype: 'fieldset',
            title: '3. Adressuppgifter',
            instructions: 'Ange din adressinformation. Obs: Ditt Mecenat-kort kommer skickas till adressen du anger här, alltså bör du ange den adress du faktiskt bor på framför din folkbokföringsadress.<br>(* markerar obligatoriska fält.)',
            defaults: {
              required  : false,
              labelAlign: 'left',
              labelWidth: '40%',
              clearIcon: true
            },
            items: [
              {
                xtype: 'namefield',
                id: 'streetCo',
                label: 'C/o-adress',
                name: 'streetCo'
              },
              {
                xtype: 'streetaddressfield',
                id: 'streetAddress',
                label: 'Gatuadress',
                vtype: 'streetaddress',
                placeHolder: 'Ange gata (ej nr.)',
                name: 'streetAddress',
                required: true
              },
              {
                xtype: 'streetnumberfield',
                id: 'streetNumber',
                label: 'Gatunr.',
                placeHolder: 'Ange gatunummer',
                name: 'streetNumber',
                required: true
              },
              {
                xtype: 'streetentrancefield',
                id: 'streetEntrance',
                label: 'Port',
                name: 'streetEntrance'
              },
              {
                xtype: 'streetapartmentfield',
                id: 'streetApartment',
                label: 'Lgh.nr.',
                vtype: 'streetapartment',
                name: 'streetApartment'
              },
              {
                xtype: 'selectfield',
                id: 'streetFloor',
                label: 'Våning',
                name: 'streetFloor',
                store: 'StreetFloor',
                displayField: 'name',
                valueField: 'value'
              },
              {
                xtype: 'zipcodefield',
                id: 'zipCode',
                label: 'Postnr.',
                placeHolder: 'Format: NNNNN',
                name: 'zipCode',
                required: true
              },
              {
                xtype: 'cityfield',
                id: 'city',
                label: 'Postort',
                name: 'city',
                placeHolder: 'Ange postort',
                required: true
              },
              {
                xtype: 'countryfield',
                id: 'country',
                label: 'Land',
                name: 'country',
                required: true,
                value: 'Sverige',
                readOnly: true
              }
            ]
          },
          {
            xtype: 'fieldset',
            title: '4. Kontaktuppgifter',
            instructions: 'Berätta hur vi bäst kan komma i kontakt med dig! (* markerar obligatoriska fält.)',
            defaults: {
              labelAlign: 'left',
              clearIcon: true,
              labelWidth: '30%'
            },
            items: [
              {
                xtype: 'emailfield',
                id: 'email',
                label: 'E-post',
                placeHolder: 'Ange e-postadress',
                required: true,
                name: 'email',
                maxLength: 40,
              },
              {
                xtype: 'phonefield',
                required  : false,
                id: 'phone',
                label: 'Telnr',
                name: 'phone',
              }
            ]
          },
          {
            xtype: 'fieldset',
            title: '5. Studentintygande',
            instructions: 'Var vänlig och intyga att du studerar vid Röda Korsets Högskola (* markerar obligatoriska fält).',
            items: [
              {
                required  : true,
                labelAlign: 'left',
                labelWidth: '81%',
                xtype: 'checkboxfield',
                id: 'studentConfirmation',
                name: 'studentConfirmation',
                label: 'Jag är studieaktiv vid RKH',
                checked: true,
              }
            ]
          },
          {
            xtype: 'fieldset',
            title: '6. Skicka in dina uppgifter',
            items: [
              {
                xtype: 'button',
                id: 'submitRegForm',
                text: 'Registrera medlemsanmälan',
              }
            ]
          },
          {
            margin: '0 -17 0 -17',
            styleHtmlContent: true,
            //xtype: 'label',
            cls: 'rm-courtesy-notice',
            html: '<p><i>Courtesy of <a href="mailto:mats.blomdahl@gmail.com">mats.blomdahl@gmail.com</a>. Please email all and any bug reports.</i></p>'
            //height: 300,
          }
        ]
      },
      {
        title: 'Ärendestatus',
        xtype: 'formpanel',
        id: 'statusForm',
        autoRender: true,
        items: [
          {
            xtype: 'fieldset',
            title: 'Ange inloggningsuppgifter',
            margin: '-17 0 27 0',
            instructions: '(* markerar obligatoriska fält.)',
            defaults: {
              labelAlign: 'left',
              clearIcon: true,
              labelWidth: '47%'
            },
            items: [
              {
                xtype: 'emailfield',
                id: 'loginUsername',
                label: 'Email-adress',
                placeHolder: 'Ange e-postadr.',
                required: true,
                clearIcon: true,
                name: 'loginUsername',
                maxLength: 40,
              },
              {
                xtype: 'paymentkeyfield',
                id: 'loginCredentials',
                label: 'Personlig kod',
                placeHolder: 'Format: XXXXX',
                required: false,
                name: 'loginCredentials',
              },
            ]
          },
          {
            xtype: 'fieldset',
            //title: 'Inloggningsuppgifter',
            items: [
              {
                xtype: 'button',
                id: 'retrieveStatus',
                disabled: true,
                text: 'Visa ärendestatus'
              }
            ]
          },
          {
            xtype: 'fieldset',
            //title: 'Inloggningsuppgifter',
            instructions: 'Ange e-postadress för att skicka ut ett påminnelse-mail med din personliga kod.',
            items: [
              {
                xtype: 'button',
                id: 'resendCredentials',
                disabled: true,
                text: 'Skicka nya inloggningsuppg.'
              }
            ]
          },
          {
            margin: '0 -17px 0 -17px',
            styleHtmlContent: true,
            //xtype: 'label',
            cls: 'rm-courtesy-notice',
            html: '<p><i>Courtesy of <a href="mailto:mats.blomdahl@gmail.com">mats.blomdahl@gmail.com</a>. Please email all and any bug reports.</i></p>'
            //height: 300,
          }
        ]
      }
    ]
  }
});