Ext.define('RKHSK.view.AdminPageM', {
  //extend: 'Ext.viewport.Viewport',
  extend: 'Ext.Panel',
  alias: 'widget.adminpagem',
  requires: [
    'Ext.form.Panel',
    'Ext.form.FieldSet',
    'Ext.field.Radio'/*,
    'RKHSK.view.cmp.PersonalIdFieldM',
    'RKHSK.view.cmp.NameFieldM',
    'RKHSK.view.cmp.AdmittedSemesterFieldM',
    'RKHSK.view.cmp.PaymentKeyFieldM',
    'RKHSK.view.cmp.StreetAddressFieldM',
    'RKHSK.view.cmp.StreetNumberFieldM',
    'RKHSK.view.cmp.StreetEntranceFieldM',
    'RKHSK.view.cmp.StreetApartmentFieldM',
    'RKHSK.view.cmp.ZipCodeFieldM',
    'RKHSK.view.cmp.CountryFieldM',
    'RKHSK.view.cmp.CityFieldM',
    'RKHSK.view.cmp.PhoneFieldM',
    'RKHSK.view.cmp.EmailFieldM'*/
  ],
  //form = Ext.Viewport.add(formBase);
  
  //autoRender: true,
  //height: 300,
  //width: 300,
  id: 'adminPageM',
  //autoScroll: true,
  //fullscreen: true,
  //layout: 'fit',
  config: {
    //style: 'background: #f6f6f6; background-image: url(resources/images/logo-bg.png);',
    styleHtmlContent: true,
    fullscreen: true,
    //layout: 'card',
    //autoRender: true,
    //scrollable: 'vertical',
    cls: 'am-label',
    minHeight: 400,
    html: [
      '<h4><b>RKH-SK: Medlemsdatabas<br>http://regga.rkh-sk.se/admin</b></h4>',
      '<p>Sidan är under uppbyggnad. Välkommen åter vid ett senare tillfälle.</p>',
      '<div style="background-image: url(resources/images/logo-bg.png); background-size: 100%; background-repeat: no-repeat; background-position: 0 0; max-width: 480px; width: 100%; min-height: 400px; position: relative; margin: auto;"></div>',
    ].join('')
  }
});