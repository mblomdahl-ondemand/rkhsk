Ext.define('RKHSK.view.AdminPageD', {
  extend: 'Ext.container.Viewport',
  requires: [
    'Ext.ux.grid.GridHeaderFilters'
  ],
  id: 'adminPageD',
  layout: {
    type: 'vbox',
    align: 'stretch'
  },
  autoScroll: false,
  style: 'background: #f6f6f6;',
  fullscreen: true,
  items: [
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      id: 'loginStatusMsg',
      //autoDestroy: false,
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      id: 'addStatusMsg',
      //autoDestroy: false,
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      id: 'editStatusMsg',
      //autoDestroy: false,
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      id: 'paymentRegStatusMsg',
      //autoDestroy: false,
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      id: 'paymentsUploadStatusMsg',
      //autoDestroy: false,
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      //autoDestroy: false,
      id: 'attachmentUploadStatusMsg',
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      //autoDestroy: false,
      id: 'mecenatStatusMsg',
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      //autoDestroy: false,
      id: 'sscoStatusMsg',
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      //autoDestroy: false,
      id: 'remoteSessionStatusMsg',
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      //autoDestroy: false,
      id: 'remoteEditStatusMsg',
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      //autoDestroy: false,
      id: 'remoteAddStatusMsg',
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      //autoDestroy: false,
      id: 'remoteMecenatSyncStatusMsg',
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'label',
      style: 'padding: 4px; background: rgba(227,227,227,0.5);',
      dock: 'top',
      //autoDestroy: false,
      id: 'remoteSscoSyncStatusMsg',
      floating: true,
      x: 105,
      y: 2
    },
    {
      xtype: 'panel',
      height: 101,
      id: 'pageHeader',
      cls: 'lg-panel',
      bodyCls: 'no-panel-borders',
      minWidth: 740,
      dockedItems: [
        {
          xtype: 'container',
          width: 342,
          style: 'background: rgba(255,255,255,0);',
          layout: {
            type: 'anchor'
          },
          dock: 'right',
          items: [
            {
              xtype: 'toolbar',
              //id: 'menuToolbar',
              items: [
                {
                  xtype: 'button',
                  id: 'menuDocumentation',
                  text: 'Användarmanual',
                  handler: function() {
                    /*Ext.create('BeijerInst.view.biAdminWindow', {
                      renderTo: Ext.getBody()
                    }).show();*/
                  }
                },
                {
                  xtype: 'button',
                  id: 'menuAdministrate',
                  text: 'Administration',
                  handler: function() {
                    /*Ext.create('BeijerInst.view.biAdminWindow', {
                      renderTo: Ext.getBody()
                    }).show();*/
                  }
                },
                {
                  xtype: 'button',
                  id: 'menuEmail',
                  text: 'E-posthantering',
                  handler: function() {
                    /*Ext.create('BeijerInst.view.biAccountWindow', {
                      renderTo: Ext.getBody()
                    }).show();*/
                  }
                },
                {
                  xtype: 'button',
                  id: 'menuLogInOut',
                  text: 'Logga in',
                  handler: function() {
                    console.log("logging out, plz")
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: 'container',
          width: 600,
          style: 'background: rgba(255,255,255,0);',
          layout: {
            type: 'anchor'
          },
          dock: 'left',
          html: '<div style="height: 110px; background: none; background-image: url(/resources/images/logo-admin-8.png); background-repeat: no-repeat;"></div>'
        }
      ]
    },
    {
      xtype: 'panel',
      id: 'pageRegPanel',
      title: 'Registrera inbetalningar',
      layout: 'hbox',
      height: (Ext.isIE9 ? 239 : 229),
      collapsible: true,
      titleCollapse: true,
      collapsed: true,
      bodyPadding: '0 5 0 5',
      frame: true,
      anchor: '100%',
      items: [
        {
          xtype: 'fieldset',
          title: 'Manuell registrering',
          flex: 1,
          height: (Ext.isIE9 ? 195 : 185),
          margin: '0 0 0 0',
          padding: (Ext.isIE9 ? '2 8 0 10' : '4 8 0 10'),
          //style: 'background: green;',
          layout: {
            align: 'stretchmax',
            type: 'hbox'
          },
          items: [
            {
              xtype: 'label',
              margin: '0 5 0 0',
              html: '<p>FÖRKLARINGSTEXT</p><p>Betalningar anges med en inbetalning per rad. Varje post måste innehålla ett datum och avsändarens personliga kod i formen "YYYY-MM-DD, XXXXX".</p><p><br><u>Exempel</u>:<br>2012-01-19, BAXQS<br>2012-01-19, FOROO<br>(o.s.v.)</p>',
              flex: 1,
            },
            {
              xtype: 'container',
              width: 150,
              layout: {
                type: 'vbox',
                align: 'stretchmax'
              },
              //height: 175,
              margin: '0 0 0 0',
              padding: '0 0 0 0',
              //style: 'background: red;',
              items: [
                {
                  xtype: 'textareafield',
                  name: 'uploadPaymentsField',
                  id: 'uploadPaymentsField',
                  fieldLabel: 'Lista inbetalningar',
                  flex: 1,
                  anchor: '100%',
                  vtype: 'paymentreg',
                  width: 148,
                  allowBlank: false,
                  height: 125,
                  maxHeight: 125,
                  labelAlign: 'top',
                  emptyText: '',
                },
                {
                  xtype: 'button',
                  id: 'uploadPaymentsButton',
                  text: 'Registrera betalningar',
                  maxHeight: 22,
                  height: 22,
                  disabled: true
                }
              ]
            }
          ]
        },
        {
          xtype: 'fieldset',
          title: 'Registrera inbetalningar via utdrag från PlusGirot',
          flex: 1,
          height: (Ext.isIE9 ? 195 : 185),
          margin: '0 0 0 5',
          padding: (Ext.isIE9 ? '2 10 0 10' : '4 10 0 10'),
          //style: 'background: blue;',
          layout: {
            align: 'stretchmax',
            type: 'vbox'
          },
          items: [
            {
              xtype: 'label',
              margin: '0 5 0 0',
              html: '<p>FÖRKLARINGSTEXT</p><p>Genom att ladda upp ett kontoutdrag från Nordea (<i>Internetbanken företag</i> &gt; <i>Kontoutdrag</i> &gt; <i>Exportera</i>) kan systemet automagiskt filtrera ut, förhandsvisa och registrera inbetalningar med rätt belopp, datum och personlig kod.</p><br><p>Fält utöver belopp, meddelande (referens) och datum kommer att ignoreras.</p>',
              flex: 1,
              height: 129,
              maxHeight: 129
            },
            {
              xtype: 'form',
              dock: 'bottom',
              id: 'pageRegForm',
              //layout: 'hbox',
              //anchor: '100%',
              //margin: '0 0 0 0',
              padding: '0 0 0 0',
              bodyCls: 'no-panel-borders',
              layout: {
                type: 'hbox',
                align: 'stretch'
              },
              margin: '0 0 0 0',
              //height: 36,
              //style: 'background: red;',
              items: [
                {
                  xtype: 'filefield',
                  name: 'datafile',
                  fieldLabel: 'Ladda upp datafil',
                  id: 'uploadBankStatementField',
                  labelWidth: 105,
                  maxHeight: 22,
                  height: 22,
                  flex: 2,
                  msgTarget: 'side',
                  allowBlank: false,
                  buttonText: 'Välj fil'
                },
                {
                  xtype: 'button',
                  id: 'uploadBankStatement',
                  text: 'Ladda upp',
                  disabled: true,
                  flex: 1,
                  margin: '0 5 0 5',
                  maxHeight: 22,
                  height: 22
                }
              ]
            }
          ]
        }
      ]
    },{
      xtype: 'form',
      id: 'pageAddForm',
      title: 'Lägg till ny medlem',
      bodyPadding: '0 5 0 5',
      anchor: '100%',
      height: (Ext.isIE9 ? 311 : 281),
      frame: true,
      collapsed: true,
      collapsible: true,
      titleCollapse: true,
      layout: 'anchor',
      items: [{
        xtype: 'container',
        height: (Ext.isIE9 ? 125 : 115),
        anchor: '100%',
        margin: '0 0 0 0',
        //style: 'background: green;',
        layout: {
          align: 'stretch',
          type: 'hbox'
        },
        items: [{
          xtype: 'fieldset',
          id: 'addMembershipFieldset',
          margin: '0 4 2 0',
          padding: (Ext.isIE9 ? '0 10 0 10' : '2 10 0 10'),
          flex: 1,
          //style: 'background: red;',
          title: 'Medlemskapsinformation ( * markerar obligatoriska fält)',
          items: [{
            xtype: 'radiogroup',
            id: 'addLastApplicationFor',
            fieldLabel: 'Medlemskap för*',
            labelAlign: 'right',
            allowBlank: false,
            blankText: 'Termin för medlemskap är obligatorisk.',
            labelPad: 10,
            height: 22,
            labelWidth: 175,
            items: [
              { boxLabel: 'Termin VT12', name: 'lastApplicationFor', inputValue: 'VT12', checked: true },
              { boxLabel: 'Termin HT12', name: 'lastApplicationFor', inputValue: 'HT12', disabled: true }
            ]
          },{
            xtype: 'datefield',
            height: 22,
            labelPad: 10,
            allowBlank: true,
            labelWidth: 175,
            id: 'addLastPaymentRecieved',
            name: 'lastPaymentRecieved',
            fieldLabel: 'Medlemsavgift inbetald',
            labelAlign: 'right',
            format: 'Y-m-d',
            minValue: new Date(2011, 05, 01),
            maxValue: new Date(),
            anchor: '100%'
          },{
            xtype: 'combobox',
            height: 22,
            id: 'addAdmittedSemester',
            fieldLabel: 'Årskurs ([HK|VK]NN)',
            labelAlign: 'right',
            forceSelection: true,
            name: 'admittedSemester',
            store: 'AdmissionSemesterD',
            displayField: 'name',
            typeAhead: true,
            typeAheadDelay: 50,
            queryMode: 'local',
            valueField: 'value',
            allowBlank: true,
            labelPad: 10,
            labelWidth: 175,
            anchor: '100%'
          }]
        },{
          xtype: 'fieldset',
          id: 'addPersonalFieldset',
          padding: (Ext.isIE9 ? '6 10 0 10' : '8 10 0 10'),
          margin: '0 0 2 4',
          //style: 'background: blue;',
          title: 'Personuppgifter ( * markerar obligatoriska fält)',
          flex: 1,
          items: [{
            xtype: 'textfield',
            height: 22,
            id: 'addPersonalId',
            fieldLabel: 'Personnr. (YYMMDD-NNNN)*',
            labelAlign: 'right',
            allowBlank: false,
            blankText: 'Fältet "personnummer" är obligatoriskt.',
            enforceMaxLength: true,
            maxLength: 13,
            vtype: 'personalid',
            name: 'personalId',
            labelPad: 10,
            labelWidth: 175,
            anchor: '100%'
          },{
            xtype: 'textfield',
            height: 22,
            id: 'addFirstName',
            fieldLabel: 'Förnamn*',
            labelAlign: 'right',
            allowBlank: false,
            blankText: 'Fältet "förnamn" är obligatoriskt.',
            enforceMaxLength: true,
            maxLength: 30,
            vtype: 'name',
            name: 'firstName',
            labelPad: 10,
            labelWidth: 175,
            anchor: '100%'
          },{
            xtype: 'textfield',
            height: 22,
            id: 'addLastName',
            fieldLabel: 'Efternamn*',
            labelAlign: 'right',
            allowBlank: false,
            blankText: 'Fältet "efternamn" är obligatoriskt.',
            enforceMaxLength: true,
            maxLength: 30,
            vtype: 'name',
            name: 'lastName',
            labelPad: 10,
            labelWidth: 175,
            anchor: '100%'
          }]
        }]
      },{
        xtype: 'container',
        height: (Ext.isIE9 ? 150 : 130),
        anchor: '100%',
        //style: 'background: grey;',
        margin: '0 0 0 0',
        layout: {
          align: 'stretch',
          type: 'hbox'
        },
        items: [{
          xtype: 'fieldset',
          id: 'addAddressFieldset',
          //style: 'background: green;',
          margin: '0 4 5 0',
          padding: (Ext.isIE9 ? '6 10 0 10' : '8 10 0 10'),
          flex: 1,
          title: 'Adressuppgifter ( * markerar obligatoriska fält)',
          items: [{
            xtype: 'container',
            height: 42,
            id: 'streetAddressWrapper',
            margin: '0 0 5 0',
            layout: {
              align: 'stretch',
              type: 'hbox'
            },
            items: [{
              xtype: 'textfield',
              id: 'addStreetCo',
              margin: '0 5 0 0',
              fieldLabel: 'C/o-adress',
              vtype: 'name',
              labelAlign: 'top',
              enforceMaxLength: true,
              maxLength: 40,
              name: 'streetCo',
              flex: 1
            },{
              xtype: 'textfield',
              id: 'addStreetAddress',
              margin: 0,
              fieldLabel: 'Gatuadress*',
              vtype: 'streetaddress',
              labelAlign: 'top',
              enforceMaxLength: true,
              maxLength: 30,
              allowBlank: false,
              blankText: 'Fältet "gatuadress" är obligatoriskt.',
              name: 'streetAddress',
              flex: 1
            },{
              xtype: 'textfield',
              id: 'addStreetNumber',
              margin: '0 0 0 5',
              width: 60,
              fieldLabel: 'Gatunr.*',
              enforceMaxLength: true,
              maxLength: 7,
              maxLengthText: 'Ett gatunummer betecknas med ett 1-3-siffrigt tal.',
              vtype: 'streetnumber',
              allowBlank: false,
              name: 'streetNumber',
              labelAlign: 'top'
            },{
              xtype: 'textfield',
              id: 'addStreetEntrance',
              margin: '0 0 0 5',
              width: 60,
              fieldLabel: 'Portnr.',
              enforceMaxLength: true,
              maxLength: 1,
              maxLengthText: 'En portuppgång betecknas med en (1) bokstav, A-Z.',
              vtype: 'streetentrance',
              name: 'streetEntrance',
              labelAlign: 'top'
            },{
              xtype: 'textfield',
              id: 'addStreetApartment',
              margin: '0 0 0 5',
              width: 60,
              fieldLabel: 'Lgh.nr.',
              enforceMaxLength: true,
              maxLength: 4,
              maxLengthText: 'Beteckningen för ett lägenhetsnummer kan, maximalt, utgöra ett fyrsiffrigt tal.',
              vtype: 'streetapartment',
              name: 'streetApartment',
              labelAlign: 'top'
            }]
          },{
            xtype: 'container',
            height: 42,
            id: 'addPostalAddressWrapper',
            //style: 'background: blue;',
            margin: '0 0 5 0',
            layout: {
              align: 'stretch',
              type: 'hbox'
            },
            items: [{
              xtype: 'combobox',
              width: 60,
              id: 'addStreetFloor',
              fieldLabel: 'Våning',
              labelAlign: 'top',
              margin: '0 5 0 0',
              forceSelection: true,
              name: 'streetFloor',
              store: 'StreetFloor',
              emptyText: '',
              displayField: 'name',
              typeAhead: true,
              typeAheadDelay: 50,
              queryMode: 'local',
              valueField: 'value',
              anchor: '100%'
            },{
              xtype: 'textfield',
              id: 'addZipCode',
              margin: '0 5 0 0',
              width: 115,
              fieldLabel: 'Postnr. (NNNNN)*',
              allowBlank: false,
              enforceMaxLength: true,
              vtype: 'zipcode',
              minLength: 5,
              minLengthText: 'Ett postnummer innehåller 5 siffor och ska anges utan blanksteg.',
              maxLength: 5,
              maxLengthText: 'Ett postnummer innehåller 5 siffor och ska anges utan blanksteg.',
              name: 'zipCode',
              labelAlign: 'top'
            },{
              xtype: 'textfield',
              id: 'addCity',
              margin: '0 5 0 0',
              fieldLabel: 'Postort*',
              allowBlank: false,
              name: 'city',
              vtype: 'city',
              enforceMaxLength: true,
              maxLength: 30,
              labelAlign: 'top',
              flex: 1
            },{
              xtype: 'textfield',
              id: 'addCountry',
              width: 60,
              margin: 0,
              fieldLabel: 'Land*',
              allowBlank: false,
              name: 'country',
              vtype: 'country',
              defaultValue: 'Sverige',
              value: 'Sverige',
              enforceMaxLength: true,
              maxLength: 7,
              labelAlign: 'top',
            }]
          }]
        },{
          xtype: 'container',
          //id: 'streetAddressWrapper',
          margin: '0 0 0 4',
          flex: 1,
          //style: 'background: red;',
          layout: {
            align: 'stretch',
            type: 'vbox'
          },
          items: [{
            xtype: 'fieldset',
            id: 'addContactFieldset',
            margin: '0 0 0 0',
            padding: (Ext.isIE9 ? '6 10 0 10' : '8 10 0 10'),
            //style: 'background: yellow;',
            title: 'Kontaktuppgifter ( * markerar obligatoriska fält)',
            items: [{
              xtype: 'container',
              height: 43,
              id: 'addContactWrapper',
              margin: '0 0 8 0',
              layout: {
                align: 'stretch',
                type: 'hbox'
              },
              items: [{
                xtype: 'textfield',
                id: 'addEmail',
                margin: '0 5 2 0',
                fieldLabel: 'E-post',
                name: 'email',
                maxLength: 40,
                //height: 22,
                //maxHeight: 22,
                enforceMaxLength: true,
                vtype: 'email',
                labelAlign: 'top',
                flex: 1
              },{
                xtype: 'textfield',
                id: 'addPhone',
                margin: '0 0 2 0',
                fieldLabel: 'Telnr',
                maxLength: 18,
                //height: 22,
                //maxHeight: 22,
                enforceMaxLength: true,
                vtype: 'phone',
                name: 'phone',
                labelAlign: 'top',
                flex: 1
              }]
            }]
          },{
            xtype: 'checkbox',
            id: 'addStudentConfirmation',
            //style: 'background: blue;',
            name: 'studentConfirmation',
            boxLabel: 'Personen studerar vid Röda Korsets Högskola.*',
            checked: true,
            margin: '2 0 2 2',
            inputValue: '1',
            anchor: '100%',
          },{
            xtype: 'container',
            height: 23,
            id: 'addActionWrapper',
            margin: '3 0 0 0',
            layout: {
              align: 'stretch',
              type: 'hbox'
            },
            anchor: '100%',
            items: [{
              xtype: 'button',
              id: 'resetAddForm',
              text: 'Rensa formuläret',
              flex: 1
            },{
              xtype: 'button',
              id: 'submitAddForm',
              margin: '0 0 0 5',
              disabled: true,
              text: 'Lägg till medlem',
              flex: 1
            }]
          }]
        }]
      }]
    },{
      xtype: 'gridpanel',
      id: 'memberGrid',
      title: 'Medlemsdatabas (och register över mottagna anmälningar)',
      columnLines: true,
      flex: 1,
      loadMask: true,
      invalidateScrollerOnRefresh: true,
      //verticalScroller: {
      //  xtype: 'paginggridscroller'
      //},
      store: 'MemberList',
      headerFilters: {
      },
      dockedItems: [
        {
          xtype: 'pagingtoolbar',
          id: 'gridPagingToolbar',
          store: 'MemberList',
          dock: 'bottom',
          displayInfo: true,
          displayMsg: 'Visar {0} – {1} av {2}',
          emptyMsg: 'Inga medlemmar inlästa',
          beforePageText: 'Sida',
          afterPageText: 'av {0}',
          nextText: 'Nästa sida',
          prevText: 'Föregående sida',
          refreshText: 'Uppdatera',
          firstText: 'Första sidan',
          lastText: 'Sista sidan'
        },
        {
          xtype: 'pagingtoolbar',
          store: 'MemberList', // same store GridPanel is using
          dock: 'top',
          displayInfo: true,
          items: [
            {
              xtype: 'button',
              margin: '0 0 0 10',
              text: 'Återställ filter',
              id: 'resetFilters',
              handler: function(button) {
                button.up('panel').resetHeaderFilters();
              }
            },
            {
              xtype: 'button',
              text: 'Tillämpa filter',
              id: 'applyFilters',
              handler: function(button) {
                button.up('panel').applyHeaderFilters();
              }
            },/*
            {
              xtype: 'button',
              margin: '0 0 0 10',
              text: 'Generera mailadresser',
              id: 'applyExtensions'
            },*/
            {
              xtype: 'button',
              margin: '0 0 0 10',
              text: 'Överför till Mecenat',
              disabled: true,
              id: 'transferToMecenat'
            },
            {
              xtype: 'button',
              margin: '0 0 0 10',
              text: 'Överför till SSSB',
              disabled: true,
              id: 'transferToSsco'
            }
          ],
          displayInfo: true,
          displayMsg: 'Visar {0} – {1} av {2}',
          emptyMsg: 'Inga medlemmar inlästa',
          beforePageText: 'Sida',
          afterPageText: 'av {0}',
          nextText: 'Nästa sida',
          prevText: 'Föregående sida',
          refreshText: 'Uppdatera',
          firstText: 'Första sidan',
          lastText: 'Sista sidan'
        }
      ],
      plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
          id: 'rowEditorPlugin'
        }),
        Ext.create('Ext.ux.grid.GridHeaderFilters')
      ],
      selModel: Ext.create('Ext.selection.RowModel', {

      }),
      columns: [
        {
          xtype: 'gridcolumn',
          id: 'applicationData',
          text: 'Senaste medlemsansökan',
          hideable: false,
          sortable: false,
          menuDisabled: true,
          columns: [
            {
              xtype: 'datecolumn',
              id: 'lastApplicationDate',
              format: 'Y-m-d',
              width: 90,
              sortable: true,
              hideable: false,
              allowBlank: false,
              dataIndex: 'lastApplicationDate',
              text: 'Anm.datum',
              editor: {
                xtype: 'datefield',
                format: 'Y-m-d',
                minValue: new Date(2011-05-01),
                maxValue: new Date()
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'lastApplicationFor',
              sortable: true,
              hideable: false,
              width: 80,
              dataIndex: 'lastApplicationFor',
              text: 'Anmäld till',
              editor: {
                xtype: 'combobox',
                forceSelection: true,
                store: 'OpenSemesters',
                emptyText: '',
                displayField: 'name',
                typeAhead: true,
                typeAheadDelay: 50,
                queryMode: 'local',
                valueField: 'value'
              }
            }
          ]
        },
        {
          xtype: 'gridcolumn',
          id: 'personalId',
          sortable: true,
          hideable: false,
          width: 95,
          dataIndex: 'personalId',
          text: 'Personnr',
          filter: {
            xtype: 'textfield',
            fieldName: 'personalIdFilter',
            fieldLabel: 'Personnr'
          }
        },
        {
          xtype: 'gridcolumn',
          id: 'firstName',
          sortable: true,
          hideable: false,
          dataIndex: 'firstName',
          text: 'Förnamn',
          filter: {
            xtype: 'textfield',
            fieldName: 'firstNameFilter',
            fieldLabel: 'Förnamn'
          },
          editor: {
            xtype: 'textfield',
            vtype: 'name',
            id: 'firstNameField',
            enforceMaxLength: true,
            maxLength: 30
          }
        },
        {
          xtype: 'gridcolumn',
          id: 'lastName',
          sortable: true,
          hideable: false,
          dataIndex: 'lastName',
          text: 'Efternamn',
          filter: {
            xtype: 'textfield',
            fieldName: 'lastNameFilter',
            fieldLabel: 'Efternamn'
          },
          editor: {
            xtype: 'textfield',
            vtype: 'name',
            id: 'lastNameField',
            enforceMaxLength: true,
            maxLength: 30
          }
        },
        {
          xtype: 'gridcolumn',
          id: 'lastPaymentKey',
          width: 90,
          sortable: true,
          hideable: true,
          dataIndex: 'lastPaymentKey',
          text: 'Personlig kod',
          filter: {
            xtype: 'textfield',
            fieldName: 'paymentKeyFilter',
            fieldLabel: 'Personlig kod'
          }
        },
        {
          xtype: 'gridcolumn',
          id: 'adminDetails',
          text: 'Administrativt för VT12',
          hideable: false,
          sortable: false,
          menuDisabled: true,
          columns: [
            {
              xtype: 'datecolumn',
              id: 'lastPaymentRecieved',
              format: 'Y-m-d',
              width: 90,
              sortable: true,
              hideable: false,
              dataIndex: 'lastPaymentRecieved',
              text: 'Avg. inbetald',
              editor: {
                xtype: 'datefield',
                format: 'Y-m-d',
                minValue: new Date(2011, 05, 01),
                maxValue: new Date()
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'lastSscoReg',
              sortable: true,
              hideable: false,
              width: 40,
              dataIndex: 'lastSscoReg',
              text: 'SSSB'
            },
            {
              xtype: 'gridcolumn',
              id: 'lastMecenatReg',
              sortable: true,
              hideable: false,
              width: 55,
              dataIndex: 'lastMecenatReg',
              text: 'Mecenat'
            }
          ]
        },
        {
          xtype: 'gridcolumn',
          id: 'contactInfo',
          text: 'Kontaktinformation',
          hideable: false,
          sortable: false,
          menuDisabled: true,
          columns: [
            {
              xtype: 'gridcolumn',
              id: 'email',
              sortable: true,
              hideable: true,
              width: 150,
              dataIndex: 'email',
              text: 'E-post',
              editor: {
                xtype: 'textfield',
                vtype: 'email',
                id: 'emailField',
                enforceMaxLength: true,
                maxLength: 40
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'phone',
              width: 90,
              sortable: true,
              hideable: true,
              dataIndex: 'phone',
              text: 'Telnr',
              editor: {
                xtype: 'textfield',
                vtype: 'phone',
                id: 'phoneField',
                enforceMaxLength: true,
                maxLength: 18,
                allowBlank: true
              }
            }
          ]
        },
        {
          xtype: 'gridcolumn',
          id: 'addressInfo',
          text: 'Adressuppgifter',
          hideable: false,
          sortable: false,
          menuDisabled: true,
          columns: [
            {
              xtype: 'gridcolumn',
              id: 'streetCo',
              sortable: true,
              hideable: true,
              dataIndex: 'streetCo',
              text: 'C/o-adress',
              editor: {
                xtype: 'textfield',
                vtype: 'name',
                id: 'streetCoField',
                enforceMaxLength: true,
                maxLength: 40
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'streetAddress',
              sortable: true,
              hideable: true,
              dataIndex: 'streetAddress',
              text: 'Gatuadress',
              editor: {
                xtype: 'textfield',
                vtype: 'streetaddress',
                id: 'streetAddressField',
                enforceMaxLength: true,
                maxLength: 30
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'streetNumber',
              sortable: false,
              hideable: true,
              width: 45,
              dataIndex: 'streetNumber',
              text: 'Gatunr',
              editor: {
                xtype: 'textfield',
                vtype: 'streetnumber',
                id: 'streetNumberField',
                enforceMaxLength: true,
                maxLength: 7
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'streetEntrance',
              sortable: false,
              hideable: true,
              width: 45,
              dataIndex: 'streetEntrance',
              text: 'Port',
              editor: {
                xtype: 'textfield',
                vtype: 'streetentrance',
                id: 'streetEntranceField',
                enforceMaxLength: true,
                maxLength: 1
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'streetApartment',
              sortable: false,
              hideable: true,
              width: 45,
              dataIndex: 'streetApartment',
              text: 'Lgh.nr',
              editor: {
                xtype: 'textfield',
                vtype: 'streetapartment',
                id: 'streetApartmentField',
                enforceMaxLength: true,
                maxLength: 4
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'streetFloor',
              sortable: false,
              hideable: true,
              width: 60,
              dataIndex: 'streetFloor',
              text: 'Våning',
              editor: {
                xtype: 'combobox',
                forceSelection: true,
                store: 'StreetFloor',
                emptyText: '',
                displayField: 'name',
                typeAhead: true,
                typeAheadDelay: 50,
                queryMode: 'local',
                valueField: 'value',
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'zipCode',
              sortable: false,
              hideable: true,
              width: 45,
              dataIndex: 'zipCode',
              text: 'Postnr',
              editor: {
                xtype: 'textfield',
                vtype: 'zipcode',
                id: 'zipCodeField',
                enforceMaxLength: true,
                maxLength: 5
              }
            },
            {
              xtype: 'gridcolumn',
              id: 'city',
              sortable: true,
              hideable: true,
              dataIndex: 'city',
              text: 'Postort',
              editor: {
                xtype: 'textfield',
                vtype: 'city',
                id: 'cityField',
                enforceMaxLength: true,
                maxLength: 30
              }
            }
          ]
        },
        {
          xtype: 'gridcolumn',
          id: 'accountDetails',
          text: 'Kontoegenskaper',
          hideable: false,
          sortable: false,
          menuDisabled: true,
          columns: [
            {
              xtype: 'templatecolumn',
              id: 'sys_administrator',
              sortable: true,
              hideable: true,
              width: 45,
              tpl: '<tpl if="sys_administrator == true">Ja</tpl><tpl if="sys_administrator == false">Nej</tpl>',
              dataIndex: 'sys_administrator',
              text: 'Admin',
              editor: {
                xtype: 'textfield',
                vtype: 'admin',
                id: 'adminField',
                allowBlank: false,
                enforceMaxLength: true,
                maxLength: 5
              }
              //render: function(value) { return value ? 'Ja' : 'Nej'; }
            },
            {
              xtype: 'templatecolumn',
              id: 'sys_accounttype',
              sortable: true,
              hideable: true,
              width: 65,
              tpl: '<tpl if="sys_accounttype == \'verified member\'">Verifierad</tpl><tpl if="sys_accounttype == \'application pending\'">Anmäld</tpl>',
              dataIndex: 'sys_accounttype',
              text: 'Status'
            },
          ]
        }
      ],
      viewConfig: {
      }
    },
    {
      anchor: '100%',
      xtype: 'panel',
      frame: true,
      margin: '10 10 5 10',
      padding: '4 3 6 3',
      html: '<center><p><i>Courtesy of <a href="mailto:mats.blomdahl@gmail.com">mats.blomdahl@gmail.com</a>. Please email all and any bug reports.</i></p></center>'
    }
  ]
});
