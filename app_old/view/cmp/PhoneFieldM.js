Ext.define('RKHSK.view.cmp.PhoneFieldM', {
  //extend: 'Ext.viewport.Viewport',
  extend: 'Ext.field.Text',
  xtype: 'phonefield',
  initialize: function() {
    var me = this;
    
    me.setMaxLength(19);
    me.callParent();
    me.getComponent().element.dom.children[0].addEventListener('keypress', function(e) {
      var index = this.value.length,
          charCode = e.charCode || e.keyCode,
          input = String.fromCharCode(charCode),
          maskRe = [
            /[\+\d]/,
            /[\d \-]/
          ];
      
      function stopEvent() {
        e.stopPropagation();
        e.preventDefault();
      }
      
      if (!index) {
        if (!maskRe[index].test(input))
          stopEvent();
      } else {
        if (!maskRe[1].test(input))
          stopEvent();
      }
    }, false);
  },
  isValid: function() {
    //console.error(this.getValue());
    return true;
  }
});