Ext.define('RKHSK.view.RegPageD', {
  extend: 'Ext.container.Viewport',
  
  layout: 'column',
  style: 'background: #f6f6f6;',
  
  id: 'regPageD',
  autoScroll: true,
  fullscreen: true,
  items: [
    {
      xtype: 'container',
      layout: 'hbox',
      columnWidth: 1,
      items: [
        {
          xtype: 'container',
          html: '<div></div>',
          flex: 1
        },
        {
          xtype: 'container',
          html: '<div style="height: 110px; background: none; background-image: url(/resources/images/logo-base-8.png); background-repeat: no-repeat;"></div>',
          height: 101,
          width: 596,
        },
        {
          xtype: 'container',
          width: 140,
          height: 101,
          layout: 'hbox',
          items: [
            {
              xtype: 'container',
              flex: 1
            },
            {
              xtype: 'button',
              id: 'adminLink',
              width: 100,
              text: 'Administration',
              margin: '5 0 0 0',
              //dock: 'right',
              //align: 'top',
              height: 22
            }
          ]
        },
        {
          xtype: 'container',
          html: '<div></div>',
          flex: 1
        },
      ]
    },
    {
      xtype: 'container',
      layout: 'hbox',
      columnWidth: 1,
      items: [
        {
          xtype: 'container',
          html: '<div></div>',
          height: 20,
          flex: 1
        },
        {
          xtype: 'panel',
          id: 'infoWrapper',
          width: 285,
          frame: true,
          margin: '0 5 0 0',
          bodyPadding: '2 5 5 5',
          layout: 'column',
          items: [
            {
              xtype: 'label',
              margin: '0 0 0 0',
              text: 'REGISTRERINGSINFORMATION',
              anchor: '100%'
            },
            {
              xtype: 'label',
              margin: '10 0 0 0',
              text: 'När du registrerat dig på http://regga.rkh-sk.se får du ett e-postmeddelande till adressen du angivit. Meddelandet innehåller uppgifter om betalningsinformation och dina personliga inloggningsupggifter.',
              anchor: '100%'
            },
            {
              xtype: 'label',
              margin: '10 0 0 0',
              text: 'Med hjälp av inloggningsuppgifterna kan du återvända till denna sida för att se status för ditt ärende och få svar på frågor som "Har min inbetalning registrerats av studentkåren?" och "Har mina uppgifter registrerats hos Mecenat och SSSB än?"',
              anchor: '100%'
            },
            {
              xtype: 'label',
              margin: '10 0 0 0',
              text: 'Medlemsanmälningar till vårterminen tas emot från och med 1 januari. För höstterminen öppnar anmälningen den 1 augusti.',
              anchor: '100%'
            },
            {
              xtype: 'label',
              margin: '10 0 0 0',
              text: 'Obs: Genom att registrera dig som medlem hos Röda Korsets Högskolas Studentkår [RKH-SK] accepterar du att dina personuppgifter behandlas i RKH-SK:s medlemsregister.',
              anchor: '100%'
            }
          ]
        },
        {
          xtype: 'tabpanel',
          id: 'interfaceWrapper',
          activeTab: 0,
          frame: true,
          layout: {
            type: 'column'
          },
          width: 445,
          items: [
            {
              xtype: 'form',
              id: 'regForm',
              bodyPadding: '2 5 0 5',
              frame: true,
              title: 'Medlemsregistrering',
              items: [
                {
                  xtype: 'fieldset',
                  id: 'membershipFieldset',
                  padding: '3 10 5 10',
                  margin: '0 0 5 0',
                  title: 'Medlemskapsinformation ( * markerar obligatoriska fält)',
                  items: [
                    {
                      xtype: 'radiogroup',
                      id: 'lastApplicationFor',
                      fieldLabel: 'Medlemskap för*',
                      labelAlign: 'right',
                      allowBlank: false,
                      blankText: 'Termin för medlemskap är obligatorisk.',
                      labelPad: 10,
                      labelWidth: 175,
                      items: [
                        { boxLabel: 'Termin VT12', name: 'lastApplicationFor', inputValue: 'VT12', checked: true },
                        { boxLabel: 'Termin HT12', name: 'lastApplicationFor', inputValue: 'HT12', disabled: true }
                      ]
                    },
                    {
                      xtype: 'combobox',
                      height: 22,
                      id: 'admittedSemester',
                      fieldLabel: 'Årskurs ([HK|VK]NN)*',
                      labelAlign: 'right',
                      forceSelection: true,
                      name: 'admittedSemester',
                      store: 'AdmissionSemesterD',
                      displayField: 'name',
                      typeAhead: true,
                      typeAheadDelay: 50,
                      queryMode: 'local',
                      valueField: 'value',
                      allowBlank: false,
                      blankText: 'Ett val i menyn "årskurs" är obligatoriskt.',
                      labelPad: 10,
                      labelWidth: 175,
                      anchor: '100%'
                    }
                  ]
                },
                {
                  xtype: 'fieldset',
                  id: 'personalFieldset',
                  margin: '0 0 5 0',
                  padding: '3 10 5 10',
                  title: 'Personuppgifter ( * markerar obligatoriska fält)',
                  anchor: '100%',
                  items: [
                    {
                      xtype: 'textfield',
                      height: 22,
                      id: 'personalId',
                      fieldLabel: 'Personnr. (YYMMDD-NNNN)*',
                      labelAlign: 'right',
                      allowBlank: false,
                      blankText: 'Fältet "personnummer" är obligatoriskt.',
                      enforceMaxLength: true,
                      maxLength: 13,
                      vtype: 'personalid',
                      name: 'personalId',
                      labelPad: 10,
                      labelWidth: 175,
                      anchor: '100%'
                    },
                    {
                      xtype: 'textfield',
                      height: 22,
                      id: 'firstName',
                      fieldLabel: 'Förnamn*',
                      labelAlign: 'right',
                      allowBlank: false,
                      blankText: 'Fältet "förnamn" är obligatoriskt.',
                      enforceMaxLength: true,
                      maxLength: 30,
                      vtype: 'name',
                      name: 'firstName',
                      labelPad: 10,
                      labelWidth: 175,
                      anchor: '100%'
                    },
                    {
                      xtype: 'textfield',
                      height: 22,
                      id: 'lastName',
                      fieldLabel: 'Efternamn*',
                      labelAlign: 'right',
                      allowBlank: false,
                      blankText: 'Fältet "efternamn" är obligatoriskt.',
                      enforceMaxLength: true,
                      maxLength: 30,
                      vtype: 'name',
                      name: 'lastName',
                      labelPad: 10,
                      labelWidth: 175,
                      anchor: '100%'
                    }
                  ]
                },
                {
                  xtype: 'fieldset',
                  id: 'addressFieldset',
                  margin: '0 0 5 0',
                  padding: '3 10 5 10',
                  title: 'Adressuppgifter ( * markerar obligatoriska fält)',
                  items: [
                    {
                      xtype: 'container',
                      height: 42,
                      id: 'streetAddressWrapper',
                      margin: '0 0 5 0',
                      layout: {
                        align: 'stretch',
                        type: 'hbox'
                      },
                      items: [
                        {
                          xtype: 'textfield',
                          id: 'streetCo',
                          margin: '0 5 0 0',
                          fieldLabel: 'C/o-adress',
                          vtype: 'name',
                          labelAlign: 'top',
                          enforceMaxLength: true,
                          maxLength: 30,
                          name: 'streetCo',
                          flex: 2
                        },
                        {
                          xtype: 'textfield',
                          id: 'streetAddress',
                          margin: 0,
                          fieldLabel: 'Gatuadress*',
                          vtype: 'streetaddress',
                          labelAlign: 'top',
                          enforceMaxLength: true,
                          maxLength: 30,
                          allowBlank: false,
                          blankText: 'Fältet "gatuadress" är obligatoriskt.',
                          name: 'streetAddress',
                          flex: 3
                        },
                        {
                          xtype: 'textfield',
                          id: 'streetNumber',
                          margin: '0 0 0 5',
                          width: 60,
                          fieldLabel: 'Gatunr.*',
                          enforceMaxLength: true,
                          maxLength: 7,
                          maxLengthText: 'Ett gatunummer betecknas med 1-3-siffrigt tal.',
                          vtype: 'streetnumber',
                          allowBlank: false,
                          name: 'streetNumber',
                          labelAlign: 'top'
                        },
                        {
                          xtype: 'textfield',
                          id: 'streetEntrance',
                          margin: '0 0 0 5',
                          width: 60,
                          fieldLabel: 'Portnr.',
                          enforceMaxLength: true,
                          maxLength: 1,
                          maxLengthText: 'En portuppgång betecknas med en (1) bokstav, A-Z.',
                          vtype: 'streetentrance',
                          name: 'streetEntrance',
                          labelAlign: 'top'
                        },
                        {
                          xtype: 'textfield',
                          id: 'streetApartment',
                          margin: '0 0 0 5',
                          width: 60,
                          fieldLabel: 'Lgh.nr.',
                          enforceMaxLength: true,
                          maxLength: 4,
                          maxLengthText: 'Beteckningen för ett lägenhetsnummer kan, maximalt, utgöra ett fyrsiffrigt tal.',
                          vtype: 'streetapartment',
                          name: 'streetApartment',
                          labelAlign: 'top'
                        }
                      ]
                    },
                    {
                      xtype: 'container',
                      height: 42,
                      id: 'postalAddressWrapper',
                      margin: '0 0 5 0',
                      layout: {
                        align: 'stretch',
                        type: 'hbox'
                      },
                      items: [
                        {
                          xtype: 'combobox',
                          width: 60,
                          id: 'streetFloor',
                          fieldLabel: 'Våning',
                          labelAlign: 'top',
                          margin: '0 5 0 0',
                          forceSelection: true,
                          name: 'streetFloor',
                          store: 'StreetFloor',
                          emptyText: '',
                          displayField: 'name',
                          typeAhead: true,
                          typeAheadDelay: 50,
                          queryMode: 'local',
                          valueField: 'value',
                          anchor: '100%'
                        },
                        {
                          xtype: 'textfield',
                          id: 'zipCode',
                          margin: '0 5 0 0',
                          width: 115,
                          fieldLabel: 'Postnr. (NNNNN)*',
                          allowBlank: false,
                          enforceMaxLength: true,
                          vtype: 'zipcode',
                          minLength: 5,
                          minLengthText: 'Ett postnummer innehåller 5 siffor och ska anges utan blanksteg.',
                          maxLength: 5,
                          maxLengthText: 'Ett postnummer innehåller 5 siffor och ska anges utan blanksteg.',
                          name: 'zipCode',
                          labelAlign: 'top'
                        },
                        {
                          xtype: 'textfield',
                          id: 'city',
                          margin: '0 5 0 0',
                          fieldLabel: 'Postort*',
                          allowBlank: false,
                          name: 'city',
                          vtype: 'city',
                          enforceMaxLength: true,
                          maxLength: 30,
                          labelAlign: 'top',
                          flex: 1
                        },
                        {
                          xtype: 'textfield',
                          id: 'country',
                          width: 60,
                          margin: 0,
                          fieldLabel: 'Land*',
                          allowBlank: false,
                          name: 'country',
                          vtype: 'country',
                          defaultValue: 'Sverige',
                          value: 'Sverige',
                          enforceMaxLength: true,
                          maxLength: 7,
                          labelAlign: 'top',
                        }
                      ]
                    }
                  ]
                },
                {
                  xtype: 'fieldset',
                  id: 'contactFieldset',
                  padding: '3 10 5 10',
                  margin: '0 0 5 0',
                  title: 'Kontaktuppgifter ( * markerar obligatoriska fält)',
                  items: [
                    {
                      xtype: 'container',
                      height: 45,
                      id: 'contactWrapper',
                      margin: '0 0 5 0',
                      layout: {
                        align: 'stretch',
                        type: 'hbox'
                      },
                      items: [
                        {
                          xtype: 'textfield',
                          id: 'email',
                          margin: '0 5 0 0',
                          fieldLabel: 'E-post*',
                          allowBlank: false,
                          name: 'email',
                          maxLength: 40,
                          enforceMaxLength: true,
                          vtype: 'email',
                          labelAlign: 'top',
                          flex: 1
                        },
                        {
                          xtype: 'textfield',
                          id: 'phone',
                          margin: 0,
                          fieldLabel: 'Telnr',
                          maxLength: 18,
                          enforceMaxLength: true,
                          vtype: 'phone',
                          name: 'phone',
                          labelAlign: 'top',
                          flex: 1
                        }
                      ]
                    }
                  ]
                },
                {
                  xtype: 'checkbox',
                  id: 'studentConfirmation',
                  name: 'studentConfirmation',
                  boxLabel: 'Härmed intygas att jag är student vid Röda Korsets Högskola.*',
                  checked: true,
                  margin: '0 0 0 7',
                  inputValue: '1',
                  anchor: '100%',
                },
                {
                  xtype: 'container',
                  height: 25,
                  id: 'actionWrapper',
                  margin: '10 0 10 0',
                  layout: {
                    align: 'stretch',
                    type: 'hbox'
                  },
                  anchor: '100%',
                  items: [
                    {
                      xtype: 'button',
                      id: 'resetRegForm',
                      text: 'Rensa formuläret',
                      flex: 1
                    },
                    {
                      xtype: 'button',
                      id: 'submitRegForm',
                      margin: '0 0 0 5',
                      disabled: true,
                      text: 'Skicka in medlemsregistrering',
                      flex: 1
                    }
                  ]
                }
              ]
            },
            {
              xtype: 'form',
              id: 'statusForm',
              frame: true,
              bodyPadding: '2 5 0 5',
              title: 'Ärendestatus (om du har registrerat dig)',
              items: [
                {
                  xtype: 'fieldset',
                  margin: '0 0 5 0',
                  padding: '3 10 5 10',
                  title: 'Inloggningsuppgifter ( * markerar obligatoriska fält)',
                  items: [
                    {
                      xtype: 'textfield',
                      height: 25,
                      id: 'loginUsername',
                      name: 'loginUsername',
                      fieldLabel: 'E-postadress*',
                      labelAlign: 'right',
                      vtype: 'email',
                      allowBlank: false,
                      labelPad: 10,
                      labelWidth: 175,
                      anchor: '100%'
                    },
                    {
                      xtype: 'textfield',
                      height: 25,
                      id: 'loginCredentials',
                      name: 'loginCredentials',
                      fieldLabel: 'Personlig kod*',
                      vtype: 'paymentkey',
                      maxLength: 5,
                      enforceMaxLength: true,
                      labelAlign: 'right',
                      allowBlank: false,
                      labelPad: 10,
                      labelWidth: 175,
                      anchor: '100%'
                    }
                  ]
                },
                {
                  xtype: 'container',
                  height: 25,
                  id: 'caseWrapper',
                  margin: '10 0 5 0',
                  layout: {
                    align: 'stretch',
                    type: 'hbox'
                  },
                  anchor: '100%',
                  items: [
                    {
                      xtype: 'button',
                      id: 'resendCredentials',
                      disabled: true,
                      text: 'Jag har glömt min personliga kod (skicka igen)',
                      flex: 1.5
                    },
                    {
                      xtype: 'button',
                      id: 'retrieveStatus',
                      disabled: true,
                      margin: '0 0 0 5',
                      text: 'Visa ärendestatus',
                      flex: 1
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          xtype: 'container',
          html: '<div></div>',
          height: 20,
          flex: 1
        },
      ]
    },
    {
      xtype: 'container',
      layout: 'hbox',
      columnWidth: 1,
      items: [
        {
          xtype: 'container',
          html: '<div></div>',
          height: 20,
          flex: 1
        },
        {
          xtype: 'panel',
          frame: true,
          margin: '10 0 5 0',
          padding: '4 3 6 3',
          html: '<center><p><i>Courtesy of <a href="mailto:mats.blomdahl@gmail.com">mats.blomdahl@gmail.com</a>. Please email all and any bug reports.</i></p></center>',
          width: 736
        },
        {
          xtype: 'container',
          html: '<div></div>',
          height: 20,
          flex: 1
        },
      ]
    },
  ]
});