Ext.define('RKHSK.view.AdminLoginWindowD', {
  extend: 'Ext.window.Window',
  height: 160,
  id: 'adminLoginWindowD',
  width: 320,
  autoScroll: false,
  modal: true,
  layout: {
    align: 'stretch',
    type: 'vbox'
  },
  defaultFocus: 'loginUsername',
  title: 'Administratörsinloggning',
  items: [
    {
      xtype: 'form',
      frame: true,
      //height: 300,//240,
      bodyPadding: 5,
      //style: 'background: red;',
      anchor: '100%',
      flex: 1,
      items: [
        {
          xtype: 'textfield',
          id: 'loginUsername',
          name: 'loginUsername',
          fieldLabel: 'E-postadress',
          margin: '2 0 4 0',
          allowBlank: false,
          maxLength: 40,
          enforceMaxLength: true,
          vtype: 'email',
          anchor: '100%'
        },
        {
          xtype: 'textfield',
          id: 'loginPassword',
          inputType: 'password',
          allowBlank: false,
          maxLength: 18,
          enforceMaxLength: true,
          name: 'loginPassword',
          disabled: true,
          vtype: 'password',
          fieldLabel: 'Lösenord',
          anchor: '100%'
        },
        {
          xtype: 'container',
          margin: '5 0 5 0',
          html: '<p> </p>',
          height: 24,
          minHeight: 24
        },
        {
          xtype: 'panel',
          height: 22,
          bodyCls: 'no-panel-borders',
          layout: {
            align: 'stretch',
            type: 'hbox'
          },
          anchor: '100%',
          items: [
            {
              xtype: 'button',
              text: 'Jag har glömt lösenordet',
              disabled: true,
              id: 'loginWindowResend'
            },
            {
              xtype: 'button',
              margin: '0 5 0 5',
              text: 'Avbryt',
              flex: 1,
              id: 'loginWindowAbort'
            },
            {
              xtype: 'button',
              text: 'Logga in',
              disabled: true,
              flex: 1,
              id: 'loginWindowSubmit'
            }
          ]
        }
      ]
    }
  ]
});