Ext.define('RKHSK.store.OpenSemesters', {
  extend: 'Ext.data.Store',
  config: {
    storeId: 'OpenSemesters',
    data: [
      { "name": "Ej anmäld", "value": "" },
      { "name": "HT12", "value": "HT12" },
      { "name": "VT12", "value": "VT12" },
      { "name": "HT11", "value": "HT11" }
    ],
    fields: [
      'name',
      'value'
    ],
    autoLoad: true
  }
});
