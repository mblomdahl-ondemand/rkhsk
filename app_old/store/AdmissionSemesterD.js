Ext.define('RKHSK.store.AdmissionSemesterD', {
  extend: 'Ext.data.Store',
  config: {
    storeId: 'AdmissionSemesterD',
    autoLoad: true,
    data: [
      { "name": "VK12", "value": "vk12" },
      { "name": "HK11", "value": "hk11" },
      { "name": "VK11", "value": "vk11" },
      { "name": "HK10", "value": "hk10" },
      { "name": "VK10", "value": "vk10" },
      { "name": "HK09", "value": "hk09" },
      { "name": "VK09", "value": "vk09" },
      { "name": "HK08", "value": "hk08" },
      { "name": "VK08", "value": "vk08" },
      { "name": "HK07", "value": "hk07" },
      { "name": "VK07", "value": "vk07" },
      { "name": "HK06", "value": "hk06" },
      { "name": "VK06", "value": "vk06" },
      { "name": "Specialistutb./VUB", "value": "SPEC"}
    ],
    fields: [
      'name',
      'value'
    ]
  }
});