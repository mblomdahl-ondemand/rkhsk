Ext.define('RKHSK.store.AdmissionSemesterM', {
  extend: 'Ext.data.Store',
  config: {
    storeId: 'AdmissionSemesterM',
    autoLoad: true,
    data: [
      { "name": "", "value": "" },
      { "name": "VK12", "value": "VK12" },
      { "name": "HK11", "value": "HK11" },
      { "name": "VK11", "value": "VK11" },
      { "name": "HK10", "value": "HK10" },
      { "name": "VK10", "value": "VK10" },
      { "name": "HK09", "value": "HK09" },
      { "name": "VK09", "value": "VK09" },
      { "name": "HK08", "value": "HK08" },
      { "name": "VK08", "value": "VK08" },
      { "name": "HK07", "value": "HK07" },
      { "name": "VK07", "value": "VK07" },
      { "name": "HK06", "value": "HK06" },
      { "name": "VK06", "value": "VK06" },
      { "name": "Specialistutb./VUB", "value": "SPEC"}
    ],
    fields: [
      'name',
      'value'
    ]
  }
});