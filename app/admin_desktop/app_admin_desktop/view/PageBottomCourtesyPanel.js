/*
 * File: app_admin_desktop/view/PageBottomCourtesyPanel.js
 *
 * This file was generated by Sencha Architect version 2.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('RKHSK.view.PageBottomCourtesyPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.pagebottomcourtesypanel',

    frame: true,
    html: '<center><p><i>Courtesy of <a href="mailto:mats.blomdahl@gmail.com">mats.blomdahl@gmail.com</a>. Please email all and any bug reports.</i></p></center>',
    minWidth: 735,
    padding: '4 3 6 3',
    width: 735,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});