Ext.define('RKHSK.controller.AdminCtrlM', {
    extend: 'Ext.app.Controller',

    id: 'AdminCtrlM',

    config: {
        //profile: Ext.os.deviceType.toLowerCase(),
        //activePage: 'homePage-intro',
        //entryHash: undefined,
        refs: {
            'AdminPageM'             : {
                selector  : '#adminPageM',
                xtype     : 'adminpagem',
                autoCreate: true
            },
            'AdminSetPasswordWindowM': {
                selector: '#adminSetPasswordWindowM'
            },
            'RegForm'                : {
                selector: '#regForm'
            },
            'StatusForm'             : {
                selector: '#statusForm'
            },
            'AdminLink'              : {
                selector: '#adminLink'
            },
            'LastApplicationFor'     : {
                selector: '#lastApplicationFor'
            },
            'AdmittedSemester'       : {
                selector: '#admittedSemester'
            },
            'PersonalId'             : {
                selector: '#personalId'
            },
            'FirstName'              : {
                selector: '#firstName'
            },
            'LastName'               : {
                selector: '#lastName'
            },
            'StreetCo'               : {
                selector: '#streetCo'
            },
            'StreetAddress'          : {
                selector: '#streetAddress'
            },
            'StreetNumber'           : {
                selector: '#streetNumber'
            },
            'StreetEntrance'         : {
                selector: '#streetEntrance'
            },
            'StreetApartment'        : {
                selector: '#streetApartment'
            },
            'StreetFloor'            : {
                selector: '#streetFloor'
            },
            'ZipCode'                : {
                selector: '#zipCode'
            },
            'City'                   : {
                selector: '#city'
            },
            'Country'                : {
                selector: '#country'
            },
            'Email'                  : {
                selector: '#email'
            },
            'Phone'                  : {
                selector: '#phone'
            },
            'StudentConfirmation'    : {
                selector: '#studentConfirmation'
            },
            'ResetForm'              : {
                selector: '#resetForm'
            },
            'SubmitRegForm'          : {
                selector: '#submitRegForm'
            },
            'LoginUsername'          : {
                selector: '#loginUsername'
            },
            'LoginCredentials'       : {
                selector: '#loginCredentials'
            },
            'ResendCredentials'      : {
                selector: '#resendCredentials'
            },
            'RetrieveStatus'         : {
                selector: '#retrieveStatus'
            }
        }
    },

    init                 : function () {
        var me = this;
        /*
         // Validator Setup: "name"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         name: function(val, field) { // vType validation function
         var name = /^[A-Za-z éüÜåäöÅÄÖ-]+$/i;
         return name.test(val);
         },
         nameText: 'Namnet innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
         nameMask: /[A-Za-z åäöÅÄÖ-]/i // vType mask property
         });
         })();

         // Validator Setup: "streetaddress"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         streetaddress: function(val, field) { // vType validation function
         var streetaddress = /^[A-Za-z :åäöÅÄÖ-]+$/i;
         return streetaddress.test(val);
         },
         streetaddressText: 'Gatuadressen innehåller ogiltiga tecken. Fältet "gatuadress" kan enbart innehålla en begränsad teckenuppsättning, närmare besämt a-ö, bindestreck och blanksteg. För gatunummer och portuppgång, se fälten "gatunummer" och "portnr."', // vType text property
         streetaddressMask: /[A-Za-z :åäöÅÄÖ-]/i // vType mask property
         });
         })();

         // Validator Setup: "streetnumber"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         streetnumber: function(val, field) { // vType validation function
         var streetnumber = /^\d{1,3}$|^\d{1,3}\-\d{1,3}$/;
         return streetnumber.test(val);
         },
         streetnumberText: 'Gatunumret innehåller ogiltiga tecken eller har en ogiltig längd. Ett gatunummer kan enbart innehålla siffror från 0-9. För portuppgång (exempelvis "B" i "2B") resp. våningsplan, se fälten "portnr." och "trappor".', // vType text property
         streetnumberMask: /[\d\-]/ // vType mask property
         });
         })();

         // Validator Setup: "streetentrance"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         streetentrance: function(val, field) { // vType validation function
         var streetentrance = /^[a-z]$/i;
         return streetentrance.test(val);
         },
         streetentranceText: 'Fältet "portuppgång" innehåller ogiltiga tecken eller har en ogiltig längd. För våningsplan (exempelvis "N.B." i "Hemgatan 4A, N.B.") se fältet "trappor".', // vType text property
         streetentranceMask: /[a-z]/i // vType mask property
         });
         })();

         // Validator Setup: "streetapartment"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         streetapartment: function(val, field) { // vType validation function
         var streetapartment = /^[\d]+$/;
         return streetapartment.test(val);
         },
         streetapartmentText: 'Lägenhetsnumret innehåller ogiltiga tecken eller har en ogiltig längd.', // vType text property
         streetapartmentMask: /[\d]/ // vType mask property
         });
         })();

         // Validator Setup: "zipcode"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         zipcode: function(val, field) { // vType validation function
         var zipcode = /^[\d]{5}$/;
         return zipcode.test(val);
         },
         zipcodeText: 'Postnumret innehåller ogiltiga tecken eller har en ogiltig längd. Korrekt format är "NNNNN" (exempelvis "11421", utan blanksteg).', // vType text property
         zipcodeMask: /[\d]/ // vType mask property
         });
         })();

         // Validator Setup: "city"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         city: function(val, field) { // vType validation function
         var city = /^[A-Za-z åäöÅÄÖ-]+$/i;
         return city.test(val);
         },
         cityText: 'Postorten innehåller ogiltiga tecken. Fältet "postort" kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
         cityMask: /[\w\(\)\?\!\s"\/',.\:_åäöÅÄÖ-]/i // vType mask property
         });
         })();

         // Validator Setup: "country"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         country: function(val, field) { // vType validation function
         var country = /^Sverige$/;
         return country.test(val);
         },
         countryText: 'RKH-SK:s registreringsformulär har för närvarande enbart stöd för svenska adresser. Det enda giltiga värdet för fältet "land" är därmed "Sverige".', // vType text property
         countryMask: /[Sverige]/i // vType mask property
         });
         })();

         // Validator Setup: "personalid"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         personalid: function(val, field) { // vType validation function
         var personalid = /^[4-9][0-9](0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-2])-[A-Z\d]{4}$/;
         return personalid.test(val);
         },
         personalidText: 'Ogiltigt personnr. Personnummer måste anges på formen YYMMDD-NNNN. Exempel: "860722-0332" – där "86" markerar år 1986, "07" juli månad, "22" dag 22 i juli månad och "0332" de sista fyra siffrorna i personnumret.', // vType text property
         personalidMask: /[\dA-Z-]/i // vType mask property
         });
         })();

         // Validator Setup: "paymentkey"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         paymentkey: function(val, field) { // vType validation function
         var paymentkey = /^[0-9a-z]{5}$/i;
         return paymentkey.test(val);
         },
         paymentkeyText: 'Ogiltig personlig kod. Din personliga kod är 5 tecken lång och består av en slumpmässig uppsättning bokstäver och siffror.', // vType text property
         paymentkeyMask: /[\da-z]/i // vType mask property
         });
         })();

         // Validator Setup: "phone"
         (function() {
         Ext.apply(Ext.form.field.VTypes, {
         phone: function(val, field) { // vType validation function
         var phone = /^[+\d][\d -]{9,18}$/;
         return phone.test(val);
         },
         phoneText: 'Ogiltigt telefonnr. Fältet "telefonnummer" kan enbart innehålla en begränsad teckenuppsättning, accepterade tecken är + (som första tecken), 0-9, blanksteg och bindestreck. Exempel på giltiga telefonnummer är "+4672-522 32 33" och "08 920 20 25"', // vType text property
         phoneMask: /[\d +-]/i // vType mask property
         });
         })();
         */

        //this.getRegPageM();

        this.control({
            '#personalId'                                 : {
                blur: this.onAssholeIdInput
            },
            '#firstName'                                  : {
                blur: this.onSloppyUserInput
            },
            '#lastName'                                   : {
                blur: this.onSloppyUserInput
            },
            '#streetCo'                                   : {
                blur: this.onSloppyUserInput
            },
            '#streetAddress'                              : {
                blur: this.onSloppyUserInput
            },
            '#streetNumber'                               : {
                blur: this.onTrimmableInput
            },
            '#streetEntrance'                             : {
                blur: this.onForceUppercaseEntry
            },
            '#zipCode'                                    : {
                blur: this.onTrimmableInput
            },
            '#city'                                       : {
                blur: this.onSloppyUserInput
            },
            '#email'                                      : {
                blur: this.onForceLowercaseEntry
            },
            '#phone'                                      : {
                blur: this.onTrimmableInput
            },
            '#studentConfirmation'                        : {
            },
            '#submitRegForm'                              : {
                tap: this.onSubmitRegForm
            },
            '#loginUsername'                              : {
                change: this.onStatusFormUsernameModified,
                blur  : this.onForceLowercaseEntry
            },
            '#loginCredentials'                           : {
                change: this.onStatusFormCredentialsModified,
                blur  : this.onForceUppercaseEntry
            },
            '#resendCredentials'                          : {
                tap: this.onResendCredentials
            },
            '#retrieveStatus'                             : {
                tap: this.onRetrieveStatus
            },
            '#adminSetPasswordWindowM button[text=Avbryt]': {
                tap: function (btn) {
                    btn.up('setpasswordmessage').destroy();
                }
            },
            '#adminSetPasswordWindowM button[text=Spara]' : {
                tap: function (btn) {
                    btn.up('setpasswordmessage').destroy();
                }
            }
        });
        myGlobals.controller = this;

    },

    /*onAdminLink: function() {
     window.location.href = '/admin';
     },*/
    onAssholeIdInput     : function (field) {
        if (field.isValid()) {
            field.setValue(field.getValue().toUpperCase());
            return true;
        }

        var input = Ext.String.trim(field.getValue()).toUpperCase();

        // test for 19832010-0221
        if (input.length === 13) {
            if (input.search(/19\d{6}-[\dA-Z]{4}/) !== -1) {
                field.setValue(input.substring(2));
                return true;
            } else {
                return false;
            }
        }

        // test for 198320100221
        if (input.length === 12) {
            if (input.search(/19\d{6}[\dA-Z]{4}/) !== -1) {
                field.setValue(input.substring(2, 8) + '-' + input.substring(8));
                return true;
            } else {
                return false;
            }
        }

        // test for 8320100221
        if (input.length === 10) {
            if (input.search(/[2-9][0-9]\d{4}[\dA-Z]{4}/) !== -1) {
                field.setValue(input.substring(0, 6) + '-' + input.substring(6));
                return true;
            } else {
                return false;
            }
        }

        field.setValue(input);
    },
    onSloppyUserInput    : function (field) {
        var input = Ext.String.trim(typeof field === 'string' ? field : field.getValue()).toLowerCase(),
            validLowerCaseWords = ['Von', 'Van', 'Väg', 'Gata'],
            validUpperCaseWords = ['Ii', 'Iii', 'Iv', 'Vi'];
        while (input.indexOf('  ') !== -1) {
            input = input.replace('  ', ' ')
        }
        input = input.replace(' -', '-').replace('- ', '-');
        while (input.indexOf('--') !== -1) {
            input = input.replace('--', '-')
        }
        input = input.split(' ');
        for (var tmp, i = 0, j = input.length;
             i < j;
             i++) {

            input[i] = Ext.String.capitalize(input[i]);
            if (input[i].split('-').length > 1) {
                tmp = input[i].split('-');
                for (var k = 0, l = tmp.length;
                     k < l;
                     k++) {
                    tmp[k] = Ext.String.capitalize(tmp[k]);
                }
                input[i] = tmp.join('-');
            } else {
                if (input[i].split(':').length > 1) { /// 'III:s gata'
                    tmp = input[i].split(':');
                    if (validUpperCaseWords.indexOf(tmp[0]) !== -1) {
                        tmp[0] = tmp[0].toUpperCase();
                        input[i] = tmp.join(':');
                    }
                } else {
                    if (validLowerCaseWords.indexOf(input[i]) !== -1) {
                        input[i] = input[i].toLowerCase();
                    } else {
                        for (var k = validUpperCaseWords.length;
                             k--;) {
                            if (input[i] === validUpperCaseWords[i] + 's') {
                                input[i] = validUpperCaseWords[i].toUpperCase() + 's';
                                break;
                            }
                        }
                    }
                }
            }
        }

        input = input.join(' ');
        if (typeof field === 'string') {
            return input;
        }
        else {
            field.setValue(input);
        }
    },
    onTrimmableInput     : function (field) {
        var input = Ext.String.trim(field.getValue());
        field.setValue(input);
    },
    onRegFormModified    : function () {
        return true;
        var regForm = this.getRegForm(),
            inputFields = regForm.getValues(),
            submitForm = this.getSubmitForm();
        myGlobals.regForm = regForm;
        if (!inputFields['city'] || !inputFields['email'] || !inputFields['zipCode'] || !inputFields['streetAddress'] || !inputFields['streetNumber'] || !inputFields['studentConfirmation'] || !inputFields['firstName'] || !inputFields['lastName'] || !inputFields['lastApplicationFor'] || !inputFields['admittedSemester'] || !inputFields['personalId'] || !inputFields['country']) {
            if (!submitForm.isDisabled()) {
                submitForm.disable(true);
            }
        } else if (regForm.isValid()) {
            if (submitForm.isDisabled()) {
                submitForm.enable(true);
            }
        } else {
            if (!submitForm.isDisabled()) {
                submitForm.disable(true);
            }
        }
    },
    onForceUppercaseEntry: function (field) {
        var input = Ext.String.trim(field.getValue()).toUpperCase();
        field.setValue(input);
    },
    onForceLowercaseEntry: function (field) {
        var input = Ext.String.trim(field.getValue()).toLowerCase();
        field.setValue(input);
    },

    onSubmitRegForm: function (button) {
        var form = button.up('formpanel'),
            failureTypes = this.getFailureTypes(),
            data = form.getValues();
        data.country = 'Sverige';
        var errors = Ext.create('MemberRegModelM', data).validate();
        myGlobals.validate = errors;
        if (errors.isValid()) {
            form.setMasked({
                xtype  : 'loadmask',
                message: 'Överför registrerings-information . . .'
            });
            form.submit({
                url    : '/account/register/',
                method : 'POST',
                timeout: 10,
                success: function (form, action) {
                    myGlobals.successAction = action;
                    Ext.Msg.alert('Medlemsregistrering sparad', 'Du är nu registrerad i RKH-SK:s medlemsdatabas och kan direkt betala in medlemsavgiften via PlusGiro eller studentkårens expedition. Se e-post för den personliga kod som måste bifogas inbetalningen.', function () {
                        form.reset();
                        form.setMasked(false);
                    });
                },
                failure: function (form, action) {
                    myGlobals.failureAction = action;
                    var failMsg = RKHSK.app.failureTypes[response.status];
                    if (!failMsg) {
                        failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
                    }
                    Ext.Msg.alert('Registreringen misslyckades', failMsg, function () {
                        form.setMasked(false);
                    });
                }
            });
        } else {
            var errorFeedback = [];
            Ext.each(errors.items, function (error, index) {
                errorFeedback.push(function () {
                    Ext.Msg.alert("Komplettering krävs", 'Fel: ' + error.getMessage(), function () {
                        var fn = errorFeedback.pop();
                        if (fn) {
                            return fn();
                        }
                    });
                });
                console.log(error.getMessage());
            });
            if (errorFeedback.length) {
                errorFeedback.pop()();
            }
            return false;
        }
    },

    onStatusFormUsernameModified   : function (field) {
        var statusForm = field.up('formpanel'),
            loginCredentials = this.getLoginCredentials(),
            resendCredentials = this.getResendCredentials(),
            retrieveStatus = this.getRetrieveStatus();

        if (field.isValid()) {
            console.log('username valid');
            if (resendCredentials.isDisabled()) {
                resendCredentials.enable(true);
            }
            if (loginCredentials.isValid()) {
                console.log('creds valid');
                if (retrieveStatus.isDisabled()) {
                    retrieveStatus.enable(true);
                }
            } else if (!retrieveStatus.isDisabled()) {
                retrieveStatus.disable(true);
            }
        } else {
            console.log('username error');
            if (!resendCredentials.isDisabled()) {
                resendCredentials.disable(true);
            }
            if (!retrieveStatus.isDisabled()) {
                retrieveStatus.disable(true);
            }
        }

    },
    onStatusFormCredentialsModified: function (field) {
        var loginUsername = this.getLoginUsername(),
            retrieveStatus = this.getRetrieveStatus();

        if (loginUsername.isValid() && field.isValid()) {
            if (retrieveStatus.isDisabled()) {
                retrieveStatus.enable(true);
            }
        } else {
            if (!retrieveStatus.isDisabled()) {
                retrieveStatus.disable(true);
            }
        }
    },
    onResendCredentials            : function (button) {
        var statusForm = button.up('formpanel'),
            loginUsername = this.getLoginUsername();
        if (loginUsername.isValid()) {
            statusForm.setMasked({
                xtype  : 'loadmask',
                message: 'Mailar inloggningsuppgifter . . .'
            });
            Ext.Ajax.request({
                method  : 'POST',
                url     : '/account/resend/',
                jsonData: {
                    loginUsername: loginUsername.getValue()
                },
                timeout : 6e3,
                success : function (response) {
                    Ext.Msg.alert('Bekräftelse', 'Dina medlems- och inloggningsuppgifter har mailats till din e-postadress. Om du inte mottagit meddelandet inom någon minut, var vänlig och kontrollera din "spam"- eller "skräppost"-mapp innan du tar kontakt med studentkårens support.', function () {
                        statusForm.setMasked(false);
                        statusForm.reset();
                        loginUsername.fireEvent('change', loginUsername);
                    });
                },
                failure : function (response) {
                    var failMsg = RKHSK.app.failureTypes[response.status];
                    if (!failMsg || response.status === 403) {
                        Ext.Msg.alert('Felmeddelande', 'Dina uppgifter kunde inte mailas. Säkerställ att e-postadressen du uppgett är densamma som du använde vid registreringstillfället.', function () {
                            statusForm.setMasked(false);
                        });
                    }
                    else {
                        Ext.Msg.alert('Kommunikationsfel', failMsg, function () {
                            statusForm.setMasked(false);
                        });
                    }
                }
            });
        }
    },
    onRetrieveStatus               : function (button) {
        var me = this,
            statusForm = button.up('formpanel'),
            loginUsername = me.getLoginUsername(),
            loginCredentials = me.getLoginCredentials();

        if (loginUsername.isValid() && loginCredentials.isValid()) {
            statusForm.setMasked({
                xtype  : 'loadmask',
                message: 'Läser in medlemsdata/ärendestatus . . .'
            });
            Ext.Ajax.request({
                method  : 'POST',
                url     : '/account/retrieve/',
                jsonData: {
                    loginUsername   : loginUsername.getValue(),
                    loginCredentials: loginCredentials.getValue()
                },
                timeout : 1e4,
                success : function (response) {
                    myGlobals.retrieved = response;
                    var data = Ext.decode(response.responseText).data;

                    Ext.create('RKHSK.view.RegStatusWindowM', {
                        data: data
                        //renderTo: Ext.getBody()
                    }).show();
                    //myGlobals.win.show();
                    /*
                     var statusMsg = Ext.create('Ext.window.MessageBox', {
                     renderTo: me.getRegPageD(),
                     title: 'Medlemsdata/ärendestatus',
                     width: 370,
                     height: 310,
                     maxHeight: 310,
                     modal: true,
                     frame: true,
                     cls: 'table-message-box',
                     msg: [
                     '<table><tbody>',
                     '<tr><td>Personnummer:</td><td>'+data.personalId+'</td></tr>',
                     '<tr><td>Efter- och förnamn:</td><td>'+data.lastName+', '+data.firstName+'</td></tr>',
                     '<tr><td>Senast anmäld till:</td><td>'+data.lastApplicationFor+'</td></tr>',
                     '<tr><td>Senaste inbetalning:</td><td>'+data.lastPaymentRecieved+'</td></tr>',
                     '<tr><td>SSSB-registreringar:</td><td>'+data.sscoHistory+'</td></tr>',
                     '<tr><td>Mecenat-registreringar:</td><td>'+data.mecenatHistory+'</td></tr>',
                     '<tr><td>Medlemskap i RKH-SK:</td><td>'+data.membershipHistory+'</td></tr>',
                     '<tr><td>Anmälningshistorik:</td><td>'+data.applicationSemesterHistory+'</td></tr>',
                     '<tr><td>Postadress:</td><td>'+data.streetAddress+'</td></tr>',
                     '<tr><td>Epost-adress:</td><td>'+data.email+'</td></tr>',
                     '<tr><td>Telefonnummer:</td><td>'+data.phone+'</td></tr>',
                     '<tr><td>Årskurs:</td><td>'+data.admittedSemester+'</td></tr>',
                     //'<tr><td>Samtycke, datalagring:</td><td>'+(data.studentConfirmation ? 'Ja' : 'Nej')+'</td></tr>',
                     '</tbody></table>'
                     ].join(''),
                     buttons: Ext.Msg.OK
                     });
                     statusMsg.show();*/
                    statusForm.setMasked(false);
                    statusForm.reset();
                    loginCredentials.fireEvent('change', loginCredentials);
                    loginUsername.fireEvent('change', loginUsername);
                },
                failure : function (response) {
                    var failMsg = RKHSK.app.failureTypes[response.status];
                    if (!failMsg || response.status === 403) {
                        Ext.Msg.alert('Medlemsdata/ärendestatus', 'Dina medlemsuppgifter kunde inte läsas in. Var vänlig och kontrollera att du har angett rätt e-postadress och en giltig personlig kod.', function () {
                            statusForm.setMasked(false);
                        });
                    }
                    else {
                        Ext.Msg.alert('Kommunikationsfel', failMsg, function () {
                            statusForm.setMasked(false);
                        });
                    }
                }
            });
        }
    }

});
