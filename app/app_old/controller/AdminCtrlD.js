Ext.define('RKHSK.controller.AdminCtrlD', {
    extend: 'Ext.app.Controller',

    id: 'AdminCtrlD',

    config: {
        //profile: Ext.os.deviceType.toLowerCase(),
        //activePage: 'homePage-intro',
        entryHash                        : undefined,
        failureTypes                     : undefined,
        paymentsUploadUri                : undefined,
        paymentsRequestId                : undefined,
        paymentsUploadTimeout            : undefined,
        attachmentUploadUri              : undefined,
        attachmentUploadTimeout          : undefined,
        attachmentRequestId              : undefined,
        loginTimeout                     : undefined,
        gaeChannel                       : undefined,
        statusMsgPositioner              : undefined,
        loggedIn                         : false,
        mecenatUsername                  : undefined,
        mecenatPassword                  : undefined,
        mecenatSyncCount                 : undefined,
        sscoUsername                     : undefined,
        sscoPassword                     : undefined,
        sscoSyncCount                    : undefined,
        mecenatDownloadedHandler         : undefined,
        mecenatCompletedHandler          : undefined,
        mecenatFailureHandler            : undefined,
        mecenatInitHandler               : undefined,
        sscoDownloadedHandler            : undefined,
        sscoCompletedHandler             : undefined,
        sscoFailureHandler               : undefined,
        sscoInitHandler                  : undefined,
        mecenatRequestId                 : undefined,
        sscoRequestId                    : undefined,
        defaultMecenatText               : undefined,
        defaultSscoText                  : undefined,
        remoteAddStatusMsgTimeout        : undefined,
        remoteEditStatusMsgTimeout       : undefined,
        remoteSscoSyncStatusMsgTimeout   : undefined,
        remoteMecenatSyncStatusMsgTimeout: undefined,
        remoteSessionStatusMsgTimeout    : undefined
    },

    require: [
        'RKHSK.store.AdmissionSemesterD',
        'RKHSK.store.StreetFloor',
        'RKHSK.store.MemberList',
        //'RKHSK.store.MemberListRemote',
        'RKHSK.store.OpenSemesters',
        'RKHSK.store.UnidentifiedMembersList',
        'RKHSK.store.RecognizedMembersList',
        'RKHSK.model.MemberListModel',
        'RKHSK.model.StatusMsgPositioner',
        'RKHSK.model.GaeChannelModel'
    ],

    views: [
        'AdminPageD',
        'AdminConfirmPaymentsD',
        'AdminLoginWindowD',
        'AdminSetPasswordWindowD',
        'AdminToMecenatWindowD',
        'AdminToSscoWindowD'
    ],

    refs: [
        {
            ref       : 'AdminPageD',
            selector  : '#adminPageD',
            autoCreate: true
        },
        {
            ref       : 'AdminConfirmPaymentsD',
            selector  : '#adminConfirmPaymentsD',
            autoCreate: true
        },
        {
            ref       : 'AdminLoginWindowD',
            selector  : '#adminLoginWindowD',
            autoCreate: true
        },
        {
            ref       : 'AdminSetPasswordWindowD',
            selector  : '#adminSetPasswordWindowD',
            autoCreate: true
        },
        {
            ref     : 'MenuLogInOut',
            selector: '#menuLogInOut'
        },
        {
            ref     : 'LoginWindowSubmit',
            selector: '#loginWindowSubmit'
        },
        {
            ref     : 'LoginWindowAbort',
            selector: '#loginWindowAbort'
        },
        {
            ref     : 'LoginWindowResend',
            selector: '#loginWindowResend'
        },
        {
            ref     : 'UploadBankStatement',
            selector: '#uploadBankStatement'
        },
        /*{
         ref       : 'RowEditorPlugin',
         selector  : '#rowEditorPlugin'
         },*/
        {
            ref     : 'SscoStatusMsg',
            selector: '#sscoStatusMsg'
        },
        {
            ref     : 'MecenatStatusMsg',
            selector: '#mecenatStatusMsg'
        },
        {
            ref     : 'LoginStatusMsg',
            selector: '#loginStatusMsg'
        },
        {
            ref     : 'EditStatusMsg',
            selector: '#editStatusMsg'
        },
        {
            ref     : 'ApplyExtensions',
            selector: '#applyExtensions'
        },
        {
            ref     : 'PaymentRegStatusMsg',
            selector: '#paymentRegStatusMsg'
        },
        {
            ref     : 'AddStatusMsg',
            selector: '#addStatusMsg'
        },
        {
            ref     : 'PaymentsUploadStatusMsg',
            selector: '#paymentsUploadStatusMsg'
        },
        {
            ref     : 'AttachmentUploadStatusMsg',
            selector: '#attachmentUploadStatusMsg'
        },
        {
            ref     : 'RemoteAddStatusMsg',
            selector: '#remoteAddStatusMsg'
        },
        {
            ref     : 'RemoteEditStatusMsg',
            selector: '#remoteEditStatusMsg'
        },
        {
            ref     : 'RemoteSscoSyncStatusMsg',
            selector: '#remoteSscoSyncStatusMsg'
        },
        {
            ref     : 'RemoteMecenatSyncStatusMsg',
            selector: '#remoteMecenatSyncStatusMsg'
        },
        {
            ref     : 'RemoteSessionStatusMsg',
            selector: '#remoteSessionStatusMsg'
        },
        {
            ref     : 'MemberGrid',
            selector: '#memberGrid'
        },
        {
            ref     : 'AddRegForm',
            selector: '#pageAddForm'
        },
        {
            ref     : 'AddLastApplicationFor',
            selector: '#addLastApplicationFor'
        },
        {
            ref     : 'AddLastPaymentRecieved',
            selector: '#addLastPaymentRecieved'
        },
        {
            ref     : 'AddAdmittedSemester',
            selector: '#addAdmittedSemester'
        },
        {
            ref     : 'AddPersonalId',
            selector: '#addPersonalId'
        },
        {
            ref     : 'AddFirstName',
            selector: '#addFirstName'
        },
        {
            ref     : 'AddLastName',
            selector: '#addLastName'
        },
        {
            ref     : 'AddStreetCo',
            selector: '#addStreetCo'
        },
        {
            ref     : 'AddStreetAddress',
            selector: '#addStreetAddress'
        },
        {
            ref     : 'AddStreetNumber',
            selector: '#addStreetNumber'
        },
        {
            ref     : 'AddStreetEntrance',
            selector: '#addStreetEntrance'
        },
        {
            ref     : 'AddZipCode',
            selector: '#addZipCode'
        },
        {
            ref     : 'AddCity',
            selector: '#addCity'
        },
        {
            ref     : 'AddCountry',
            selector: '#addCountry'
        },
        {
            ref     : 'AddEmail',
            selector: '#addEmail'
        },
        {
            ref     : 'AddPhone',
            selector: '#addPhone'
        },
        {
            ref     : 'AddStudentConfirmation',
            selector: '#addStudentConfirmation'
        },
        {
            ref     : 'ResetAddForm',
            selector: '#resetAddForm'
        },
        {
            ref     : 'SubmitAddForm',
            selector: '#submitAddForm'
        },
        {
            ref     : 'UploadPaymentsField',
            selector: '#uploadPaymentsField'
        },
        {
            ref     : 'UploadPaymentsButton',
            selector: '#uploadPaymentsButton'
        },
        {
            ref     : 'LoginUsername',
            selector: '#loginUsername'
        },
        {
            ref     : 'LoginPassword',
            selector: '#loginPassword'
        },
        {
            ref     : 'ResendCredentials',
            selector: '#resendCredentials'
        },
        {
            ref     : 'TransferToMecenat',
            selector: '#transferToMecenat'
        },
        {
            ref     : 'TransferToMecenatSubmit',
            selector: '#transferToMecenatSubmit'
        },
        {
            ref     : 'TransferToSsco',
            selector: '#transferToSsco'
        },
        {
            ref     : 'TransferToSscoSubmit',
            selector: '#transferToSscoSubmit'
        },
        {
            ref     : 'SetLoginPassword',
            selector: '#setLoginPassword'
        },
        {
            ref     : 'SetLoginPasswordCfrm',
            selector: '#setLoginPasswordCfrm'
        },
        {
            ref     : 'SetPasswordWindowAbort',
            selector: '#setPasswordWindowAbort'
        },
        {
            ref     : 'SetPasswordWindowSubmit',
            selector: '#setPasswordWindowSubmit'
        }
    ],

    stores: [
        'StreetFloor',
        'OpenSemesters',
        'MemberList',
        //'MemberListRemote',
        'AdmissionSemesterD',
        'UnidentifiedMembersList',
        'RecognizedMembersList'
    ],

    init: function () {

        this.initConfig({
            statusMsgPositioner: Ext.create('RKHSK.model.StatusMsgPositioner'),
            gaeChannel         : Ext.create('RKHSK.model.GaeChannelModel')
        });

        var me = this,
            failureTypes = {},
            actionClass = Ext.form.Action;
        memberList = me.getMemberListStore();
        failureTypes[actionClass.SERVER_INVALID] = 'Ett kommunikationsfel uppstod p.g.a. fel i serverkonfigurationen. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
        failureTypes[actionClass.CLIENT_INVALID] = 'Din användaridentitet kunde inte verifieras. Ladda om sidan och logga in igen.';
        failureTypes[actionClass.CONNECT_FAILURE] = 'Ett anslutningsfel inträffade. Var god kontrollera att du är ansluten till internet och försök igen.';
        failureTypes[actionClass.LOAD_FAILURE] = 'Information från servern kunde inte laddas, sannolikt till följd av ett fel i serverkonfigurationen. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
        failureTypes[403] = failureTypes[actionClass.CLIENT_INVALID];
        failureTypes[404] = failureTypes[actionClass.CONNECT_FAILURE];
        failureTypes[400] = failureTypes[actionClass.LOAD_FAILURE];
        failureTypes[0] = failureTypes[actionClass.CONNECT_FAILURE];
        failureTypes[500] = failureTypes[actionClass.SERVER_INVALID];

        me.setFailureTypes(failureTypes);

        // Validator Setup: "name"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                name    : function (val, field) { // vType validation function
                    var name = /^[A-Za-z éüÜåäöÅÄÖ\-]+$/;
                    return name.test(val);
                },
                nameText: 'Namnet innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
                nameMask: /[A-Za-z éüÜåäöÅÄÖ\-]/ // vType mask property
            });
        })();

        // Validator Setup: "streetaddress"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                streetaddress    : function (val, field) { // vType validation function
                    var streetaddress = /^[A-Za-z :åäöÅÄÖ\-]+$/;
                    return streetaddress.test(val);
                },
                streetaddressText: 'Gatuadressen innehåller ogiltiga tecken. Fältet "gatuadress" kan enbart innehålla en begränsad teckenuppsättning, närmare besämt a-ö, bindestreck och blanksteg. För gatunummer och portuppgång, se fälten "gatunummer" och "portnr."', // vType text property
                streetaddressMask: /[A-Za-z :åäöÅÄÖ\-]/ // vType mask property
            });
        })();

        // Validator Setup: "streetnumber"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                streetnumber    : function (val, field) { // vType validation function
                    var streetnumber = /^\d{1,3}$|^\d{1,3}\-\d{1,3}$/;
                    return streetnumber.test(val);
                },
                streetnumberText: 'Gatunumret innehåller ogiltiga tecken eller har en ogiltig längd. Ett gatunummer kan enbart innehålla siffror från 0-9. För portuppgång (exempelvis "B" i "2B") resp. våningsplan, se fälten "portnr." och "trappor".', // vType text property
                streetnumberMask: /[\d\-]/ // vType mask property
            });
        })();

        // Validator Setup: "streetentrance"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                streetentrance    : function (val, field) { // vType validation function
                    var streetentrance = /^[a-z]$/i;
                    return streetentrance.test(val);
                },
                streetentranceText: 'Portuppgången innehåller ogiltiga tecken eller har en ogiltig längd. För våningsplan (exempelvis "N.B." i "Hemgatan 4A, N.B.") se fältet "trappor".', // vType text property
                streetentranceMask: /[a-z]/i // vType mask property
            });
        })();

        // Validator Setup: "streetapartment"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                streetapartment    : function (val, field) { // vType validation function
                    var streetapartment = /^\d+$/;
                    return streetapartment.test(val);
                },
                streetapartmentText: 'Lägenhetsnumret innehåller ogiltiga tecken eller har en ogiltig längd.', // vType text property
                streetapartmentMask: /\d/ // vType mask property
            });
        })();

        // Validator Setup: "zipcode"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                zipcode    : function (val, field) { // vType validation function
                    var zipcode = /^[\d]{5}$/;
                    return zipcode.test(val);
                },
                zipcodeText: 'Postnumret innehåller ogiltiga tecken eller har en ogiltig längd. Korrekt format är "NNNNN" (exempelvis "11421", utan blanksteg).', // vType text property
                zipcodeMask: /[\d]/ // vType mask property
            });
        })();

        // Validator Setup: "city"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                city    : function (val, field) { // vType validation function
                    var city = /^[A-Za-z åäöÅÄÖ\-]+$/;
                    return city.test(val);
                },
                cityText: 'Postorten innehåller ogiltiga tecken. Fältet "postort" kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
                cityMask: /[A-Za-z åäöÅÄÖ\-]/ // vType mask property
            });
        })();

        // Validator Setup: "country"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                country    : function (val, field) { // vType validation function
                    var country = /^Sverige$/;
                    return country.test(val);
                },
                countryText: 'RKH-SK:s registreringsformulär har för närvarande enbart stöd för svenska adresser. Det enda giltiga värdet för fältet "land" är därmed "Sverige".', // vType text property
                countryMask: /[Sverige]/i // vType mask property
            });
        })();

        // Validator Setup: "personalid"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                personalid    : function (val, field) { // vType validation function
                    var personalid = /^[4-9][0-9](0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-2])\-[A-Z\d][\d]{3}$/;
                    return personalid.test(val);
                },
                personalidText: 'Ogiltigt personnr. Personnummer måste anges på formen YYMMDD-NNNN. Exempel: "860722-0332" – där "86" markerar år 1986, "07" juli månad, "22" dag 22 i juli månad och "0332" de sista fyra siffrorna i personnumret.', // vType text property
                personalidMask: /[\dA-Z\-]/i // vType mask property
            });
        })();

        // Validator Setup: "paymentkey"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                paymentkey    : function (val, field) { // vType validation function
                    var paymentkey = /^[0-9a-z]{5}$/;
                    return paymentkey.test(val);
                },
                paymentkeyText: 'Ogiltig personlig kod. Din personliga kod är 5 tecken lång och består av en slumpmässig uppsättning bokstäver och siffror.', // vType text property
                paymentkeyMask: /[\da-z]/i // vType mask property
            });
        })();

        // Validator Setup: "paymentreg"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                paymentreg    : function (val, field) { // vType validation function
                    var paymentreg = /(^\s*$)|(^201[1-2]\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|3[0-2]),\s*[0-9a-z]{5}\s*$)/im,
                        valid = true,
                        fieldValues = val.split('\n');

                    for (var i = 0, j = fieldValues.length;
                         i < j;
                         i++) {
                        if (!paymentreg.test(fieldValues[i])) {
                            valid = false;
                        }
                    }
                    return valid;
                },
                paymentregText: 'Ogiltig listning. Förteckningen över betalningar ska anges på formatet "YYYY-MM-DD, XXXXXX" med en inbetalning per rad.', // vType text property
                paymentregMask: /[\d,\-\sa-z]/im // vType mask property
            });
        })();

        // Validator Setup: "phone"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                phone    : function (val, field) { // vType validation function
                    var phone = /^[\+\d][\d \-]{9,18}$/;
                    return phone.test(val);
                },
                phoneText: 'Ogiltigt telefonnr. Fältet "telefonnummer" måste innehålla minst 8 tecken och kan enbart innehålla en begränsad teckenuppsättning. Accepterade tecken är + (som första tecken), 0-9, blanksteg och bindestreck. Exempel på giltiga telefonnummer är "+4672-522 32 33" och "08 920 20 25"', // vType text property
                phoneMask: /[\d \+\-]/ // vType mask property
            });
        })();

        // Validator Setup: "admin"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                admin    : function (val, field) { // vType validation function
                    var admin = /^(true|false)$/;
                    return admin.test(val);
                },
                adminText: 'Ogiltigt värde. Fältet "administratör" kan enbart lagra två diskreta värden: "true" (Ja) eller "false" (Nej).', // vType text property
                adminMask: /[truefals]/ // vType mask property
            });
        })();

        // Validator Setup: "password"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                password    : function (val, field) { // vType validation function
                    var password = /^\w{6,18}$/;
                    return password.test(val);
                },
                passwordText: 'Ogiltigt värde. Ett lösenord är minst 6 tecken långt och får ej innehålla skiljetecken.', // vType text property
                passwordMask: /\w/ // vType mask property
            });
        })();

        // Validator Setup: "setpasswordcfrm"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                setpasswordcfrm    : function (val, field) { // vType validation function
                    var initialPassword = Ext.getCmp('setLoginPassword');
                    return (val === initialPassword.getValue() && initialPassword.isValid());
                },
                setpasswordcfrmText: 'Felaktigt värde. Fältet måste innehålla samma lösenord som angivits ovan och måste ha en längd på minst 6 tecken.', // vType text property
                setpasswordcfrmMask: /\w/ // vType mask property
            });
        })();

        Ext.create('RKHSK.view.AdminPageD', {
            renderTo: Ext.getBody()
        });

        this.setDefaultSscoText(this.getTransferToSsco().getText());

        this.setDefaultMecenatText(this.getTransferToMecenat().getText());

        this.control({
            /*'#rowEditorPlugin': {
             'edit': this.onRowEdit
             },*/
            '#memberGrid'                : {
                edit: this.onRowEdit
            },
            '#applyExtensions'           : {
                click: this.onApplyExtensions
            },
            '#adminConfirmPaymentsD'     : {
                destroy: this.onConfirmPaymentsDestroy
            },
            '#adminToMecenatWindowD'     : {
                hide: this.onTransferToMecenatHide
            },
            '#adminToSscoWindowD'        : {
                hide: this.onTransferToSscoHide
            },
            '#menuLogInOut'              : {
                click: this.onLogInOut
            },
            '#uploadBankStatement'       : {
                click: this.onUploadBankStatement
            },
            '#uploadBankStatementField'  : {
                change: this.onUploadFormModified
            },
            '#confirmPaymentsAbort'      : {
                click: this.onConfirmPaymentsAbort
            },
            '#confirmPaymentsSubmit'     : {
                click: this.onConfirmPaymentsSubmit
            },
            '#mecenatStatusMsg'          : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#sscoStatusMsg'             : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#loginStatusMsg'            : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#addStatusMsg'              : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#paymentRegStatusMsg'       : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#editStatusMsg'             : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#paymentsUploadStatusMsg'   : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#attachmentUploadStatusMsg' : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#remoteAddStatusMsg'        : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#remoteEditStatusMsg'       : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#remoteSscoSyncStatusMsg'   : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#remoteMecenatSyncStatusMsg': {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#remoteSessionStatusMsg'    : {
                beforehide: this.onStatusMsgBeforeHide,
                show      : this.onStatusMsgShow,
                resize    : this.onStatusMsgResize,
                beforeshow: this.onStatusMsgBeforeShow
            },
            '#firstNameField'            : {
                blur: this.onSloppyUserInput
            },
            '#lastNameField'             : {
                blur: this.onSloppyUserInput
            },
            '#streetCoField'             : {
                blur: this.onSloppyUserInput
            },
            '#streetAddressField'        : {
                blur: this.onSloppyUserInput
            },
            '#streetNumberField'         : {
                blur: this.onTrimmableInput
            },
            '#streetApartmentField'      : {
                blur: this.onTrimmableInput
            },
            '#streetEntranceField'       : {
                blur: this.onForceUppercaseEntry
            },
            '#zipCodeField'              : {
                blur: this.onTrimmableInput
            },
            '#cityField'                 : {
                blur: this.onSloppyUserInput
            },
            '#emailField'                : {
                blur: this.onForceLowercaseEntry
            },
            '#phoneField'                : {
                blur: this.onTrimmableInput
            },
            '#addLastApplicationFor'     : {
                change: this.onAddRegFormModified
            },
            '#addLastPaymentRecieved'    : {
                change: this.onAddRegFormModified
            },
            '#addAdmittedSemester'       : {
                change: this.onAddRegFormModified
            },
            '#addPersonalId'             : {
                change: this.onAddRegFormModified,
                blur  : this.onAssholeIdInput
            },
            '#addFirstName'              : {
                change: this.onAddRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#addLastName'               : {
                change: this.onAddRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#addCoAddress'              : {
                change: this.onAddRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#addStreetAddress'          : {
                change: this.onAddRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#addStreetNumber'           : {
                change: this.onAddRegFormModified,
                blur  : this.onTrimmableInput
            },
            '#addStreetEntrance'         : {
                change: this.onAddRegFormModified,
                blur  : this.onForceUppercaseEntry
            },
            '#addZipCode'                : {
                change: this.onAddRegFormModified,
                blur  : this.onTrimmableInput
            },
            '#addCity'                   : {
                change: this.onAddRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#addCountry'                : {
                change: this.onAddRegFormModified,
                blur  : this.onForceCountry
            },
            '#addEmail'                  : {
                change: this.onAddRegFormModified,
                blur  : this.onForceLowercaseEntry
            },
            '#addPhone'                  : {
                change: this.onAddRegFormModified,
                blur  : this.onTrimmableInput
            },
            '#addStudentConfirmation'    : {
                change: this.onAddRegFormModified
            },
            '#resetAddForm'              : {
                click: this.onResetAddForm
            },
            '#submitAddForm'             : {
                click: this.onSubmitAddForm
            },
            '#uploadPaymentsField'       : {
                change: this.onRegPaymentsModified
            },
            '#uploadPaymentsButton'      : {
                click: this.onUploadPaymentsList
            },
            '#loginUsername'             : {
                change    : this.onLoginFormModified,
                specialkey: this.onLoginWindowEnter,
                blur      : this.onForceLowercaseEntry,
                show      : this.onShowFocus,
                render    : this.onShowFocus
            },
            '#loginPassword'             : {
                specialkey: this.onLoginWindowEnter,
                change    : this.onLoginFormModified
            },
            '#loginWindowResend'         : {
                click: this.onLoginWindowResend
            },
            '#loginWindowAbort'          : {
                click: this.onLoginWindowAbort
            },
            '#loginWindowSubmit'         : {
                click: this.onLoginWindowSubmit
            },
            '#gridPagingToolbar'         : {
                beforechange: this.onGridChangeStart,
                change      : this.onGridChangeComplete
            },
            '#transferToMecenat'         : {
                click: this.onTransferToMecenat
            },
            '#transferToMecenatSubmit'   : {
                click: this.onTransferToMecenatSubmit
            },
            '#transferToMecenatAbort'    : {
                click: this.onTransferToMecenatAbort
            },
            '#transferToSsco'            : {
                click: this.onTransferToSsco
            },
            '#transferToSscoSubmit'      : {
                click: this.onTransferToSscoSubmit
            },
            '#transferToSscoAbort'       : {
                click: this.onTransferToSscoAbort
            },
            '#setLoginPassword'          : {
                change    : this.onSetLoginPasswordChange,
                specialkey: this.onSetLoginPasswordWindowEnter,
                show      : this.onShowFocus,
                render    : this.onShowFocus
            },
            '#setLoginPasswordCfrm'      : {
                change    : this.onSetLoginPasswordCfrmChange,
                specialkey: this.onSetLoginPasswordWindowEnter
            },
            '#setPasswordWindowSubmit'   : {
                click: this.onSetLoginPasswordWindowSubmit
            },
            '#setPasswordWindowAbort'    : {
                click: this.onSetLoginPasswordWindowAbort
            },
            '#adminSetPasswordWindowD'   : {
                destroy: this.onSetLoginPasswordWindowDestroy
            }
        });
        //this.getUnidentifiedMembersListStore.clearData()
        //this.getRecognizedMembersListStore.clearData();
        myGlobals.controller = this;
        /*var storeLoadMask = new Ext.LoadMask(Ext.getBody(), {
         id   : 'storeLoadMask',
         title: 'Uppdaterar',
         msg  : 'Läser in medlemmar . . .',
         });
         storeLoadMask.show();
         storeLoadMask.hide();*/
        //myMask.show();

        var hash = window.location.hash;
        if (hash) {
            hash = hash.substring(1);
        }
        if (hash.length == 26) {
            this.setEntryHash(hash);
            var loadingMsg = new Ext.LoadMask(me.getAdminPageD(), { msg: 'Verifierar engångskod . . .' });
            loadingMsg.show()
            Ext.Ajax.request({
                timeout       : 1e4,
                url           : '/admin/passwd/' + hash,
                disableCaching: false,
                success       : function (response) {
                    myGlobals.res = response;
                    var data = Ext.decode(response.responseText);
                    loadingMsg.hide();
                    Ext.create('RKHSK.view.AdminSetPasswordWindowD', {
                        loginUsername: data.data.loginUsername
                    }).show();
                },
                failure       : function (response) {
                    var failMsg;
                    if (response.status === 403) {
                        failMsg = 'Din engångskod är antingen felaktig eller utgången. Du kan begära en ny kod via "Jag har glömt lösenordet"-knappen i inloggningsfönstret.';
                    }
                    else {
                        failMsg = failureTypes[response.status];
                    }
                    if (!failMsg) {
                        failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
                    }
                    loadingMsg.hide();
                    Ext.Msg.alert('Verifieringen misslyckades', failMsg);
                    return false;
                }
            });
        } else {
            this.onLogInOut();
        }

        me.getGaeChannel().addListener(me.onRemoteSscoSyncEvent, {
            remoteEvent: [
                'ssco_sync_initiated', 'ssco_sync_data_downloaded', 'ssco_sync_completed', 'ssco_sync_failed'
            ],
            scope      : this
        });

        me.getGaeChannel().addListener(me.onRemoteMecenatSyncEvent, {
            remoteEvent: [
                'mecenat_sync_initiated', 'mecenat_sync_data_downloaded', 'mecenat_sync_completed',
                'mecenat_sync_failed'
            ],
            scope      : this
        });

        me.getGaeChannel().addListener(me.onRemoteAddEvent, {
            remoteEvent: 'member_created',
            scope      : this
        });

        me.getGaeChannel().addListener(me.onRemoteEditEvent, {
            remoteEvent: 'member_updated',
            scope      : this
        });

        me.getGaeChannel().addListener(me.onRemoteSessionEvent, {
            remoteEvent: ['admin_connected', 'admin_disconnected'],
            scope      : this
        });

    },

    launch: function () {

    },

    onRemoteAddEvent        : function (remoteEvent, data) {
        var statusMsg = this.getRemoteAddStatusMsg(),
            oldTimeout = this.getRemoteAddStatusMsgTimeout(),
            buildString = 'Info: ',
            memberData = data.memberData;

        // params: sender, timeStamp, memberData
        if (data.sender !== "anonymous") {
            buildString += data.sender + ' uppdaterade databasen med en ny medlem – ' + memberData.firstName + ' ' + memberData.lastName + ' (' + memberData.personalId + ').';
        }
        else {
            buildString += memberData.firstName + ' ' + memberData.lastName + ' (' + memberData.personalId + ') registrerade en medlemsanmälan för  ' + memberData.lastApplicationFor + '.';
        }

        var store = this.getMemberListStore(),
            entry = store.getById(memberData.personalId);

        if (!entry) {
            store.addSorted(Ext.create('MemberListModel', memberData));
            this.onRefreshSscoSyncDetails();
            this.onRefreshMecenatSyncDetails();
            //entry.data = memberData;
            //entry.commit();
        }

        console.log(buildString);
        if (data.sender_username !== this.getLoggedIn()) {
            if (oldTimeout) {
                window.clearTimeout(oldTimeout);
            }

            statusMsg.hide();
            statusMsg.setText(buildString);
            statusMsg.show();

            this.setRemoteAddStatusMsgTimeout(window.setTimeout(function () {
                statusMsg.hide();
            }, 3e3));
        }
    },
    onRemoteEditEvent       : function (remoteEvent, data) {
        var statusMsg = this.getRemoteEditStatusMsg(),
            oldTimeout = this.getRemoteEditStatusMsgTimeout(),
            buildString = 'Info: ',
            memberData = data.memberData;

        // params: sender, timeStamp, memberData
        if (data.sender !== "anonymous") {
            buildString += data.sender + ' uppdaterade medlemsuppgifter för ' + memberData.firstName + ' ' + memberData.lastName + ' (' + memberData.personalId + ').';
        }
        else {
            buildString += memberData.firstName + ' ' + memberData.lastName + ' (' + memberData.personalId + ') registrerade en medlemsanmälan för  ' + memberData.lastApplicationFor + '.';
        }

        var store = this.getMemberListStore(),
            entry = store.getById(memberData.personalId);

        if (entry) {
            entry.data = memberData;
            entry.commit();
            this.onRefreshSscoSyncDetails();
            this.onRefreshMecenatSyncDetails();
            console.log('member update committed');
        }

        console.log(buildString);
        if (data.sender_username !== this.getLoggedIn()) {
            if (oldTimeout) {
                window.clearTimeout(oldTimeout);
            }

            statusMsg.hide();
            statusMsg.setText(buildString);
            statusMsg.show();

            this.setRemoteEditStatusMsgTimeout(window.setTimeout(function () {
                statusMsg.hide();
            }, 3e3));
        }
    },
    onRemoteSscoSyncEvent   : function (remoteEvent, data) {
        var statusMsg = this.getRemoteSscoSyncStatusMsg(),
            buildString = "Info: ",
            oldTimeout = this.getRemoteSscoSyncStatusMsgTimeout(),
            newTimeout = 3e3;

        switch (remoteEvent) {
            case 'ssco_sync_initiated': // params: sender, timeStamp, sscoSyncCount, requestId
                buildString += data.sender + ' förbereder överföring av ' + data.sscoSyncCount + ' medlemmar till SSSB (id=' + data.requestId + ').';
                break;
            case 'ssco_sync_data_downloaded': // params: sender, timeStamp, sscoSyncCount, requestId
                buildString += data.sender + ' laddade ner en datafil med ' + data.sscoSyncCount + ' objekt till SSSB (id=' + data.requestId + ').';
                break;
            case 'ssco_sync_completed': // params: sender, timeStamp, sscoSyncCount, requestId
                buildString += data.sender + ' bekräftade genomförd transaktion med ' + data.sscoSyncCount + ' medlemmar till SSSB (id=' + data.requestId + ').';
                var sscoSyncCount = this.getSscoSyncCount() - data.sscoSyncCount,
                    newText = this.getDefaultSscoText() + ' (' + sscoSyncCount + ')';
                this.getTransferToSsco().setText(newText);
                if (sscoSyncCount <= 0) {
                    me.getTransferToSsco().disable(true);
                }
                me.setSscoSyncCount(sscoSyncCount);
                break;
            case 'ssco_sync_failed': // params: sender, timeStamp, requestId
                buildString += data.sender + ' misslyckades med att överföra medlemmar till SSSB (id=' + data.requestId + ').';
                timeout = 6e3;
                break;
        }

        console.log(buildString);
        if (data.sender_username !== this.getLoggedIn()) {
            if (oldTimeout) {
                window.clearTimeout(oldTimeout);
            }

            statusMsg.hide();
            statusMsg.setText(buildString);
            statusMsg.show();

            this.setRemoteSscoSyncStatusMsgTimeout(window.setTimeout(function () {
                statusMsg.hide();
            }, timeout));
        }
    },
    onRemoteMecenatSyncEvent: function (remoteEvent, data) {
        var statusMsg = this.getRemoteMecenatSyncStatusMsg(),
            buildString = "Info: ",
            oldTimeout = this.getRemoteMecenatSyncStatusMsgTimeout(),
            newTimeout = 3e3;

        switch (remoteEvent) {
            case 'mecenat_sync_initiated': // params: sender, timeStamp, mecenatSyncCount, requestId
                buildString += data.sender + ' förbereder överföring av ' + data.mecenatSyncCount + ' medlemmar till Mecenat (id=' + data.requestId + ').';
                break;
            case 'mecenat_sync_data_downloaded': // params: sender, timeStamp, mecenatSyncCount, requestId
                buildString += data.sender + ' laddade ner en datafil med ' + data.mecenatSyncCount + ' objekt till Mecenat (id=' + data.requestId + ').';
                break;
            case 'mecenat_sync_completed': // params: sender, timeStamp, mecenatSyncCount, requestId
                buildString += data.sender + ' bekräftade genomförd transaktion med ' + data.mecenatSyncCount + ' medlemmar till Mecenat (id=' + data.requestId + ').';
                var mecenatSyncCount = this.getMecenatSyncCount() - data.mecenatSyncCount,
                    newText = this.getDefaultMecenatText() + ' (' + mecenatSyncCount + ')';
                this.getTransferToMecenat().setText(newText);
                if (mecenatSyncCount <= 0) {
                    me.getTransferToMecenat().disable(true);
                }
                me.setMecenatSyncCount(mecenatSyncCount);
                break;
            case 'mecenat_sync_failed': // params: sender, timeStamp, requestId
                buildString += data.sender + ' misslyckades med att överföra medlemmar till Mecenat (id=' + data.requestId + ').';
                timeout = 6e3;
                break;
        }

        console.log(buildString);
        if (data.sender_username !== this.getLoggedIn()) {
            if (oldTimeout) {
                window.clearTimeout(oldTimeout);
            }

            statusMsg.hide();
            statusMsg.setText(buildString);
            statusMsg.show();

            this.setRemoteMecenatSyncStatusMsgTimeout(window.setTimeout(function () {
                statusMsg.hide();
            }, timeout));
        }
    },
    onRemoteSessionEvent    : function (remoteEvent, data) {
        var statusMsg = this.getRemoteSessionStatusMsg(),
            buildString = "Info: ",
            oldTimeout = this.getRemoteSessionStatusMsgTimeout();

        switch (remoteEvent) {
            case 'admin_connected': // params: sender, timeStamp
                buildString += data.sender + ' kopplade upp sig mot medlemsdatabasen.';
                break;
            case 'admin_disconnected': // params: sender, timeStamp
                buildString += data.sender + ' loggade ut från medlemsdatabasen.';
                break;
        }
        console.log(buildString);
        if (data.sender_username !== this.getLoggedIn()) {
            if (oldTimeout) {
                window.clearTimeout(oldTimeout);
            }

            statusMsg.hide();
            statusMsg.setText(buildString);
            statusMsg.show();

            this.setRemoteSessionStatusMsgTimeout(window.setTimeout(function () {
                statusMsg.hide();
            }, 6e3));
        }
    },

    onTransferToMecenat      : function (button) {

        if (!this.getMecenatSyncCount()) {
            return false;
        }

        var me = this,
            statusMsg = me.getMecenatStatusMsg(),
            failureTypes = me.getFailureTypes(),
            gaeChannel = me.getGaeChannel(),
            requestId = gaeChannel.generateRequestId(),
            initCallbackRecieved = false,
            downloadCallbackRecieved = false;

        statusMsg.hide();
        statusMsg.setText('Status: Bearbetar data för överföring till Mecenat (id=' + requestId + '). . .');
        statusMsg.show();

        me.setMecenatRequestId(requestId);

        var transferWindow = Ext.getCmp('adminToMecenatWindowD');
        if (transferWindow) {
            Ext.apply(transferWindow, { fileDownloadUrl: '/admin/mecenat/download_csv/' + requestId });
        }
        else {
            transferWindow = Ext.create('RKHSK.view.AdminToMecenatWindowD', {
                renderTo       : Ext.getBody(),
                width          : (window.innerWidth < 1030 ? window.innerWidth - 20 : 1010),
                height         : window.innerHeight - 20,
                memberCount    : me.getMecenatSyncCount(),
                mecenatPassword: me.getMecenatPassword(),
                mecenatUsername: me.getMecenatUsername(),
                fileDownloadUrl: '/admin/mecenat/download_csv/' + requestId
            });
        }
        transferWindow.show();

        function initResponseHandler(remoteEvent, data) {
            if (initCallbackRecieved) {
                return false;
            }
            else {
                initCallbackRecieved = true;
            }
            if (!data) {
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 5e3);
                statusMsg.hide();
                statusMsg.setText('Resultat: FEL – Dataöverföring till Mecenat är inte tillgänglig för tillfället (id=' + requestId + ').');
                statusMsg.show();
                transferWindow.hide();
                Ext.Msg.alert('Databearbetning misslyckades', 'En dataöverföring till Mecenat kan inte genomföras vid denna tidpunkt. Om problemet återkommer – skicka en buggrapport till mats.blomdahl@gmail.com.');
            } else {
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 3e3);
                statusMsg.hide();
                statusMsg.setText('Resultat: OK – Dataöverföring förberedd (id=' + requestId + ').');
                statusMsg.show();
                myGlobals.setupSuccessAction = data;
                //me.getAdminConfirmPaymentsD()
            }
        }

        function downloadedResponseHandler(remoteEvent, data) {
            if (downloadCallbackRecieved) {
                return false;
            }
            else {
                downloadCallbackRecieved = true;
            }
            if (data) {
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 3e3);
                statusMsg.hide();
                statusMsg.setText('Resultat: Datafil med ' + data.mecenatSyncCount + ' medlemmar nerladdad (id=' + requestId + ').');
                statusMsg.show();
                me.getTransferToMecenatSubmit().enable();
                myGlobals.downloadSuccessAction = data;
                //me.getAdminConfirmPaymentsD()
                function failureResponseHandler(remoteEvent, data) {
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – Dataöverföringen misslyckades (id=' + requestId + ').');
                    statusMsg.show();
                    Ext.Msg.alert('Datalagringsfel', 'Dataöverföringen till Mecenat kunde inte genomföras. Var god försök igen vid ett senare tillfälle, om problmet återstår är det av högsta vikt att du skickar en buggrapport till mats.blomdahl@gmail.com.');
                }

                function completedResponseHandler(remoteEvent, data) {
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);
                    transferWindow.hide();
                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – ' + data.mecenatSyncCount + ' medlemmar har framgångsrikt överförts till Mecenat-databasen (id=' + requestId + ').');
                    statusMsg.show();
                    Ext.Msg.alert('Medlemsuppgifter överförda', data.mecenatSyncCount + ' medlemmar har framgångsrikt registrerats som överförda till Mecenat-databasen.');
                }

                me.setMecenatFailureHandler(gaeChannel.addListener(failureResponseHandler, { remoteEvent: 'mecenat_sync_failed', requestId: requestId, one: true }));
                me.setMecenatCompletedHandler(gaeChannel.addListener(completedResponseHandler, { remoteEvent: 'mecenat_sync_completed', requestId: requestId, one: true }));
            }
        }

        me.setMecenatInitHandler(gaeChannel.addListener(initResponseHandler, { remoteEvent: 'mecenat_sync_initiated', requestId: requestId, one: true }));
        me.setMecenatDownloadedHandler(gaeChannel.addListener(downloadedResponseHandler, { remoteEvent: 'mecenat_sync_data_downloaded', requestId: requestId, one: true }));

        Ext.Ajax.request({
            url    : '/admin/mecenat/setup_sync_operation/' + requestId,
            method : 'GET',
            timeout: 10e3,
            success: function (response) {
                //gridElement.record.set(gridElement.field, gridValue);
                return true;
            },
            failure: function (response) {
                //var preChangeHeight = statusMsg.getHeight();
                initResponseHandler(false);
                //var postChangeHeight = statusMsg.getHeight();
                //if (preChangeHeight !== postChangeHeight) {
                //  statusMsg.setHeight(postChangeHeight);
                //}
                //console.log('sync payment uri failure')
            }
        });
    },
    onTransferToMecenatSubmit: function (button) {
        var requestId = this.getMecenatRequestId();
        Ext.Ajax.request({
            url    : '/admin/mecenat/confirm_transfer/' + requestId,
            method : 'GET',
            timeout: 10e3,
            success: function (response) {
                //gridElement.record.set(gridElement.field, gridValue);
                console.error('confirm transfer succeeded');
                return true;
            },
            failure: function (response) {
                //var preChangeHeight = statusMsg.getHeight();
                console.error('confirm transfer failed');
                return false;
                //initResponseHandler(false);
                //var postChangeHeight = statusMsg.getHeight();
                //if (preChangeHeight !== postChangeHeight) {
                //  statusMsg.setHeight(postChangeHeight);
                //}
                //console.log('sync payment uri failure')
            }
        });
    },
    onTransferToMecenatAbort : function (button) {
        button.up('window').hide();
    },
    onTransferToMecenatHide  : function () {
        var gaeChannel = this.getGaeChannel(),
            mecenatFailureHandler = this.getMecenatFailureHandler(),
            mecenatCompletedHandler = this.getMecenatCompletedHandler(),
            mecenatDownloadedHandler = this.getMecenatDownloadedHandler(),
            mecenatInitHandler = this.getMecenatInitHandler();

        if (mecenatFailureHandler) {
            gaeChannel.removeListener(mecenatFailureHandler)
        }
        if (mecenatCompletedHandler) {
            gaeChannel.removeListener(mecenatCompletedHandler)
        }
        if (mecenatDownloadedHandler) {
            gaeChannel.removeListener(mecenatDownloadedHandler)
        }
        if (mecenatInitHandler) {
            gaeChannel.removeListener(mecenatInitHandler)
        }
    },

    onTransferToSsco      : function (button) {

        if (!this.getSscoSyncCount()) {
            return false;
        }

        var me = this,
            statusMsg = me.getSscoStatusMsg(),
            failureTypes = me.getFailureTypes(),
            gaeChannel = me.getGaeChannel(),
            requestId = gaeChannel.generateRequestId(),
            initCallbackRecieved = false,
            downloadCallbackRecieved = false;

        statusMsg.hide();
        statusMsg.setText('Status: Bearbetar data för överföring till SSSB (id=' + requestId + '). . .');
        statusMsg.show();

        me.setSscoRequestId(requestId);

        var transferWindow = Ext.getCmp('adminToSscoWindowD');
        if (transferWindow) {
            Ext.apply(transferWindow, { fileDownloadUrl: '/admin/ssco/download_csv/' + requestId });
        }
        else {
            transferWindow = Ext.create('RKHSK.view.AdminToSscoWindowD', {
                renderTo       : Ext.getBody(),
                width          : (window.innerWidth < 1030 ? window.innerWidth - 20 : 1010),
                height         : window.innerHeight - 20,
                memberCount    : me.getSscoSyncCount(),
                sscoPassword   : me.getSscoPassword(),
                sscoUsername   : me.getSscoUsername(),
                fileDownloadUrl: '/admin/ssco/download_csv/' + requestId
            });
        }
        transferWindow.show();

        function initResponseHandler(remoteEvent, data) {
            if (initCallbackRecieved) {
                return false;
            }
            else {
                initCallbackRecieved = true;
            }
            if (!data) {
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 5e3);
                statusMsg.hide();
                statusMsg.setText('Resultat: FEL – Dataöverföring till SSSB är inte tillgänglig för tillfället (id=' + requestId + ').');
                statusMsg.show();
                transferWindow.hide();
                Ext.Msg.alert('Databearbetning misslyckades', 'En dataöverföring till SSSB kan inte genomföras vid denna tidpunkt. Om problemet återkommer – skicka en buggrapport till mats.blomdahl@gmail.com.');
            } else {
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 3e3);
                statusMsg.hide();
                statusMsg.setText('Resultat: OK – Dataöverföring förberedd (id=' + requestId + ').');
                statusMsg.show();
                myGlobals.setupSuccessAction = data;
                //me.getAdminConfirmPaymentsD()
            }
        }

        function downloadedResponseHandler(remoteEvent, data) {
            if (downloadCallbackRecieved) {
                return false;
            }
            else {
                downloadCallbackRecieved = true;
            }
            if (data) {
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 3e3);
                statusMsg.hide();
                statusMsg.setText('Resultat: Datafil med ' + data.sscoSyncCount + ' medlemmar nerladdad (id=' + requestId + ').');
                statusMsg.show();
                me.getTransferToSscoSubmit().enable();
                myGlobals.downloadSuccessAction = data;
                //me.getAdminConfirmPaymentsD()
                function failureResponseHandler(remoteEvent, data) {
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – Dataöverföringen misslyckades (id=' + requestId + ').');
                    statusMsg.show();
                    Ext.Msg.alert('Datalagringsfel', 'Dataöverföringen till SSSB kunde inte genomföras. Var god försök igen vid ett senare tillfälle, om problmet återstår är det av högsta vikt att du skickar en buggrapport till mats.blomdahl@gmail.com.');
                }

                function completedResponseHandler(remoteEvent, data) {
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);
                    transferWindow.hide()
                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – ' + data.sscoSyncCount + ' medlemmar har framgångsrikt överförts till SSSB-databasen (id=' + requestId + ').');
                    statusMsg.show();
                    Ext.Msg.alert('Medlemsuppgifter överförda', data.sscoSyncCount + ' medlemmar har framgångsrikt registrerats som överförda till SSSB-databasen.');
                }

                me.setSscoFailureHandler(gaeChannel.addListener(failureResponseHandler, { remoteEvent: 'ssco_sync_failed', requestId: requestId, one: true }));
                me.setSscoCompletedHandler(gaeChannel.addListener(completedResponseHandler, { remoteEvent: 'ssco_sync_completed', requestId: requestId, one: true }));
            }
        }

        me.setSscoInitHandler(gaeChannel.addListener(initResponseHandler, { remoteEvent: 'ssco_sync_initiated', requestId: requestId, one: true }));
        me.setSscoDownloadedHandler(gaeChannel.addListener(downloadedResponseHandler, { remoteEvent: 'ssco_sync_data_downloaded', requestId: requestId, one: true }));

        Ext.Ajax.request({
            url    : '/admin/ssco/setup_sync_operation/' + requestId,
            method : 'GET',
            timeout: 10e3,
            success: function (response) {
                //gridElement.record.set(gridElement.field, gridValue);
                return true;
            },
            failure: function (response) {
                //var preChangeHeight = statusMsg.getHeight();
                initResponseHandler(false);
                //var postChangeHeight = statusMsg.getHeight();
                //if (preChangeHeight !== postChangeHeight) {
                //  statusMsg.setHeight(postChangeHeight);
                //}
                //console.log('sync payment uri failure')
            }
        });
    },
    onTransferToSscoSubmit: function (button) {
        var requestId = this.getSscoRequestId();
        Ext.Ajax.request({
            url    : '/admin/ssco/confirm_transfer/' + requestId,
            method : 'GET',
            timeout: 10e3,
            success: function (response) {
                //gridElement.record.set(gridElement.field, gridValue);
                console.log('confirm transfer succeeded');
                return true;
            },
            failure: function (response) {
                //var preChangeHeight = statusMsg.getHeight();
                console.log('confirm transfer failed');
                return false;
                //initResponseHandler(false);
                //var postChangeHeight = statusMsg.getHeight();
                //if (preChangeHeight !== postChangeHeight) {
                //  statusMsg.setHeight(postChangeHeight);
                //}
                //console.log('sync payment uri failure')
            }
        });
    },
    onTransferToSscoAbort : function (button) {
        button.up('window').hide();
    },
    onTransferToSscoHide  : function () {
        var gaeChannel = this.getGaeChannel(),
            sscoFailureHandler = this.getSscoFailureHandler(),
            sscoCompletedHandler = this.getSscoCompletedHandler(),
            sscoDownloadedHandler = this.getSscoDownloadedHandler(),
            sscoInitHandler = this.getSscoInitHandler();

        if (sscoFailureHandler) {
            gaeChannel.removeListener(sscoFailureHandler)
        }
        if (sscoCompletedHandler) {
            gaeChannel.removeListener(sscoCompletedHandler)
        }
        if (sscoDownloadedHandler) {
            gaeChannel.removeListener(sscoDownloadedHandler)
        }
        if (sscoInitHandler) {
            gaeChannel.removeListener(sscoInitHandler)
        }
    },

    onGridChangeStart   : function (paging, page, opts) {
        //console.log('load started');
        return true;
    },
    onGridChangeComplete: function (paging, page, opts) {
        //console.log('load complete');
        return true;
    },

    onRowEdit: function (gridElement) {

        myGlobals.e = gridElement;

        var record = gridElement.context.record;

        record.set('firstName', this.onSloppyUserInput(record.get('firstName')));
        record.set('lastName', this.onSloppyUserInput(record.get('lastName')));

        record.set('streetCo', this.onSloppyUserInput(record.get('streetCo')));
        record.set('streetAddress', this.onSloppyUserInput(record.get('streetAddress')));
        record.set('streetNumber', this.onTrimmableInput(record.get('streetNumber')));
        record.set('streetApartment', this.onTrimmableInput(record.get('streetApartment')));
        record.set('streetEntrance', this.onForceUppercaseEntry(record.get('streetEntrance')));
        record.set('zipCode', this.onTrimmableInput(record.get('zipCode')));
        record.set('city', this.onSloppyUserInput(record.get('city')));

        record.set('phone', this.onTrimmableInput(record.get('phone')));
        record.set('email', this.onForceLowercaseEntry(record.get('email')));

        //if (gridElement.newValues.lastApplicationFor === null && gridElement.originalValues.lastApplicationFor !== null)
        //  record.set('lastApplicationFor', gridElement.originalValues.lastApplicationFor)

        var memberGrid = this.getMemberGrid(),
            data = record.data,
            member = data.firstName + ' ' + data.lastName + ' (' + data.personalId + ')',
            failureTypes = this.getFailureTypes(),
            gridValue = gridElement.value,
            statusMsg = this.getEditStatusMsg(),
            requestData = {
                members: [
                    {
                        sys_accounttype    : data.sys_accounttype,
                        sys_administrator  : data.sys_administrator,
                        lastApplicationDate: data.lastApplicationDate ? Ext.Date.format(data.lastApplicationDate, 'Y-m-d') : null,
                        city               : data.city,
                        email              : data.email,
                        firstName          : data.firstName,
                        lastName           : data.lastName,
                        lastMecenatReg     : data.lastMecenatReg,
                        lastApplicationFor : data.lastApplicationFor,
                        lastPaymentKey     : data.lastPaymentKey,
                        lastPaymentRecieved: data.lastPaymentRecieved ? Ext.Date.format(data.lastPaymentRecieved, 'Y-m-d') : null,
                        personalId         : data.personalId,
                        phone              : data.phone,
                        lastSscoReg        : data.lastSscoReg,
                        streetCo           : data.streetCo,
                        streetAddress      : data.streetAddress,
                        streetApartment    : data.streetApartment,
                        streetEntrance     : data.streetEntrance,
                        streetFloor        : data.streetFloor,
                        streetNumber       : data.streetNumber,
                        zipCode            : data.zipCode
                    }
                ]
            },
            formFields = [];

        statusMsg.setText('Status: Överför uppdaterad medlemsinformation för ' + member + ' . . .');
        statusMsg.show();

        Ext.Ajax.request({
            method  : 'POST',
            //params: 'foobar-param',
            url     : '/admin/update/',
            timeout : 1e4,
            jsonData: requestData,
            //emptyText: 'foobaremptytext',
            success : function (response, opts) {
                //gridElement.record.set(gridElement.field, gridValue);
                statusMsg.hide();
                statusMsg.setText('Resultat: OK – Medlemsinformation för ' + member + ' uppdaterad.');
                statusMsg.show();
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 5e3);
                record.commit();
                //memberGrid.getView().refresh();

                myGlobals.updateSuccessResponse = response;
            },
            failure : function (response, opts) {
                statusMsg.hide();
                statusMsg.setText('Resultat: FEL – Medlemsinformation för ' + member + ' kunde inte uppdateras.');
                statusMsg.show();
                window.setTimeout(function () {
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – ' + failureTypes[response.failureType]);
                    statusMsg.show();
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 5e3);
                }, 5e3)
                myGlobals.updateFailureResponse = response;
                //Ext.Msg.alert('Registreringen misslyckades', failureTypes[response.failureType]);
            }
        });

    },

    onOwaContactsUpdate: function (data) {
        Ext.Ajax.request({
            url     : "/admin/update/",
            method  : 'POST',
            jsonData: { batch: data },
            timeout : 1e5,
            success : function (response, opt) {
                //gridElement.record.set(gridElement.field, gridValue);
                //statusMsg.setText('Resultat: OK – Medlemsinformation för '+member+' uppdaterad.');
                //statusMsg.show();
                //window.setTimeout(function() { statusMsg.hide(); }, 5e3);
                //record.commit();
                //memberGrid.getView().refresh();
                console.log("updated successful!")
                myGlobals.updateSuccessResponse = response;
                //Ext.Msg.alert('Medlemsregistrering skickad', 'Du är nu registrerad i RKH-SK:s medlemsdatabas och kan betala in medlemsavgiften via PlusGiro eller studentkårens expedition. Se e-post för den personliga kod du måste bifoga inbetalningen.');
            },
            failure : function (response, opt) {
                //statusMsg.setText('Resultat: FEL – Medlemsinformation för '+member+' kunde inte uppdateras.');
                //statusMsg.show();
                //window.setTimeout(function() {
                //  statusMsg.setText('Resultat: FEL – '+failureTypes[response.failureType]);
                //  statusMsg.show();
                //  window.setTimeout(function() { statusMsg.hide(); }, 5e3);
                //}, 5e3)
                console.error("update failure!")
                myGlobals.updateFailureResponse = response;
                //Ext.Msg.alert('Registreringen misslyckades', failureTypes[response.failureType]);
            }
        });
    },

    onLogInOut: function (button) {
        if (this.getLoggedIn()) { // logout
            var store = this.getMemberListStore();
            store.clearData();
            store.proxy.data = {};
            store.load();
            this.onRefreshLogin(true);
            this.getGaeChannel().disconnect();
            window.location.href = '/admin/logout/';
        } else { // login
            Ext.create('RKHSK.view.AdminLoginWindowD').show();
        }
    },

    onAssholeIdInput: function (field) {
        if (field.isValid()) {
            field.setValue(field.getValue().toUpperCase());
            return true;
        }

        var input = Ext.String.trim(field.getValue()).toUpperCase();

        // test for 19832010-0221
        if (input.length === 13) {
            if (input.search(/19\d{6}-[\dA-Z]{4}/) !== -1) {
                field.setValue(input.substring(2));
                return true;
            } else {
                return false;
            }
        }

        // test for 198320100221
        if (input.length === 12) {
            if (input.search(/19\d{6}[\dA-Z]{4}/) !== -1) {
                field.setValue(input.substring(2, 8) + '-' + input.substring(8));
                return true;
            } else {
                return false;
            }
        }

        // test for 8320100221
        if (input.length === 10) {
            if (input.search(/[2-9][0-9]\d{4}[\dA-Z]{4}/) !== -1) {
                field.setValue(input.substring(0, 6) + '-' + input.substring(6));
                return true;
            } else {
                return false;
            }
        }

        field.setValue(input);
    },

    onSloppyUserInput: function (field) {
        var input = Ext.String.trim(typeof field === 'string' ? field : field.getValue()).toLowerCase(),
            validLowerCaseWords = ['Von', 'Van', 'Väg', 'Gata'],
            validUpperCaseWords = ['Ii', 'Iii', 'Iv', 'Vi'];
        while (input.indexOf('  ') !== -1) {
            input = input.replace('  ', ' ')
        }
        input = input.replace(' -', '-').replace('- ', '-');
        while (input.indexOf('--') !== -1) {
            input = input.replace('--', '-')
        }
        input = input.split(' ');
        for (var tmp, i = 0, j = input.length;
             i < j;
             i++) {

            input[i] = Ext.String.capitalize(input[i]);
            if (input[i].split('-').length > 1) {
                tmp = input[i].split('-');
                for (var k = 0, l = tmp.length;
                     k < l;
                     k++) {
                    tmp[k] = Ext.String.capitalize(tmp[k]);
                }
                input[i] = tmp.join('-');
            } else {
                if (input[i].split(':').length > 1) { /// 'III:s gata'
                    tmp = input[i].split(':');
                    if (validUpperCaseWords.indexOf(tmp[0]) !== -1) {
                        tmp[0] = tmp[0].toUpperCase();
                        input[i] = tmp.join(':');
                    }
                } else {
                    if (validLowerCaseWords.indexOf(input[i]) !== -1) {
                        input[i] = input[i].toLowerCase();
                    } else {
                        for (var k = validUpperCaseWords.length;
                             k--;) {
                            if (input[i] === validUpperCaseWords[i] + 's') {
                                input[i] = validUpperCaseWords[i].toUpperCase() + 's';
                                break;
                            }
                        }
                    }
                }
            }
        }

        input = input.join(' ');
        if (typeof field === 'string') {
            return input;
        }
        else {
            field.setValue(input);
        }
    },
    onTrimmableInput : function (field) {
        var input = Ext.String.trim(typeof field === 'string' ? field : field.getValue());
        if (typeof field === 'string') {
            return input;
        }
        else {
            field.setValue(input);
        }
    },

    onAddRegFormModified: function () {
        var regForm = this.getAddRegForm(),
            inputFields = regForm.getValues(),
            submitAddForm = this.getSubmitAddForm();
        myGlobals.regForm = regForm;
        if (!inputFields['city'] || !inputFields['zipCode'] || !inputFields['streetAddress'] || !inputFields['streetNumber'] || !inputFields['studentConfirmation'] || !inputFields['firstName'] || !inputFields['lastName'] || !inputFields['lastApplicationFor'] || !inputFields['personalId'] || !inputFields['country']) {
            if (!submitAddForm.isDisabled()) {
                submitAddForm.disable(true);
            }
        } else if (regForm.getForm().isValid()) {
            if (submitAddForm.isDisabled()) {
                submitAddForm.enable(true);
            }
        } else {
            if (!submitAddForm.isDisabled()) {
                submitAddForm.disable(true);
            }
        }
    },
    onForceCountry      : function (field) {
        if (!field.isValid()) {
            field.setValue(field.defaultValue);
        }
    },

    onForceUppercaseEntry: function (field) {
        var input = Ext.String.trim(typeof field === 'string' ? field : field.getValue()).toUpperCase();
        if (typeof field === 'string') {
            return input;
        }
        else {
            field.setValue(input);
        }
    },
    onForceLowercaseEntry: function (field) {
        var input = Ext.String.trim(typeof field === 'string' ? field : field.getValue()).toLowerCase();
        if (typeof field === 'string') {
            return input;
        }
        else {
            field.setValue(input);
        }
    },

    onResetAddForm : function (button) {
        button.up('form').getForm().reset();
    },
    onSubmitAddForm: function (button) {
        var form = button.up('form').getForm(),
            failureTypes = this.getFailureTypes(),
            paymentSubmitted = form.getValues()['lastPaymentRecieved'] ? true : false;

        // tester för e-postadress och personnr?
        if (form.isValid()) {
            form.submit({
                method   : 'POST',
                //params: 'foobar-param',
                reset    : true,
                type     : 'submit',
                url      : '/admin/create/',
                waitTitle: 'Medlemsregistrering',
                timeout  : 1e4,
                waitMsg  : 'Registreringsinformation för ny medlem sparas ...',
                //emptyText: 'foobaremptytext',
                success  : function (form, action) {
                    myGlobals.successAction = action;
                    Ext.Msg.alert('Medlem sparad', 'Registreringsinformation för den nya medlemmen har sparats till databasen.' + (paymentSubmitted ? '' : ' För att medlemskapet ska aktiveras måste även inbetalning av medlemsavgift registreras.'));
                },
                failure  : function (form, action) {
                    myGlobals.failureAction = action;
                    var failMsg = failureTypes[action.status];
                    if (!failMsg) {
                        failMsg = 'Ett oväntat fel inträffade (kod: ' + action.status + ')';
                    }
                    Ext.Msg.alert('Medlemsregistreringen misslyckades', failMsg);
                }
            });
        }
    },

    onLoginFormModified: function (field) {
        var loginForm = field.up('form').getForm(),
            loginUsername = this.getLoginUsername(),
            loginPassword = this.getLoginPassword(),
            loginWindowSubmit = this.getLoginWindowSubmit(),
            loginWindowResend = this.getLoginWindowResend();

        if (loginUsername.isValid() && loginPassword.isValid() && !loginPassword.isDisabled()) {
            if (loginWindowSubmit.isDisabled()) {
                loginWindowSubmit.enable(true);
            }
            if (loginWindowResend.isDisabled()) {
                loginWindowResend.enable(true);
            }
        } else {
            if (!loginWindowSubmit.isDisabled()) {
                loginWindowSubmit.disable(true);
            }

            if (loginUsername.isValid()) {
                if (loginWindowResend.isDisabled()) {
                    loginWindowResend.enable(true);
                }
                if (loginPassword.isDisabled()) {
                    loginPassword.enable(true);
                }
            } else {
                if (!loginWindowResend.isDisabled()) {
                    loginWindowResend.disable(true);
                }
                if (!loginPassword.isDisabled()) {
                    loginPassword.disable(true);
                }
            }
        }
    },
    onLoginWindowEnter : function (field, keyEvent) {
        if (keyEvent.getKey() === keyEvent.ENTER) {
            this.onLoginWindowSubmit(field);
        }
    },
    onLoginWindowSubmit: function (button) {
        var loginForm = button.up('form').getForm();

        if (loginForm.isValid()) {

            var me = this,
                statusMsg = me.getLoginStatusMsg(),
                loginWindow = button.up('window'),
                loginUsername = loginForm.getValues().loginUsername,
                failureTypes = me.getFailureTypes();

            statusMsg.setText('Status: Verifierar inloggningsuppgifter . . .');
            statusMsg.show();

            loginForm.submit({
                method   : 'POST',
                reset    : true,
                type     : 'submit',
                url      : '/admin/login/',
                waitTitle: 'Loggar in',
                timeout  : 10,
                waitMsg  : 'Inloggningsuppgifter överförs ...',
                success  : function (form, action) {
                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – inloggningsuppgifter  verifierade.');
                    statusMsg.show();

                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);

                    loginWindow.destroy();

                    myGlobals.successAction = action;
                    /*
                     var storeLoadMask = new Ext.LoadMask(me.getMemberGrid(), {
                     title: 'Uppdaterar',
                     msg  : 'Läser in medlemmar . . .'
                     });
                     storeLoadMask.show();

                     Ext.data.JsonWithPadding.request({
                     url: '/admin/read/',
                     success: function(response) {
                     proxy = Ext.create('Ext.ux.data.PagingMemoryProxy', {
                     //type: 'pagingmemory',
                     //model: 'MemberListModel',
                     data: response,
                     //format: 'json',
                     /*api: {
                     create: '/admin/create/',
                     read: '/admin/read/',
                     update: '/admin/update/',
                     destroy: '/admin/destroy/'
                     },*/
                    //extraParams: {
                    //  total: 50000
                    //},
                    /*actionMethods: {
                     create: 'GET',
                     update: 'GET',
                     read: 'GET',
                     destroy: 'GET'
                     },*/
                    //callbackKey: 'callback',
                    //simpleSortMode: true
                    /*reader: {
                     type: 'json',
                     idProperty: 'personalId',
                     totalProperty: 'memberCount',
                     root: 'members'
                     }
                     });/*
                     reader = Ext.create('Ext.data.reader.Json', {
                     idProperty: 'personalId',
                     totalProperty: 'memberCount',
                     root: 'members'
                     });*/
                    //myGlobals.reader = reader;
                    /*myGlobals.proxy = proxy;


                     memberList.setProxy(proxy);
                     memberList.load()
                     myGlobals.res = response;
                     console.log("'success'")
                     storeLoadMask.hide();
                     },
                     failure: function(response) {
                     console.log('we failed.')
                     myGlobals.res = response;
                     storeLoadMask.hide();
                     }
                     });
                     */
                    me.getMenuLogInOut().setText('Logga ut');
                    me.setLoggedIn(loginUsername);

                    me.onRefreshPaymentsUri();

                    window.setTimeout(function () {
                        me.onRefreshAttachmentUri();
                    }, 6e3);
                    window.setTimeout(function () {
                        me.onRefreshMecenatSyncDetails(true);
                    }, 3e3);
                    window.setTimeout(function () {
                        me.onRefreshSscoSyncDetails(true);
                    }, 2e3);

                    me.setLoginTimeout(window.setTimeout(function () {
                        me.onRefreshLogin();
                    }, 5e5));

                    me.getGaeChannel().connect();
                    me.getMemberListStore().load();

                },
                failure  : function (form, action) {
                    myGlobals.failureAction = action;

                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – inloggningsuppgifter  verifierade.');
                    statusMsg.show();

                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);

                    var failMsg = failureTypes[action.status];
                    if (!failMsg) {
                        failMsg = 'Ett oväntat fel inträffade (kod: ' + action.status + ')';
                    }
                    Ext.Msg.alert('Inloggningen misslyckades', failMsg);
                }
            });
        }
    },
    onLoginWindowAbort : function (button) {
        button.up('window').destroy();
    },
    onLoginWindowResend: function (button) {
        var me = this,
            loginForm = button.up('form').getForm();
        loginUsername = me.getLoginUsername(),
            failureTypes = me.getFailureTypes();

        if (!loginUsername.isValid()) {
            return false;
        }

        // push ajax request to server
        loginForm.submit({
            method          : 'POST',
            clientValidation: false,
            url             : '/admin/passwd/reset',
            waitTitle       : 'Skickar e-post',
            timeout         : 10,
            waitMsg         : 'Skickar uppgifter för återställning av lösenord . . .',
            success         : function (form, action) {
                myGlobals.res = action.result;
                button.up('window').destroy();
                Ext.Msg.alert('Återställningsmail skickat', 'De uppgifter du behöver för att återställa ditt lösenord har skickats till ' + loginUsername.getValue() + '. Observera att det kan ta upp till flera minuter innan meddelandet anländer.');
                //var loginBtn = me.getMenuLogInOut();
                //loginBtn.fireEvent('click', loginBtn);
            },
            failure         : function (form, action) {
                var failMsg;
                if (action.status === 400) {
                    failMsg = 'Ditt återställningsförsök misslyckades. Var god kontrollera att du angett den e-postadress du registrerat dig med och att du har kvar dina administratörsrättigheter i RKH-SK:s medlemsdatabas.';
                }
                else {
                    failMsg = failureTypes[action.status];
                }
                if (!failMsg) {
                    failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
                }
                Ext.Msg.alert('Återställningen misslyckades', failMsg);
                return false;
            }
        });
    },

    onRefreshLogin        : function (pause) {
        var me = this,
            statusMsg = me.getLoginStatusMsg(),
            failureTypes = me.getFailureTypes();

        if (pause && (timeout = me.getLoginTimeout())) {
            window.clearTimeout(timeout);
            me.setLoginTimeout(null);
            if (statusMsg.isVisible()) {
                statusMsg.hide();
            }
            return true;
        }

        statusMsg.setText('Status: Synkroniserar inloggningsnycklar . . .');
        statusMsg.show();
        Ext.Ajax.request({
            url    : '/admin/login/',
            method : 'GET',
            timeout: 10e3,
            success: function (response, opt) {
                statusMsg.hide();
                statusMsg.setText('Resultat: OK – Inloggningsnycklar uppdaterade.');
                statusMsg.show();
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 3e3);
                me.setLoginTimeout(window.setTimeout(function () {
                    me.onRefreshLogin();
                }, 5e5));
            },
            failure: function (response, opt) {
                statusMsg.hide();
                statusMsg.setText('Resultat: FEL – Dina inloggningsnycklar kunde inte uppdateras.');
                statusMsg.show();
                window.setTimeout(function () {
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – ' + failureTypes[response.failureType]);
                    statusMsg.show();
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 6e3);
                }, 3e3)
                me.setLoginTimeout(window.setTimeout(function () {
                    me.onRefreshLogin();
                }, 18e3));
            }
        });

    },
    onRefreshAttachmentUri: function (pause) {
        var me = this,
            statusMsg = this.getAttachmentUploadStatusMsg(),
            failureTypes = this.getFailureTypes();

        if (pause && (timeout = me.getAttachmentUploadTimeout())) {
            window.clearTimeout(timeout);
            me.setAttachmentUploadTimeout(null);
            if (statusMsg.isVisible()) {
                statusMsg.hide();
            }
            return true;
        }

        var requestId = this.getGaeChannel().generateRequestId();

        statusMsg.setText('Status: Synkroniserar nycklar för filöverföring (id=' + requestId + ') . . .');
        statusMsg.show();
        Ext.Ajax.request({
            url    : '/admin/upload/attachment/' + requestId,
            method : 'GET',
            timeout: 10e3,
            success: function (response, opt) {
                //gridElement.record.set(gridElement.field, gridValue);
                statusMsg.hide();
                statusMsg.setText('Resultat: OK – Nycklar för filöverföring uppdaterade.');
                statusMsg.show();
                //console.log('sync attach. success')

                window.setTimeout(function () {
                    statusMsg.hide();
                }, 3e3);

                me.setAttachmentUploadTimeout(window.setTimeout(function () {
                    me.onRefreshAttachmentUri();
                }, 5e5));

                //memberGrid.getView().refresh();
                me.setAttachmentRequestId(requestId)
                me.setAttachmentUploadUri(Ext.decode(response.responseText).uploadToken);
                //Ext.Msg.alert('Medlemsregistrering skickad', 'Du är nu registrerad i RKH-SK:s medlemsdatabas och kan betala in medlemsavgiften via PlusGiro eller studentkårens expedition. Se e-post för den personliga kod du måste bifoga inbetalningen.');
            },
            failure: function (response, opt) {
                statusMsg.hide();
                statusMsg.setText('Resultat: FEL – Nycklar för filöverföring kunde inte uppdateras.');
                statusMsg.show();
                //console.log('sync attach. failed')
                window.setTimeout(function () {
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – ' + failureTypes[response.status]);
                    statusMsg.show();
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 6e3);
                }, 3e3)

                me.setAttachmentUploadTimeout(window.setTimeout(function () {
                    me.onRefreshAttachmentUri();
                }, 18e3));

                //Ext.Msg.alert('Registreringen misslyckades', failureTypes[response.failureType]);
            }
        });

    },
    onRefreshPaymentsUri  : function (pause) {
        var me = this,
            statusMsg = this.getPaymentsUploadStatusMsg(),
            failureTypes = this.getFailureTypes();

        if (pause && (timeout = me.getPaymentsUploadTimeout())) {
            window.clearTimeout(timeout);
            me.setPaymentsUploadTimeout(null);
            if (statusMsg.isVisible()) {
                statusMsg.hide();
            }
            return true;
        }

        var requestId = this.getGaeChannel().generateRequestId();

        statusMsg.setText('Status: Synkroniserar nycklar för betalningsinformation (id=' + requestId + ') . . .');
        statusMsg.show();

        Ext.Ajax.request({
            url    : '/admin/upload/payments/' + requestId,
            method : 'GET',
            timeout: 10e3,
            success: function (response) {
                //gridElement.record.set(gridElement.field, gridValue);
                statusMsg.hide();
                statusMsg.setText('Resultat: OK – Nycklar för betalningsinformation uppdaterade.');
                statusMsg.show();
                //console.log('sync payment uri success')
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 3e3);

                me.setPaymentsUploadTimeout(window.setTimeout(function () {
                    me.onRefreshPaymentsUri();
                }, 5e5));

                //memberGrid.getView().refresh();

                me.setPaymentsRequestId(requestId);
                me.setPaymentsUploadUri(Ext.decode(response.responseText).uploadToken);
                //Ext.Msg.alert('Medlemsregistrering skickad', 'Du är nu registrerad i RKH-SK:s medlemsdatabas och kan betala in medlemsavgiften via PlusGiro eller studentkårens expedition. Se e-post för den personliga kod du måste bifoga inbetalningen.');
            },
            failure: function (response) {
                //var preChangeHeight = statusMsg.getHeight();
                statusMsg.hide();
                statusMsg.setText('Resultat: FEL – Nycklar för betalningsinformation kunde inte uppdateras.');
                statusMsg.show();
                //var postChangeHeight = statusMsg.getHeight();
                //if (preChangeHeight !== postChangeHeight) {
                //  statusMsg.setHeight(postChangeHeight);
                //}
                //console.log('sync payment uri failure')
                window.setTimeout(function () {
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – ' + failureTypes[response.status]);
                    statusMsg.show();
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 6e3);
                }, 3e3)

                me.setPaymentsUploadTimeout(window.setTimeout(function () {
                    me.onRefreshPaymentsUri();
                }, 18e3));

                //Ext.Msg.alert('Registreringen misslyckades', failureTypes[response.failureType]);
            }
        });

    },

    onRefreshMecenatSyncDetails: function (forceRefresh) {
        var me = this,
            statusMsg = me.getMecenatStatusMsg(),
            failureTypes = me.getFailureTypes();

        if (forceRefresh) {
            if (statusMsg.isVisible()) {
                statusMsg.hide();
            }
            statusMsg.setText('Status: Synkroniserar synkroniseringsstatus för Mecenat . . .');
            statusMsg.show();
            Ext.Ajax.request({
                url    : '/admin/mecenat/get_sync_details',
                method : 'GET',
                timeout: 10e3,
                success: function (response) {
                    //gridElement.record.set(gridElement.field, gridValue);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – Synkroniseringsstatus för Mecenat uppdaterad.');
                    statusMsg.show();
                    //console.log('sync payment uri success')
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);

                    var responseData = Ext.decode(response.responseText),
                        newText = me.getDefaultMecenatText() + ' (' + responseData.mecenatSyncCount + ')';
                    me.getTransferToMecenat().setText(newText);
                    if (responseData.mecenatSyncCount) {
                        me.getTransferToMecenat().enable()
                    }
                    me.setMecenatSyncCount(responseData.mecenatSyncCount);
                    me.setMecenatUsername(responseData.mecenatUsername);
                    me.setMecenatPassword(responseData.mecenatPassword);
                },
                failure: function (response) {
                    //var preChangeHeight = statusMsg.getHeight();
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – Synkroniseringsstatus för Mecenat kunde inte uppdateras.');
                    statusMsg.show();
                    //var postChangeHeight = statusMsg.getHeight();
                    //if (preChangeHeight !== postChangeHeight) {
                    //  statusMsg.setHeight(postChangeHeight);
                    //}
                    //console.log('sync payment uri failure')
                    window.setTimeout(function () {
                        statusMsg.hide();
                        statusMsg.setText('Resultat: FEL – ' + failureTypes[response.status]);
                        statusMsg.show();
                        window.setTimeout(function () {
                            statusMsg.hide();
                        }, 6e3);
                    }, 3e3)

                    window.setTimeout(function () {
                        me.onRefreshMecenatSyncDetails(true);
                    }, 15e3);
                }
            })
        }
    },
    onRefreshSscoSyncDetails   : function (forceRefresh) {
        var me = this,
            statusMsg = me.getSscoStatusMsg(),
            failureTypes = me.getFailureTypes();

        if (forceRefresh) {
            if (statusMsg.isVisible()) {
                statusMsg.hide();
            }
            statusMsg.setText('Status: Synkroniserar synkroniseringsstatus för SSSB . . .');
            statusMsg.show();
            Ext.Ajax.request({
                url    : '/admin/ssco/get_sync_details',
                method : 'GET',
                timeout: 10e3,
                success: function (response) {
                    //gridElement.record.set(gridElement.field, gridValue);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – Synkroniseringsstatus för SSSB uppdaterad.');
                    statusMsg.show();
                    //console.log('sync payment uri success')
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);

                    var responseData = Ext.decode(response.responseText),
                        newText = me.getDefaultSscoText() + ' (' + responseData.sscoSyncCount + ')';
                    me.getTransferToSsco().setText(newText);
                    if (responseData.sscoSyncCount) {
                        me.getTransferToSsco().enable()
                    }
                    me.setSscoSyncCount(responseData.sscoSyncCount);
                    me.setSscoUsername(responseData.sscoUsername);
                    me.setSscoPassword(responseData.sscoPassword);
                },
                failure: function (response) {
                    //var preChangeHeight = statusMsg.getHeight();
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – Synkroniseringsstatus för SSSB kunde inte uppdateras.');
                    statusMsg.show();
                    //var postChangeHeight = statusMsg.getHeight();
                    //if (preChangeHeight !== postChangeHeight) {
                    //  statusMsg.setHeight(postChangeHeight);
                    //}
                    //console.log('sync payment uri failure')
                    window.setTimeout(function () {
                        statusMsg.hide();
                        statusMsg.setText('Resultat: FEL – ' + failureTypes[response.status]);
                        statusMsg.show();
                        window.setTimeout(function () {
                            statusMsg.hide();
                        }, 6e3);
                    }, 3e3)

                    window.setTimeout(function () {
                        me.onRefreshSscoSyncDetails(true);
                    }, 15e3);
                }
            })
        }
    },

    onUploadBankStatement: function (button) {
        var me = this,
            uploadForm = button.up('form').getForm(),
            requestId = me.getPaymentsRequestId();

        if (!requestId) {
            return false;
        }

        if (uploadForm.isValid()) {
            me.onRefreshPaymentsUri(pause = true);
            var statusMsg = me.getPaymentRegStatusMsg();

            statusMsg.setText('Status: Kontoutdrag överförs till servern för analys (id=' + requestId + ') . . .');
            statusMsg.show();

            function responseHandler(remoteEvent, data) {
                if (callbackRecieved) {
                    return false;
                }
                else {
                    callbackRecieved = true;
                }
                button.disable();
                window.setTimeout(function () {
                    me.onRefreshPaymentsUri();
                }, 3e3);
                if (!data) {
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 5e3);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – Datafilen kunde inte överföras (id=' + requestId + ').');
                    statusMsg.show();
                    Ext.Msg.alert('Överföringen misslyckades', 'Överföringen kunde inte genomföras, av en eller annan anledning. Var vänlig och kontrollera din internetanslutning, försök igen och – om det fortfarande inte fungerar – skicka en buggrapport till mats.blomdahl@gmail.com');
                } else {
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – Datafilen har överförts och analyserats (id=' + requestId + ').');
                    statusMsg.show();
                    myGlobals.uploadSuccessAction = data;
                    me.getRecognizedMembersListStore().add(
                        data.itemsRecognized
                    );
                    me.getUnidentifiedMembersListStore().add(
                        data.itemsUnidentified
                    );
                    Ext.create('RKHSK.view.AdminConfirmPaymentsD', {
                        renderTo       : Ext.getBody(),
                        contentCategory: 'datalist'
                    }).show();
                    //me.getAdminConfirmPaymentsD()
                }
            }

            var gaeChannel = me.getGaeChannel(),
                callbackRecieved = false;

            gaeChannel.addListener(responseHandler, { requestId: requestId, one: true })

            uploadForm.submit({
                url      : me.getPaymentsUploadUri(),
                waitTitle: 'Laddar upp data ...',
                waitMsg  : 'Din datafil överförs till servern.',
                timeout  : 1e4,
                type     : 'submit',
                reset    : false,
                success  : function () {
                    console.log('success');
                },
                failure  : function () {
                    console.log('failure');
                }
            });
            window.setTimeout(responseHandler, 95e2);
        }
    },
    onUploadFormModified : function (field) {
        var uploadButton = this.getUploadBankStatement();
        if (field.isValid()) {
            if (uploadButton.isDisabled()) {
                uploadButton.enable()
            }
        } else if (!x.isDisabled()) {
            uploadButton.disable()
        }
    },

    onUploadPaymentsList : function (button) {

        var me = this,
            fieldValues = me.getUploadPaymentsField().getValue().split('\n'),
            payments = [],
            statusMsg = me.getPaymentRegStatusMsg();

        for (var i = fieldValues.length;
             i--;) {
            paymentDate = fieldValues[i].match(/\d{4}\-\d{2}\-\d{2}/);
            paymentKey = fieldValues[i].match(/[a-z]{5}/i);
            if (paymentDate && paymentKey) {
                payments.push({
                    lastPaymentKey     : paymentKey[0],
                    lastPaymentRecieved: paymentDate[0]
                })
            }
        }

        if (payments.length) {
            var callbackRecieved = false,
                requestId = this.getGaeChannel().generateRequestId(),
                loadingMsg = new Ext.LoadMask(me.getAdminPageD(), { title: 'Verifiering', msg: 'Inbetalningar verifieras . . .' });

            statusMsg.setText('Status: Inbetalningslista överförs till servern för verifiering (id=' + requestId + ') . . .');
            statusMsg.show();
            loadingMsg.show();

            function responseHandler(remoteEvent, data) {
                if (callbackRecieved) {
                    return false;
                }
                else {
                    callbackRecieved = true;
                }
                window.setTimeout(function () {
                    me.onRefreshPaymentsUri();
                }, 3e3);
                if (!data) {
                    loadingMsg.hide();
                    window.setTimeout(function () {
                        statusMsg.hide();
                        me.onRefreshPaymentsUri();
                    }, 5e3);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – Listan över inbetalningar kunde inte överföras (id=' + requestId + ').');
                    statusMsg.show();
                    Ext.Msg.alert('Överföringen misslyckades', 'Överföringen kunde inte genomföras, av en eller annan anledning. Var vänlig och kontrollera din internetanslutning, försök igen och – om det fortfarande inte fungerar – skicka en buggrapport till mats.blomdahl@gmail.com');
                } else {
                    loadingMsg.hide();
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – Inbetalningslistan överförd och analyserad (id=' + requestId + ').');
                    statusMsg.show();
                    me.getRecognizedMembersListStore().add(
                        data.itemsRecognized
                    );
                    me.getUnidentifiedMembersListStore().add(
                        data.itemsUnidentified
                    );
                    Ext.create('RKHSK.view.AdminConfirmPaymentsD', {
                        renderTo       : Ext.getBody(),
                        contentCategory: 'datafile'
                    }).show();
                }
            }

            var gaeChannel = me.getGaeChannel(),
                callbackRecieved = false;

            gaeChannel.addListener(responseHandler, { requestId: requestId, one: true })

            Ext.Ajax.request({
                url      : '/admin/update/' + requestId,
                method   : 'POST',
                waitTitle: 'Laddar upp listning ...',
                waitMsg  : 'Din lista över inbetalningar överförs till servern.',
                timeout  : 1e4,
                success  : function () {
                    console.log('success');
                },
                failure  : function () {
                    console.log('failure');
                },
                jsonData : { payments_verification: payments }
            });

            window.setTimeout(responseHandler, 9e3);
        }
    },
    onRegPaymentsModified: function (field) {
        var regButton = this.getUploadPaymentsButton();
        if (field.isValid()) {
            if (regButton.isDisabled()) {
                regButton.enable();
            }
        } else if (!regButton.isDisabled()) {
            regButton.disable();
        }
    },

    onConfirmPaymentsAbort  : function (button) {
        button.up('window').destroy();
    },
    onConfirmPaymentsSubmit : function (button) {
        var confirmationWindow = button.up('window'),
            validPaymentsStore = this.getRecognizedMembersListStore()
        payments = validPaymentsStore.getRange(0, validPaymentsStore.getCount()),
            failureTypes = this.getFailureTypes(),
            requestData = [];

        for (var tmp, i = payments.length;
             i--;) {
            tmp = payments[i].data;
            tmp.lastApplicationDate = Ext.Date.format(tmp.lastApplicationDate, 'Y-m-d');
            tmp.lastPaymentRecieved = Ext.Date.format(tmp.lastPaymentRecieved, 'Y-m-d');
            requestData.push(tmp)
        }
        if (!requestData.length) {
            return false;
        }

        var statusMsg = this.getPaymentsUploadStatusMsg(),
            loadingMsg = new Ext.LoadMask(me.getAdminPageD(), { title: 'Registrerar', msg: 'Avgiftsinbetalningar registreras . . .' });

        statusMsg.setText('Status: Betalningar registreras hos servern . . .');
        statusMsg.show();
        loadingMsg.show();

        Ext.Ajax.request({
            url     : "/admin/update/",
            method  : 'POST',
            jsonData: { payments: requestData },
            timeout : 1e4,
            success : function (response, opt) {
                loadingMsg.hide();
                data = Ext.decode(response.responseText)
                descriptor = data.paymentsRegistered === 1 ? 'betalning' : 'betalningar';
                confirmationWindow.destroy();
                statusMsg.setText('Resultat: OK – ' + data.paymentsRegistered + ' in' + descriptor + ' registrerades.');
                statusMsg.show();
                window.setTimeout(function () {
                    statusMsg.hide();
                }, 3e3);
                Ext.Msg.alert('Inbetalningar registrerade', 'Inbetalning av ' + data.paymentsRegistered + ' medlemsavgifter har registrerats i databasen.');
            },
            failure : function (response, opt) {
                loadingMsg.hide();
                statusMsg.setText('Resultat: FEL – Inga inbetalningar registrerades i databasen.');
                statusMsg.show();
                window.setTimeout(function () {
                    statusMsg.setText('Resultat: FEL – ' + failureTypes[response.failureType]);
                    statusMsg.show();
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 5e3);
                }, 5e3)
                //myGlobals.updateFailureResponse = response;
                Ext.Msg.alert('Registreringen misslyckades', failureTypes[response.failureType]);
            }
        });

    },
    onConfirmPaymentsDestroy: function () {
        this.getUnidentifiedMembersListStore().clearData();
        this.getRecognizedMembersListStore().clearData();
    },

    onApplyExtensions: function (button) {
        Ext.Ajax.request({
            url     : "/admin/update/",
            method  : 'POST',
            jsonData: { extensions: true },
            timeout : 1e5,
            success : function (response, opt) {
                //gridElement.record.set(gridElement.field, gridValue);
                //statusMsg.setText('Resultat: OK – Medlemsinformation för '+member+' uppdaterad.');
                //statusMsg.show();
                //window.setTimeout(function() { statusMsg.hide(); }, 5e3);
                //record.commit();
                //memberGrid.getView().refresh();
                console.log("updated successful!")
                myGlobals.updateSuccessResponse = response;
                //Ext.Msg.alert('Medlemsregistrering skickad', 'Du är nu registrerad i RKH-SK:s medlemsdatabas och kan betala in medlemsavgiften via PlusGiro eller studentkårens expedition. Se e-post för den personliga kod du måste bifoga inbetalningen.');
            },
            failure : function (response, opt) {
                //statusMsg.setText('Resultat: FEL – Medlemsinformation för '+member+' kunde inte uppdateras.');
                //statusMsg.show();
                //window.setTimeout(function() {
                //  statusMsg.setText('Resultat: FEL – '+failureTypes[response.failureType]);
                //  statusMsg.show();
                //  window.setTimeout(function() { statusMsg.hide(); }, 5e3);
                //}, 5e3)
                console.error("update failure!")
                myGlobals.updateFailureResponse = response;
                //Ext.Msg.alert('Registreringen misslyckades', failureTypes[response.failureType]);
            }
        });
        return true;
    },

    onStatusMsgBeforeShow: function (label) {
        label.setWidth(window.innerWidth - 105 - 348);
    },
    onStatusMsgShow      : function (label) {
        this.getStatusMsgPositioner().position(label);
    },
    onStatusMsgResize    : function (label, adjWidth, adjHeight) {
        console.log('adjWidth: ' + adjWidth + ' / adjHeight: ' + adjHeight);
    },
    onStatusMsgBeforeHide: function (label) {
        this.getStatusMsgPositioner().deposition(label);
    },

    onSetLoginPasswordChange    : function (field) {
        var setLoginPasswordCfrm = this.getSetLoginPasswordCfrm(),
            setLoginPasswordSubmit = this.getSetPasswordWindowSubmit();

        if (field.isValid()) {
            if (setLoginPasswordCfrm.isDisabled()) {
                setLoginPasswordCfrm.enable(true);
            }
        } else {
            if (!setLoginPasswordCfrm.isDisabled()) {
                setLoginPasswordCfrm.disable(true);
            }
            if (!setLoginPasswordSubmit.isDisabled()) {
                setLoginPasswordSubmit.disable(true);
            }
        }
    },
    onSetLoginPasswordCfrmChange: function (field) {
        var setLoginPasswordSubmit = this.getSetPasswordWindowSubmit();

        if (field.isValid()) {
            if (setLoginPasswordSubmit.isDisabled()) {
                setLoginPasswordSubmit.enable(true);
            }
        } else {
            if (!setLoginPasswordSubmit.isDisabled()) {
                setLoginPasswordSubmit.disable(true);
            }
        }
    },

    onSetLoginPasswordWindowDestroy: function (field) {
        this.onLogInOut();
    },
    onSetLoginPasswordWindowAbort  : function (button) {
        button.up('window').destroy();
    },
    onSetLoginPasswordWindowEnter  : function (field, keyEvent) {
        if (keyEvent.getKey() === keyEvent.ENTER) {
            this.onSetLoginPasswordWindowSubmit(field);
        }
    },
    onSetLoginPasswordWindowSubmit : function (button) {
        var passwordForm = button.up('form').getForm();

        if (passwordForm.isValid()) {
            var me = this,
                failureTypes = me.getFailureTypes();
            hash = me.getEntryHash();
            passwordForm.submit({
                timeout  : 10,
                method   : 'POST',
                url      : '/admin/passwd/' + hash,
                modal    : true,
                waitTitle: 'Sparar',
                waitMsg  : 'Ditt lösenord sparas . . .',
                success  : function (form, action) {
                    myGlobals.res = action.result;
                    button.up('window').destroy();
                    Ext.Msg.alert('Lösenord sparat', 'Ditt nya lösenord har sparats och du kan nu logga in systemet.', function () {
                        window.location = '/admin';
                    });
                    //var loginBtn = me.getMenuLogInOut();
                    //loginBtn.fireEvent('click', loginBtn);
                },
                failure  : function (form, action) {
                    var failMsg;
                    if (action.status === 403) {
                        failMsg = 'Din engångskod har gått ut. Du kan begära en ny kod via "Jag har glömt lösenordet"-knappen i inloggningsfönstret.';
                    }
                    else {
                        failMsg = failureTypes[action.status];
                    }
                    if (!failMsg) {
                        failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
                    }
                    Ext.Msg.alert('Verifieringen misslyckades', failMsg);
                    return false;
                }
            });
        }
    },

    onShowFocus: function (field) {
        window.setTimeout(function () {
            field.focus(false, true);
        }, 50);
    }
});
