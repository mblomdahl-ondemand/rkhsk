Ext.define('RKHSK.controller.RegCtrlD', {
    extend: 'Ext.app.Controller',

    id: 'RegCtrlD',

    config: {
        //profile: Ext.os.deviceType.toLowerCase(),
        //activePage: 'homePage-intro',
        entryHash   : window.location.hash,
        failureTypes: undefined
    },

    requires: [
        'RKHSK.store.AdmissionSemesterD',
        'RKHSK.store.StreetFloor'
    ],
    refs    : [
        {
            ref       : 'RegPageD',
            selector  : '#regPageD',
            autoCreate: true
        },
        {
            ref       : 'RegStatusWindowD',
            selector  : '#regStatusWindowD',
            autoCreate: true
        },
        {
            ref     : 'RegForm',
            selector: '#regForm'
        },
        {
            ref     : 'StatusForm',
            selector: '#statusForm'
        },
        {
            ref     : 'AdminLink',
            selector: '#adminLink'
        },
        {
            ref     : 'LastApplicationFor',
            selector: '#lastApplicationFor'
        },
        {
            ref     : 'AdmittedSemester',
            selector: '#admittedSemester'
        },
        {
            ref     : 'PersonalId',
            selector: '#personalId'
        },
        {
            ref     : 'FirstName',
            selector: '#firstName'
        },
        {
            ref     : 'LastName',
            selector: '#lastName'
        },
        {
            ref     : 'StreetCo',
            selector: '#streetCo'
        },
        {
            ref     : 'StreetAddress',
            selector: '#streetAddress'
        },
        {
            ref     : 'StreetNumber',
            selector: '#streetNumber'
        },
        {
            ref     : 'StreetEntrance',
            selector: '#streetEntrance'
        },
        {
            ref     : 'StreetApartment',
            selector: '#streetApartment'
        },
        {
            ref     : 'StreetFloor',
            selector: '#streetFloor'
        },
        {
            ref     : 'ZipCode',
            selector: '#zipCode'
        },
        {
            ref     : 'City',
            selector: '#city'
        },
        {
            ref     : 'Country',
            selector: '#country'
        },
        {
            ref     : 'Email',
            selector: '#email'
        },
        {
            ref     : 'Phone',
            selector: '#phone'
        },
        {
            ref     : 'StudentConfirmation',
            selector: '#studentConfirmation'
        },
        {
            ref     : 'ResetRegForm',
            selector: '#resetRegForm'
        },
        {
            ref     : 'SubmitRegForm',
            selector: '#submitRegForm'
        },
        {
            ref     : 'LoginUsername',
            selector: '#loginUsername'
        },
        {
            ref     : 'LoginCredentials',
            selector: '#loginCredentials'
        },
        {
            ref     : 'ResendCredentials',
            selector: '#resendCredentials'
        },
        {
            ref     : 'RetrieveStatus',
            selector: '#retrieveStatus'
        }
    ],

    stores: [
        'AdmissionSemesterD',
        'StreetFloor'
    ],

    views: [
        'RegPageD',
        'RegStatusWindowD'
    ],

    init: function () {

        var me = this,
            failureTypes = {},
            actionClass = Ext.form.Action;
        failureTypes[actionClass.SERVER_INVALID] = 'Ett kommunikationsfel uppstod p.g.a. fel i serverkonfigurationen. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
        failureTypes[actionClass.CLIENT_INVALID] = 'Din användaridentitet kunde inte verifieras. Ladda om sidan och försök pånytt.';
        failureTypes[actionClass.CONNECT_FAILURE] = 'Ett anslutningsfel inträffade. Var god kontrollera att du är ansluten till internet och försök igen.';
        failureTypes[actionClass.LOAD_FAILURE] = 'Information från servern kunde inte laddas, sannolikt till följd av ett konfigurationsfel på serversidan. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
        failureTypes[403] = failureTypes[actionClass.CLIENT_INVALID];
        failureTypes[404] = failureTypes[actionClass.CONNECT_FAILURE];
        failureTypes[400] = failureTypes[actionClass.LOAD_FAILURE];
        failureTypes[0] = failureTypes[actionClass.CONNECT_FAILURE];
        failureTypes[500] = failureTypes[actionClass.SERVER_INVALID];

        this.setFailureTypes(failureTypes);

        // Validator Setup: "name"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                name    : function (val, field) { // vType validation function
                    var name = /^[A-Za-z éüÜåäöÅÄÖ\-]+$/;
                    return name.test(val);
                },
                nameText: 'Namnet innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
                nameMask: /[A-Za-z éüÜåäöÅÄÖ\-]/ // vType mask property
            });
        })();

        // Validator Setup: "streetaddress"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                streetaddress    : function (val, field) { // vType validation function
                    var streetaddress = /^[A-Za-z :åäöÅÄÖ\-]+$/;
                    return streetaddress.test(val);
                },
                streetaddressText: 'Gatuadressen innehåller ogiltiga tecken. Fältet "gatuadress" kan enbart innehålla en begränsad teckenuppsättning, närmare besämt a-ö, bindestreck och blanksteg. För gatunummer och portuppgång, se fälten "gatunummer" och "portnr."', // vType text property
                streetaddressMask: /[A-Za-z :åäöÅÄÖ\-]/ // vType mask property
            });
        })();

        // Validator Setup: "streetnumber"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                streetnumber    : function (val, field) { // vType validation function
                    var streetnumber = /^\d{1,3}$|^\d{1,3}\-\d{1,3}$/;
                    return streetnumber.test(val);
                },
                streetnumberText: 'Gatunumret innehåller ogiltiga tecken eller har en ogiltig längd. Ett gatunummer kan enbart innehålla siffror från 0-9. För portuppgång (exempelvis "B" i "2B") resp. våningsplan, se fälten "portnr." och "trappor".', // vType text property
                streetnumberMask: /[\d\-]/ // vType mask property
            });
        })();

        // Validator Setup: "streetentrance"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                streetentrance    : function (val, field) { // vType validation function
                    var streetentrance = /^[a-z]$/i;
                    return streetentrance.test(val);
                },
                streetentranceText: 'Portuppgången innehåller ogiltiga tecken eller har en ogiltig längd. För våningsplan (exempelvis "N.B." i "Hemgatan 4A, N.B.") se fältet "trappor".', // vType text property
                streetentranceMask: /[a-z]/i // vType mask property
            });
        })();

        // Validator Setup: "streetapartment"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                streetapartment    : function (val, field) { // vType validation function
                    var streetapartment = /^\d+$/;
                    return streetapartment.test(val);
                },
                streetapartmentText: 'Lägenhetsnumret innehåller ogiltiga tecken eller har en ogiltig längd.', // vType text property
                streetapartmentMask: /\d/ // vType mask property
            });
        })();

        // Validator Setup: "zipcode"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                zipcode    : function (val, field) { // vType validation function
                    var zipcode = /^\d{5}$/;
                    return zipcode.test(val);
                },
                zipcodeText: 'Postnumret innehåller ogiltiga tecken eller har en ogiltig längd. Korrekt format är "NNNNN" (exempelvis "11421", utan blanksteg).', // vType text property
                zipcodeMask: /\d/ // vType mask property
            });
        })();

        // Validator Setup: "city"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                city    : function (val, field) { // vType validation function
                     var city = /^[A-Za-z åäöÅÄÖ\-]+$/;
                    return city.test(val);
                },
                cityText: 'Postorten innehåller ogiltiga tecken. Fältet "postort" kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
                cityMask: /[A-Za-z åäöÅÄÖ\-]/ // vType mask property
            });
        })();

        // Validator Setup: "country"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                country    : function (val, field) { // vType validation function
                    var country = /^Sverige$/;
                    return country.test(val);
                },
                countryText: 'RKH-SK:s registreringsformulär har för närvarande enbart stöd för svenska adresser. Det enda giltiga värdet för fältet "land" är därmed "Sverige".', // vType text property
                countryMask: /[Sverige]/i // vType mask property
            });
        })();

        // Validator Setup: "personalid"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                personalid    : function (val, field) { // vType validation function
                    var personalid = /^[4-9][0-9](0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-2])\-[A-Z\d]{4}$/;
                    return personalid.test(val);
                },
                personalidText: 'Ogiltigt personnr. Personnummer måste anges på formen YYMMDD-NNNN. Exempel: "860722-0332" – där "86" markerar år 1986, "07" juli månad, "22" dag 22 i juli månad och "0332" de sista fyra siffrorna i personnumret.', // vType text property
                personalidMask: /[\dA-Z\-]/i // vType mask property
            });
        })();

        // Validator Setup: "paymentkey"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                paymentkey    : function (val, field) { // vType validation function
                    var paymentkey = /^[0-9a-z]{5}$/i;
                    return paymentkey.test(val);
                },
                paymentkeyText: 'Ogiltig personlig kod. Din personliga kod är 5 tecken lång och består av en slumpmässig uppsättning bokstäver och siffror.', // vType text property
                paymentkeyMask: /[\da-z]/i // vType mask property
            });
        })();

        // Validator Setup: "phone"
        (function () {
            Ext.apply(Ext.form.field.VTypes, {
                phone    : function (val, field) { // vType validation function
                    var phone = /^[\+\d][\d \-]{9,18}$/;
                    return phone.test(val);
                },
                phoneText: 'Ogiltigt telefonnr. Fältet "telefonnummer" kan enbart innehålla en begränsad teckenuppsättning, accepterade tecken är + (som första tecken), 0-9, blanksteg och bindestreck. Exempel på giltiga telefonnummer är "+4672-522 32 33" och "08 920 20 25"', // vType text property
                phoneMask: /[\d \+\-]/ // vType mask property
            });
        })();

        Ext.create('RKHSK.view.RegPageD', {
            renderTo: Ext.getBody()
        });

        this.control({
            '#adminLink'          : {
                click: this.onAdminLink
            },
            '#lastApplicationFor' : {
                change: this.onRegFormModified
            },
            '#admittedSemester'   : {
                change: this.onRegFormModified
            },
            '#personalId'         : {
                change: this.onRegFormModified,
                blur  : this.onAssholeIdInput
            },
            '#firstName'          : {
                change: this.onRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#lastName'           : {
                change: this.onRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#streetCo'           : {
                change: this.onRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#streetAddress'      : {
                change: this.onRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#streetNumber'       : {
                change: this.onRegFormModified,
                blur  : this.onTrimmableInput
            },
            '#streetEntrance'     : {
                change: this.onRegFormModified,
                blur  : this.onForceUppercaseEntry
            },
            '#zipCode'            : {
                change: this.onRegFormModified,
                blur  : this.onTrimmableInput
            },
            '#city'               : {
                change: this.onRegFormModified,
                blur  : this.onSloppyUserInput
            },
            '#country'            : {
                change: this.onRegFormModified,
                blur  : this.onForceCountry
            },
            '#email'              : {
                change: this.onRegFormModified,
                blur  : this.onForceLowercaseEntry
            },
            '#phone'              : {
                change: this.onRegFormModified,
                blur  : this.onTrimmableInput
            },
            '#studentConfirmation': {
                change: this.onRegFormModified
            },
            '#resetRegForm'       : {
                click: this.onResetRegForm
            },
            '#submitRegForm'      : {
                click: this.onSubmitRegForm
            },
            '#loginUsername'      : {
                change: this.onStatusFormUsernameModified,
                blur  : this.onForceLowercaseEntry
            },
            '#loginCredentials'   : {
                change: this.onStatusFormCredentialsModified,
                blur  : this.onForceUppercaseEntry
            },
            '#resendCredentials'  : {
                click: this.onResendCredentials
            },
            '#retrieveStatus'     : {
                click: this.onRetrieveStatus
            }
        });
        myGlobals.controller = this;

        var hash = window.location.hash;
        if (hash) {
            hash = hash.substring(1);
        }

        if (hash.length >= 24) {
            var //loadingMsg = new Ext.LoadMask(me.getRegPageD(), { msg: 'Laddar . . .' }),
                loadUrl = '/account/load/' + hash;
            //loadingMsg.show()
            // push ajax request to server
            if (hash.length === 30) { // regform
                var regForm = me.getRegForm().getForm();
                regForm.load({
                    timeout  : 6,
                    waitTitle: 'Laddar . . .',
                    waitMsg  : 'Läser in formdata',
                    url      : loadUrl,
                    success  : function (form, action) {
                        if (action.result.data.email) {
                            me.getLoginUsername().setValue(action.result.data.email);
                        }
                    },
                    failure  : function (form, action) {
                        var failMsg = failureTypes[action.status];
                        if (!failMsg) {
                            failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
                        }
                        Ext.Msg.alert('Inläsningen misslyckades', failMsg);
                        return false;
                    }
                });
            }
            else if (hash.length === 26) { // accountview
                var statusForm = me.getStatusForm().getForm(),
                    submitBtn = me.getRetrieveStatus();
                statusForm.load({
                    timeout  : 6,
                    waitTitle: 'Laddar . . .',
                    waitMsg  : 'Läser in medlemsuppgifter',
                    url      : loadUrl,
                    success  : function (form, action) {
                        if (action.result.data.loginUsername && action.result.data.loginCredentials) {
                            Ext.getCmp('interfaceWrapper').setActiveTab(1);
                            submitBtn.fireEvent('click', submitBtn);
                        }
                    },
                    failure  : function (form, action) {
                        var failMsg;
                        if (action.status === 403) {
                            failMsg = 'Din engångskod har gått ut, för att begära en ny kod klickar du på "Jag har glömt min personliga kod".';
                        }
                        else {
                            failMsg = failureTypes[action.status];
                        }
                        if (!failMsg) {
                            failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
                        }
                        Ext.Msg.alert('Inläsningen misslyckades', failMsg);
                        return false;
                    }
                });
            }
        }
    },

    launch                         : function () {
    },
    onAdminLink                    : function () {
        window.location.href = '/admin';
    },
    onAssholeIdInput               : function (field) {
        if (field.isValid()) {
            field.setValue(field.getValue().toUpperCase());
            return true;
        }

        var input = Ext.String.trim(field.getValue()).toUpperCase();

        // test for 19832010-0221
        if (input.length === 13) {
            if (input.search(/19\d{6}-[\dA-Z]{4}/) !== -1) {
                field.setValue(input.substring(2));
                return true;
            } else {
                return false;
            }
        }

        // test for 198320100221
        if (input.length === 12) {
            if (input.search(/19\d{6}[\dA-Z]{4}/) !== -1) {
                field.setValue(input.substring(2, 8) + '-' + input.substring(8));
                return true;
            } else {
                return false;
            }
        }

        // test for 8320100221
        if (input.length === 10) {
            if (input.search(/[2-9][0-9]\d{4}[\dA-Z]{4}/) !== -1) {
                field.setValue(input.substring(0, 6) + '-' + input.substring(6));
                return true;
            } else {
                return false;
            }
        }

        field.setValue(input);
    },
    onSloppyUserInput              : function (field) {
        var input = Ext.String.trim(typeof field === 'string' ? field : field.getValue()).toLowerCase(),
            validLowerCaseWords = ['Von', 'Van', 'Väg', 'Gata'],
            validUpperCaseWords = ['Ii', 'Iii', 'Iv', 'Vi'];
        while (input.indexOf('  ') !== -1) {
            input = input.replace('  ', ' ')
        }
        input = input.replace(' -', '-').replace('- ', '-');
        while (input.indexOf('--') !== -1) {
            input = input.replace('--', '-')
        }
        input = input.split(' ');
        for (var tmp, i = 0, j = input.length;
             i < j;
             i++) {

            input[i] = Ext.String.capitalize(input[i]);
            if (input[i].split('-').length > 1) {
                tmp = input[i].split('-');
                for (var k = 0, l = tmp.length;
                     k < l;
                     k++) {
                    tmp[k] = Ext.String.capitalize(tmp[k]);
                }
                input[i] = tmp.join('-');
            } else {
                if (input[i].split(':').length > 1) { /// 'III:s gata'
                    tmp = input[i].split(':');
                    if (validUpperCaseWords.indexOf(tmp[0]) !== -1) {
                        tmp[0] = tmp[0].toUpperCase();
                        input[i] = tmp.join(':');
                    }
                } else {
                    if (validLowerCaseWords.indexOf(input[i]) !== -1) {
                        input[i] = input[i].toLowerCase();
                    } else {
                        for (var k = validUpperCaseWords.length;
                             k--;) {
                            if (input[i] === validUpperCaseWords[i] + 's') {
                                input[i] = validUpperCaseWords[i].toUpperCase() + 's';
                                break;
                            }
                        }
                    }
                }
            }
        }

        input = input.join(' ');
        if (typeof field === 'string') {
            return input;
        }
        else {
            field.setValue(input);
        }
    },
    onTrimmableInput               : function (field) {
        var input = Ext.String.trim(field.getValue());
        field.setValue(input);
    },
    onRegFormModified              : function () {
        var regForm = this.getRegForm(),
            inputFields = regForm.getValues(),
            submitRegForm = this.getSubmitRegForm();
        myGlobals.regForm = regForm;
        if (!inputFields['city'] || !inputFields['email'] || !inputFields['zipCode'] || !inputFields['streetAddress'] || !inputFields['streetNumber'] || !inputFields['studentConfirmation'] || !inputFields['firstName'] || !inputFields['lastName'] || !inputFields['lastApplicationFor'] || !inputFields['admittedSemester'] || !inputFields['personalId'] || !inputFields['country']) {
            if (!submitRegForm.isDisabled()) {
                submitRegForm.disable(true);
            }
        } else if (regForm.getForm().isValid()) {
            if (submitRegForm.isDisabled()) {
                submitRegForm.enable(true);
            }
        } else {
            if (!submitRegForm.isDisabled()) {
                submitRegForm.disable(true);
            }
        }
    },
    onForceCountry                 : function (field) {
        if (!field.isValid()) {
            field.setValue(field.defaultValue);
        }
    },
    onForceUppercaseEntry          : function (field) {
        var input = Ext.String.trim(field.getValue()).toUpperCase();
        field.setValue(input);
    },
    onForceLowercaseEntry          : function (field) {
        var input = Ext.String.trim(field.getValue()).toLowerCase();
        field.setValue(input);
    },
    onResetRegForm                 : function (button) {
        button.up('form').getForm().reset();
    },
    onSubmitRegForm                : function (button) {
        var form = button.up('form').getForm(),
            failureTypes = this.getFailureTypes();

        if (form.isValid()) {
            form.submit({
                url      : '/account/register/',
                reset    : true,
                type     : 'submit',
                waitTitle: 'Registreringsformuläret skickas',
                timeout  : 10,
                waitMsg  : 'Din registreringsinformation överförs ...',
                success  : function (form, action) {
                    myGlobals.successAction = action;
                    Ext.Msg.alert('Medlemsregistrering sparad', 'Du är nu registrerad i RKH-SK:s medlemsdatabas och kan direkt betala in medlemsavgiften via PlusGiro eller studentkårens expedition. Se e-post för den personliga kod som måste bifogas inbetalningen.');
                },
                failure  : function (form, action) {
                    //myGlobals.failureAction = action;
                    var failMsg = failureTypes[action.status];
                    if (!failMsg) {
                        failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
                    }
                    Ext.Msg.alert('Registreringen misslyckades', failMsg);
                    return false;
                }
            });
        }
    },
    onStatusFormUsernameModified   : function (field) {
        var statusForm = field.up('form').getForm(),
            resendCredentials = this.getResendCredentials(),
            retrieveStatus = this.getRetrieveStatus();

        if (field.isValid()) {
            if (resendCredentials.isDisabled()) {
                resendCredentials.enable(true);
            }
            if (statusForm.isValid()) {
                if (retrieveStatus.isDisabled()) {
                    retrieveStatus.enable(true);
                }
            } else if (!retrieveStatus.isDisabled()) {
                retrieveStatus.disable(true);
            }
        } else {
            if (!resendCredentials.isDisabled()) {
                resendCredentials.disable(true);
            }
            if (!retrieveStatus.isDisabled()) {
                retrieveStatus.disable(true);
            }
        }

    },
    onStatusFormCredentialsModified: function (field) {
        var statusForm = field.up('form').getForm(),
            retrieveStatus = this.getRetrieveStatus();

        if (statusForm.isValid()) {
            if (retrieveStatus.isDisabled()) {
                retrieveStatus.enable(true);
            }
        } else {
            if (!retrieveStatus.isDisabled()) {
                retrieveStatus.disable(true);
            }
        }
    },
    onResendCredentials            : function () {
        var me = this,
            loginUsername = me.getLoginUsername(),
            failureTypes = me.getFailureTypes();

        if (!loginUsername.isValid()) {
            return false;
        }

        loadingMsg = new Ext.LoadMask(me.getRegPageD(), { msg: 'Mailar inloggningsuppgifter . . .' });
        loadingMsg.show();

        // push ajax request to server
        Ext.Ajax.request({
            method  : 'POST',
            url     : '/account/resend/',
            jsonData: {
                loginUsername: loginUsername.getValue()
            },
            timeout : 6e3,
            success : function (response) {
//                loadingMsg.hide();
                Ext.Msg.alert('Bekräftelse', 'Dina medlems- och inloggningsuppgifter har mailats till din e-postadress. Om du inte mottagit meddelandet inom någon minut, var vänlig och kontrollera din "spam"- eller "skräppost"-mapp innan du tar kontakt med studentkårens support.');
            },
            failure : function (response) {
                loadingMsg.hide();
                var failMsg = failureTypes[response.status];
                if (!failMsg || response.status === 400) {
                    Ext.Msg.alert('Felmeddelande', 'Dina uppgifter kunde inte mailas. Säkerställ att e-postadressen du uppgett är densamma som du använde vid registreringstillfället.');
                }
                else {
                    Ext.Msg.alert('Kommunikationsfel', failMsg);
                }
            }
        });
    },
    onRetrieveStatus               : function (button) {
        var me = this,
            statusForm = button.up('form').getForm(),
            failureTypes = this.getFailureTypes();
        myGlobals.statusForm = statusForm;
        if (statusForm.isValid()) {
            //var values = statusForm.getValues(),
            //    loadingMsg = new Ext.LoadMask(me.getRegPageD(), { title: 'Hämtar ärendestatus', msg: '' });

            //loadingMsg.show()

            statusForm.submit({
                url      : '/account/retrieve/',
                timeout  : 10,
                reset    : true,
                waitTitle: 'Hämtar ärendestatus',
                waitMsg  : 'Läser in medlemsdata och ärendestatus . . .',
                success  : function (form, action) {
                    myGlobals.successAction = action;
                    //var data = Ext.decode(response.responseText).memberData;

                    //loadingMsg.hide();
                    myGlobals.win = Ext.create('RKHSK.view.RegStatusWindowD', {
                        data    : action.result.data,
                        renderTo: Ext.getBody()
                    });
                    myGlobals.win.show();

                },
                failure  : function (form, action) {
                    myGlobals.failureAction = action;
                    //loadingMsg.hide();
                    if (action.response.status === 400) {
                        Ext.Msg.alert('Medlemsdata/ärendestatus', 'Dina medlemsuppgifter kunde inte läsas in. Var vänlig och kontrollera att du har angett rätt e-postadress och en giltig personlig kod.');
                    }
                    else if (failureTypes[action.failureType]) {
                        Ext.Msg.alert('Kommunikationsfel', failureTypes[action.failureType]);
                    }
                }
            });
        }
    }

});
