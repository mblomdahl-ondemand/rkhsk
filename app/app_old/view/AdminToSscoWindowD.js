Ext.define('RKHSK.view.AdminToSscoWindowD', {
    extend     : 'Ext.window.Window',
    id         : 'adminToSscoWindowD',
    autoScroll : true,
    autoDestroy: false,
    config     : {
        width          : 720,
        height         : 500,
        memberCount    : undefined,
        sscoUsername   : undefined,
        sscoPassword   : undefined,
        fileDownloadUrl: undefined
    },
    constructor: function (config) {
        this.initConfig(config);

        myGlobals.win = this;
        //if (this.up().contentCategory == 'datafile') {
        //  console.log('datafile');
        //}
        //this.value = infoRow1['datafile'];
        //this.items[1].value = infoRow2[this.contentCategoy];

        this.callParent();
        return this;
    },
    listeners  : {
        beforeshow: function () {
            //Ext.getCmp('sscoInstruction1')[0].setData({ sscoUsername: 'foobar', sscoPassword: 'barfoo' });
            //Ext.getCmp('sscoInstruction4')[0].setData({ memberCount: '3' });
            return true;
        }
    },
    modal      : true,
    layout     : {
        type : 'vbox',
        align: 'stretch'
    },
    title      : 'Överför medlemsuppgifter till SSSB',
    items      : [
        {
            xtype : 'tabpanel',
            frame : true,
            id    : 'sscoInstructionsPanel',
            margin: '0 0 0 0',
            height: 90,
            //layout: 'hbox',
            items : [
                {
                    title      : 'Steg 1: Inloggning',
                    xtype      : 'panel',
                    id         : 'sscoInstruction1',
                    frame      : true,
                    bodyPadding: 10,
                    style      : 'text-align: center;',
                    html       : '<p><u>Steg 1</u>: Logga in med användarnamn "<span id="instruction1Username"></span>" och lösenord "<span id="instruction1Password"></span>".</p>',
                    listeners  : {
                        show  : function () {
                            //Ext.getCmp('sscoInstruction1')[0]
                            Ext.query('#instruction1Username', this.dom)[0].innerText = this.up('window').sscoUsername;
                            Ext.query('#instruction1Password', this.dom)[0].innerText = this.up('window').sscoPassword;
                        },
                        render: function () {
                            //Ext.getCmp('sscoInstruction1')[0]
                            Ext.query('#instruction1Username', this.dom)[0].innerText = this.up('window').sscoUsername;
                            Ext.query('#instruction1Password', this.dom)[0].innerText = this.up('window').sscoPassword;
                        }
                    }
                },
                {
                    title      : 'Steg 2: Ladda upp datafil',
                    xtype      : 'panel',
                    id         : 'sscoInstruction2',
                    frame      : true,
                    bodyPadding: 10,
                    style      : 'text-align: center;',
                    html       : '<p><u>Steg 2</u>: Ladda ner datafilen till SSSB via <a href="#" id="instruction2DownloadUrl" target="_blank">den här</a> länken. Ladda sedan upp samma fil till SSSB genom att välja <i>Choose File</i> och därefter <i>Importera</i>.</p>',
                    listeners  : {
                        show: function () {
                            Ext.query('#instruction2DownloadUrl', this.dom)[0].href = this.up('window').fileDownloadUrl;
                            //myGlobals.inst4 = this;
                            //memberCount = '3';
                        }
                    }
                },
                {
                    title      : 'Steg 3: Bekräfta uppladdning',
                    xtype      : 'panel',
                    id         : 'sscoInstruction3',
                    frame      : true,
                    bodyPadding: 10,
                    style      : 'text-align: center;',
                    html       : '<p><u>Steg 3</u>: För att slutföra överföringen och uppdatera de <span id="instruction3MemberCount">?</span> medlemmarna i RKH-SK:s databas, klicka på <i>Bekräfta genomförd överföring till SSSB</i>. Meddelandet "CSV filuppladdningen kommer att pågå i bakgrunden" kan ignoreras.</p>',
                    listeners  : {
                        show: function () {
                            Ext.query('#instruction3MemberCount', this.dom)[0].innerText = this.up('window').memberCount;
                            //myGlobals.inst4 = this;
                            //memberCount = '3';
                        }
                    }
                }
            ]
        },
        {
            xtype : 'container',
            frame : true,
            id    : 'sscoIframe',
            margin: '0 0 0 0',
            html  : '<iframe src="http://ssco.heroku.com/users/sign_in?unauthenticated=true"></iframe>',
            flex  : 1
        }
    ],
    dockedItems: [
        {
            xtype      : 'panel',
            frame      : true,
            height     : 31,
            layout     : {
                type: 'anchor'
            },
            dock       : 'bottom',
            dockedItems: [
                {
                    xtype : 'button',
                    margin: '0 0 0 5',
                    id    : 'transferToSscoAbort',
                    text  : 'Avbryt överföring till SSSB',
                    dock  : 'right'
                },
                {
                    xtype   : 'button',
                    margin  : '0 0 0 5',
                    id      : 'transferToSscoSubmit',
                    disabled: true,
                    text    : 'Bekräfta genomförd överföring till SSSB',
                    dock    : 'right',
                    weight  : 1
                }
            ]
        }
    ]
});
