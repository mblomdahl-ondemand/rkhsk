Ext.define('RKHSK.view.AdminSetPasswordWindowM', {
    extend: 'Ext.MessageBox',
    id    : 'adminSetPasswordWindowM',
    xtype : 'setpasswordmessage',
    /*initialize: function() {
     console.log('win-initz');
     },*/
    config: {
        //height: 340,
        //width: 300,
        listeners       : {
            updatedata: function () {
                this.setHtml(this.getTpl().apply(this.getData()));
            }
        },
        styleHtmlContent: true,
        cls             : 'setpassword-message-box',
        data            : { },
        scrollable      : 'vertical',
        centered        : true,
        tpl             : [
            '<div><p>Personnummer:</p><p>{personalId}</p></div>',
            '<div><p>Efter- och förnamn:</p><p>{lastName}, {firstName}</p></div>',
            '<div><p>Senast anmäld till:</p><p>{lastApplicationFor}</p></div>',
            '<div><p>Senaste inbetalning:</p><p>{lastPaymentRecieved}</p></div>',
            '<div><p>SSSB-registreringar:</p><p>{sscoHistory}</p></div>',
            '<div><p>Mecenat-registreringar:</p><p>{mecenatHistory}</p></div>',
            '<div><p>Medlemskap i RKH-SK:</p><p>{membershipHistory}</p></div>',
            '<div><p>Anmälningshistorik:</p><p>{applicationSemesterHistory}</p></div>',
            '<div><p>Postadress:</p><p>{streetAddress}</p></div>',
            '<div><p>Email-adress:</p><p>{email}</p></div>',
            '<div><p>Telefonnummer:</p><p>{phone}</p></div>',
            '<div><p>Årskurs:</p><p>{admittedSemester}</p></div>'
        ],
        style           : 'background-color: #EEE;',
        stretchX        : true,
        stretchY        : true,
        margin          : 10,
        title           : 'Medlemsdata/ärendestatus',
        buttons         : [
            {
                xtype: 'button',
                text : 'Avbryt',
                width: '25%'
            },
            {
                xtype: 'button',
                text : 'Spara',
                width: '25%'
            }
        ]
    }
    /*
     dockedItems: [
     {
     xtype: 'panel',
     frame: true,
     height: 31,
     layout: {
     type: 'anchor'
     },
     dock: 'bottom',
     dockedItems: [
     {
     xtype: 'button',
     id: 'confirmPaymentsSubmit',
     text: 'OK',
     dock: 'right',
     weight: 1
     }
     ]
     }
     ]*/
});
