Ext.define('RKHSK.view.cmp.CityFieldM', {
    //extend: 'Ext.viewport.Viewport',
    extend    : 'Ext.field.Text',
    xtype     : 'cityfield',
    initialize: function () {
        var me = this;

        me.setMaxLength(30);
        me.callParent();
        me.getComponent().element.dom.children[0].addEventListener('keypress', function (e) {
            var charCode = e.charCode || e.keyCode,
                input = String.fromCharCode(charCode),
                maskRe = /[a-zA-Z åäöÅÄÖ\-]/;

            if (!maskRe.test(input)) {
                e.stopPropagation();
                e.preventDefault();
            }
        }, false);
    },
    isValid   : function () {
        //console.error(this.getValue());
        return true;
    }
});
