Ext.define('RKHSK.view.cmp.StreetNumberFieldM', {
    //extend: 'Ext.viewport.Viewport',
    extend    : 'Ext.field.Text',
    xtype     : 'streetnumberfield',
    initialize: function () {
        var me = this;

        me.setMaxLength(7);
        me.callParent();
        me.getComponent().element.dom.children[0].addEventListener('keypress', function (e) {
            var value = this.value,
                index = this.value.length,
                charCode = e.charCode || e.keyCode,
                input = String.fromCharCode(charCode),
                maskRe = [
                    /\d/,
                    /[\d\-]/,
                    /\d/
                ];

            function stopEvent() {
                e.stopPropagation();
                e.preventDefault();
            }

            if (!index) {
                if (!maskRe[0].test(input)) {
                    stopEvent();
                }
            } else if (value.indexOf('-') === -1) {
                if (!maskRe[1].test(input)) {
                    stopEvent();
                }
            } else {
                if (!maskRe[2].test(input)) {
                    stopEvent();
                }
            }
        }, false);
    },
    isValid   : function () {
        //console.error(this.getValue());
        return true;
    }
});
