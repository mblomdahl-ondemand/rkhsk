Ext.define('RKHSK.view.AdminSetPasswordWindowD', {
    extend      : 'Ext.window.Window',
    //requires: {
    //  'Ext.form.field.Hidden'
    //},
    height      : 170,
    config      : {
        loginUsername: undefined
    },
    id          : 'adminSetPasswordWindowD',
    width       : 340,
    autoScroll  : false,
    modal       : true,
    layout      : {
        align: 'stretch',
        type : 'vbox'
    },
    listeners   : {
        render: function () {
            this.down('#setLoginUsernameDisplay').setValue(this.loginUsername);
            this.down('#setLoginUsernameField').setValue(this.loginUsername);
        },
        show  : function () {
            this.down('#setLoginUsernameDisplay').setValue(this.loginUsername);
            this.down('#setLoginUsernameField').setValue(this.loginUsername);
        }
    },
    autoDestroy : true,
    defaultFocus: 'setLoginUsername',
    title       : 'Välj ett administratörslösenord',
    items       : [
        {
            xtype      : 'form',
            frame      : true,
            height     : 137,
            bodyPadding: 5,
            anchor     : '100%',
            flex       : 1,
            items      : [
                {
                    xtype     : 'displayfield',
                    labelWidth: 130,
                    id        : 'setLoginUsernameDisplay',
                    fieldLabel: 'Användarnamn/e-post',
                    value     : '',
                    anchor    : '100%',
                    margin    : '0 0 10 0'
                },
                {
                    xtype: 'hidden',
                    id   : 'setLoginUsernameField',
                    name : 'loginUsername',
                    value: ''
                },
                {
                    xtype           : 'textfield',
                    id              : 'setLoginPassword',
                    inputType       : 'password',
                    allowBlank      : false,
                    maxLength       : 18,
                    enforceMaxLength: true,
                    name            : 'loginPassword',
                    vtype           : 'password',
                    fieldLabel      : 'Lösenord',
                    labelWidth      : 130,
                    anchor          : '100%'
                },
                {
                    xtype           : 'textfield',
                    id              : 'setLoginPasswordCfrm',
                    inputType       : 'password',
                    allowBlank      : false,
                    maxLength       : 18,
                    enforceMaxLength: true,
                    name            : 'cfrmLoginPassword',
                    fieldLabel      : 'Bekräfta lösenordet',
                    vtype           : 'setpasswordcfrm',
                    disabled        : true,
                    labelWidth      : 130,
                    anchor          : '100%'
                }
            ],
            dockedItems: [
                {
                    xtype : 'panel',
                    frame : true,
                    height: 31,
                    layout: {
                        align: 'stretch',
                        type : 'hbox'
                    },
                    dock  : 'bottom',
                    items : [
                        {
                            xtype : 'button',
                            margin: '0 5 0 0',
                            text  : 'Avbryt',
                            flex  : 1,
                            id    : 'setPasswordWindowAbort'
                        },
                        {
                            xtype   : 'button',
                            text    : 'Spara nytt lösenord',
                            flex    : 2,
                            disabled: true,
                            id      : 'setPasswordWindowSubmit'
                        }
                    ]
                }
            ]
        }
    ]
});
