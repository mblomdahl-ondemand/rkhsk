Ext.define('RKHSK.view.AdminConfirmPaymentsD', {
    extend     : 'Ext.window.Window',
    height     : 500,
    id         : 'adminConfirmPaymentsD',
    minHeight  : 500,
    width      : 720,
    autoScroll : true,
    config     : {
        contentCategory: 'datafile',
        infoRow1       : {
            datafile: 'Med "Registrera verifierade inbetalningar" registreras studenterna listade under "Verifierade inbetalningar" som medlemmar i studentkåren för VT12. Medlemmarnas uppgifter synkas automagiskt mot Mecenats och SSSB:s databaser. Algoritmen som analyserar kontoutdraget från internetbanken är på intet sätt "intelligent" – om du är osäker på någon post, kontrollera och justera utdraget innan du går vidare.',
            datalist: 'Med "Registrera verifierade inbetalningar" registreras studenterna listade under "Verifierade inbetalningar" som medlemmar i studentkåren för HT12. Medlemmarnas uppgifter synkas automagiskt mot Mecenats och SSSB:s databaser så var noga med att fylla i rätt koder vid manuell inmatning!'
        },
        infoRow2       : {
            datafile: 'Eventuella poster under "Oidentifierade inbetalningar" kräver manuell bearbetning och kommer tills vidare att ignoreras av systemet. Ändra uppenbara fel och/eller ta kontakt med avsändaren "när stjärnorna står rätt".',
            datalist: 'Eventuella poster under "Oidentifierade inbetalningar" är orsakade av att systemet inte funnit någon medlemsanmälan som matchar angivna inbetalningskoder. Dubbelkolla att inga slarvfel har smygit sig in i din lista.'
        }
    },
    constructor: function (config) {
        this.initConfig(config);

        myGlobals.win = this;
        //if (this.up().contentCategory == 'datafile') {
        //  console.log('datafile');
        //}
        //this.value = infoRow1['datafile'];
        //this.items[1].value = infoRow2[this.contentCategoy];

        this.callParent();
        return this;
    },
    listeners  : {
        beforeshow: function () {

            Ext.getCmp('confirmInfoRow1').setValue(this.infoRow1[this.contentCategory]);
            Ext.getCmp('confirmInfoRow2').setValue(this.infoRow2[this.contentCategory]);

        }
    },
    modal      : true,
    layout     : {
        type: 'anchor'
    },
    title      : 'Bekräfta registrering',
    items      : [
        {
            xtype     : 'displayfield',
            frame     : true,
            id        : 'confirmInfoRow1',
            margin    : '3 10 3 5',
            value     : '',
            fieldLabel: 'Information',
            labelWidth: 80,
            anchor    : '100%'
        },
        {
            xtype         : 'displayfield',
            frame         : true,
            id            : 'confirmInfoRow2',
            margin        : '0 10 7 5',
            value         : '',
            fieldLabel    : ' ',
            labelSeparator: ' ',
            labelWidth    : 80,
            anchor        : '100%'
        },
        {
            xtype     : 'gridpanel',
            frame     : true,
            id        : 'recognizedPaymentsGrid',
            title     : 'Verifierade inbetalningar',
            store     : 'RecognizedMembersList',
            anchor    : '100%',
            columns   : [
                {
                    xtype    : 'datecolumn',
                    id       : 'verifyLastPaymentRecieved',
                    width    : 90,
                    dataIndex: 'lastPaymentRecieved',
                    text     : 'Avg. inbetald',
                    format   : 'Y-m-d',
                    hideable : false
                },
                {
                    xtype       : 'gridcolumn',
                    id          : 'verifyPayment',
                    width       : 75,
                    dataIndex   : 'payment',
                    text        : 'Inbetalning',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                },
                {
                    xtype       : 'gridcolumn',
                    id          : 'verifyPersonalId',
                    width       : 90,
                    dataIndex   : 'personalId',
                    text        : 'Personnr',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                },
                {
                    xtype       : 'gridcolumn',
                    id          : 'verifyFirstName',
                    dataIndex   : 'firstName',
                    flex        : 1,
                    text        : 'Förnamn',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                },
                {
                    xtype       : 'gridcolumn',
                    id          : 'verifyLastName',
                    dataIndex   : 'lastName',
                    flex        : 1,
                    text        : 'Efternamn',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                },
                {
                    xtype       : 'gridcolumn',
                    id          : 'verifyLastPaymentKey',
                    width       : 85,
                    dataIndex   : 'lastPaymentKey',
                    text        : 'Personlig kod',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                },
                {
                    xtype       : 'datecolumn',
                    id          : 'verifyLastApplicationDate',
                    width       : 80,
                    dataIndex   : 'lastApplicationDate',
                    text        : 'Anm.datum',
                    format      : 'Y-m-d',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                },
                {
                    xtype       : 'gridcolumn',
                    id          : 'verifyLastApplicationFor',
                    width       : 80,
                    dataIndex   : 'lastApplicationFor',
                    text        : 'Anmäld till',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                }
            ],
            viewConfig: {

            }
        },
        {
            xtype     : 'gridpanel',
            frame     : true,
            id        : 'unidentifiedPaymentsGrid',
            title     : 'Oidentifierade inbetalningar',
            store     : 'UnidentifiedMembersList',
            anchor    : '100%',
            columns   : [
                {
                    xtype    : 'datecolumn',
                    id       : 'dirtyLastPaymentRecieved',
                    width    : 110,
                    dataIndex: 'lastPaymentRecieved',
                    text     : 'Betalningsdatum',
                    format   : 'Y-m-d',
                    hideable : false
                },
                {
                    xtype       : 'gridcolumn',
                    id          : 'dirtyPayment',
                    width       : 75,
                    dataIndex   : 'payment',
                    text        : 'Inbetalning',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                },
                {
                    xtype       : 'gridcolumn',
                    id          : 'dirtyLastPaymentKey',
                    width       : 85,
                    dataIndex   : 'lastPaymentKey',
                    text        : 'Personlig kod',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                },
                {
                    xtype       : 'gridcolumn',
                    id          : 'dirtyMessage',
                    dataIndex   : 'message',
                    flex        : 1,
                    text        : 'Meddelande',
                    hideable    : false,
                    sortable    : false,
                    menuDisabled: true
                }
            ],
            viewConfig: {

            }
        }
    ],
    dockedItems: [
        {
            xtype      : 'panel',
            frame      : true,
            height     : 31,
            layout     : {
                type: 'anchor'
            },
            dock       : 'bottom',
            dockedItems: [
                {
                    xtype : 'button',
                    margin: '0 5 0 5',
                    id    : 'confirmPaymentsAbort',
                    text  : 'Avbryt registrering',
                    dock  : 'right'
                },
                {
                    xtype : 'button',
                    id    : 'confirmPaymentsSubmit',
                    text  : 'Registrera verifierade inbetalningar',
                    dock  : 'right',
                    weight: 1
                }
            ]
        }
    ]
});
