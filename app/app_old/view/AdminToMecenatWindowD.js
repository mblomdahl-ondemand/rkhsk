Ext.define('RKHSK.view.AdminToMecenatWindowD', {
    extend     : 'Ext.window.Window',
    id         : 'adminToMecenatWindowD',
    autoScroll : true,
    autoDestroy: false,
    config     : {
        width          : 720,
        height         : 500,
        memberCount    : undefined,
        mecenatUsername: undefined,
        mecenatPassword: undefined,
        fileDownloadUrl: undefined
    },
    constructor: function (config) {
        this.initConfig(config);

        myGlobals.win = this;
        //if (this.up().contentCategory == 'datafile') {
        //  console.log('datafile');
        //}
        //this.value = infoRow1['datafile'];
        //this.items[1].value = infoRow2[this.contentCategoy];

        this.callParent();
        return this;
    },
    listeners  : {
        beforeshow: function () {
            //Ext.getCmp('mecenatInstruction1')[0].setData({ mecenatUsername: 'foobar', mecenatPassword: 'barfoo' });
            //Ext.getCmp('mecenatInstruction4')[0].setData({ memberCount: '3' });
            return true;
        }
    },
    modal      : true,
    layout     : {
        type : 'vbox',
        align: 'stretch'
    },
    title      : 'Överför medlemsuppgifter till Mecenat',
    items      : [
        {
            xtype : 'tabpanel',
            frame : true,
            id    : 'mecenatInstructionsPanel',
            margin: '0 0 0 0',
            height: 100,
            //layout: 'hbox',
            items : [
                {
                    title      : 'Steg 1: Inloggning',
                    xtype      : 'panel',
                    id         : 'mecenatInstruction1',
                    frame      : true,
                    bodyPadding: 10,
                    style      : 'text-align: center;',
                    html       : '<p><u>Steg 1</u>: Logga in med användarnamn "<span id="instruction1Username"></span>" och lösenord "<span id="instruction1Password"></span>".</p>',
                    listeners  : {
                        show  : function () {
                            //Ext.getCmp('mecenatInstruction1')[0]
                            Ext.query('#instruction1Username', this.dom)[0].innerText = this.up('window').mecenatUsername;
                            Ext.query('#instruction1Password', this.dom)[0].innerText = this.up('window').mecenatPassword;
                        },
                        render: function () {
                            //Ext.getCmp('mecenatInstruction1')[0]
                            Ext.query('#instruction1Username', this.dom)[0].innerText = this.up('window').mecenatUsername;
                            Ext.query('#instruction1Password', this.dom)[0].innerText = this.up('window').mecenatPassword;
                        }
                    }
                },
                {
                    title      : 'Steg 2: Fortsätt till sidan "Filer"',
                    xtype      : 'panel',
                    id         : 'mecenatInstruction2',
                    frame      : true,
                    bodyPadding: 10,
                    style      : 'text-align: center;',
                    html       : '<p><u>Steg 2</u>: Fortsätt till sidan <i>Filer</i> och scrolla ner till formuläret <i>Fil-uppladdning</i>.</p>'
                },
                {
                    title      : 'Steg 3: Förhandsgranska fil-uppladdning',
                    xtype      : 'panel',
                    id         : 'mecenatInstruction3',
                    frame      : true,
                    bodyPadding: 10,
                    style      : 'text-align: center;',
                    html       : '<p><u>Steg 3</u>: Fortsätt till sidan <i>Filer</i> och scrolla ner till formuläret <i>Fil-uppladdning</i>. Fyll i formuläret med dina uppgifter. <i>Filtyp</i> måste anges som "Studentfil (kompletta personuppgifter)" och checkboxen <i>Villkor</i> ska vara ikryssad. Datafilen som ska laddas upp finner du <a href="#" id="instruction3DownloadUrl" target="_blank">här</a>. När dessa moment är genomförda går du vidare till <i>Förhandsgranska</i>.</p>',
                    listeners  : {
                        show: function () {
                            Ext.query('#instruction3DownloadUrl', this.dom)[0].href = this.up('window').fileDownloadUrl;
                            //myGlobals.inst4 = this;
                            //memberCount = '3';
                        }
                    }
                },
                {
                    title      : 'Steg 4: Bekräfta uppladdning',
                    xtype      : 'panel',
                    id         : 'mecenatInstruction4',
                    frame      : true,
                    bodyPadding: 10,
                    style      : 'text-align: center;',
                    html       : '<p><u>Steg 4</u>: Kontrollera att förhandsvisningen listar <span id="instruction4MemberCount">X</span> medlemmar och välj sedan <i>Ladda upp</i>. För att slutföra överföringen och uppdatera RKH-SK:s medlemsdatabas, klicka på <i>Bekräfta genomförd överföring till Mecenat</i>.</p>',
                    listeners  : {
                        show: function () {
                            Ext.query('#instruction4MemberCount', this.dom)[0].innerText = this.up('window').memberCount;
                            //myGlobals.inst4 = this;
                            //memberCount = '3';
                        }
                    }
                }
            ]
        },
        {
            xtype : 'container',
            frame : true,
            id    : 'mecenatIframe',
            margin: '0 0 0 0',
            html  : '<iframe src="https://www.mecenat.se/student-admin/SignIn.aspx?ReturnUrl=%2fstudent-admin%2f"></iframe>',
            flex  : 1
        }
    ],
    dockedItems: [
        {
            xtype      : 'panel',
            frame      : true,
            height     : 31,
            layout     : {
                type: 'anchor'
            },
            dock       : 'bottom',
            dockedItems: [
                {
                    xtype : 'button',
                    margin: '0 0 0 5',
                    id    : 'transferToMecenatAbort',
                    text  : 'Avbryt överföring till Mecenat',
                    dock  : 'right'
                },
                {
                    xtype   : 'button',
                    margin  : '0 0 0 5',
                    id      : 'transferToMecenatSubmit',
                    disabled: true,
                    text    : 'Bekräfta genomförd överföring till Mecenat',
                    dock    : 'right',
                    weight  : 1
                }
            ]
        }
    ]
});
