Ext.define('RKHSK.store.MemberList', {
    extend : 'Ext.data.Store',
    config : {
        storeId        : 'MemberList',
        autoLoad       : false,
        pageSize       : 125,
        clearOnPageLoad: true
    },
    model  : 'RKHSK.model.MemberListModel',
    /*constructor: function(config) {
     if (!config)
     config = {}
     this.initConfig(config);
     //this.callParent();
     },*/
    sorters: [
        { property: 'lastApplicationDate', direction: 'DESC' }
    ]
    //autoSync: true,
    //remoteSort: true,
    //buffered: true,
    /*listeners: {
     load: function(store, operation, opts) {
     console.error('load here!');
     }
     }*/
});
