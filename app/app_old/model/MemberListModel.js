Ext.define('RKHSK.model.MemberListModel', {
    extend            : 'Ext.data.Model',
    alternateClassName: 'MemberListModel',
    requires          : [
        //  'Ext.DateExtras'
    ],
    store             : 'MemberList',
//  filters: [
//    { property: 'category', value: 'news' },
//    { property: 'category', value: 'press' }
//  ],
    idProperty        : 'personalId',
    fields            : [
        {
            name      : 'lastApplicationDate',
            type      : 'date',
            dateFormat: 'Y-m-d'
        },
        {
            name: 'lastApplicationFor',
            type: 'string'
        },
        {
            name: 'personalId',
            type: 'string'
        },
        {
            name: 'firstName',
            type: 'string'
        },
        {
            name: 'lastName',
            type: 'string'
        },
        {
            name: 'lastPaymentKey',
            type: 'string'
        },
        {
            name      : 'lastPaymentRecieved',
            type      : 'date',
            dateFormat: 'Y-m-d'
        },
        {
            name: 'lastMecenatReg',
            type: 'string'
        },
        {
            name: 'lastSscoReg',
            type: 'string'
        },
        {
            name: 'phone',
            type: 'string'
        },
        {
            name: 'email',
            type: 'string'
        },
        {
            name: 'streetCo',
            type: 'string'
        },
        {
            name: 'streetAddress',
            type: 'string'
        },
        {
            name: 'streetNumber',
            type: 'string'
        },
        {
            name: 'streetEntrance',
            type: 'string'
        },
        {
            name: 'streetApartment',
            type: 'string'
        },
        {
            name: 'streetFloor',
            type: 'string'
        },
        {
            name: 'zipCode',
            type: 'string'
        },
        {
            name: 'city',
            type: 'string'
        },
        {
            name: 'sys_administrator',
            type: 'bool'
        },
        {
            name: 'sys_accounttype',
            type: 'string'
        }
    ],
    proxy             : {
        //type: 'pagingmemory',
        type         : 'rest',
        //format: 'json',
        api          : {
            create : '/admin/create/',
            read   : '/admin/read/',
            update : '/admin/update/',
            destroy: '/admin/destroy/'
        },
        //extraParams: {
        //  total: 50000
        //},
        actionMethods: {
            create : 'GET',
            update : 'GET',
            read   : 'GET',
            destroy: 'GET'
        },
        reader       : {
            type         : 'json',
            idProperty   : 'personalId',
            totalProperty: 'memberCount',
            root         : 'members'
        }
        //callbackKey: 'callback',
        //simpleSortMode: true
    }

    // itemTpl: '{date}: {title}'
});
