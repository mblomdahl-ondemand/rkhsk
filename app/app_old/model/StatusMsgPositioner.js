Ext.define('RKHSK.model.StatusMsgPositioner', {
    extend            : 'Ext.data.Model',
    alternateClassName: 'StatusMsgPositioner',

    config: {
        msgStack       : [],
        verticalPadding: 2,
        msgItems       : {},
        baselineY      : 2,
        baselineX      : 105
    },

    constructor: function (config) {
        this.initConfig(config);
        //this.connect();
    },

    position  : function (item) {

        // check if item is displaying
        var offsetY = item.getHeight() + this.verticalPadding,
            baselineY = this.baselineY,
            baselineX = this.baselineX,
            itemId = item.id;

        this.msgStack.push(itemId);
        this.msgItems[itemId] = {
            item     : item,
            offsetY  : offsetY,
            positionY: baselineY
        }

        item.setPosition(baselineX, baselineY);

        this.baselineY = baselineY + offsetY;
    },
    deposition: function (item) {

        var oldMsgStack = this.msgStack,
            newMsgStack = [],
            baselineX = this.baselineX,
            baselineReeval = 2;

        // states: !found && !identified => !found && identified => found
        for (var itemId = item.id, found = false, i = 0, j = oldMsgStack.length;
             i < j;
             i++) {
            if (!found && this.msgStack[i] !== itemId) {// !found && !identified
                newMsgStack.push(oldMsgStack[i]);
            } else if (!found) { // !found && identified
                //var offsetY = this.msgItems[itemId].offsetY;
                baselineReeval = this.msgItems[itemId].positionY;
                found = true;
                continue;
            } else { // found
                this.msgItems[oldMsgStack[i]].positionY = baselineReeval;
                this.msgItems[oldMsgStack[i]].item.setPosition(baselineX, baselineReeval)
                baselineReeval += this.msgItems[oldMsgStack[i]].offsetY;
                newMsgStack.push(oldMsgStack[i]);
            }
        }

        this.baselineY = baselineReeval;
        this.msgStack = newMsgStack;
        delete this.msgItems[item.id];
    }

});
