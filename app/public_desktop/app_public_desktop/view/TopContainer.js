/*
 * File: app_public_desktop/view/TopContainer.js
 *
 * This file was generated by Sencha Architect version 2.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('RKHSK.view.TopContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.topcontainer',

    requires: [
        'RKHSK.view.TopFillerContainer',
        'RKHSK.view.TopActionContainer'
    ],

    layout: {
        pack: 'center',
        type: 'hbox'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'topfillercontainer',
                    flex: 1
                },
                {
                    xtype: 'container',
                    height: 101,
                    html: '<div style="height: 110px; background: none; background-image: url(/resources/images/logo-base-8.png); background-repeat: no-repeat;">',
                    minWidth: 596,
                    width: 596
                },
                {
                    xtype: 'topactioncontainer'
                },
                {
                    xtype: 'topfillercontainer',
                    flex: 1
                }
            ]
        });

        me.callParent(arguments);
    }

});