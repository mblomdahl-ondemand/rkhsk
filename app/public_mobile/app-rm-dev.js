var RKHSKAppVersionRM = 1.0,
    BASEURL = '',
    myGlobals = {};

myGlobals.config = [];
myGlobals.count = [];

/*Ext.require([
 'Ext.DateExtras',
 'Ext.data.Types',
 'Ext.Anim',
 'Ext.Container',
 'Ext.carousel.Carousel',
 'Ext.fx.Easing',
 'Ext.fx.easing.EaseOut',
 'Ext.util.SizeMonitor',
 'Ext.carousel.Item',
 'Ext.carousel.Indicator',
 'Ext.layout.Carousel',
 'Neonode.store.NewsroomREST',
 'Neonode.store.NewsroomRemote',
 'Neonode.store.NewsroomLocal',
 'Neonode.model.NewsModel',
 'Neonode.model.GeneratorModel',
 'Neonode.model.NewsroomModel',
 'Neonode.model.HomeModel',
 'Neonode.model.ContactModel',
 'Neonode.model.FutureConceptModel',
 'Neonode.model.InvestorRelationsModel',
 'Neonode.model.ProductsModel',
 'Neonode.model.AboutModel',
 'Neonode.model.NewsroomRemote',
 'Neonode.controller.ContentFeedr'
 ]);*/

Ext.application({
    name        : "RKHSK",
    appFolder   : "app",
    models      : [
        'MemberRegModelM'
    ],
    stores      : [
        'AdmissionSemesterM',
        'StreetFloor'
    ],
    views       : [
        'RegPageM',
        'RegStatusWindowM'
    ],
    controllers : [
        'RegCtrlM'
    ],
    failureTypes: {
        403: "Din användaridentitet kunde inte verifieras. Ladda om sidan och försök pånytt. Fungerar det fortfarande inte? Maila medlemsadministrationen på regga@rkh-sk.se.",
        404: "Ett anslutningsfel inträffade. Var god kontrollera att du är ansluten till internet och försök igen.",
        400: "Information från servern kunde inte laddas, sannolikt till följd av ett konfigurationsfel på serversidan. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.",
        500: "Ett kommunikationsfel uppstod p.g.a. fel i serverkonfigurationen. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande."
    },
    launch      : function () {
        var regPage = Ext.create('RKHSK.view.RegPageM'),
            hash = window.location.hash;
        Ext.Viewport.add(regPage);
        //console.log('hash: '+hash);
        if (hash) {
            hash = hash.substring(1);
        }
        if (hash.length >= 24) {
            var loadUrl = '/account/load/' + hash,
                regForm = regPage.down('#regForm'),
                statusForm = regPage.down('#statusForm');
            if (hash.length === 30) { // regform
                Ext.Viewport.setMasked({ xtype: "loadmask", message: "Laddar . . ." });
                Ext.Ajax.request({
                    timeout: 1e4,
                    url    : loadUrl,
                    success: function (response) {
                        var data = Ext.decode(response.responseText).data;
                        regForm.setValues(data);
                        if (data.email) {
                            var loginField = statusForm.down('#loginUsername');
                            loginField.setValue(data.email);
                            loginField.fireEvent('change', loginField);
                        }
                        Ext.Viewport.setMasked(false);
                    },
                    failure: function (response) {
                        var failMsg = RKHSK.app.failureTypes[response.status];
                        if (!failMsg) {
                            failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
                        }
                        Ext.Msg.alert('Inläsningen misslyckades', failMsg, function () {
                            Ext.Viewport.setMasked(false);
                        });
                        return false;
                    }
                });
            } else if (hash.length === 26) { // accountview
                Ext.Viewport.setMasked({ xtype: "loadmask", message: "Laddar . . ." });
                Ext.Ajax.request({
                    timeout: 1e4,
                    url    : loadUrl,
                    success: function (response) {
                        var data = Ext.decode(response.responseText).data;
                        statusForm.setValues(data);
                        Ext.Viewport.setMasked(false);
                        if (data.loginUsername && data.loginCredentials) {
                            regPage.setActiveItem(1);
                            var usernameField = statusForm.down('#loginUsername'),
                                credentialsField = statusForm.down('#loginCredentials'),
                                submitBtn = statusForm.down('#retrieveStatus');
                            usernameField.setValue(data.email);
                            usernameField.fireEvent('change', usernameField);
                            credentialsField.setValue(data.email);
                            credentialsField.fireEvent('change', credentialsField)

                            submitBtn.fireEvent('tap', submitBtn);
                        }
                    },
                    failure: function (response) {
                        var failMsg = RKHSK.app.failureTypes[response.status];
                        if (!failMsg) {
                            failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';
                        }
                        Ext.Msg.alert('Inläsningen misslyckades', failMsg, function () {
                            Ext.Viewport.setMasked(false);
                        });
                        return false;
                    }
                });
            }
        }
    }
});
