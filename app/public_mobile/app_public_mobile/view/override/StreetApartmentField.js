Ext.define('RKHSK.view.override.StreetApartmentField', {
    override: 'RKHSK.view.StreetApartmentField',
    initialize: function () {
        var me = this;

        me.setMaxLength(4);
        me.callParent();
        me.getComponent().element.dom.children[0].addEventListener('keypress', function (e) {
            var charCode = e.charCode || e.keyCode,
                input = String.fromCharCode(charCode),
                maskRe = /\d/;

            if (!maskRe.test(input)) {
                e.stopPropagation();
                e.preventDefault();
            }
        }, false);
    },
    isValid   : function () {
        //console.error(this.getValue());
        return true;
    }
});