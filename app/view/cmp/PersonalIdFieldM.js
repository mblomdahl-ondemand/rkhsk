Ext.define('RKHSK.view.cmp.PersonalIdFieldM', {
    extend    : 'Ext.field.Text',
    xtype     : 'personalidfield',
    initialize: function () {
        var me = this;
        me.setMaxLength(11);
        me.callParent();
        me.getComponent().element.dom.children[0].addEventListener('keypress', function (e) {
            var value = this.value,
                index = this.value.length,
                charCode = e.charCode || e.keyCode,
                input = String.fromCharCode(charCode),
                maskRe = [
                    /[6-9]/, // year1
                    /[0-9]/, // year2
                    [/0/, /1/], // month1
                    [/[1-9]/, /[0-2]/], // month2
                    [/0/, /[1-2]/, /3/], // day1
                    [/[1-9]/, /[0-9]/, /[0-1]/], // day2
                    /\-/, // separator
                    /[0-9a-z]/i, // n1
                    /[0-9]/, // n2
                    /[0-9]/, // n3
                    /[0-9]/ // n4
                ];

            function stopEvent() {
                e.stopPropagation();
                e.preventDefault();
            }

            if (index === maskRe.length) {
                return stopEvent();
            }
            if (!Ext.isArray(maskRe[index])) {
                if (!maskRe[index].test(input)) {
                    stopEvent();
                }
            } else {
                if (Ext.isArray(maskRe[index - 1]) && maskRe[index - 1].length === maskRe[index].length) {
                    for (var matched = false, i = maskRe[index].length;
                         i--;) {
                        if (maskRe[index - 1][i].test(value[index - 1]) && maskRe[index][i].test(input)) {
                            matched = true;
                        }
                        if (!i && !matched) {
                            stopEvent();
                        }
                    }
                } else {
                    for (var matched = false, i = maskRe[index].length;
                         i--;) {
                        if (maskRe[index][i].test(input)) {
                            matched = true;
                        }
                        if (!i && !matched) {
                            stopEvent();
                        }
                    }
                }
            }
        }, false);
    }, /*
     onKeyUp: function(e) {
     var me = this;
     if (me.filterKeys(me, e) !== false) {
     console.error('keypress4');
     me.parent.fireAction('keyup', [me, e], 'doKeyUp');
     } else {
     return false;
     }
     },
     filterKeys : function(scope, e) {
     /*
     * On European keyboards, the right alt key, Alt Gr, is used to type certain special characters.
     * JS detects a keypress of this as ctrlKey & altKey. As such, we check that alt isn't pressed
     * so we can still process these special characters.
     */
    /*
     myGlobals.e = e;

     function isSpecialKey(kEvent, keyCode) {
     return (kEvent.type == 'keypress' && kEvent.ctrlKey) ||
     (keyCode == 8) || // Backspace
     (keyCode >= 16 && keyCode <= 20) || // Shift, Ctrl, Alt, Pause, Caps Lock
     (keyCode >= 44 && keyCode <= 46);   // Print Screen, Insert, Delete
     }

     if (e.event.ctrlKey && !e.event.altKey) {
     console.error('keypress1');
     return;
     }

     var keyCode = e.event.keyCode,
     charCode = String.fromCharCode(keyCode);

     if(isSpecialKey(e.event, keyCode) || !charCode) {
     console.error('keypress2');
     return;
     }

     if(!scope.maskRe.test(charCode)) {
     console.error('keypress3:'+charCode);
     e.stopEvent();
     return false;
     }
     */
    //},
    isValid   : function () {
        //console.error(this.getValue());
        return true;
    }
});
