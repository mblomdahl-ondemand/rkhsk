Ext.define('RKHSK.view.RegStatusWindowD', {
    extend     : 'Ext.window.Window',
    height     : 340,
    id         : 'regStatusWindowD',
    width      : 360,
    bodyPadding: '10 0 0 10',

    listeners: {
        beforeshow: function () {
            return true;
            //Ext.getCmp('confirmInfoRow1').setValue(this.infoRow1[this.contentCategory]);
            //Ext.getCmp('confirmInfoRow2').setValue(this.infoRow2[this.contentCategory]);

        }
    },
    modal    : true,
    layout   : {
        type: 'anchor'
    },
    title    : 'Medlemsdata/ärendestatus',
    bodyCls  : 'table-message-box',
    tpl      : [
        '<table><tbody>',
        '<tr><td>Personnummer:</td><td>{personalId}</td></tr>',
        '<tr><td>Efter- och förnamn:</td><td>{lastName}, {firstName}</td></tr>',
        '<tr><td>Senast anmäld till:</td><td>{lastApplicationFor}</td></tr>',
        '<tr><td>Senaste inbetalning:</td><td>{lastPaymentRecieved}</td></tr>',
        '<tr><td>SSSB-registreringar:</td><td>{sscoHistory}</td></tr>',
        '<tr><td>Mecenat-registreringar:</td><td>{mecenatHistory}</td></tr>',
        '<tr><td>Medlemskap i RKH-SK:</td><td>{membershipHistory}</td></tr>',
        '<tr><td>Anmälningshistorik:</td><td>{applicationSemesterHistory}</td></tr>',
        '<tr><td>Postadress:</td><td>{streetAddress}</td></tr>',
        '<tr><td>Epost-adress:</td><td>{email}</td></tr>',
        '<tr><td>Telefonnummer:</td><td>{phone}</td></tr>',
        '<tr><td>Årskurs:</td><td>{admittedSemester}</td></tr>',
        //'<tr><td>Samtycke, datalagring:</td><td>'+(data.studentConfirmation ? 'Ja' : 'Nej')+'</td></tr>',
        '</tbody></table>'
    ].join('')/*,
     dockedItems: [
     {
     xtype: 'panel',
     frame: true,
     height: 31,
     layout: {
     type: 'anchor'
     },
     dock: 'bottom',
     dockedItems: [
     {
     xtype: 'button',
     id: 'confirmPaymentsSubmit',
     text: 'OK',
     dock: 'right',
     weight: 1
     }
     ]
     }
     ]*/
});
/*height: 310,
 id: 'regStatusWindowD',
 width: 370,
 //autoScroll: false,
 modal: true,
 frame: true,
 frameHeader: 'true',
 //bodyCls: 'table-message-box',
 layout: 'column',
 //html: '<p>foobar</p>',
 title: 'Medlemsdata arendestatus',
 items: [
 {
 xtype: 'panel',
 frame: true,
 columnWidth: 1,
 html: '<p> foobar</p>'

 tpl: [
 '<table><tbody>',
 '<tr><td>Personnummer:</td><td>{personalId}</td></tr>',
 '<tr><td>Efter- och förnamn:</td><td>{lastName}, {firstName}</td></tr>',
 '<tr><td>Senast anmäld till:</td><td>{lastApplicationFor}</td></tr>',
 '<tr><td>Senaste inbetalning:</td><td>{lastPaymentRecieved}</td></tr>',
 '<tr><td>SSSB-registreringar:</td><td>{sscoHistory}</td></tr>',
 '<tr><td>Mecenat-registreringar:</td><td>{mecenatHistory}</td></tr>',
 '<tr><td>Medlemskap i RKH-SK:</td><td>{membershipHistory}</td></tr>',
 '<tr><td>Anmälningshistorik:</td><td>{applicationSemesterHistory}</td></tr>',
 '<tr><td>Postadress:</td><td>{streetAddress}</td></tr>',
 '<tr><td>Epost-adress:</td><td>{email}</td></tr>',
 '<tr><td>Telefonnummer:</td><td>{phone}</td></tr>',
 '<tr><td>Årskurs:</td><td>{admittedSemester}</td></tr>',
 //'<tr><td>Samtycke, datalagring:</td><td>'+(data.studentConfirmation ? 'Ja' : 'Nej')+'</td></tr>',
 '</tbody></table>'
 ].join(''),
 }
 ],
 fbar: [
 { type: 'button', text: 'Button 1' }
 ]
 });*/
