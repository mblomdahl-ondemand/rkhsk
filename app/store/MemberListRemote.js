Ext.define('RKHSK.store.MemberListRemote', {
    extend         : 'Ext.ux.data.PagingStore',
    storeId        : 'MemberListRemote',
    model          : 'RKHSK.model.MemberListModel',
    sorters        : [
        { property: 'lastApplicationDate', direction: 'DESC' }
    ],
    autoLoad       : false,
    //autoSync: false,
    //remoteSort: true,
    //buffered: true,
    pageSize       : 80,
    clearOnPageLoad: false,
    proxy          : {
        type  : 'ajax',
        url   : '/admin/read/', /*
         //format: 'json',
         api: {
         create: BASEURL+'/admin/create/',
         read: BASEURL+'/admin/read/',
         update: BASEURL+'/admin/update/',
         destroy: BASEURL+'/admin/destroy/'
         },
         //extraParams: {
         //  total: 50000
         //},
         actionMethods: {
         create: 'GET',
         update: 'GET',
         read: 'GET',
         destroy: 'GET'
         },*/
        reader: {
            type         : 'json',
            idProperty   : 'personalId',
            totalProperty: 'memberCount',
            root         : 'members'
        }
        //callbackKey: 'callback',
        //simpleSortMode: true
    }
});
