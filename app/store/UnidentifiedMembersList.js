Ext.define('RKHSK.store.UnidentifiedMembersList', {
    extend : 'Ext.data.Store',
    config : {
        storeId: 'UnidentifiedMembersList'
    },
    sorters: [
        { property: 'lastPaymentRecieved', direction: 'DESC' }
    ],
    //data: {
    //  members: undefined
    //},
    fields : [
        {
            name      : 'lastPaymentRecieved',
            type      : 'date',
            dateFormat: 'Y-m-d'
        },
        {
            name: 'payment',
            type: 'string'
        },
        {
            name: 'message',
            type: 'string'
        },
        {
            name: 'lastPaymentKey',
            type: 'string'
        }
    ],
    proxy  : {
        type: 'memory'
        //id  : 'members'
    }
    // itemTpl: '{date}: {title}'
});
