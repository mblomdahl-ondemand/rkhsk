Ext.define('RKHSK.store.RecognizedMembersList', {
    extend : 'Ext.data.Store',
    config : {
        storeId: 'RecognizedMembersList',
        fields : [
            {
                name      : 'lastApplicationDate',
                type      : 'date',
                dateFormat: 'Y-m-d'
            },
            {
                name: 'lastApplicationFor',
                type: 'string'
            },
            {
                name: 'personalId',
                type: 'string'
            },
            {
                name: 'firstName',
                type: 'string'
            },
            {
                name: 'lastName',
                type: 'string'
            },
            {
                name: 'lastPaymentKey',
                type: 'string'
            },
            {
                name      : 'lastPaymentRecieved',
                type      : 'date',
                dateFormat: 'Y-m-d'
            },
            {
                name: 'payment',
                type: 'string'
            },
            {
                name: 'sys_administrator',
                type: 'bool'
            },
            {
                name: 'sys_accounttype',
                type: 'string'
            }
        ]
    },
    sorters: [
        { property: 'lastPaymentRecieved', direction: 'DESC' }
    ],
    //data: {
    //  members: undefined
    //},
    proxy  : {
        type: 'memory'
        //root: 'members'
    }

    // itemTpl: '{date}: {title}'
});
