Ext.define('RKHSK.model.MemberRegModelM', {
    extend            : 'Ext.data.Model',
    alternateClassName: 'MemberRegModelM',
    requires          : [
        //  'Ext.DateExtras'
    ],
    //store: 'MemberList',
//  filters: [
//    { property: 'category', value: 'news' },
//    { property: 'category', value: 'press' }
//  ],
    config            : {
        idProperty : 'personalId',
        fields     : [
            {
                name: 'admittedSemester',
                type: 'string'
            },
            {
                name: 'lastApplicationFor',
                type: 'string'
            },
            {
                name: 'personalId',
                type: 'string'
            },
            {
                name: 'firstName',
                type: 'string'
            },
            {
                name: 'lastName',
                type: 'string'
            },
            {
                name: 'phone',
                type: 'string'
            },
            {
                name: 'email',
                type: 'string'
            },
            {
                name: 'streetCo',
                type: 'string'
            },
            {
                name: 'streetAddress',
                type: 'string'
            },
            {
                name: 'streetNumber',
                type: 'string'
            },
            {
                name: 'streetEntrance',
                type: 'string'
            },
            {
                name: 'streetApartment',
                type: 'string'
            },
            {
                name: 'streetFloor',
                type: 'string'
            },
            {
                name: 'zipCode',
                type: 'string'
            },
            {
                name: 'city',
                type: 'string'
            },
            {
                name: 'country',
                type: 'string'
            },
            {
                name: 'studentConfirmation',
                type: 'string'
            }
        ],
        validations: [
            // personalId
            {
                type   : 'format',
                name   : 'personalId',
                matcher: /^[4-9][0-9](0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-2])\-[A-Z\d]\d{3}$/,
                message: 'Ogiltigt personnr. Personnummer måste anges på formen YYMMDD-NNNN. Exempel: "860722-0332" – där "86" markerar år 1986, "07" juli månad, "22" dag 22 i juli månad och "0332" de sista fyra siffrorna i personnumret.'
            },
            {
                type   : 'presence',
                name   : 'personalId',
                message: 'Fältet "personnummer" är obligatoriskt.'
            },

            // city
            {
                type   : 'format',
                name   : 'city',
                matcher: /^[A-Za-z åäöÅÄÖ\-]+$/,
                message: 'Postorten innehåller ogiltiga tecken. Fältet "postort" kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.'
            },
            {
                type   : 'presence',
                name   : 'city',
                message: 'Fältet "postort" är obligatoriskt.'
            },

            // zipCode
            {
                type   : 'format', name: 'zipCode',
                matcher: /^\d{5}$/,
                message: 'Postnumret innehåller ogiltiga tecken eller har en ogiltig längd. Korrekt format är "NNNNN" (exempelvis "11421", utan blanksteg).'
            },
            {
                type   : 'presence',
                name   : 'zipCode',
                message: 'Fältet "postnummer" är obligatoriskt.'
            },

            // country
            {
                type   : 'presence',
                name   : 'country',
                message: 'Fältet "land" är obligatoriskt".'
            },
            {
                type   : 'format',
                name   : 'country',
                matcher: /^(Sverige)$/,
                message: 'Fältet "Land" accepterar för närvarande bara ett värde (Sverige) – ta kontakt med medlemsregistreringen på regga@rkh-sk.se om du saknar svensk adress.'
            },

            // phone
            {
                type   : 'format',
                name   : 'phone',
                matcher: /^$()|^[\+\d][\d \-]{9,18}$/,
                message: 'Ogiltigt telefonnr. Fältet "telefonnummer" kan enbart innehålla en begränsad teckenuppsättning, accepterade tecken är + (som första tecken), 0-9, blanksteg och bindestreck. Exempel på giltiga telefonnummer är "+4672-522 32 33" och "08 920 20 25".'
            },

            // email
            {
                type   : 'format',
                name   : 'email',
                matcher: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
                message: 'Fältet "epost-adress" är felformaterat. Korrekt format är "user@mailprovider.com".'
            },
            {
                type   : 'presence',
                name   : 'email',
                message: 'Fältet "epost-adress" är obligatoriskt. Det är viktigt att du anger en korrekt adress här eftersom dina personliga uppgifter för inbetalning av medlemsavgift överförs via mail.'
            },

            // streetApartment
            {
                type   : 'format',
                name   : 'streetApartment',
                matcher: /^\d*$/,
                message: 'Lägenhetsnumret innehåller ogiltiga tecken eller har en ogiltig längd.'
            },

            // streetEntrance
            {
                type   : 'format',
                name   : 'streetEntrance',
                matcher: /^[A-Z]*$/,
                message: 'Fältet "portuppgång" innehåller ogiltiga tecken eller har en ogiltig längd. För våningsplan (exempelvis "N.B." i "Hemgatan 4A, N.B.") se fältet "trappor".'
            },

            // streetNumber
            {
                type   : 'format',
                name   : 'streetNumber',
                matcher: /^\d{1,3}$|^\d{1,3}\-\d{1,3}$/,
                message: 'Gatunumret innehåller ogiltiga tecken eller har en ogiltig längd. Ett gatunummer kan enbart innehålla siffror från 0-9. För portuppgång (exempelvis "B" i "2B") resp. våningsplan, se fälten "portnr." och "trappor".'
            },
            {
                type   : 'presence',
                name   : 'streetNumber',
                message: 'Fältet "gatunummer" är obligatoriskt.'
            },

            // streetAddress
            {
                type   : 'format',
                name   : 'streetAddress',
                matcher: /^[A-Za-z :åäöÅÄÖ\-]+$/,
                message: 'Gatuadressen innehåller ogiltiga tecken. Fältet "gatuadress" kan enbart innehålla en begränsad teckenuppsättning, närmare besämt a-ö, bindestreck och blanksteg. För gatunummer och portuppgång, se fälten "gatunummer" och "portnr."'
            },
            {
                type   : 'presence',
                name   : 'streetAddress',
                message: 'Fältet "gatuadress" är obligatoriskt.'
            },

            // firstName
            {
                type   : 'format',
                name   : 'firstName',
                matcher: /^[A-Za-z éüÜåäöÅÄÖ\-]+$/,
                message: 'Ditt förnamn innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.'
            },
            {
                type   : 'presence',
                name   : 'firstName',
                message: 'Fältet "förnamn" är obligatoriskt.'
            },

            // lastName
            {
                type   : 'format',
                name   : 'lastName',
                matcher: /^[A-Za-z éüÜåäöÅÄÖ\-]+$/,
                message: 'Ditt efternamn innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.'
            },
            {
                type   : 'presence',
                name   : 'lastName',
                message: 'Fältet "efternamn" är obligatoriskt.'
            },

            // lastApplicationFor
            {
                type   : 'inclusion',
                name   : 'lastApplicationFor', list: ['VT12'],
                message: ''
            },
            {
                type   : 'presence',
                name   : 'lastApplicationFor',
                message: 'Du måste ange vilken termin medlemsansökan anger. Se rubrik 1.'
            },

            // admittedSemester
            {
                type   : 'format', name: 'admittedSemester',
                matcher: /^$|^[HV]K\d\d$/,
                message: 'Årskursen innehåller ogiltiga tecken. Fältet "årskurs" kan enbart innehålla värden på formatet [HV]K<YY>.'
            },
            {
                type   : 'presence',
                name   : 'admittedSemester',
                message: 'Fältet "årskurs" är obligatoriskt.'
            },

            // studentConfirmation
            {
                type   : 'presence',
                name   : 'studentConfirmation',
                message: 'Du måste vara studieaktiv vid Röda Korsets Högskola för att kunna vara med i studentkåren.'
            }

        ],
        proxy      : {
            //type: 'pagingmemory',
            type  : 'ajax',
            url   : '/account/load/',
            //format: 'json',
            reader: {
                type        : 'json',
                idProperty  : 'personalId',
                rootProperty: 'memberData'
            }
            //callbackKey: 'callback',
            //simpleSortMode: true
        }
    }
    // itemTpl: '{date}: {title}'
});
