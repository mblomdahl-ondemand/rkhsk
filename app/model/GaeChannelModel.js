Ext.define('RKHSK.model.GaeChannelModel', {
    extend            : 'Ext.data.Model',
    alternateClassName: 'GaeChannelModel',

    config: {
        connected    : false,
        socket       : undefined,
        listeners    : {},
        autoReconnect: false,
        requestIds   : {}
    },

    constructor: function (config) {
        this.initConfig(config);
        //this.connect();
    },

    connect   : function () {
        //return true;
        var me = this;

        if (me.connected) {
            return false;
        }

        Ext.Ajax.request({
            url    : '/admin/channel/create',
            method : 'GET',
            //jsonData: requestData,
            timeout: 6e3,
            format : 'json',
            success: function (response) {
                //gridElement.record.set(gridElement.field, gridValue);
                //statusMsg.setText('Resultat: OK – Medlemsinformation för '+member+' uppdaterad.');
                //statusMsg.show();
                //window.setTimeout(function() { statusMsg.hide(); }, 5e3);
                //record.commit();
                //memberGrid.getView().refresh();
                myGlobals.gaeres = response;
                channel = new goog.appengine.Channel(Ext.decode(response.responseText).channelToken);
                me.socket = channel.open();
                me.socket.onopen = function () {
                    me.onChannelOpen();
                };
                me.socket.onmessage = function (msg) {
                    me.onChannelMessage(msg);
                };
                me.socket.onerror = function (error) {
                    me.onChannelError(error);
                };
                me.socket.onclose = function (msg) {
                    me.onChannelClose();
                };

                //console.error("'channel up'");
                //myGlobals.updateSuccessResponse = response;
                //Ext.Msg.alert('Medlemsregistrering skickad', 'Du är nu registrerad i RKH-SK:s medlemsdatabas och kan betala in medlemsavgiften via PlusGiro eller studentkårens expedition. Se e-post för den personliga kod du måste bifoga inbetalningen.');
            },
            failure: function (response) {
                //statusMsg.setText('Resultat: FEL – Medlemsinformation för '+member+' kunde inte uppdateras.');
                //statusMsg.show();
                //window.setTimeout(function() {
                //  statusMsg.setText('Resultat: FEL – '+failureTypes[response.failureType]);
                //  statusMsg.show();
                //  window.setTimeout(function() { statusMsg.hide(); }, 5e3);
                //}, 5e3)
                //myGlobals.updateFailureResponse = response;
                //Ext.Msg.alert('Registreringen misslyckades', failureTypes[response.failureType]);
            }
        });
    },
    disconnect: function () {
        if (this.socket) {
            this.setAutoReconnect(false);
            this.getSocket().close();
        }
    },

    generateRequestId: function () {
        var now = new Date();
        for (id in
            this.requestIds) {
            if ((now - this.requestIds[id]) > 6e5) {
                delete this.requestIds[id];
                //console.error('deleted id '+id);
            }
        }
        while (true) {
            var newId = Math.random().toString().substring(2, 6);
            if (!this.requestIds[newId]) {
                this.requestIds[newId] = now;
                return newId;
            }
        }
    },
    addListener      : function (callback, config) { // remoteEvent, one, requestId
        var id, idList = [];
        if (!Ext.isArray(config.remoteEvent)) {
            config.remoteEvent = [config.remoteEvent];
        }
        for (var i = config.remoteEvent.length;
             i--;) {
            while (true) {
                id = "listener" + Math.random().toString().substring(2, 6);
                if (!this.listeners[id]) {
                    break;
                }
            }
            this.listeners[id] = {
                callback   : callback,
                remoteEvent: config.remoteEvent[i],
                one        : config.one,
                requestId  : config.requestId,
                scope      : config.scope
            }
            idList.push(id)
            //console.error('listener '+id+' added.');
        }
        return idList;
    },
    removeListener   : function (listenerId) {
        delete this.listeners[listenerId]
        //console.error('listener '+listenerId+' removed.');
    },

    onChannelOpen   : function () {
        //console.error('channel opened, connected'+this.connected)
        this.setConnected(true);
        this.setAutoReconnect(true);
        //console.error('channel opened, connected'+this.connected)
    },
    onChannelMessage: function (message) {
        //console.error('channel message'+message.data);//.substring(0,500));
        var data = Ext.decode(message.data);
        for (listener in
            this.listeners) {
            var tmp = this.listeners[listener]
            if ((tmp.remoteEvent && tmp.remoteEvent === data.remoteEvent && data.requestId && data.requestId === tmp.requestId)) {
                (function (scope, callback, remoteEvent, msg) {
                    if (scope) {
                        scope.callback = callback;
                        scope.callback(remoteEvent, msg);
                    } else {
                        callback(remoteEvent, msg);
                    }
                })(tmp.scope, tmp.callback, tmp.remoteEvent, data);
                if (tmp.one) {
                    delete this.listeners[listener]
                }
            } else if ((!data.requestId && tmp.remoteEvent && tmp.remoteEvent === data.remoteEvent) || (!data.remoteEvent && data.requestId && data.requestId === tmp.requestId)) {
                (function (scope, callback, remoteEvent, msg) {
                    if (scope) {
                        scope.callback = callback;
                        scope.callback(remoteEvent, msg);
                    } else {
                        callback(remoteEvent, msg);
                    }
                })(tmp.scope, tmp.callback, tmp.remoteEvent, data);
                if (tmp.one) {
                    delete this.listeners[listener]
                }
            }
        }

        return true;
    },
    onChannelError  : function (error) {
        console.error('GaeChannel error (' + error.code + '): ' + error.description)
    },
    onChannelClose  : function () {
        //console.error('channel closed, connected'+this.connected)
        if (this.getAutoReconnect()) {
            this.connect()
        }
        else {
            this.setConnected(false);
        }
        //console.error('channel closed, connected'+this.connected)
    }
});
