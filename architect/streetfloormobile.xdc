{
    "xdsVersion": "2.1.0",
    "frameworkVersion": "touch20",
    "internals": {
        "type": "arraystore",
        "reference": {
            "name": "items",
            "type": "array"
        },
        "codeClass": null,
        "userConfig": {
            "designer|userClassName": "StreetFloorMobile",
            "designer|userAlias": "streetfloor",
            "alternateClassName": [
                "StreetFloor"
            ],
            "autoLoad": true,
            "clearOnPageLoad": false,
            "data": "[\n    { \"name\": \"\", \"value\": \"\"       },\n    { \"name\": \"N.B.\", \"value\": \"N.B.\"   },\n                { \"name\": \"1 tr.\", \"value\": \"1 tr.\"  },\n            { \"name\": \"2 tr.\", \"value\": \"2 tr.\"  },\n            { \"name\": \"3 tr.\", \"value\": \"3 tr.\"  },\n            { \"name\": \"4 tr.\", \"value\": \"4 tr.\"  },\n            { \"name\": \"5 tr.\", \"value\": \"5 tr.\"  },\n            { \"name\": \"6 tr.\", \"value\": \"6 tr.\"  },\n            { \"name\": \"7 tr.\", \"value\": \"7 tr.\"  },\n            { \"name\": \"8 tr.\", \"value\": \"8 tr.\"  },\n            { \"name\": \"9 tr.\", \"value\": \"9 tr.\"  },\n            { \"name\": \"10 tr.\", \"value\": \"10 tr.\" },\n            { \"name\": \"11 tr.\", \"value\": \"11 tr.\" },\n            { \"name\": \"12 tr.\", \"value\": \"12 tr.\" }\n    ]",
            "storeId": "StreetFloor"
        },
        "cn": [
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField17",
                    "name": "name",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField18",
                    "name": "value",
                    "type": "string"
                }
            }
        ]
    },
    "linkedNodes": {},
    "boundStores": {},
    "boundModels": {}
}