{
    "xdsVersion": "2.1.0",
    "frameworkVersion": "touch20",
    "internals": {
        "type": "controller",
        "reference": {
            "name": "items",
            "type": "array"
        },
        "codeClass": null,
        "userConfig": {
            "designer|userClassName": "StatusFormCtrl",
            "designer|userAlias": "statusformctrl",
            "views": [
                "StatusFormPanel",
                "StatusWindow"
            ]
        },
        "cn": [
            {
                "type": "controllerref",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "StatusFormPanel",
                    "ref": "StatusFormPanel",
                    "selector": "#statusFormPanel",
                    "xtype": "statusformpanel"
                }
            },
            {
                "type": "controllerref",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "LoginUsername",
                    "ref": "LoginUsername",
                    "selector": "#statusFormPanel emailfield"
                }
            },
            {
                "type": "controllerref",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "LoginCredentials",
                    "ref": "LoginCredentials",
                    "selector": "#statusFormPanel paymentkeyfield"
                }
            },
            {
                "type": "controllerref",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "ResendCredentials",
                    "ref": "ResendCredentials",
                    "selector": "#resendCredentialsButton"
                }
            },
            {
                "type": "controllerref",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "RetrieveStatus",
                    "ref": "RetrieveStatus",
                    "selector": "#retrieveStatusButton"
                }
            },
            {
                "type": "controllerref",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "StatusWindow",
                    "ref": "StatusWindow",
                    "selector": "statuswindow",
                    "xtype": "statuswindow"
                }
            },
            {
                "type": "fixedfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "init",
                    "fn": "init",
                    "designer|params": [
                        "application"
                    ],
                    "implHandler": [
                        "",
                        "this.control({",
                        "    '#statusFormPanel emailfield': {",
                        "        change: this.onStatusFormUsernameModified,",
                        "        blur: this.onForceLowercaseEntry",
                        "    },",
                        "    '#statusFormPanel paymentkeyfield': {",
                        "        change: this.onStatusFormCredentialsModified,",
                        "        blur: this.onForceUppercaseEntry",
                        "    },",
                        "    '#resendCredentialsButton': {",
                        "        tap: this.onResendCredentials",
                        "    },",
                        "    '#retrieveStatusButton': {",
                        "        tap: this.onRetrieveStatus",
                        "    },",
                        "    'statuswindow button[text=OK]': {",
                        "        tap: function (btn) {",
                        "            btn.up('statusmessage').destroy();",
                        "        }",
                        "    }",
                        "});"
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onForceLowercaseEntry",
                    "fn": "onForceLowercaseEntry",
                    "designer|params": [
                        "field"
                    ],
                    "implHandler": [
                        "var input = Ext.String.trim(field.getValue()).toLowerCase();",
                        "",
                        "field.setValue(input);"
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onStatusFormUsernameModified",
                    "fn": "onStatusFormUsernameModified",
                    "designer|params": [
                        "field"
                    ],
                    "implHandler": [
                        "var statusForm = field.up('formpanel'),",
                        "    loginCredentials = this.getLoginCredentials(),",
                        "    resendCredentials = this.getResendCredentials(),",
                        "    retrieveStatus = this.getRetrieveStatus();",
                        "",
                        "if (field.isValid()) {",
                        "",
                        "    //console.log('username valid');",
                        "    if (resendCredentials.isDisabled()) {",
                        "        resendCredentials.enable(true);",
                        "    }",
                        "",
                        "    if (loginCredentials.isValid()) {",
                        "",
                        "        //console.log('creds valid');",
                        "        if (retrieveStatus.isDisabled()) {",
                        "            retrieveStatus.enable(true);",
                        "        }",
                        "",
                        "    } else if (!retrieveStatus.isDisabled()) {",
                        "        retrieveStatus.disable(true);",
                        "    }",
                        "",
                        "} else {",
                        "",
                        "    //console.log('username error');",
                        "    if (!resendCredentials.isDisabled()) {",
                        "        resendCredentials.disable(true);",
                        "    }",
                        "    if (!retrieveStatus.isDisabled()) {",
                        "        retrieveStatus.disable(true);",
                        "    }",
                        "",
                        "}"
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onForceUppercaseEntry",
                    "fn": "onForceUppercaseEntry",
                    "designer|params": [
                        "field"
                    ],
                    "implHandler": [
                        "var input = Ext.String.trim(field.getValue()).toUpperCase();",
                        "",
                        "field.setValue(input);"
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onStatusFormCredentialsModified",
                    "fn": "onStatusFormCredentialsModified",
                    "designer|params": [
                        "field"
                    ],
                    "implHandler": [
                        "var loginUsername = this.getLoginUsername(),",
                        "    retrieveStatus = this.getRetrieveStatus();",
                        "",
                        "if (loginUsername.isValid() && field.isValid()) {",
                        "    if (retrieveStatus.isDisabled()) {",
                        "        retrieveStatus.enable(true);",
                        "    }",
                        "",
                        "} else {",
                        "    if (!retrieveStatus.isDisabled()) {",
                        "        retrieveStatus.disable(true);",
                        "    }",
                        "",
                        "}"
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onResendCredentials",
                    "fn": "onResendCredentials",
                    "designer|params": [
                        "button"
                    ],
                    "implHandler": [
                        "var statusForm = button.up('formpanel'),",
                        "    loginUsername = this.getLoginUsername();",
                        "",
                        "if (loginUsername.isValid()) {",
                        "    ",
                        "    statusForm.setMasked({",
                        "        xtype: 'loadmask',",
                        "        message: 'Mailar inloggningsuppgifter . . .'",
                        "    });",
                        "    ",
                        "    Ext.Ajax.request({",
                        "        ",
                        "        method: 'POST',",
                        "        ",
                        "        url: '/account/resend/',",
                        "        ",
                        "        jsonData: {",
                        "            loginUsername: loginUsername.getValue()",
                        "        },",
                        "        ",
                        "        timeout : 6e3,",
                        "        ",
                        "        success : function (response) {",
                        "            ",
                        "            Ext.Msg.alert('Bekräftelse', 'Dina medlems- och inloggningsuppgifter har mailats till din e-postadress. Om du inte mottagit meddelandet inom någon minut, var vänlig och kontrollera din \"spam\"- eller \"skräppost\"-mapp innan du tar kontakt med studentkårens support.', function () {",
                        "                statusForm.setMasked(false);",
                        "                statusForm.reset();",
                        "                loginUsername.fireEvent('change', loginUsername);",
                        "            });",
                        "            ",
                        "        },",
                        "        ",
                        "        failure : function (response) {",
                        "            ",
                        "            var failMsg = RKHSK.app.failureTypes[response.status];",
                        "            ",
                        "            if (!failMsg || response.status === 400) {",
                        "                ",
                        "                Ext.Msg.alert('Felmeddelande', 'Dina uppgifter kunde inte mailas. Säkerställ att e-postadressen du uppgett är densamma som du använde vid registreringstillfället.', function () {",
                        "                    statusForm.setMasked(false);",
                        "                });",
                        "                ",
                        "            } else {",
                        "                ",
                        "                Ext.Msg.alert('Kommunikationsfel', failMsg, function () {",
                        "                    statusForm.setMasked(false);",
                        "                });",
                        "                ",
                        "            }",
                        "        }",
                        "    });",
                        "    ",
                        "}"
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onRetrieveStatus",
                    "fn": "onRetrieveStatus",
                    "designer|params": [
                        "button"
                    ],
                    "implHandler": [
                        "var me = this,",
                        "    statusForm = button.up('formpanel'),",
                        "    loginUsername = me.getLoginUsername(),",
                        "    loginCredentials = me.getLoginCredentials();",
                        "",
                        "if (loginUsername.isValid() && loginCredentials.isValid()) {",
                        "    ",
                        "    statusForm.setMasked({",
                        "        xtype: 'loadmask',",
                        "        message: 'Läser in medlemsdata/ärendestatus . . .'",
                        "    });",
                        "    ",
                        "    Ext.Ajax.request({",
                        "        ",
                        "        method: 'POST',",
                        "        ",
                        "        url: '/account/retrieve/',",
                        "        ",
                        "        jsonData: {",
                        "            loginUsername: loginUsername.getValue(),",
                        "            loginCredentials: loginCredentials.getValue()",
                        "        },",
                        "        ",
                        "        timeout : 1e4,",
                        "        ",
                        "        success : function (response) {",
                        "            ",
                        "            //myGlobals.retrieved = response;",
                        "            ",
                        "            var data = Ext.decode(response.responseText).data;",
                        "            ",
                        "            Ext.create('RKHSK.view.StatusWindow', {",
                        "                data: data",
                        "                //renderTo: Ext.getBody()",
                        "            }).show();",
                        "            //myGlobals.win.show();",
                        "            /*",
                        "            var statusMsg = Ext.create('Ext.window.MessageBox', {",
                        "            renderTo: me.getRegPageD(),",
                        "            title: 'Medlemsdata/ärendestatus',",
                        "            width: 370,",
                        "            height: 310,",
                        "            maxHeight: 310,",
                        "            modal: true,",
                        "            frame: true,",
                        "            cls: 'table-message-box',",
                        "            msg: [",
                        "            '<table><tbody>',",
                        "            '<tr><td>Personnummer:</td><td>'+data.personalId+'</td></tr>',",
                        "            '<tr><td>Efter- och förnamn:</td><td>'+data.lastName+', '+data.firstName+'</td></tr>',",
                        "            '<tr><td>Senast anmäld till:</td><td>'+data.lastApplicationFor+'</td></tr>',",
                        "            '<tr><td>Senaste inbetalning:</td><td>'+data.lastPaymentRecieved+'</td></tr>',",
                        "            '<tr><td>SSSB-registreringar:</td><td>'+data.sscoHistory+'</td></tr>',",
                        "            '<tr><td>Mecenat-registreringar:</td><td>'+data.mecenatHistory+'</td></tr>',",
                        "            '<tr><td>Medlemskap i RKH-SK:</td><td>'+data.membershipHistory+'</td></tr>',",
                        "            '<tr><td>Anmälningshistorik:</td><td>'+data.applicationSemesterHistory+'</td></tr>',",
                        "            '<tr><td>Postadress:</td><td>'+data.streetAddress+'</td></tr>',",
                        "            '<tr><td>Epost-adress:</td><td>'+data.email+'</td></tr>',",
                        "            '<tr><td>Telefonnummer:</td><td>'+data.phone+'</td></tr>',",
                        "            '<tr><td>Årskurs:</td><td>'+data.admittedSemester+'</td></tr>',",
                        "            //'<tr><td>Samtycke, datalagring:</td><td>'+(data.studentConfirmation ? 'Ja' : 'Nej')+'</td></tr>',",
                        "            '</tbody></table>'",
                        "            ].join(''),",
                        "            buttons: Ext.Msg.OK",
                        "            ",
                        "            statusMsg.show();*/",
                        "            statusForm.setMasked(false);",
                        "            statusForm.reset();",
                        "            loginCredentials.fireEvent('change', loginCredentials);",
                        "            loginUsername.fireEvent('change', loginUsername);",
                        "        },",
                        "        ",
                        "        failure : function (response) {",
                        "            var failMsg = RKHSK.app.failureTypes[response.status];",
                        "            if (!failMsg || response.status === 403) {",
                        "                Ext.Msg.alert('Medlemsdata/ärendestatus', 'Dina medlemsuppgifter kunde inte läsas in. Var vänlig och kontrollera att du har angett rätt e-postadress och en giltig personlig kod.', function () {",
                        "                    statusForm.setMasked(false);",
                        "                });",
                        "            }",
                        "            else {",
                        "                Ext.Msg.alert('Kommunikationsfel', failMsg, function () {",
                        "                    statusForm.setMasked(false);",
                        "                });",
                        "            }",
                        "        }",
                        "    });",
                        "  ",
                        "}"
                    ]
                }
            }
        ]
    },
    "linkedNodes": {},
    "boundStores": {},
    "boundModels": {}
}