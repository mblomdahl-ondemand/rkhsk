{
    "xdsVersion": "2.1.0",
    "frameworkVersion": "touch20",
    "internals": {
        "type": "controller",
        "reference": {
            "name": "items",
            "type": "array"
        },
        "codeClass": null,
        "userConfig": {
            "designer|userClassName": "RegFormCtrl",
            "designer|userAlias": "regformctrl",
            "models": [
                "MemberRegMobile"
            ],
            "views": [
                "RegFormPanel"
            ]
        },
        "cn": [
            {
                "type": "controllerref",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "RegFormPanel",
                    "ref": "RegFormPanel",
                    "selector": "#regFormPanel",
                    "xtype": "regformpanel"
                }
            },
            {
                "type": "controllerref",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "RegFormSubmitButton",
                    "ref": "RegFormSubmitButton",
                    "selector": "#regFormSubmitButton"
                }
            },
            {
                "type": "fixedfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "init",
                    "fn": "init",
                    "designer|params": [
                        "application"
                    ],
                    "implHandler": [
                        "",
                        "this.control({",
                        "    'personalidfield': {",
                        "        blur: this.onAssholeIdInput",
                        "    },",
                        "    'namefield': {",
                        "        blur: this.onSloppyUserInput",
                        "    },",
                        "    'streetaddressfield': {",
                        "        blur: this.onSloppyUserInput",
                        "    },",
                        "    'cityfield': {",
                        "        blur: this.onSloppyUserInput",
                        "    },",
                        "    'streetnumberfield': {",
                        "        blur: this.onTrimmableInput",
                        "    },",
                        "    'streetentrancefield': {",
                        "        blur: this.onForceUppercaseEntry",
                        "    },",
                        "    'zipcodefield': {",
                        "        blur: this.onTrimmableInput",
                        "    },",
                        "    'phonefield': {",
                        "        blur: this.onTrimmableInput",
                        "    },",
                        "    'emailfield': {",
                        "        blur: this.onForceLowercaseEntry",
                        "    },",
                        "    '#regFormSubmitButton': {",
                        "        tap: this.onRegFormSubmit",
                        "    }",
                        "",
                        "});",
                        ""
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onAssholeIdInput",
                    "fn": "onAssholeIdInput",
                    "designer|params": [
                        "field"
                    ],
                    "implHandler": [
                        "",
                        "if (field.isValid()) {",
                        "    field.setValue(field.getValue().toUpperCase());",
                        "    return true;",
                        "}",
                        "",
                        "var input = Ext.String.trim(field.getValue()).toUpperCase();",
                        "",
                        "// test for 19832010-0221",
                        "if (input.length === 13) {",
                        "    if (input.search(/19\\d{6}-[\\dA-Z]{4}/) !== -1) {",
                        "    field.setValue(input.substring(2));",
                        "    return true;",
                        "} else {",
                        "    return false;",
                        "}",
                        "}",
                        "",
                        "// test for 198320100221",
                        "if (input.length === 12) {",
                        "if (input.search(/19\\d{6}[\\dA-Z]{4}/) !== -1) {",
                        "field.setValue(input.substring(2, 8) + '-' + input.substring(8));",
                        "return true;",
                        "} else {",
                        "return false;",
                        "}",
                        "}",
                        "",
                        "// test for 8320100221",
                        "if (input.length === 10) {",
                        "if (input.search(/[2-9][0-9]\\d{4}[\\dA-Z]{4}/) !== -1) {",
                        "field.setValue(input.substring(0, 6) + '-' + input.substring(6));",
                        "return true;",
                        "} else {",
                        "return false;",
                        "}",
                        "}",
                        "",
                        "field.setValue(input);",
                        ""
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onSloppyUserInput",
                    "fn": "onSloppyUserInput",
                    "designer|params": [
                        "field"
                    ],
                    "implHandler": [
                        "var input = Ext.String.trim(typeof field === 'string' ? field : field.getValue()).toLowerCase(),",
                        "    validLowerCaseWords = ['Von', 'Van', 'Väg', 'Gata'],",
                        "    validUpperCaseWords = ['Ii', 'Iii', 'Iv', 'Vi'];",
                        "",
                        "while (input.indexOf('  ') !== -1) {",
                        "    input = input.replace('  ', ' ');",
                        "}",
                        "",
                        "input = input.replace(' -', '-').replace('- ', '-');",
                        "",
                        "while (input.indexOf('--') !== -1) {",
                        "    input = input.replace('--', '-');",
                        "}",
                        "",
                        "input = input.split(' ');",
                        "",
                        "for (var tmp, i = 0, j = input.length; i < j; i++) {",
                        "    console.log('looping3');",
                        "    input[i] = Ext.String.capitalize(input[i]);",
                        "    if (input[i].split('-').length > 1) {",
                        "        tmp = input[i].split('-');",
                        "        for (var k = 0, l = tmp.length; k < l; k++) {",
                        "            tmp[k] = Ext.String.capitalize(tmp[k]);",
                        "        }",
                        "        input[i] = tmp.join('-');",
                        "    } else {",
                        "        if (input[i].split(':').length > 1) { /// 'III:s gata'",
                        "            tmp = input[i].split(':');",
                        "            if (validUpperCaseWords.indexOf(tmp[0]) !== -1) {",
                        "                tmp[0] = tmp[0].toUpperCase();",
                        "                input[i] = tmp.join(':');",
                        "            }",
                        "        } else {",
                        "            if (validLowerCaseWords.indexOf(input[i]) !== -1) {",
                        "                input[i] = input[i].toLowerCase();",
                        "            } else {",
                        "                for (var k = validUpperCaseWords.length; k--;) {",
                        "                    if (input[i] === validUpperCaseWords[i] + 's') {",
                        "                        input[i] = validUpperCaseWords[i].toUpperCase() + 's';",
                        "                        break;",
                        "                    }",
                        "                }",
                        "            }",
                        "        }",
                        "    }",
                        "}",
                        "",
                        "input = input.join(' ');",
                        "",
                        "if (typeof field === 'string') {",
                        "    return input;",
                        "} else {",
                        "    field.setValue(input);",
                        "}"
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onTrimmableInput",
                    "fn": "onTrimmableInput",
                    "designer|params": [
                        "field"
                    ],
                    "implHandler": [
                        "var input = Ext.String.trim(field.getValue());",
                        "",
                        "field.setValue(input);"
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onForceUppercaseEntry",
                    "fn": "onForceUppercaseEntry",
                    "designer|params": [
                        "field"
                    ],
                    "implHandler": [
                        "var input = Ext.String.trim(field.getValue()).toUpperCase();",
                        "",
                        "field.setValue(input);"
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onForceLowercaseEntry",
                    "fn": "onForceLowercaseEntry",
                    "designer|params": [
                        "field"
                    ],
                    "implHandler": [
                        "var input = Ext.String.trim(field.getValue()).toLowerCase();",
                        "",
                        "field.setValue(input);",
                        ""
                    ]
                }
            },
            {
                "type": "basicfunction",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "onRegFormSubmit",
                    "fn": "onRegFormSubmit",
                    "designer|params": [
                        "button"
                    ],
                    "implHandler": [
                        "var form = button.up('formpanel'),",
                        "    failureTypes = RKHSK.app.failureTypes,",
                        "    data = form.getValues();",
                        "",
                        "data.country = 'Sverige';",
                        "",
                        "console.log(data);",
                        "",
                        "var errors = Ext.create('RKHSK.model.MemberRegMobile', data).validate();",
                        "",
                        "myGlobals.validate = errors;",
                        "",
                        "if (errors.isValid()) {",
                        "",
                        "    form.setMasked({",
                        "        xtype: 'loadmask',",
                        "        message: 'Överför registrerings-information . . .'",
                        "    });",
                        "",
                        "    form.submit({",
                        "        url: '/account/register/',",
                        "        method: 'POST',",
                        "        timeout: 10,",
                        "        success: function (form, action) {",
                        "            myGlobals.successAction = action;",
                        "            Ext.Msg.alert('Medlemsregistrering sparad', 'Du är nu registrerad i RKH-SK:s medlemsdatabas och kan direkt betala in medlemsavgiften via PlusGiro eller studentkårens expedition. Se e-post för den personliga kod som måste bifogas inbetalningen.', function () {",
                        "                form.reset();",
                        "                form.setMasked(false);",
                        "            });",
                        "        },",
                        "        failure: function (form, action) {",
                        "            myGlobals.failureAction = action;",
                        "            var failMsg = RKHSK.app.failureTypes[response.status];",
                        "            if (!failMsg) {",
                        "                failMsg = 'Ett okänt fel uppstod. Var vänlig och skicka en buggrapport till mats.blomdahl@gmail.com för åtgärdande.';",
                        "            }",
                        "            Ext.Msg.alert('Registreringen misslyckades', failMsg, function () {",
                        "                form.setMasked(false);",
                        "            });",
                        "        }",
                        "    });",
                        "",
                        "} else {",
                        "    var errorFeedback = [];",
                        "    Ext.each(errors.items, function (error, index) {",
                        "        errorFeedback.push(function () {",
                        "            Ext.Msg.alert(\"Komplettering krävs\", 'Fel: ' + error.getMessage(), function () {",
                        "                var fn = errorFeedback.pop();",
                        "                if (fn) {",
                        "                    return fn();",
                        "                }",
                        "            });",
                        "        });",
                        "        console.log(error.getMessage());",
                        "    });",
                        "",
                        "    if (errorFeedback.length) {",
                        "        errorFeedback.pop()();",
                        "    }",
                        "",
                        "    return false;",
                        "}"
                    ]
                }
            }
        ]
    },
    "linkedNodes": {},
    "boundStores": {},
    "boundModels": {}
}