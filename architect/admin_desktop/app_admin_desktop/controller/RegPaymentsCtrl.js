/*
 * File: app_admin_desktop/controller/RegPaymentsCtrl.js
 *
 * This file was generated by Sencha Architect version 2.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('RKHSK.controller.RegPaymentsCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.regpaymentsctrl',

    views: [
        'RegPaymentsPanel',
        'PageHeaderStatusContainer'
    ],

    refs: [
        {
            ref: 'UploadManualListButton',
            selector: '#uploadManualListButton',
            xtype: 'button'
        },
        {
            ref: 'UploadTextArea',
            selector: '#uploadManualListTextArea',
            xtype: 'textareafield'
        },
        {
            ref: 'UploadBankStatementButton',
            selector: '#uploadBankStatementButton',
            xtype: 'button'
        },
        {
            ref: 'UploadFileField',
            selector: '#uploadBankStatementFileField',
            xtype: 'filefield'
        },
        {
            ref: 'PaymentRegStatusMsg',
            selector: '#paymentRegStatusMsg',
            xtype: 'statusmsg'
        },
        {
            ref: 'PaymentUploadStatusMsg',
            selector: '#paymentUploadStatusMsg',
            xtype: 'statusmsg'
        },
        {
            ref: 'AttachmentUploadStatusMsg',
            selector: '#attachmentUploadStatusMsg',
            xtype: 'statusmsg'
        }
    ],

    init: function(application) {

        this.control({
            '#uploadManualListButton': {
                click: this.onUploadManualListButtonClick
            },
            '#uploadBankStatementButton': {
                click: this.onUploadBankStatementButtonClick
            },
            '#uploadManualListTextArea': {
                change: this.onUploadTextAreaChange
            },
            '#uploadBankStatementFileField': {
                change: this.onUploadFileFieldChange
            }
        });

        this.bankStatementUploadTimeout = undefined;
        this.bankStatementUploadRequestId = undefined;
        this.bankStatementUploadUri = undefined;

        this.loggedIn = false;

        application.on({
            logout: {
                fn: this.onLogout,
                scope: this
            },
            logincompleted: {
                fn: this.onLoginCompleted,
                scope: this
            }
        });
    },

    refreshBankStatementUploadUri: function(pause) {
        var me = this,
            statusMsg = this.getPaymentUploadStatusMsg(),
            failureTypes = this.application.failureTypes;

        if (pause && (timeout = me.bankStatementUploadTimeout)) {
            window.clearTimeout(timeout);
            me.bankStatementUploadTimeout = undefined;
            if (statusMsg.isVisible()) {
                statusMsg.hide();
            }
            return true;
        }

        var requestId = this.getGaeChannel().generateRequestId();

        statusMsg.setText('Status: Synkroniserar nycklar för betalningsinformation (id=' + requestId + ') . . .');
        statusMsg.show();

        Ext.Ajax.request({
            url    : '/admin/upload/payments/' + requestId,
            method : 'GET',
            timeout: 10e3,
            success: function (response) {
                if (!me.loggedIn) {
                    return false;
                }

                statusMsg.hide();
                statusMsg.setText('Resultat: OK – Nycklar för betalningsinformation uppdaterade.');
                statusMsg.show();

                window.setTimeout(function () {
                    statusMsg.hide();
                }, 3e3);

                me.bankStatementUploadTimeout = window.setTimeout(function () {
                    me.onRefreshPaymentsUri();
                }, 5e5);

                me.bankStatementUploadRequestId = requestId;
                me.bankStatementUploadUri = Ext.decode(response.responseText).uploadToken;
            },
            failure: function (response) {
                if (!me.loggedIn) {
                    return false;
                }

                statusMsg.hide();
                statusMsg.setText('Resultat: FEL – Nycklar för betalningsinformation kunde inte uppdateras.');
                statusMsg.show();

                window.setTimeout(function () {
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – ' + failureTypes[response.status]);
                    statusMsg.show();
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 6e3);
                }, 3e3);

                me.bankStatementUploadTimeout = window.setTimeout(function () {
                    me.onRefreshPaymentsUri();
                }, 18e3);

            }
        });
    },

    onUploadManualListButtonClick: function(button) {
        console.log(button);

        var me = this,
            fieldValues = this.getUploadTextArea().getValue().split('\n'),
            payments = [],
            statusMsg = this.getPaymentRegStatusMsg();

        for (var i = fieldValues.length; i--;) {
            paymentDate = fieldValues[i].match(/\d{4}\-\d{2}\-\d{2}/);
            paymentKey = fieldValues[i].match(/[a-z]{5}/i);
            if (paymentDate && paymentKey) {
                payments.push({
                    lastPaymentKey     : paymentKey[0],
                    lastPaymentRecieved: paymentDate[0]
                });
            }
        }

        if (payments.length) {
            var callbackRecieved = false,
                requestId = this.getGaeChannel().generateRequestId(),
                loadingMsg = new Ext.LoadMask(me.getAdminPageD(), {
                    title: 'Verifiering',
                    msg: 'Inbetalningar verifieras . . .'
                });

            statusMsg.setText('Status: Inbetalningslista överförs till servern för verifiering (id=' + requestId + ') . . .');
            statusMsg.show();
            loadingMsg.show();

            function responseHandler(remoteEvent, data) {
                if (callbackRecieved) {
                    return false;
                }
                else {
                    callbackRecieved = true;
                }
                //window.setTimeout(function () {
                //    me.refreshPaymentsUri();
                //}, 3e3);
                if (!data) {
                    loadingMsg.hide();

                    window.setTimeout(function () {
                        statusMsg.hide();
                        me.onRefreshPaymentsUri();
                    }, 5e3);

                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – Listan över inbetalningar kunde inte överföras (id=' + requestId + ').');
                    statusMsg.show();

                    Ext.Msg.alert('Överföringen misslyckades', 'Överföringen kunde inte genomföras, av en eller annan anledning. Var vänlig och kontrollera din internetanslutning, försök igen och – om det fortfarande inte fungerar – skicka en buggrapport till mats.blomdahl@gmail.com');

                } else {
                    loadingMsg.hide();

                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);

                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – Inbetalningslistan överförd och analyserad (id=' + requestId + ').');
                    statusMsg.show();

                    me.getRecognizedMembersListStore().add(data.itemsRecognized);
                    me.getUnidentifiedMembersListStore().add(data.itemsUnidentified);

                    Ext.create('RKHSK.view.AdminConfirmPaymentsD', {
                        renderTo       : Ext.getBody(),
                        contentCategory: 'datafile'
                    }).show();
                }
            }

            var gaeChannel = me.getGaeChannel(),
                callbackRecieved = false;

            gaeChannel.addListener(responseHandler, { requestId: requestId, one: true });

            Ext.Ajax.request({
                url      : '/admin/update/' + requestId,
                method   : 'POST',
                waitTitle: 'Laddar upp listning ...',
                waitMsg  : 'Din lista över inbetalningar överförs till servern.',
                timeout  : 1e4,
                success  : function () {
                    console.log('success');
                },
                failure  : function () {
                    console.log('failure');
                },
                jsonData : { payments_verification: payments }
            });

            window.setTimeout(responseHandler, 9e3);
        }

    },

    onUploadBankStatementButtonClick: function(button) {
        console.log(button);


        var me = this,
            uploadForm = button.up('form').getForm(),
            requestId = me.bankStatementUploadRequestId;

        if (!requestId) {
            return false;
        }

        if (uploadForm.isValid()) {
            me.refreshBankStatementUploadUri(true);
            var statusMsg = me.getPaymentRegStatusMsg();

            statusMsg.setText('Status: Kontoutdrag överförs till servern för analys (id=' + requestId + ') . . .');
            statusMsg.show();

            function responseHandler(remoteEvent, data) {
                if (callbackRecieved) {
                    return false;
                }
                else {
                    callbackRecieved = true;
                }
                button.disable();
                me.refreshBankStatementUploadTimeout = window.setTimeout(function () {
                    me.refreshStatementUploadUri();
                }, 3e3);
                if (!data) {
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 5e3);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: FEL – Datafilen kunde inte överföras (id=' + requestId + ').');
                    statusMsg.show();
                    Ext.Msg.alert('Överföringen misslyckades', 'Överföringen kunde inte genomföras, av en eller annan anledning. Var vänlig och kontrollera din internetanslutning, försök igen och – om det fortfarande inte fungerar – skicka en buggrapport till mats.blomdahl@gmail.com');
                } else {
                    window.setTimeout(function () {
                        statusMsg.hide();
                    }, 3e3);
                    statusMsg.hide();
                    statusMsg.setText('Resultat: OK – Datafilen har överförts och analyserats (id=' + requestId + ').');
                    statusMsg.show();
                    me.application.globals.uploadSuccessAction = data;
                    me.getRecognizedMembersListStore().add(
                    data.itemsRecognized
                    );
                    me.getUnidentifiedMembersListStore().add(
                    data.itemsUnidentified
                    );
                    Ext.create('RKHSK.view.AdminConfirmPaymentsD', {
                        renderTo       : Ext.getBody(),
                        contentCategory: 'datalist'
                    }).show();
                    //me.getAdminConfirmPaymentsD()
                }
            }

            var gaeChannel = me.getGaeChannel(),
                callbackRecieved = false;

            gaeChannel.addListener(responseHandler, { requestId: requestId, one: true });

            uploadForm.submit({
                url      : me.bankStatementUploadUri,
                waitTitle: 'Laddar upp data ...',
                waitMsg  : 'Din datafil överförs till servern.',
                timeout  : 1e4,
                type     : 'submit',
                reset    : false,
                success  : function () {
                    console.log('success');
                },
                failure  : function () {
                    console.log('failure');
                }
            });
            window.setTimeout(responseHandler, 95e2);
        }

    },

    onUploadTextAreaChange: function(field) {
        console.log(field);

        var uploadButton = this.getUploadManualListButton();
        if (field.isValid()) {
            if (uploadButton.isDisabled()) {
                uploadButton.enable()
            }
        } else if (!uploadButton.isDisabled()) {
            uploadButton.disable()
        }

    },

    onUploadFileFieldChange: function(field) {
        console.log(field);

        var uploadButton = this.getUploadBankStatementButton();
        if (field.isValid()) {
            if (uploadButton.isDisabled()) {
                uploadButton.enable()
            }
        } else if (!uploadButton.isDisabled()) {
            uploadButton.disable()
        }
    },

    onLogout: function() {

        this.loggedIn = false;
        this.refreshBankStatementUploadUri(true);
    },

    onLoginCompleted: function() {

        this.loggedIn = true;
    }

});
