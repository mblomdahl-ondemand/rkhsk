/*
 * File: app_admin_desktop/model/MemberRegDesktop.js
 *
 * This file was generated by Sencha Architect version 2.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('RKHSK.model.MemberRegDesktop', {
    extend: 'Ext.data.Model',

    alternateClassName: [
        'MemberReg'
    ],

    idProperty: 'personalId',

    fields: [
        {
            name: 'admittedSemester',
            type: 'string'
        },
        {
            name: 'lastApplicationFor',
            type: 'string'
        },
        {
            name: 'personalId',
            type: 'string'
        },
        {
            name: 'firstName',
            type: 'string'
        },
        {
            name: 'lastName',
            type: 'string'
        },
        {
            name: 'phone',
            type: 'string'
        },
        {
            name: 'email',
            type: 'string'
        },
        {
            name: 'streetCo',
            type: 'string'
        },
        {
            name: 'streetAddress',
            type: 'string'
        },
        {
            name: 'streetNumber',
            type: 'string'
        },
        {
            name: 'streetEntrance',
            type: 'string'
        },
        {
            name: 'streetApartment',
            type: 'string'
        },
        {
            name: 'streetFloor',
            type: 'string'
        },
        {
            name: 'zipCode',
            type: 'string'
        },
        {
            name: 'city',
            type: 'string'
        },
        {
            name: 'country',
            type: 'string'
        },
        {
            name: 'studentConfirmation',
            type: 'string'
        }
    ]
});