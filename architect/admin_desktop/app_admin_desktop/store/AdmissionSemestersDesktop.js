/*
 * File: app_admin_desktop/store/AdmissionSemestersDesktop.js
 *
 * This file was generated by Sencha Architect version 2.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('RKHSK.store.AdmissionSemestersDesktop', {
    extend: 'Ext.data.Store',

    alternateClassName: [
        'AdmissionSemesters'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
            storeId: 'admissionSemesters',
            data: [
                {
                    name: 'HK12',
                    value: 'hk12'
                },
                {
                    name: 'VK12',
                    value: 'vk12'
                },
                {
                    name: 'HK11',
                    value: 'hk11'
                },
                {
                    name: 'VK11',
                    value: 'vk11'
                },
                {
                    name: 'HK10',
                    value: 'hk10'
                },
                {
                    name: 'VK10',
                    value: 'vk10'
                },
                {
                    name: 'HK09',
                    value: 'hk09'
                },
                {
                    name: 'VK09',
                    value: 'vk09'
                },
                {
                    name: 'HK08',
                    value: 'hk08'
                },
                {
                    name: 'VK08',
                    value: 'vk08'
                },
                {
                    name: 'HK07',
                    value: 'hk07'
                },
                {
                    name: 'VK07',
                    value: 'vk07'
                },
                {
                    name: 'HK06',
                    value: 'hk06'
                },
                {
                    name: 'VK06',
                    value: 'vk06'
                },
                {
                    name: 'Specialistutb./VUB',
                    value: 'SPEC'
                }
            ],
            fields: [
                {
                    name: 'name',
                    type: 'string'
                },
                {
                    name: 'value',
                    type: 'string'
                }
            ]
        }, cfg)]);
    }
});