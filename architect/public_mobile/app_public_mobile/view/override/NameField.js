Ext.define('RKHSK.view.override.NameField', {
    override: 'RKHSK.view.NameField',
    initialize: function () {
        var me = this;

        me.setMaxLength(30);
        me.callParent();
        me.getComponent().element.dom.children[0].addEventListener('keypress', function (e) {
            var charCode = e.charCode || e.keyCode,
                input = String.fromCharCode(charCode),
                maskRe = /[A-Za-z éüÜåäöÅÄÖ\-]/;

            if (!maskRe.test(input)) {
                e.stopPropagation();
                e.preventDefault();
            }
        }, false);
    },
    isValid   : function () {
        //console.error(this.getValue());
        return true;
    }
});