Ext.define('RKHSK.view.override.EmailField', {
    override: 'RKHSK.view.EmailField',
    initialize: function () {
        var me = this;

        me.setMaxLength(30);
        me.callParent();
        me.getComponent().element.dom.children[0].addEventListener('keypress', function (e) {
            var charCode = e.charCode || e.keyCode,
                input = String.fromCharCode(charCode),
                maskRe = /[a-z\d@_\.\-]/i;

            if (!maskRe.test(input)) {
                e.stopPropagation();
                e.preventDefault();
            }
        }, false);
    },
    isValid: function () {
        var matcher = /^([a-z\d_\.\-])+\@(([a-z\d\-])+\.)+([a-z\d]{2,4})+$/i;
        if (matcher.test(this.getValue())) {
            return true;
        } else {
            return false;
        }
    }
});