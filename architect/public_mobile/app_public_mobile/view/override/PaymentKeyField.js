Ext.define('RKHSK.view.override.PaymentKeyField', {
    override: 'RKHSK.view.PaymentKeyField',
    initialize: function () {
        var me = this;

        me.setMaxLength(5);
        me.callParent();
        me.getComponent().element.dom.children[0].addEventListener('keypress', function (e) {
            var charCode = e.charCode || e.keyCode,
                input = String.fromCharCode(charCode),
                maskRe = /[a-z]/i;

            if (!maskRe.test(input)) {
                e.stopPropagation();
                e.preventDefault();
            }
        }, false);
    },
    
    isValid: function () {
        var matcher = /^[a-z]{5}$/i;
        if (matcher.test(this.getValue())) {
            return true;
        } else {
            return false;
        }
    }
});