Ext.define('RKHSK.view.override.StreetEntranceField', {
    override: 'RKHSK.view.StreetEntranceField',
    initialize: function () {
        var me = this;

        me.setMaxLength(1);
        me.callParent();
        me.getComponent().element.dom.children[0].addEventListener('keypress', function (e) {
            var charCode = e.charCode || e.keyCode,
                input = String.fromCharCode(charCode),
                maskRe = /[a-z]/i;

            if (!maskRe.test(input)) {
                e.stopPropagation();
                e.preventDefault();
            }
        }, false);
    },
    isValid: function () {
        //console.error(this.getValue());
        return true;
    }
    
});