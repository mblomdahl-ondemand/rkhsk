{
    "xdsVersion": "2.1.0",
    "frameworkVersion": "touch20",
    "internals": {
        "type": "arraystore",
        "reference": {
            "name": "items",
            "type": "array"
        },
        "codeClass": null,
        "userConfig": {
            "designer|userClassName": "OpenSemestersMobile",
            "designer|userAlias": "opensemesters",
            "alternateClassName": [
                "OpenSemesters"
            ],
            "autoLoad": true,
            "clearOnPageLoad": false,
            "data": "[\n            { \"name\": \"Ej anmäld\", \"value\": \"\" },\n            { \"name\": \"HT12\", \"value\": \"HT12\" },\n            { \"name\": \"VT12\", \"value\": \"VT12\" },\n            { \"name\": \"HT11\", \"value\": \"HT11\" }\n            ]",
            "storeId": "OpenSemesters"
        },
        "cn": [
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField17",
                    "name": "name",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField18",
                    "name": "value",
                    "type": "string"
                }
            }
        ]
    },
    "linkedNodes": {},
    "boundStores": {},
    "boundModels": {}
}