{
    "xdsVersion": "2.1.0",
    "frameworkVersion": "touch20",
    "internals": {
        "type": "arraystore",
        "reference": {
            "name": "items",
            "type": "array"
        },
        "codeClass": null,
        "userConfig": {
            "designer|userClassName": "AdmissionSemesterMobile",
            "designer|userAlias": "admissionsemester",
            "alternateClassName": [
                "AdmissionSemester"
            ],
            "autoLoad": true,
            "clearOnPageLoad": false,
            "data": "[\n    { \"name\": \"\", \"value\": \"\" },\n    { \"name\": \"HK12\", \"value\": \"hk12\" },\n    { \"name\": \"VK12\", \"value\": \"VK12\" },\n    { \"name\": \"HK11\", \"value\": \"HK11\" },\n    { \"name\": \"VK11\", \"value\": \"VK11\" },\n    { \"name\": \"HK10\", \"value\": \"HK10\" },\n    { \"name\": \"VK10\", \"value\": \"VK10\" },\n    { \"name\": \"HK09\", \"value\": \"HK09\" },\n    { \"name\": \"VK09\", \"value\": \"VK09\" },\n    { \"name\": \"HK08\", \"value\": \"HK08\" },\n    { \"name\": \"VK08\", \"value\": \"VK08\" },\n    { \"name\": \"HK07\", \"value\": \"HK07\" },\n    { \"name\": \"VK07\", \"value\": \"VK07\" },\n    { \"name\": \"HK06\", \"value\": \"HK06\" },\n    { \"name\": \"VK06\", \"value\": \"VK06\" },\n    { \"name\": \"Specialistutb./VUB\", \"value\": \"SPEC\"}\n]\n",
            "storeId": "AdmissionSemester"
        },
        "cn": [
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField17",
                    "name": "name",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField18",
                    "name": "value",
                    "type": "string"
                }
            }
        ]
    },
    "linkedNodes": {},
    "boundStores": {},
    "boundModels": {}
}