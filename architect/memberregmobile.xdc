{
    "xdsVersion": "2.1.0",
    "frameworkVersion": "touch20",
    "internals": {
        "type": "datamodel",
        "reference": {
            "name": "items",
            "type": "array"
        },
        "codeClass": null,
        "userConfig": {
            "designer|userClassName": "MemberRegMobile",
            "designer|userAlias": "memberreg",
            "alternateClassName": [
                "MemberReg"
            ],
            "idProperty": "personalId"
        },
        "cn": [
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField",
                    "name": "personalId",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField1",
                    "name": "admittedSemester",
                    "type": "string"
                },
                "cn": [
                    {
                        "type": "fixedfunction",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "designer|userClassName": "convert",
                            "fn": "convert",
                            "designer|params": [
                                "v",
                                "rec"
                            ],
                            "implHandler": [
                                "if (Ext.isArray(v)) {",
                                "    return v[0];",
                                "} else {",
                                "    return v;",
                                "}"
                            ]
                        }
                    }
                ]
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField2",
                    "name": "lastApplicationFor",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField3",
                    "name": "firstName",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField4",
                    "name": "lastName",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField5",
                    "name": "phone",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField6",
                    "name": "email",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField7",
                    "name": "streetCo",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField8",
                    "name": "streetAddress",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField9",
                    "name": "streetNumber",
                    "type": "string"
                },
                "cn": [
                    {
                        "type": "fixedfunction",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "designer|userClassName": "convert",
                            "fn": "convert",
                            "designer|params": [
                                "v",
                                "rec"
                            ],
                            "implHandler": [
                                "if (Ext.isArray(v)) {",
                                "    return v[0];",
                                "} else {",
                                "    return v;",
                                "}"
                            ]
                        }
                    }
                ]
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField10",
                    "name": "streetEntrance",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField11",
                    "name": "streetApartment",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField12",
                    "name": "streetFloor",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField13",
                    "name": "zipCode",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField14",
                    "name": "city",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField15",
                    "name": "country",
                    "type": "string"
                }
            },
            {
                "type": "datafield",
                "reference": {
                    "name": "fields",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyField16",
                    "name": "studentConfirmation",
                    "type": "string"
                }
            },
            {
                "type": "ajaxproxy",
                "reference": {
                    "name": "proxy",
                    "type": "object"
                },
                "codeClass": null,
                "userConfig": {
                    "designer|userClassName": "MyAjaxProxy",
                    "designer|displayName": "MemberRegProxy",
                    "url": "/account/load/"
                },
                "cn": [
                    {
                        "type": "jsonreader",
                        "reference": {
                            "name": "reader",
                            "type": "object"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "designer|userClassName": "MyJsonReader",
                            "designer|displayName": "MemberRegReader",
                            "idProperty": "personalId",
                            "rootProperty": "memberData"
                        }
                    }
                ]
            },
            {
                "type": "inclusionvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Du måste ange vilken termin medlemsansökan anger. Se rubrik 1.",
                    "designer|userClassName": "MyInclusionValidation",
                    "designer|displayName": "lastApplicationForInclusionValidation",
                    "field": "lastApplicationFor",
                    "list": [
                        "['HT12']"
                    ]
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "lastApplicationForPresenceValidation",
                    "field": "lastApplicationFor"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^(|SPEC|[HV]K\\d{2})$/"
                    ],
                    "message": "Årskursen innehåller ogiltiga tecken. Fältet \"årskurs\" kan enbart innehålla värden på formatet [HV]K<YY>. Se rubrik 2.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "admittedSemesterFormatValidation",
                    "field": "admittedSemester"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"årskurs\" är obligatoriskt. Se rubrik 2.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "admittedSemesterPresenceValidation",
                    "field": "admittedSemester"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^(|[4-9][0-9](0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-2])\\-[A-Z\\d]\\d{3})$/"
                    ],
                    "message": "Ogiltigt personnr. Personnummer måste anges på formen YYMMDD-NNNN. Exempel: \"860722-0332\" – där \"86\" markerar år 1986, \"07\" juli månad, \"22\" dag 22 i juli månad och \"0332\" de sista fyra siffrorna i personnumret. Se rubrik 2.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "personalIdFormatValidation",
                    "field": "personalId"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"personnummer\" är obligatoriskt. Se rubrik 2.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "personalIdPresenceValidation",
                    "field": "personalId"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^[A-Za-z éüÜåäöÅÄÖ\\-]*$/"
                    ],
                    "message": "Ditt förnamn innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg. Se rubrik 2.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "firstNameFormatValidation",
                    "field": "firstName"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"förnamn\" är obligatoriskt. Se rubrik 2.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "firstNamePresenceValidation",
                    "field": "firstName"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^[A-Za-z éüÜåäöÅÄÖ\\-]*$/"
                    ],
                    "message": "Ditt efternamn innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg. Se rubrik 2.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "lastNameFormatValidation",
                    "field": "lastName"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"efternamn\" är obligatoriskt. Se rubrik 2.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "lastNamePresenceValidation",
                    "field": "lastName"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^[A-Za-z :åäöÅÄÖ\\-]*$/"
                    ],
                    "message": "Gatuadressen innehåller ogiltiga tecken. Fältet \"gatuadress\" kan enbart innehålla en begränsad teckenuppsättning, närmare besämt a-ö, bindestreck och blanksteg. För gatunummer och portuppgång, se fälten \"gatunr.\" resp. \"port\" (rubrik 3).",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "streetAddressFormatValidation",
                    "field": "streetAddress"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"gatuadress\" är obligatoriskt. Se rubrik 3.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "streetAddressPresenceValidation",
                    "field": "streetAddress"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^(|\\d{1,3}|\\d{1,3}\\-\\d{1,3})$/"
                    ],
                    "message": "Gatunumret innehåller ogiltiga tecken eller har en ogiltig längd. Ett gatunummer kan enbart innehålla siffror från 0-9. För portuppgång (exempelvis \"B\" i \"2B\") resp. våningsplan, se fälten \"portnr.\" resp. \"våning\" (rubrik 3).",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "streetNumberFormatValidation",
                    "field": "streetNumber"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"gatunummer\" är obligatoriskt. Se rubrik 3.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "streetNumberPresenceValidation",
                    "field": "streetNumber"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^(|[A-Z])$/"
                    ],
                    "message": "Fältet \"portuppgång\" innehåller ogiltiga tecken eller har en ogiltig längd. För våningsplan (exempelvis \"N.B.\" i \"Hemgatan 4A, N.B.\") se fältet \"våning\" (rubrik 3).",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "streetEntranceFormatValidation",
                    "field": "streetEntrance"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^\\d{0,4}$/"
                    ],
                    "message": "Lägenhetsnumret innehåller ogiltiga tecken eller har en ogiltig längd. Se rubrik 3.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "streetApartmentFormatValidation",
                    "field": "streetApartment"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^[A-Za-z åäöÅÄÖ\\-]*$/"
                    ],
                    "message": "Postorten innehåller ogiltiga tecken. Fältet \"postort\" kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg. Se rubrik 3.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "cityFormatValidation",
                    "field": "city"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"postort\" är obligatoriskt. Se rubrik 3.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "cityPresenceValidation",
                    "field": "city"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^(|\\d{5})$/"
                    ],
                    "message": "Postnumret innehåller ogiltiga tecken eller har en ogiltig längd. Korrekt format är \"NNNNN\" (exempelvis \"11421\", utan blanksteg). Se rubrik 3.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "zipCodeFormatValidation",
                    "field": "zipCode"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"postnummer\" är obligatoriskt. Se rubrik 3.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "zipCodePresenceValidation",
                    "field": "zipCode"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^(|Sverige)$/"
                    ],
                    "message": "Fältet \"Land\" accepterar för närvarande bara ett värde (Sverige) – ta kontakt med medlemsregistreringen på regga@rkh-sk.se om du saknar svensk adress. Se rubrik 3.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "countryFormatValidation",
                    "field": "country"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"land\" är obligatoriskt. Se rubrik 3.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "countryPresenceValidation",
                    "field": "country"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^(|([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+)$/"
                    ],
                    "message": "Fältet \"epost-adress\" är felformaterat. Korrekt format är \"user@mailprovider.com\". Se rubrik 4.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "emailFormatValidation",
                    "field": "email"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Fältet \"epost-adress\" är obligatoriskt. Det är viktigt att du anger en korrekt adress här eftersom dina personliga uppgifter för inbetalning av medlemsavgift överförs via mail. Se rubrik 4.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "emailPresenceValidation",
                    "field": "email"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "formatvalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "matcher": [
                        "/^$()|^[\\+\\d][\\d \\-]{9,18}$/"
                    ],
                    "message": "Ogiltigt telefonnr. Fältet \"telefonnummer\" kan enbart innehålla en begränsad teckenuppsättning, accepterade tecken är + (som första tecken), 0-9, blanksteg och bindestreck. Exempel på giltiga telefonnummer är \"+4672-522 32 33\" och \"08 920 20 25\". Se rubrik 4.",
                    "designer|userClassName": "MyFormatValidation",
                    "designer|displayName": "phoneFormatValidation",
                    "field": "phone"
                },
                "configAlternates": {
                    "matcher": "object"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "matcher",
                        "type": "string"
                    },
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            },
            {
                "type": "presencevalidation",
                "reference": {
                    "name": "validations",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "message": "Du måste vara studieaktiv vid Röda Korsets Högskola för att kunna vara med i studentkåren.",
                    "designer|userClassName": "MyPresenceValidation",
                    "designer|displayName": "studentConfirmationPresenceValidation",
                    "field": "studentConfirmation"
                },
                "customConfigs": [
                    {
                        "group": "(Custom Properties)",
                        "name": "message",
                        "type": "string"
                    }
                ]
            }
        ]
    },
    "linkedNodes": {},
    "boundStores": {},
    "boundModels": {}
}