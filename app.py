#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   Default Handler
# Author:  Mats Blomdahl
# Version: 2012-08-25

import logging

from webapp2 import WSGIApplication, Route
from webapp2_extras import sessions
from rkhsk.model.generic.time import SwedishTzInfo
from rkhsk.model.generic.time import UtcTzInfo

config = {
    # global constants
    '_DEVMODE': True,
    '_ACTIVE_ADMINS': 'admin_active_users',
    '_CONNECTED_ADMINS': 'admin_connected_channels',
    '_ADMIN_COOKIE_EXPIRATION': 600,
    '_SESSION_DEFAULT_EXPIRATION': 86400,
    '_STD_ENC': 'utf_8',
    '_SWE_TZINFO': SwedishTzInfo( ),
    '_UTC_TZINFO': UtcTzInfo( ),

    # session management
    'session_dict': sessions.SessionDict( {}, data=None, new=False ),
    'webapp2_extras.sessions': {
        'secret_key': 'my-super-s3cret-key-modz',
        'cookie_name': 'RKHSK',
        'cookie_args': {
            'path': '/',
            'max_age': 600
        }
    }
}

# usage:
#_UTC_TZINFO = self.app.config.get('_UTC_TZINFO')
#_SWE_TZINFO = self.app.config.get('_SWE_TZINFO')
#_STD_ENC = self.app.config.get('_STD_ENC')
#_SESSION_DEFAULT_EXPIRATION = self.app.config.get('_SESSION_DEFAULT_EXPIRATION')
#_ADMIN_COOKIE_EXPIRATION = self.app.config.get('_ADMIN_COOKIE_EXPIRATION')
#_CONNECTED_ADMINS = self.app.config.get('_CONNECTED_ADMINS')
#_ACTIVE_ADMINS = self.app.config.get('_ACTIVE_ADMINS')


def handle_404(request, response, exception):
    logging.exception( exception )
    response.write( 'Oops! I could swear this page was here!' )
    response.set_status( 404 )


def handle_500(request, response, exception):
    logging.exception( exception )
    response.write( 'A server error occurred!' )
    response.set_status( 500 )


app = WSGIApplication( routes=[
    Route('/_ah/mail/<email_address:.+>', handler='rkhsk.handler.sys.EmailInterface', methods=['POST']),
    Route('/tasks/<task_name:(\w+)>/<cmd:(|\w+)>', handler='rkhsk.handler.sys.TaskInterface', methods=['POST']),
    Route('/admin/<service:(upload)>/<file_type:(\w+)>/<request_id:($|\w+)>', handler='rkhsk.handler.sys.BlobInterface', methods=['POST']),
    Route('/admin/<service:(serve)>/<file_type:(\w+)>/<request_id:($|\w+)>', handler='rkhsk.handler.sys.BlobInterface', methods=['GET']),

    Route('/admin/<service:(upload)>/<cmd:(\w+)>/<request_id:($|\w+)>', handler='rkhsk.handler.admin.AdminApiInterface', methods=['GET']),
    Route('/admin/mail/<cmd:($|.+)>', handler='rkhsk.handler.admin.MailHandler', methods=['GET', 'POST']),
    Route('/admin/<service:(\w+)>/<cmd:(\w+)>/<request_id:(\w+)>', handler='rkhsk.handler.admin.AdminApiInterface', methods=['GET', 'POST']),
    Route('/admin/<service:(\w+)>/<cmd:(|\w+)>', handler='rkhsk.handler.admin.AdminApiInterface', methods=['GET', 'POST']),
    Route('/<service:(admin|app_am|app_ad)>', handler='rkhsk.handler.admin.AdminFrontPageInterface', methods=['GET']),

    Route('/account/<service:(\w+)>/<cmd:(|\w+)>', handler='rkhsk.handler.user.AccountApiInterface', methods=['GET', 'POST']),

    Route('/<service:(|app_pd|app_pm)>', handler='rkhsk.handler.public.FrontPageInterface', methods=['GET']),

    Route('/devcmd/<param:(clear_cookies)>', handler='rkhsk.handler.DevCmdHandler', methods=['GET'])
], config=config, debug=True )

app.error_handlers[ 404 ] = handle_404
app.error_handlers[ 500 ] = handle_500

def main():
    app.run( )

if __name__ == '__main__':
    main( )

