#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   AccountInterfacehandler
# Author:  Mats Blomdahl
# Version: 2012-03-18

import logging
import random
import time

from webapp2 import RequestHandler, cached_property
from webapp2_extras.sessions import SessionStore
from webapp2_extras import sessions

from google.appengine.api import taskqueue

from generic_ui import UniHandler
from rkhsk_handler.command import AccountCommand
from rkhsk_model.member import RkhskMember, AccountEmail, AccountUri, PaymentKey, SysTmpKey

class AccountInterfaceHandler(RequestHandler):
  """The API interface for the web application's registration page served at
    http://regga.rkh-sk.se/
    
  The request handler routes all incomming request through self.__unihandler
    where a command is set up and the appropriate method is called.
    
  The handler deploys secure cookies using the 'account' store and expiration
    is controlled by the global variable SESSION_DEFAULT_EXPIRATION.
  """
  
  # session management
  def dispatch(self):
    # Get a session store for this request.
    self.session_store = sessions.get_store(request=self.request)
    
    try:
      # Dispatch the request.
      RequestHandler.dispatch(self)
    finally:
      # Save all sessions.
      self.session_store.save_sessions(self.response)
  
  @cached_property
  def session(self):
    # Returns a session using the default cookie key.
    return self.session_store.get_session(name='default', max_age = SESSION_DEFAULT_EXPIRATION, backend = 'securecookie')
  
  
  # redirects
  def get(self, service, cmd):
    """Handler for redirecting HTTP-GET requests to the member interface."""
    
    return self.__unihandler('GET', cmd, param)
  
  def post(self, service, cmd):
    """Handler for redirecting HTTP-POST requests to the member interface."""
    
    return self.__unihandler('POST', service, cmd)
  
  def __unihandler(self, method, service, cmd):
    """Method for delegating requested functionality to the appropriate service.
      
    Arguments:
      method: The HTTP method used (i.e. 'GET' or 'POST').
      service: The main functionality requested.
      cmd: An optional, service-specific, command.
    """
    
    starttime = time.time()
    
    command = AccountCommand(
      request = self.request,
      response = self.response,
      method = method,
      service = service,
      cmd = cmd,
      session_id = self.session.get('session_id'),
      user_id = self.session.get('user_id'),
      dev_mode = DEVMODE
    )
    
    unihandler = UniHandler(handler_name = 'AccountInterfaceHandler.__unihandler()', command = command, dev_mode = DEVMODE)
    
    unihandler.handle_request()
    
    unihandler.serve_response(loadtime = { 'loadTime': '%s ms' % str(int((time.time() - starttime)*1000)) })
  
  
  # /account/retrieve/
  def retrieve(self, command):
    """Method for retrieving account data to the details/status view on the web
      application's registration page.
      
    Arguments:
      param_data['loginUsername']: The member's email address (passed through
        AccountCommand).
      param_data['loginCredentials']: One of the member's recent payment keys
        (passed through AccountCommand).
      
    Returns:
      A JSON object with pretty-print member data.
    """
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 403,
        'errors'     : { 'invalidSession': 'The user\'s session is invalid.' }
      }
    
    command.set_action('user_details_retrieval')
    command.set_description('A user-initiated account details retrieval command.')
    command.set_valid_keys([
      'firstName',
      'lastName',
      'lastApplicationFor',
      'admittedSemester',
      'email',
      'phone',
      'studentConfirmation'
    ])
    
    try:
      email = command.param_data['loginUsername'].lower()
      payment_key = command.param_data['loginCredentials'].upper()
      logging.info('AccountInterfaceHandler.__retrieve().loginUsername: %s' % email)
      logging.info('AccountInterfaceHandler.__retrieve().loginCredentials: %s' % email)
    except (KeyError):
      return {
        'success'    : False,
        'status_code': 400,
        'errors'     : { 'invalidArguments': 'Required arguments are missing.' }
      }
    
    if re.search('^[A-Z]{5}$', payment_key) is not None and re.search('^[a-z_\-\.\d]+@[a-z_\-\.\d]+\.[a-z]{2,4}$', email, flags=re.I) is not None:
      member_connector = PaymentKey.get_by_key_name(payment_key)
      if not member_connector: # PaymentKey instance not found
        return {
          'success'    : False,
          'status_code': 403,
          'errors'     : { 'invalidCredentials': 'Submitted credentials does not match a registered member.' }
        }
      
      member = member_connector.memberRef
      if member and member.email == email: # Compile a JSON response object
        street_address = '%s %s%s' % (member.streetAddress, member.streetNumber, member.streetEntrance)
        if member.streetApartment:
          street_address += ' / lgh. %s' % member.streetApartment
        if member.streetFloor:
          street_address += ', %s' % member.streetFloor
        street_address += '<br>SE-%s  %s' % (member.zipCode, member.city)
        if member.streetCo:
          street_address = 'c/o %s<br>%s' % (member.streetCo, street_address)
        
        application_semester_history = '[ Inga anmälningar registrerade ]'
        for i in range(len(member.applicationSemesterHistory)):
          if i == 0:
            application_semester_history = ''
          else:
            application_semester_history += ', '
          application_semester_history += '%s (%s)' % (member.applicationSemesterHistory[i], member.applicationDateHistory[i].strftime('%y-%m-%d'))
        
        try:
          last_payment_recieved = member.lastPaymentRecieved.strftime('%Y-%m-%d')
        except (NameError, AttributeError):
          last_payment_recieved = '[ Inga betalningar registrerade ]'
        
        membership_history = '[ Tidigare medlemskap saknas ]'
        if (len(member.membershipHistory)):
          membership_history = member.membershipHistory[0]
          for i in range(len(member.membershipHistory)):
            if i == 0:
              membership_history = ''
            else:
              membership_history += ', ' 
            membership_history += member.membershipHistory[i]
        
        mecenat_history = '[ Tidigare registreringar saknas ]'
        for i in range(len(member.mecenatHistory)):
          if i == 0:
            mecenat_history = ''
          else:
            mecenat_history += ', '
          mecenat_history += member.mecenatHistory[i]
        
        ssco_history = '[ Tidigare registreringar saknas ]'
        for i in range(len(member.sscoHistory)):
          if i == 0:
            ssco_history = ''
          else:
            ssco_history += ', '
          ssco_history += member.sscoHistory[i]
        
        member_data = command.get_account_data(member)
        member_data.update({
          'lastPaymentRecieved'       : last_payment_recieved,
          'applicationSemesterHistory': application_semester_history,
          'sscoHistory'               : ssco_history,
          'mecenatHistory'            : mecenat_history,
          'membershipHistory'         : membership_history,
          'streetAddress'             : street_address
        })
        
        if member.sys_email_verified is False:
          member.set_email_verified()
          member.put()
        
        return {
          'success': True,
          'data'   : member_data
        }
    
    return {
      'success'    : False,
      'status_code': 403,
      'errors'     : { 'invalidCredentials': 'Submitted credentials does not match a registered member.' }
    }
  
  
  # /account/register/
  def register(self, command):
    """Method for recieving new membership applications.
      
    Arguments:
      param_data['admittedSemester']: The member's admission semester.
      param_data['lastApplicationFor']: The target application semester.
      param_data['firstName']: The member's first name.
      param_data['lastName']: The member's last name.
      param_data['streetCo']: The member's c/o address (optional).
      param_data['streetAddress']: The member's street address.
      param_data['streetNumber']: The member's street address number.
      param_data['streetEntrance']: The member's street entrance (optional).
      param_data['streetApartment']: The member's apartment number (optional).
      param_data['streetFloor']: The member's building floor (optional).
      param_data['zipCode']: The member's zip code.
      param_data['city']: The member's city.
      param_data['country']: The member's country.
      param_data['email']: The member's email address.
      param_data['phone']: The member's phone number (optional).
      param_data['studentConfirmation']: The member's confirmation of currently
        studying at RKH.
      
    Returns:
      A JSON object with the registered member data.
    """
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 403,
        'errors'     : { 'invalidSession': 'The user\'s session is invalid.' }
      }
    
    command.set_action('user_registration')
    command.set_description('A user-initiated registration.')
    command.set_valid_keys([
      'admittedSemester',
      'lastApplicationFor',
      'firstName',
      'lastName',
      'streetCo',
      'streetAddress',
      'streetNumber',
      'streetEntrance',
      'streetApartment',
      'streetFloor',
      'zipCode',
      'city',
      'country',
      'email',
      'phone',
      'studentConfirmation'
    ])
    
    if command.param_data['studentConfirmation']:
      command.param_data['studentConfirmation'] = True
    else:
      command.param_data['studentConfirmation'] = False
    command.param_data['email'] = command.param_data['email'].lower()
    command.param_data['admittedSemester'] = command.param_data['admittedSemester'].upper()
    
    logging.info('AccountInterfaceHandler.__register().command.cmd: %s' % str(command.param_data))
    
    if not (command.param_data['admittedSemester'] and command.param_data['lastApplicationFor'] and command.param_data['personalId'] and command.param_data['firstName'] and command.param_data['lastName'] and command.param_data['streetAddress'] and command.param_data['streetNumber'] and command.param_data['zipCode'] and command.param_data['city'] and command.param_data['country'] and command.param_data['email'] and command.param_data['studentConfirmation']):
      return {
        'success'    : False,
        'status_code': 400,
        'errors'     : { 'invalidArguments': 'One or more required arguments are missing.' }
      }
    
    if RkhskMember.create_or_update(command):
      return {
        'success': True,
        'data'   : member_data
      }
    else:
      return {
        'success'    : False,
        'status_code': 403,
        'errors'     : { 'invalidRequest': 'The submitted registration form contained an invalid dataset.' }
      }
  
  
  # /account/load/
  def load(self, command):
    """Method for loading a personalized application form.
      
    Arguments:
      cmd: A member's unique accountPageUri attribute (through AccountCommand).
      
    Returns:
      An array containing member details.
    """
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 403,
        'errors'     : { 'invalidSession': 'The user\'s session is invalid.' }
      }
    
    if len(command.cmd) == 30:
      command.set_action('user_details_loader')
      command.set_description('Load user details to the membership application form.')
      command.set_valid_keys([
        'firstName',
        'lastName',
        'streetCo',
        'streetAddress',
        'streetNumber',
        'streetEntrance',
        'streetApartment',
        'admittedSemester',
        'streetFloor',
        'zipCode',
        'city',
        'phone',
        'email'
      ])
      
      logging.info('AccountInterfaceHandler.__load().command.cmd: %s' % command.cmd)
      if re.search('^[\da-z]{30}$', command.cmd, flags=re.I) is not None:
        account_link = AccountUri.get_by_key_name(command.cmd)
        if account_link is None:
          return { 'success'    : False, 'status_code': 403, 'body': False } # forbidden
        try:
          member = account_link.memberRef
        except:
          return {
            'success'    : False,
            'status_code': 500, # internal server error
            'errors'     : { 'system': 'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.' }
          }
        if member.sys_deleted:
          return {
            'success'    : False,
            'status_code': 400, # bad request
            'errors'     : { 'accountDeleted': 'The associated account has been deleted.' }
          }
        if member.sys_email_verified is False:
          member.set_email_verified()
          member.put()
        member_data = command.get_account_data(member)
        return {
          'success': True,
          'data'   : member_data
        }
    
    elif len(command.cmd) == 26:
      command.set_action('user_credentials_loader')
      command.set_description('Load user details to the membership application form.')
      command.set_valid_keys([
        'email',
        'lastPaymentKey',
      ])
      
      logging.info('AccountInterfaceHandler.__load().command.cmd: %s' % command.cmd)
      if re.search('^[\da-z]{26}$', command.cmd, flags=re.I) is not None:
        account_link = SysTmpKey.get_by_key_name(command.cmd)
        if account_link is None or account_link.is_member_login_reminder() is False:
          return { 'success'    : False, 'status_code': 403, 'body': False }
        if account_link.is_expired():
          return {
            'success'    : False,
            'status_code': 403,
            'errors'     : { 'linkExpired': 'Link expired %s' % account_link.sys_expired.isoformat().split('.')[0] }
          }
        member = account_link.get_member()
        if member is None:
          logging.warning('Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.')
          return {
            'success'    : False,
            'status_code': 500,
            'errors'     : { 'system': 'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.' }
          }
        if member.sys_deleted:
          return {
            'success'    : False,
            'status_code': 400,
            'errors'     : { 'accountDeleted': 'The associated account has been deleted.' }
          }
        else:
          account_link.set_expired()
          if member.sys_email_verified is False:
            member.set_email_verified()
            member.put()
          member_data = command.get_account_data(member)
          return {
            'success': True,
            'data'   : { 'loginUsername': member_data['email'], 'loginCredentials': member_data['lastPaymentKey'] }
          }
    
    logging.info('AccountInterfaceHandler.__load().command.cmd: Unrecognized command \'%s\'' % command.cmd)
    
    return {
      'success'    : False,
      'status_code': 403,
      'errors'     : { 'invalidSession': 'The user\'s session is invalid.' }
    }
  
  
  # /account/resend/
  def resend(self, command):
    """Method for sending out reminders with login information.
      
    Arguments:
      param_data['loginUsername']: The members email adress.
      
    Returns:
      A confirmation of submission to task queue.
    """
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 403,
        'errors'     : { 'invalidSession': 'The user\'s session is invalid.' }
      }
    
    command.set_action('user_login_reminder')
    command.set_description('Resend user login details.')
    command.set_valid_keys([])
    
    try:
      email = command.param_data['loginUsername'] #self.request.get('loginUsername', default_value = '')
      logging.info('AccountInterfaceHandler.__resend().email: %s' % email)
    except (KeyError):
      logging.info('AccountInterfaceHandler.__resend(): No loginUsername supplied.')
    if email:
      account_link = AccountEmail.get_by_key_name(email)
      if account_link and account_link.memberRef:
        member = command.get_member(account_link.memberRef)
        member.set_tmpkey('member_login_reminder')
        member.put()
        
        taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_member_details_reminder/',
          params = {
            'personal_id': member.personalId,
            'client_ip'  : command.client_ip
          },
          countdown = 5
        )
        
        return {
          'success': True,
          'data'   : { 'emailSent': 'TaskQueued' }
        }
    
    return {
      'success'    : False,
      'status_code': 400,
      'errors'     : { }
    }
  

