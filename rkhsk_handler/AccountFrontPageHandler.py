#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   AccountFrontPageHandler
# Author:  Mats Blomdahl
# Version: 2012-03-18

from webapp2 import RequestHandler, cached_property
from webapp2_extras.sessions import SessionStore
from webapp2_extras import sessions
from generic_model import SessionTrace

class AccountFrontPageHandler(RequestHandler):
  """The public interface for retrieving the registration page at located at
    http://regga.rkh-sk.se/
    
  The interface deploys secure cookies using the 'default' store and cookie
    expiration is controlled by the global variable SESSION_DEFAULT_EXPIRATION.
  """
  
  # session management
  def dispatch(self):
    # Get a session store for this request.
    self.session_store = sessions.get_store(request=self.request)
    
    try:
      # Dispatch the request.
      RequestHandler.dispatch(self)
    finally:
      # Save all sessions.
      self.session_store.save_sessions(self.response)
  
  @cached_property
  def session(self):
    # Returns a session using the default cookie key.
    return self.session_store.get_session(name = 'default', backend = 'securecookie')
  
  
  # /
  # /account_framed
  # /account_device_m
  # /account_device_d
  def get(self, param = ''):
    """Request handler for serving the web application.
      
    The request handler will also ensure that the client's session cookie is
      set. The session cookie is will be required by any and all subsystems
      within this solution.
      
    Arguments:
      param: Supported values are '', 'account_framed', 'account_device_m',
        and 'account_device_d'.
      
    Returns:
      param='': The web application registration interface. The client's
        'user_agent' header determine if the mobile or desktop version is
        served.
      param='account_framed': A HTML page containing a single iframe where the
        HTTPS version of the app is inserted, https://regga-rkh-sk.appspot.com.
      param='account_device_m': The web application registration interface with
        the mobile version enforced.
      param='account_device_d': The web application registration interface with
        the desktop version enforced.
    """
    
    self.response.headers.add_header("Access-Control-Allow-Origin", "*")
    
    is_mobile = False
    mobile_tokens = [
      'Mobile Safari',
      'BlackBerry',
      'PlayBook',
      'iPhone',
      'iPod',
      'iPad',
      'Android'
    ]
    
    user_agent = self.request.headers['User-Agent']
    
    logging.info('AccountFrontPageHandler.get(param=\'%s\').user_agent: %s' % (param, user_agent))
    
    for token in mobile_tokens:
      if re.search(token, user_agent) is not None:
        logging.info('AccountFrontPageHandler.get(param=\'%s\').mobile_token: %s' % (param, token))
        is_mobile = True
    
    if param == '':
      page = 'index-rd.html'
      if is_mobile:
        page = 'index-rm.html'
      regpage = open(page, 'r')
      self.response.write(regpage.read())
      regpage.close()
    
    elif param == 'account_framed':
      self.response.write(self.__wrapper_iframe())
    
    elif param == 'account_device_m':
      regpagem = open('index-rm.html', 'r')
      self.response.write(regpagem.read())
      regpagem.close()
    
    elif param == 'account_device_d':
      regpaged = open('index-rd.html', 'r')
      self.response.write(regpaged.read())
      regpaged.close()
    
    else:
      self.error(404)
    
    session_id = self.session.get('session')
    if session_id is None:
      self.session['session'] = SessionTrace.generate_session_id()
  
  
  def __wrapper_iframe():
    """Method for returning a HTML page with an iframe linking in the
      registration interface over HTTPS.
      
    Returns:
      A unicode string containing the HTML page.
    """
    
    wrapper = (
      u'<!DOCTYPE html>'
      u'<html lang="sv">'
      u'<head>'
      u'  <meta http-equiv="content-type" content="text/html; charset=utf-8" />'
      u'  <meta name="generator" content="TextMate http://macromates.com/" />'
      u'  <meta name="keywords" content="medlemsregistrering, registrering, medlem, röda korsets högskola, röda korsets högskolas studentkår, rkh-sk, rkhsk" />'
      u'  <meta name="description" content="Röda Korsets Högskolas Studentkår [RKH-SK] är en ideell förening som arbetar med att tillvarata studenternas intressen vid Röda Korsets Högskola [RKH]." />'
      u'  <meta name="author" content="Mats Blomdahl (2012-03-18)" />'
      u'  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="resources/images/touch-icon-iphone4.png" />'
      u'  <link rel="canonical" href="http://regga.rkh-sk.se" />'
      u'  <title>Medlemsregistrering, RKH-SK</title>'
      u'  <link rel="pingback" href="http://regga.rkh-sk.se" />'
      u'  <style>'
      u'  iframe {'
      u'    width: 100%;'
      u'    height: 100%;'
      u'    position: absolute;'
      u'    border: none;'
      u'    overflow: hidden;'
      u'    padding: 0;'
      u'    margin: 0;'
      u'  }'
      u'  body {'
      u'    margin: 0;'
      u'    padding: 0;'
      u'    overflow: hidden;'
      u'  }'
      u'  </style>'
      u'</head>'
      u'<body><iframe src="/"></iframe></body>'
      u'</html>'
    )
    
    return wrapper
  

