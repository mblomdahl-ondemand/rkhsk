#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   RKH-SK Members Datastore
# Author:  Mats Blomdahl
# Version: 2012-03-08

import cgi
import webapp2 # web application framework
#from webapp2_extras import jinja2
from webapp2_extras.sessions import SessionStore
from webapp2_extras import sessions
import os
import re
import json
import logging
import random
import urllib
import pickle
import datetime
import time
import string
import locale
# email test start
#from webapp2.mail_handlers import InboundMailHandler

from email.utils import parsedate_tz, mktime_tz

from webob import Response, Request
# email test end
from google.appengine.api import mail
from google.appengine.api import taskqueue
from google.appengine.ext import db
from google.appengine.ext import ndb
from google.appengine.api import namespace_manager
from google.appengine.api import channel
from google.appengine.api import memcache
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import blobstore

from generic_ui import Command, UniHandler

from generic_model.mail import InboundMail, SentMail
from generic_model.time import SwedishTz, UtcTz
from generic_model import SessionTrace, TempFile

from rkhsk_model import MecenatPushTask, SscoPullTask, OwaUser, SysConfig
from rkhsk_model.member import RkhskMember, AccountEmail, AccountUri, PaymentKey, SysTmpKey

#jinja_environment = jinja2.Environment(
#    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

#locale.setlocale(locale.LC_ALL, 'sv_SE')
config = { 
  'webapp2_extras.sessions': {
    'secret_key': 'my-super-s3cret-key-modz',
    'cookie_name': 'RKHSK',
    'cookie_args': {
      'path': '/',
      'max_age': 600
    }
  },
  'session_dict': sessions.SessionDict({}, data = None, new = False)
}

# TODO(mats.blomdahl@gmail.com): Document.
def admin_view_wrapper(url_postfix = ''):
  """
  Method that returns a HTML page containing an iframe linking in the admin interface.
  """
  
  wrapper = (
    u'<!DOCTYPE html>'
    u'<html lang="sv">'
    u'<head>'
    u'  <meta http-equiv="content-type" content="text/html; charset=utf-8" />'
    u'  <meta name="generator" content="TextMate http://macromates.com/" />'
    u'  <meta name="keywords" content="medlemsregistrering, registrering, medlem, röda korsets högskola, röda korsets högskolas studentkår, rkh-sk, rkhsk" />'
    u'  <meta name="description" content="Röda Korsets Högskolas Studentkår [RKH-SK] är en ideell förening som arbetar med att tillvarata studenternas intressen vid Röda Korsets Högskola [RKH]." />'
    u'  <meta name="author" content="Mats Blomdahl (2012-01-24)" />'
    u'  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="resources/images/touch-icon-iphone4.png" />'
    u'  <link rel="canonical" href="http://regga.rkh-sk.se" />'
    u'  <title>Medlemsregistrering, RKH-SK</title>'
    u'  <link rel="pingback" href="http://regga.rkh-sk.se" />'
    u'  <style>'
    u'  iframe {'
    u'    width: 100%;'
    u'    height: 100%;'
    u'    position: absolute;'
    u'    border: none;'
    u'    overflow: hidden;'
    u'    padding: 0;'
    u'    margin: 0;'
    u'  }'
    u'  body {'
    u'    margin: 0;'
    u'    padding: 0;'
    u'    overflow: hidden;'
    u'  }'
    u'  </style>'
    u'</head>'
    u'<body><iframe src="/admin"></iframe></body>'
    u'</html>'
  )
  
  return wrapper

def reg_view_wrapper(url_postfix = ''):
  """
  Method that returns a HTML page containing an iframe linking in the registration interface.
  """
  
  wrapper = (
    u'<!DOCTYPE html>'
    u'<html lang="sv">'
    u'<head>'
    u'  <meta http-equiv="content-type" content="text/html; charset=utf-8" />'
    u'  <meta name="generator" content="TextMate http://macromates.com/" />'
    u'  <meta name="keywords" content="medlemsregistrering, registrering, medlem, röda korsets högskola, röda korsets högskolas studentkår, rkh-sk, rkhsk" />'
    u'  <meta name="description" content="Röda Korsets Högskolas Studentkår [RKH-SK] är en ideell förening som arbetar med att tillvarata studenternas intressen vid Röda Korsets Högskola [RKH]." />'
    u'  <meta name="author" content="Mats Blomdahl (2012-01-24)" />'
    u'  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="resources/images/touch-icon-iphone4.png" />'
    u'  <link rel="canonical" href="http://regga.rkh-sk.se" />'
    u'  <title>Medlemsregistrering, RKH-SK</title>'
    u'  <link rel="pingback" href="http://regga.rkh-sk.se" />'
    u'  <style>'
    u'  iframe {'
    u'    width: 100%;'
    u'    height: 100%;'
    u'    position: absolute;'
    u'    border: none;'
    u'    overflow: hidden;'
    u'    padding: 0;'
    u'    margin: 0;'
    u'  }'
    u'  body {'
    u'    margin: 0;'
    u'    padding: 0;'
    u'    overflow: hidden;'
    u'  }'
    u'  </style>'
    u'</head>'
    u'<body><iframe src="/"></iframe></body>'
    u'</html>'
  )
  
  return wrapper


# globals
STDENC = 'utf_8'
SWETIME = SwedishTime()
UTCTIME = UtcTime()
DEVMODE = True
ACTIVE_ADMINS = 'admin_active_users'
CONNECTED_ADMINS = 'admin_current_channels'
ADMIN_COOKIE_EXPIRATION = 600
SESSION_DEFAULT_EXPIRATION = 86400

COMPATIBILITY_PLAINTEXT = u'Kompatibilitetsinfo: RKH-SK:s system har fullt stöd för öppna webbstandarder och fungerar i alla moderna webbläsare, inkl. Android och iOS (iPhone). Om du använder en gammal version av Internet Explorer som inte implementerar webbstandarder så kommer du uppmanas att aktivera Chrome Frame. Processen tar c:a 30 sekunder och därefter kommer du kunna dra nytta av alla moderna webbapplikationer, trots att du använder legacy IE.'

COMPATIBILITY_HTML = u'<u>Kompatibilitetsinfo</u>: RKH-SK:s system har fullt stöd för <a href="http://www.whatwg.org/" target="_blank">öppna webbstandarder</a> och fungerar i alla moderna webbläsare, inkl. Android och iOS (iPhone). Om du använder en gammal version av Internet Explorer som inte implementerar webbstandarder så kommer du uppmanas att aktivera <a href="http://www.google.com/chromeframe" target="_blank">Chrome Frame</a>. Processen tar c:a 30 sekunder och därefter kommer du kunna dra nytta av alla moderna webbapplikationer, trots att du använder <i>legacy IE</i>.'

# TODO(mats.blomdahl@gmail.com): Document. Restructure into a handler that takes 2 args, first of which is the queue, second the actual command (share more code!)
class RkhskTasks(webapp2.RequestHandler):
  """The private interface for queueing tasks such as mail-outs and large sets
    of updates.
    
  The request handler is secured by App Engine's built-in authentication 
    configured in the app.yaml config.
  """
  
  # /tasks/<task>
  def post(self, task):
    """Handler for the TaskQueue service.
      
    The handler redirects queued tasks to the appropriate handler method.
      
    Arguments:
      task: The task identifier, a string.
    """
    
    func = getattr(self, '__%s' % task, None)
    if func is not None:
      if func() is False:
        self.error(404)
  
  
  # /tasks/send_admin_activation_email/
  def __send_admin_activation_email(self):
    """A task that sends out an admin activation email to the user specified
      by the 'personal_id' param.
      
    This task belong to the queue group 'mailer' which contains the individual
      queues mailer0, mailer1, mailer2 and mailer3.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_admin_activation_email/',
          params = {
            'personal_id'  : member.personalId,
            'sys_activator': task.user_id
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      personal_id: The recipient member's key name/personal id, a string.
      sys_activator: The admin initiating the activation process, a string.
    """
    
    logging.info('recieved send_activation_email')
    logging.info('recieved args: %s' % self.request.arguments())
    
    recipient = RkhskMember.get_by_key_name(self.request.get('personal_id'))
    if recipient is None:
      logging.info('failed, reason: member not found.')
      return False
    
    sys_activator = self.request.get('sys_activator')
    sender = sys_activator
    if sender != 'su':
      sender = RkhskMember.get_by_key_name(sender)
      if sender is None:
        logging.info('failed, reason: member not found.')
        return False
      sender = '%s %s (%s)' % (sender.firstName, sender.lastName, sender.email)
    
    plaintext = (
      u'%s,\n'
      u'\n'
      u'Du har tilldelats administratörsprivilegier för RKH-SK:s medlemsdatabas, rättigheterna har tilldelats dig av %s. Om du inte hör till studentkårens funktionärer och tänker att rättigheterna kanske tilldelats dig av misstag – skriv omedelbart tillbaka och påtala misstaget!\n'
      u'\n'
      u'För att komma igång:\n'
      u'1. Välj ett lösenord via http://regga.rkh-sk.se/admin#%s (Obs! Länken är temporär och kommer inte kunna användas mer än en gång.)\n'
      u'2. Logga in i medlemsdatabasen via http://regga.rkh-sk.se/admin med din e-postadress och det lösenord du valt.\n'
      u'\n'
      u'%s\n'
      u'\n'
      u'\n'
      u'Varma hälsningar,\n'
      u'\n'
      u'Medlemsdatabasen\n'
      u'Röda Korsets Högskolas Studentkår\n'
      u'' % (recipient.firstName, sender, recipient.sys_tmpkey, COMPATIBILITY_PLAINTEXT)
    )
    
    html = (
      u'<p style="font-size: 12px;">%s,<br></p>'
      u'<p style="font-size: 12px;">Du har tilldelats administratörsprivilegier för RKH-SK:s medlemsdatabas, rättigheterna har tilldelats dig av %s. Om du inte hör till studentkårens funktionärer och tänker att rättigheterna kanske tilldelats dig av misstag – skriv omedelbart tillbaka och påtala misstaget!<br></p>'
      u'<p style="font-size: 12px;"><u>För att komma igång</u>:<br><ol style="font-size: 12px;">'
      u'<li>Välj ett lösenord via <a href="http://regga.rkh-sk.se/admin#%s" target="_blank">regga.rkh-sk.se/admin#%s</a> (Obs! Länken är temporär och kommer inte kunna användas mer än en gång.)</li>'
      u'<li>Logga in i medlemsdatabasen via <a href="http://regga.rkh-sk.se/admin" target="_blank">regga.rkh-sk.se/admin</a> med din e-postadress och det lösenord du valt.</li></ol></p>'
      u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
      u'<p style="font-size: 12px;">Varma hälsningar,<br>'
      u'<br>'
      u'Medlemsdatabasen<br>'
      u'Röda Korsets Högskolas Studentkår<br></p>' % (recipient.firstName, sender, recipient.sys_tmpkey, recipient.sys_tmpkey, COMPATIBILITY_HTML)
    )
    
    sentmail = SentEmail.send_wrapper(
      to = '%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
      subject = u'Du har utsetts till administratör av RKH-SK:s medlemsdatabas!',
      body_plaintext = plaintext,
      body_html = html,
      sys_sentby = sender,
      sys_creator = sys_activator
    )
    
    if sentmail is False:
      logging.info('failed, reason: mail not sent.')
      return False
    else:
      logging.info('success! mail sent')
      
      recipient.sys_emailstouser.append(sentmail.key())
      recipient.sys_modifieddate = datetime.datetime.today()
      changelog = {
        'action'          : 'Sent Admin Activation Notice',
        'reminderSentTo'  : recipient.email,
        'sys_activator'   : sys_activator,
        'sys_modifieddate': recipient.sys_modifieddate.isoformat().split('.')[0]
      }
      recipient.sys_changelog.append(db.Text(json.dumps(changelog)))
      recipient.put()
    
    SysConfig.update(sys_admins = ('add', recipient.personalId))
  
  
  # /tasks/send_admin_deactivation_email/
  def __send_admin_deactivation_email(self):
    """A task that sends out an admin deactivation email to the user specified
      by the 'personal_id' param.
      
    This task belong to the queue group 'mailer' which contains the individual
      queues mailer0, mailer1, mailer2 and mailer3.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_admin_deactivation_email/',
          params = {
            'personal_id'    : member.personalId,
            'sys_deactivator': task.user_id
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      personal_id: The recipient member's key name/personal id, a string.
      sys_deactivator: The admin initiating the activation process, a string.
    """
    
    logging.info('recieved send_deactivation_email')
    logging.info('recieved args: %s' % self.request.arguments())
    
    
    recipient = RkhskMember.get_by_key_name(self.request.get('personal_id'))
    if recipient is None:
      logging.info('failed, reason: recipient not found.')
      return False
    
    sys_deactivator = self.request.get('sys_deactivator')
    sender = sys_deactivator
    if sender != 'su':
      sender = RkhskMember.get_by_key_name(sender)
      if sender is None:
        logging.info('failed, reason: sender not found.')
        return False
      sender = '%s %s (%s)' % (sender.firstName, sender.lastName, sender.email)
    
    plaintext = (
      u'%s,\n'
      u'\n'
      u'Dina administratörsprivilegier i RKH-SK:s medlemsdatabas har upphävts. Administratörsrättigheterna har tagits bort av %s. Om den här ändringen var oväntad och du tror att det är ett misstag – skriv omedelbart tillbaka och påtala misstaget!\n'
      u'\n'
      u'\n'
      u'Varma hälsningar,\n'
      u'\n'
      u'Medlemsdatabasen\n'
      u'Röda Korsets Högskolas Studentkår\n'
      u'' % (recipient.firstName, sender)
    )
    
    html = (
      u'<p style="font-size: 12px;">%s,<br></p>'
      u'<p style="font-size: 12px;">Dina administratörsprivilegier i RKH-SK:s medlemsdatabas har upphävts. Administratörsrättigheterna har tagits bort av %s. Om den här ändringen var oväntad och du tror att det är ett misstag – skriv omedelbart tillbaka och påtala misstaget!<br><br></p>'
      u'<p style="font-size: 12px;">Varma hälsningar,<br>'
      u'<br>'
      u'Medlemsdatabasen<br>'
      u'Röda Korsets Högskolas Studentkår<br></p>' % (recipient.firstName, sender)
    )
    
    sentmail = SentEmail.send_wrapper(
      to = '%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
      subject = u'Du har avregistrerats som administratör för RKH-SK:s medlemsdatabas!',
      body_plaintext = plaintext,
      body_html = html,
      sys_sentby = sender,
      sys_creator = sys_deactivator
    )
    
    if sentmail is False:
      logging.info('failed, reason: sendmail failed.')
      return False
    else:
      logging.info('success!, reason: sendmail sent.')
      recipient.sys_emailstouser.append(sentmail.key())
      recipient.sys_modifieddate = datetime.datetime.today()
      changelog = {
        'action'          : 'Sent Admin Deactivation Notice',
        'reminderSentTo'  : recipient.email,
        'sys_deactivator' : sys_deactivator,
        'sys_modifieddate': recipient.sys_modifieddate.isoformat().split('.')[0]
      }
      recipient.sys_changelog.append(db.Text(json.dumps(changelog)))
      recipient.put()
    
    SysConfig.update(sys_admins = ('remove', recipient.personalId))
  
  
  # /tasks/send_admin_password_reset_email/
  def __send_admin_password_reset_email(self):
    """A task that sends out an temporary link by email in order for an admin
      to reset his/her login password.
      
    This task belong to the queue group 'mailer' which contains the individual
      queues mailer0, mailer1, mailer2 and mailer3.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_admin_password_reset_email/',
          params = {
            'personal_id': member.personalId,
            'client_ip'  : task.client_ip
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      personal_id: The recipient member's key name/personal id, a string.
      client_ip: The client IP address initiating the password reset, a string.
    """
    
    logging.info('recieved send_admin_password_reset_email')
    logging.info('recieved args: %s' % self.request.arguments())
    
    recipient = RkhskMember.get_by_key_name(self.request.get('personal_id'))
    if recipient is None:
      logging.info('failed, reason: member not found.')
      return False
    
    client_ip = self.request.get('client_ip')
    
    plaintext = (
      u'%s,\n'
      u'\n'
      u'Här följer en länk som du kan använda för att återställa ditt administratörslösenord i RKH-SK:s medlemsdatabas. Om du inte känner igen att du skulle ha begärt en återställning av lösenordet så kan du helt bortse från detta mail (begäran om återställning mottags från IP-adress %s).\n'
      u'\n'
      u'Du återställer lösenordet via http://regga.rkh-sk.se/admin#%s\n'
      u'\n'
      u'%s\n'
      u'\n'
      u'\n'
      u'Varma hälsningar,\n'
      u'\n'
      u'Medlemsdatabasen\n'
      u'Röda Korsets Högskolas Studentkår\n'
      u'' % (recipient.firstName, client_ip, recipient.sys_tmpkey, COMPATIBILITY_PLAINTEXT)
    )
    
    html = (
      u'<p style="font-size: 12px;">%s,<br></p>'
      u'<p style="font-size: 12px;">Här följer en länk som du kan använda för att återställa ditt administratörslösenord i RKH-SK:s medlemsdatabas. Om du inte känner igen att du skulle ha begärt en återställning av lösenordet så kan du helt bortse från detta mail (begäran om återställning mottags från ip-adress %s).<br></p>'
      u'<p style="font-size: 12px;">Du återställer lösenordet via <a href="http://regga.rkh-sk.se/admin#%s" target="_blank">regga.rkh-sk.se/admin#%s</a><br></p>'
      u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
      u'<p style="font-size: 12px;">Varma hälsningar,<br>'
      u'<br>'
      u'Medlemsdatabasen<br>'
      u'Röda Korsets Högskolas Studentkår</p>'
      u'<br>' % (recipient.firstName, client_ip, recipient.sys_tmpkey, recipient.sys_tmpkey, COMPATIBILITY_HTML)
    )
    
    sentmail = SentEmail.send_wrapper(
      to = '%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
      subject = u'Återställning av administratörslösenord för RKH-SK:s medlemsdatabas',
      body_plaintext = plaintext,
      body_html = html,
      sys_sentby = client_ip,
      sys_creator = recipient.email
    )
    
    if sentmail is False:
      logging.info('failed, reason: mail not sent.')
      return False
    else:
      logging.info('success! mail sent')
      
      recipient.sys_emailstouser.append(sentmail.key())
      recipient.sys_modifieddate = datetime.datetime.today()
      changelog = {
        'action'              : 'Sent Admin Password Reset',
        'resetSentTo'         : recipient.email,
        'sys_origin_client_ip': client_ip,
        'sys_modifieddate'    : recipient.sys_modifieddate.isoformat().split('.')[0]
      }
      recipient.sys_changelog.append(db.Text(json.dumps(changelog)))
      recipient.put()
  
  
  # /tasks/send_member_registration_notice/
  def __send_member_registration_notice(self):
    """A task that sends out a member application/registration notice.
      
    This task belong to the queue group 'mailer' which contains the individual
      queues mailer0, mailer1, mailer2 and mailer3.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_admin_password_reset_email/',
          params = {
            'personal_id'    : member.personalId,
            'sys_registrator': task.user_id,
            'client_ip'      : task.client_ip,
            'user_initated'  : False
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      personal_id: The recipient member's key name/personal id, a string.
      client_ip: The client IP address initiating the password reset, a string.
      user_initated: A flag marking the registration as user initiated (bool).
      sys_registrator: The registrator's key name (if not user initiated).
    """
    
    recipient = RkhskMember.get_by_key_name(self.request.get('personal_id'))
    if recipient is None:
      return False
    
    client_ip = self.request.get('client_ip', default_value = None)
    sys_registrator = self.request.get('sys_registrator', default_value = None)
    
    if self.request.get('user_initiated'):
      operatorline = u'Du får det här mailet som en bekräftelse på din registrering i RKH-SK:s databas. '
    else:
      operatorline = u'Du får det här mailet som en bekräftelse på att RKH-SK:s funktionärer registrerat din medlemsansökan till studentkåren. '
    
    plaintext = (
      u'%s,\n'
      u'\n'
      u'%sFör att ditt medlemskap för termin %s ska aktiveras måste medlemsavgiften (100 kr) betalas in och registreras korrekt i systemet.\n'
      u'\n'
      u'Medlemsavgiften kan antingen betalas in till studentkårens PlusGiro, 126 98 26-2, eller kontant i kårens studentexpedition tisdagar & torsdagar kl. 12-13. Oavsett betalningssätt är det av högsta vikt att du uppger din personliga kod som referens – annars riskerar du att ditt ärende "faller mellan stolarna" och att du får vänta.\n'
      u'\n'
      u'DINA INBETALNINGSUPPGIFTER\n'
      u'Referens & personlig kod: %s\n'
      u'Medlemsavgift: 100 kr\n'
      u'PlusGiro: 126 98 26-2\n'
      u'\n'
      u'För att se status för din medlemsansökan och vilka uppgifter vi har registrerade för dig:\n'
      u'1. Återvänd till registreringssidan http://regga.rkh-sk.se och välj fliken "Ärendestatus"\n'
      u'2. Ange e-postadress och personlig kod ("%s" resp. "%s") och klicka på "Visa ärendestatus"\n'
      u'\n'
      u'%s\n'
      u'\n'
      u'\n'
      u'Varma hälsningar,\n'
      u'\n'
      u'%s\n'
      u'Röda Korsets Högskolas Studentkår\n'
      u'' % (recipient.firstName, operatorline, recipient.lastApplicationFor, recipient.lastPaymentKey, recipient.email, recipient.lastPaymentKey, COMPATIBILITY_PLAINTEXT, 'Medlemsregistreringen')
    )
    
    html = (
      u'<p style="font-size: 12px;">%s,<br></p>'
      u'<p style="font-size: 12px;">%sFör att ditt medlemskap för termin %s ska aktiveras måste medlemsavgiften (100 kr) betalas in och registreras korrekt i systemet.<br></p>'
      u'<p style="font-size: 12px;">Medlemsavgiften kan antingen betalas in till studentkårens PlusGiro, 126 98 26-2, eller kontant i kårens studentexpedition tisdagar & torsdagar kl. 12-13. Oavsett betalningssätt är det <b>av högsta vikt att du uppger din personliga kod som referens</b> – annars riskerar du att ditt ärende "faller mellan stolarna" och att du får vänta.<br></p>'
      u'<p style="font-size: 12px;"><u>Dina inbetalningsuppgifter</u>:<br>'
      u'Referens & personlig kod: %s<br>'
      u'Medlemsavgift: 100 kr<br>'
      u'PlusGiro: 126 98 26-2<br></p>'
      u'<p style="font-size: 12px;">För att se status för din medlemsansökan och vilka uppgifter vi har registrerade för dig:<ol style="font-size: 12px;"><li>Återvänd till registreringssidan <a href="http://regga.rkh-sk.se" target="_blank">regga.rkh-sk.se</a> och välj fliken "Ärendestatus"</li>'
      u'<li>Ange e-postadress och personlig kod ("%s" resp. "%s") och klicka på "Visa ärendestatus"</li></ol></p>'
      u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
      u'<p style="font-size: 12px;">Varma hälsningar,<br>'
      u'<br>'
      u'%s<br>'
      u'Röda Korsets Högskolas Studentkår<br>'
      u'<br></p>' % (recipient.firstName, operatorline, recipient.lastApplicationFor, recipient.lastPaymentKey, recipient.email, recipient.lastPaymentKey, COMPATIBILITY_HTML, 'Medlemsregistreringen')
    )
    
    sentmail = SentEmail.send_wrapper(
      to = '%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
      subject = u'Registreringsbekräftelse från RKH-SK:s medlemsdatabas',
      body_plaintext = plaintext,
      body_html = html,
      sys_sentby = client_ip,
      sys_creator = userId
    )
    
    if sentmail is None:
      return False
    else:
      recipient.sys_emailstouser.append(sentmail.key())
      recipient.sys_modifieddate = datetime.datetime.today()
      changelog = {
        'action'              : 'Sent Account Registration Notice Email',
        'regNoticeSentTo'     : recipient.email,
        'sys_origin_client_ip': client_ip,
        'sys_modifieddate'    : recipient.sys_modifieddate.isoformat().split('.')[0]
      }
      recipient.sys_changelog.append(db.Text(json.dumps(changelog)))
      recipient.put()
  
  
  # /tasks/send_member_details_reminder/
  def __send_member_details_reminder(self):
    """A task that sends out a reminder with member account details. 
      
    This task belong to the queue group 'mailer' which contains the individual
      queues mailer0, mailer1, mailer2 and mailer3.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_admin_password_reset_email/',
          params = {
            'personal_id': member.personalId,
            'client_ip'  : task.client_ip,
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      personal_id: The recipient member's key name/personal id, a string.
      client_ip: The client IP address initiating the password reset, a string.
    """
    
    recipient = RkhskMember.get_by_key_name(self.request.get('personal_id'))
    if recipient is None:
      return False
    
    client_ip = self.request.get('client_ip')
    
    def txn_store_sysTmpKey(owner, tmpkey):
      SysTmpKey(
        key_name = tmpkey,
        tmpKey = tmpkey,
        memberRef = owner,
        useCase = 'member_login_reminder'
      ).put()
    
    db.run_in_transaction(txn_store_sysTmpKey, recipient, recipient.sys_tmpkey)
    
    plaintext = (
      u'%s,\n'
      u'\n'
      u'Det här är en påminnelse med dina personliga inloggningsuppgifter till RKH-SK:s medlemssida, http://regga.rkh-sk.se. Om du inte känner igen att du skulle ha begärt en påminnelse så är det förmodligen någon annan student (IP-adress: %s) som råkat ange fel adress och du kan helt bortse från innehållet i det här mailet.\n'
      u'\n'
      u'INLOGGNINGSUPPGIFTER\n'
      u'E-postadress: %s\n'
      u'Personlig kod: %s\n'
      u'\n'
      u'Personlig adress, direkt-inloggning: http://regga.rkh-sk.se/#%s (Obs: Länken är temporär och du kommer inte kunna använda den mer än en gång.)\n'
      u'\n'
      u'\n'
      u'BETALNINGSINFORMATION\n'
      u'Om du ännu inte betalat in din medlemsavgift för denna termin så finns två alternativ att tillgå:\n'
      u'1) Inbetalning till studentkårens PlusGiro, 126 98 26-2, eller\n'
      u'2) Kontant betalning hos kårens studentexpedition tisdagar & torsdagar kl. 12-13.\n'
      u'\n'
      u'Avgiften är 100 kr per termin och oavsett betalningssätt är det av högsta vikt att du uppger din personliga kod som referens – annars riskerar du att ditt ärende "faller mellan stolarna".\n'
      u'\n'
      u'%s\n'
      u'\n'
      u'\n'
      u'Varma hälsningar,\n'
      u'\n'
      u'Medlemsregistreringen\n'
      u'Röda Korsets Högskolas Studentkår\n'
      u'' % (recipient.firstName, client_ip, recipient.email, recipient.lastPaymentKey, recipient.sys_tmpkey, COMPATIBILITY_PLAINTEXT)
    )
    
    html = (
      u'<p style="font-size: 12px;">%s,<br></p>'
      u'<p style="font-size: 12px;">Det här är en påminnelse med dina personliga inloggningsuppgifter till RKH-SK:s medlemssida, <a href="http://regga.rkh-sk.se" target="_blank">regga.rkh-sk.se</a>. Om du inte känner igen att du skulle ha begärt en påminnelse så är det förmodligen någon annan student (IP: %s) som råkat ange fel adress och du kan helt bortse från innehållet i det här mailet.<br></p>'
      u'<p style="font-size: 12px;"><u>Inloggningsuppgifter</u>:<br>'
      u'E-postadress: %s<br>'
      u'Personlig kod: %s<br></p>'
      u'<p style="font-size: 12px;">Personlig adress, direkt-inloggning: <a href="http://regga.rkh-sk.se/#%s" target="_blank">regga.rkh-sk.se/#%s</a> (Obs: Länken är temporär och du kommer inte kunna använda den mer än en gång.)<br><br></p>'
      u'<p style="font-size: 12px;"><u>Betalningsinformation</u>:<br>'
      u'Om du ännu inte betalat in din medlemsavgift för denna termin så finns två alternativ att tillgå:'
      u'<ol style="font-size: 12px;"><li>Inbetalning till studentkårens PlusGiro, 126 98 26-2, eller</li>'
      u'<li>Kontant betalning hos kårens studentexpedition tisdagar & torsdagar kl. 12-13.</li></ol>'
      u'Avgiften är 100 kr per termin och oavsett betalningssätt är det <b>av högsta vikt att du uppger din personliga kod som referens</b> – annars riskerar du att ditt ärende "faller mellan stolarna".<br></p>'
      u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
      u'<p style="font-size: 12px;">Varma hälsningar,<br>'
      u'<br>'
      u'Medlemsregistreringen<br>'
      u'Röda Korsets Högskolas Studentkår<br></p>'
      u'' % (recipient.firstName, client_ip, recipient.email, recipient.lastPaymentKey, recipient.sys_tmpkey, recipient.sys_tmpkey, COMPATIBILITY_HTML)
    )
    
    sentmail = SentEmail.send_wrapper(
      to = '%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
      subject = u'Inloggningsuppgifter till RKH-SK:s medlemssida',
      body_plaintext = plaintext,
      body_html = html,
      sys_sentby = recipient.personalId,
      sys_creator = client_ip
    )
    
    if sentmail is False:
      return False
    else:
      recipient.sys_emailstouser.append(sentmail.key())
      recipient.sys_modifieddate = datetime.datetime.today()
      changelog = {
        'action'              : 'Sent Account Details Reminder Email',
        'reminderSentTo'      : recipient.email,
        'sys_origin_client_ip': client_ip,
        'sys_modifieddate'    : recipient.sys_modifieddate.isoformat().split('.')[0]
      }
      recipient.sys_changelog.append(db.Text(json.dumps(changelog)))
      recipient.put()
  
  
  # TODO(mats.blomdahl@gmail.com): Implementation + unit tests!
  # /tasks/send_member_registration_reminder/
  def __send_member_registration_reminder(self):
    """A task that sends out a reminder to old members in order to inspire them
      to renew their memberships.s
      
    This task belong to the queue group 'mailer' which contains the individual
      queues mailer0, mailer1, mailer2 and mailer3.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_admin_password_reset_email/',
          params = {
            'personal_id': member.personalId,
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      personal_id: The recipient member's key name/personal id, a string.
    """
    
    pass
  
  
  # /tasks/send_member_activation_notice/
  def __send_member_activation_notice(self):
    """A task that sends out membership activation notifications.
      
    This task belong to the queue group 'mailer' which contains the individual
      queues mailer0, mailer1, mailer2 and mailer3.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_member_activation_notice/',
          params = {
            'personal_id': member.personalId,
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      personal_id: The recipient member's key name/personal id, a string.
    """
    
    recipient = RkhskMember.get_by_key_name(self.request.get('personal_id'))
    if recipient is None:
      return False
    
    plaintext = (
      u'%s,\n'
      u'\n'
      u'Du får det här mailet som en bekräftelse på att studentkårens funktionärer har registrerat din medlemsansökan till termin %s i RKH-SK:s medlemsdatabas, http://regga.rkh-sk.se. När det blir dags att förnya ditt medlemskap för nästa termin återvänder du helt enkelt till http://regga.rkh-sk.se och skickar in dina uppgifter pånytt, varpå du erhåller en ny personlig kod kopplad till din ansökan.\n'
      u'\n'
      u'INLOGGNINGSUPPGIFTER FÖR %s\n'
      u'E-postadress: %s\n'
      u'Personlig kod: %s\n'
      u'\n'
      u'För att kontrollera att din inbetalning av medlemsavgiften registrerats och se ärendestatus för din medlemsansökan (inkl. Mecenat/SSSB) så räcker det att utföra två enkla steg:\n'
      u'1. Besök registreringssidan http://regga.rkh-sk.se och välj fliken "Ärendestatus"\n'
      u'2. Ange e-postadress och personlig kod ("%s" resp. "%s") och klicka på "Visa ärendestatus"\n'
      u'\n'
      u'%s\n'
      u'\n'
      u'\n'
      u'Varma hälsningar,\n'
      u'\n'
      u'Medlemsregistreringen\n'
      u'Röda Korsets Högskolas Studentkår\n'
      u'' % (recipient.firstName, recipient.lastApplicationFor, recipient.lastApplicationFor, recipient.email, recipient.lastPaymentKey, recipient.email, recipient.lastPaymentKey, COMPATIBILITY_PLAINTEXT)
    )
    
    html = (
      u'<p style="font-size: 12px;">%s,<br></p>'
      u'<p style="font-size: 12px;">Du får det här mailet som en bekräftelse på att studentkårens funktionärer har registrerat din medlemsansökan till termin %s i RKH-SK:s medlemsdatabas, <a href="http://regga.rkh-sk.se" target="_blank">regga.rkh-sk.se</a>. När det blir dags att förnya ditt medlemskap för nästa termin återvänder du helt enkelt till <a href="http://regga.rkh-sk.se" target="_blank">regga.rkh-sk.se</a> och skickar in dina uppgifter pånytt, varpå du erhåller en ny personlig kod kopplad till din ansökan.<br></p>'
      u'<p style="font-size: 12px;"><u>Inloggningsuppgifter för %s</u>:<br>'
      u'E-postadress: %s<br>'
      u'Personlig kod: %s<br></p>'
      u'<p style="font-size: 12px;">För att kontrollera att din inbetalning av medlemsavgiften registrerats och se ärendestatus för din medlemsansökan (inkl. Mecenat/SSSB) så räcker det att utföra två enkla steg:'
      u'<ol style="font-size: 12px;"><li>Besök registreringssidan http://regga.rkh-sk.se och välj fliken "Ärendestatus"</li>'
      u'<li>Ange e-postadress och personlig kod ("%s" resp. "%s") och klicka på "Visa ärendestatus"</li></ol></p>'
      u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
      u'<p style="font-size: 12px;">Varma hälsningar,<br>'
      u'<br>'
      u'Medlemsregistreringen<br>'
      u'Röda Korsets Högskolas Studentkår<br></p>'
      u'' % (recipient.firstName, recipient.lastApplicationFor, recipient.lastApplicationFor, recipient.email, recipient.lastPaymentKey, recipient.email, recipient.lastPaymentKey, COMPATIBILITY_HTML)
    )
    
    sentmail = SentEmail.send_wrapper(
      to = '%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
      subject = u'Registreringsbekräftelse för RKH-SK:s medlemsdatabas',
      body_plaintext = plaintext,
      body_html = html,
      sys_sentby = 'su',
      sys_creator = 'su'
    )
    
    if sentmail is False:
      return False
    else:
      recipient.sys_emailstouser.append(sentmail.key())
      recipient.sys_modifieddate = datetime.datetime.today()
      changelog = {
        'action'          : 'Sent Account Activation Notice',
        'reminderSentTo'  : recipient.email,
        'sys_notifier'    : 'su',
        'sys_modifieddate': recipient.sys_modifieddate.isoformat().split('.')[0]
      }
      recipient.sys_changelog.append(db.Text(json.dumps(changelog)))
      recipient.put()
  
  
  # /tasks/admin_broadcast/
  def __admin_broadcast(self):
    """A task that performs a broadcast to the admins currently connected.
      
    This task belong to the queue 'broadcast' and distribute messages to
      connected admins through App Engine's channel service.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'broadcaster',
          url = '/tasks/admin_broadcast/',
          params = {
            'broadcaster' : task.user_id,
            'timestamp'   : pickle.dumps(datetime.datetime.today()),
            'remote_event': <event name>,
            'data'        : json.dumps(data)
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      broadcaster: The broadcaster's key name/personal id, a string.
      timestamp: A pickled datetime instance (UTC).
      remote_event: The event to be broadcasted, one of: 'admin_connected',
        'admin_disconnected', 'member_created', 'member_updated',
        'mecenat_sync_failed', 'mecenat_sync_completed', 
        'mecenat_sync_data_downloaded', 'mecenat_sync_initiated',
        'ssco_sync_failed', 'ssco_sync_completed', 'ssco_sync_data_downloaded'
        and 'ssco_sync_initiated'.
      data: Semi-optional payload for the broadcast, as JSON.
    """
    
    remote_events = ['admin_connected', 'admin_disconnected', 'member_created', 'member_updated', 'mecenat_sync_failed', 'mecenat_sync_completed', 'mecenat_sync_data_downloaded', 'mecenat_sync_initiated', 'ssco_sync_failed', 'ssco_sync_completed', 'ssco_sync_data_downloaded', 'ssco_sync_initiated']
    
    broadcaster = self.request.get('broadcaster', default_value = None)
    broadcast = {}
    if broadcaster == 'su':
      broadcast.update({
        'sender'        : broadcaster,
        'senderUsername': 'mats.blomdahl@gmail.com'
      })
    elif broadcaster == 'anonymous':
      broadcast.update({
        'sender'        : broadcaster,
        'senderUsername': None
      })
    else:
      member = RkhskMember.get_by_key_name(broadcaster)
      if member is None:
        logging.warning('admin_broadcast: Fake broadcast sent from user \'%s\'. Aborting...' % broadcaster)
        return False
      broadcast['sender'] = u'%s %s' % (member.firstName, member.lastName)
      broadcast['senderUsername'] = member.email
    
    timestamp = str(self.request.get('timestamp', default_value = None))
    try:
      timestamp = pickle.loads(timestamp)
    except:
      logging.warning('admin_broadcast: Broken task (missing timestamp) sent from user \'%s\' (timestamp=%s). Aborting...' % (broadcaster, str(timestamp)))
      return False
    
    remote_event = self.request.get('remote_event', default_value = None)
    if remote_event is None or remote_event in remote_events is False:
      logging.warning('admin_broadcast: Broken task (missing remote_event) sent from user \'%s\' (remote_event=%s). Aborting...' % (broadcaster, str(remote_event)))
      return False
    
    
    #message = self.request.get('message', default_value = None)
    
    broadcast.update({
      'timestamp'  : timestamp.isoformat().split('.')[0],
      'remoteEvent': remote_event
    })
    if remote_event == 'member_created' or remote_event == 'member_updated':
      data = self.request.get('data', default_value = None)
      if data is None:
        logging.warning('admin_broadcast: Broken task, \'category=%s\' but missing \'data\' param sent from user \'%s\'. Aborting...' % (remote_event, broadcaster))
        return False
      else:
        broadcast.update({
          'data': json.loads(data)
        })
    elif remote_event == 'admin_disconnected':
      pass
    elif remote_event == 'admin_connected':
      pass
    elif remote_event == 'mecenat_sync_initiated' or remote_event == 'mecenat_sync_data_downloaded' or remote_event == 'mecenat_sync_completed' or remote_event == 'mecenat_sync_failed':
      data = self.request.get('data', default_value = None)
      if data is None:
        logging.warning('admin_broadcast: Broken task, \'category=%s\' but missing \'data\' param sent from user \'%s\'. Aborting...' % (remote_event, broadcaster))
        return False
      else:
        broadcast.update(json.loads(data))
    elif remote_event == 'ssco_sync_initiated' or remote_event == 'ssco_sync_data_downloaded' or remote_event == 'ssco_sync_completed' or remote_event == 'ssco_sync_failed':
      data = self.request.get('data', default_value = None)
      if data is None:
        logging.warning('admin_broadcast: Broken task, \'category=%s\' but missing \'data\' param sent from user \'%s\'. Aborting...' % (remote_event, broadcaster))
        return False
      else:
        broadcast.update(json.loads(data))
    
    json_broadcast = json.dumps(broadcast)
    recipients = memcache.get(CONNECTED_ADMINS)
    if recipients is not None:
      for recipient in recipients:
        try:
          channel.send_message(recipient, json_broadcast)
        except:
          logging.warning('admin_broadcast: Message recipient \'%s\' unavailable (message=%s). Working on...' % (recipient, message))
  
  
  # /tasks/save_mecenat_transfer/
  def __save_mecenat_transfer(self):
    """A task that saves successful transfer-to-Mecenat operations to the
      datastore.
      
    This task belong to the queue group 'mecenatupdater' which contains the 
      individual queues mecenatupdater0, mecenatupdater1, mecenatupdater2 and
      mecenatupdater3.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'mecenatupdater'+str(random.randrange(4)),
          url = '/tasks/save_mecenat_transfer/',
          params = {
            'key'      : str(task.key()),
            'timestamp': pickle.dumps(datetime.datetime.today()),
            'updater'  : task.user_id
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      key: The datastore task descriptor for the update performed, a string.
      timestamp: A pickled datetime instance (UTC).
      updater: The updating admin's key name (i.e. personal ID), a string.
    """
    
    key = self.request.get('key', default_value = None)
    timestamp = pickle.loads(str(self.request.get('timestamp')))
    updater = self.request.get('updater', default_value = None)
    
    if key is None:
      logging.info('save_mecenat_transfer: \'key\' parameter is None. Aborting...')
      return False
    task = MecenatPushTask.get(key)
    if task is None:
      logging.info('save_mecenat_transfer: No task resolved to \'key\' parameter. Aborting...')
      return False
    
    logging.info('save_mecenat_transfer: Successfully added Mecenat transfer for member %s, updated by %s.' % (task.personalId, updater))
    task.sys_pushsuccesses.append(timestamp)
    task.save_success_state()
  
  
  # /tasks/save_mecenat_transfer/
  def __save_ssco_transfer(self):
    """A task that saves successful transfer-to-SSCO operations to the
      datastore.
      
    This task belong to the queue group 'sscoupdater' which contains the 
      individual queues sscoupdater0, sscoupdater1, sscoupdater2 and
      sscoupdater3.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'sscoupdater'+str(random.randrange(4)),
          url = '/tasks/save_ssco_transfer/',
          params = {
            'key'      : str(task.key()),
            'timestamp': pickle.dumps(datetime.datetime.today()),
            'updater'  : task.user_id
          }
        )
    
      
    Arguments (i.e. HTTP-POST params):
      key: The datastore task descriptor for the update performed, a string.
      timestamp: A pickled datetime instance (UTC).
      updater: The updating admin's key name (i.e. personal ID), a string.
    """
    
    key = self.request.get('key', default_value = None)
    timestamp = pickle.loads(str(self.request.get('timestamp')))
    updater = self.request.get('updater', default_value = None)
    
    if key is None:
      logging.info('save_ssco_transfer: \'key\' parameter is None. Aborting...')
      return False
    task = SscoPullTask.get(key)
    if task is None:
      logging.info('save_ssco_transfer: No task resolved to \'key\' parameter. Aborting...')
      return False
    
    logging.info('save_ssco_transfer: Successfully added SSCO transfer for member %s, updated by %s.' % (task.personalId, updater))
    task.sys_pullsuccesses.append(timestamp)
    task.save_success_state()
  
  
  # /tasks/store_session_trace/
  def __store_session_trace(self):
    """A task that queues rapid session trace save operations.
      
    This task belong to the queue group 'sessiontrace' which contains the 
      individual queues 'sessiontrace0', 'sessiontrace1', ..., 'sessiontrace9'.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'sessiontrace'+random.randrage(10),
          url = '/tasks/store_session_trace/',
          params = {
            'key_name' : str(session_trace_instance.key_name),
            'timestamp': pickle.dumps(datetime.datetime.today())
          },
          countdown = 60
        )
    
      
    Arguments (i.e. HTTP-POST params):
      key_name: The instance key name, a string.
      timestamp: A pickled datetime instance (UTC).
    """
    
    key_name = self.request.get('key_name', default_value = None)
    if key_name is not None:
      session_trace = memcache.get('session%s' % key_name)
      if session_trace is not None:
        session_trace.save()
      else:
        logging.info('RkhskTasks.__store_session_trace(): Task execution failed! Memcache object unavailable.')
    else:
      logging.info('RkhskTasks.__store_session_trace(): Task execution failed! No key supplied.')
      return False
  
  
  # /tasks/apply_changelog_update/
  def __apply_changelog_update(self):
    """A task that queues rapid changelog updates to select model instances.
      
    This task belong to the queue group 'changelogger' which contains the 
      individual queues 'systmpkeychangelogger' and 'rkhskmemberchangelogger'.
      
    To use:
    >>> taskqueue.add(
          queue_name = 'systmpkeychangelogger',
          url = '/tasks/apply_changelog_update/',
          params = {
            'key'      : str(model_instance.key()),
            'timestamp': pickle.dumps(datetime.datetime.today()),
            'updates'  : json.dumps(data)
          },
          countdown = 10
        )
    
      
    Arguments (i.e. HTTP-POST params):
      key: The datastore task descriptor for the update performed, a string.
      timestamp: A pickled datetime instance (UTC).
      updates: The updates to be applied, as JSON.
    """
    
    supported_models = ['SysTmpKey', 'RkhskMember']
    model = self.request.get('model', default_value = None)
    key = self.request.get('key', default_value = None)
    updates = self.request.get('updates', default_value = None)
    
    if model in supported_models:
      model = eval(model)
      instance = model.get(key)
      instance.sys_changelog.append(db.Text(updates))
      instance.put()
    else:
      return False
  


class RkhskFrontpage(webapp2.RequestHandler):
  """The public interface for the member UI at regga.rkh-sk.se/
    
  The request handler routes all incomming request through self.__unihandler
    where a task is set up and the appropriate method is called.
    
  The handler deploys secure cookies using the 'default' store and expiration
    is controlled by the global variable SESSION_DEFAULT_EXPIRATION.
  """
  
  # session management
  def dispatch(self):
    # Get a session store for this request.
    self.session_store = sessions.get_store(request=self.request)
    
    try:
      # Dispatch the request.
      webapp2.RequestHandler.dispatch(self)
    finally:
      # Save all sessions.
      self.session_store.save_sessions(self.response)
  
  @webapp2.cached_property
  def session(self):
    # Returns a session using the default cookie key.
    return self.session_store.get_session(name = 'default', backend = 'securecookie')
  
  
  # /
  # /admin
  def get(self, param = ''):
    """Request handler for serving the web application.
      
    Arguments:
      param: Supported values are '' (implies path '/') and 'admin' (implies
        path '/admin').
    """
    
    #self.session['session'] = param
    #config['session_dict'].add_flash(('foobarzz', param), level = None, key = '_flash')
    
    self.response.headers.add_header("Access-Control-Allow-Origin", "*")
    
    is_mobile = False
    mobile_tokens = [
      'Mobile Safari',
      'BlackBerry',
      'PlayBook',
      'iPhone',
      'iPod',
      'iPad',
      'Android'
    ]
    
    user_agent = self.request.headers['User-Agent']
    
    logging.info('RkhskFrontpage.get(param=\'%s\').user_agent: %s' % (param, user_agent))
    
    for token in mobile_tokens:
      if re.search(token, user_agent) is not None:
        logging.info('RkhskFrontpage.get(param=\'%s\').mobile_token: %s' % (param, token))
        is_mobile = True
    
    if param == '': # or len(param) >= 26:
      page = 'index-rd.html'
      if is_mobile:
        page = 'index-rm.html'
      regpage = open(page, 'r')
      self.response.write(regpage.read())
      regpage.close()
    
    elif param == 'admin':
      page = 'index-ad.html'
      if is_mobile:
        page = 'index-am.html'
      adminpage = open(page, 'r')
      
      self.response.write(adminpage.read())
      adminpage.close()    
    
    elif param == 'm':
      regpagem = open('index-rm.html', 'r')
      self.response.write(regpagem.read())
      regpagem.close()
    
    elif param == 'd':
      regpaged = open('index-rd.html', 'r')
      self.response.write(regpaged.read())
      regpaged.close()
      
    elif param == 'frame-r':
      self.response.write(reg_view_wrapper)
    
    elif param == 'frame-a':
      self.response.write(admin_view_wrapper)
    
    elif param == 'clear_cookies':
      self.session.clear()
      return
    
    else:
      self.error(404)
    
    session_id = self.session.get('session')
    if session_id is None:
      self.session['session'] = SessionTrace.generate_session_id()
  


class AccountCommand(Command):
  """Wrapper class for the member account UI functions."""
  
  def __init__(self, request, response, method, service, cmd, session_id, user_id, dev_mode = False):
    """Initialization method for the command wrapper.
      
    Arguments:
      request: A WebOb-compatible request object.
      method: The HTTP method used (i.e. "GET" or "POST").
      service: The main functionality requested.
      cmd: An optional, service-specific, command.
    """
    
    if dev_mode:
      logging.info('AccountCommand.__init__(request, response, method=\'%s\', service=\'%s\', cmd=\'%s\', session_id=\'%s\', user_id=\'%s\', dev_mode=%s)' % (str(method), str(service), str(cmd), str(session_id), str(user_id), str(dev_mode)))
    
    super(AccountCommand, self).__init__(RkhskMember, request, response, method, service, cmd, session_id, user_id, dev_mode, user_class = 'member', interface = 'account_interface')
    
    self.set_valid_keys() # defaults to match the dataset used by the registration form
  
  
  def set_valid_keys(self, valid_keys = None):
    """Setter for applying the 'valid_keys' attribute.
      
    The 'valid_keys' attribute control which attributes of a union member will
    be available for update as well as what set of attributes are returned by
    self.get_member_data(member).
      
    Arguments:
      valid_keys: An array containing the attributes to work with.
    """
    
    if valid_keys is None:
      valid_keys = ['firstName', 'lastName', 'streetCo', 'streetAddress', 'streetNumber', 'streetEntrance', 'streetApartment', 'streetFloor', 'zipCode', 'city', 'email', 'phone', 'lastApplicationFor']
    if self.dev_mode:
      logging.info('AccountCommand.set_valid_keys(%s)' % str(valid_keys))
    super(AccountCommand, self).set_valid_keys(valid_keys = valid_keys)
  
  
  def save_session(self):
    """Method for updating the session trace and trigger the evaluation
      process for user interactions.
    """
    
    self.session.update_session_trace(
      action = self.action,
      interface = self.interface,
      status_code = self.response_data['status_code'],
      request_data = self.dumps()
    )
  
  
  def dumps(self):
    """Method for dumping the command data.
      
    Returns:
      A dict with the command attribute configuration.
    """
    
    instance_data = super(AccountCommand, self).dumps([
      'client_ip',
      'session_id',
      'method',
      'service',
      'cmd',
      'user_class',
      'interface',
      'action',
      'description',
      'date_format',
      'valid_keys',
      'json_data',
      'param_data',
      'response_data'
    ])
    
    return instance_data
  


class RkhskAccountInterface(webapp2.RequestHandler):
  """The public interface for the member UI at regga.rkh-sk.se/
    
  The request handler routes all incomming request through self.__unihandler
    where a task is set up and the appropriate method is called.
    
  The handler deploys secure cookies using the 'account' store and expiration
    is controlled by the global variable SESSION_DEFAULT_EXPIRATION.
  """
  
  # session management
  def dispatch(self):
    # Get a session store for this request.
    self.session_store = sessions.get_store(request=self.request)
    
    try:
      # Dispatch the request.
      webapp2.RequestHandler.dispatch(self)
    finally:
      # Save all sessions.
      self.session_store.save_sessions(self.response)
  
  @webapp2.cached_property
  def session(self):
    # Returns a session using the default cookie key.
    return self.session_store.get_session(name='default', max_age = SESSION_DEFAULT_EXPIRATION, backend = 'securecookie')
  
  
  # redirects
  def get(self, service, cmd):
    """Handler for redirecting HTTP-GET requests to the member interface."""
    
    return self.__unihandler('GET', cmd, param)
  
  def post(self, service, cmd):
    """Handler for redirecting HTTP-POST requests to the member interface."""
    
    return self.__unihandler('POST', service, cmd)
  
  def __unihandler(self, method, service, cmd):
    """Method for delegating requested functionality to the appropriate service.
      
    Arguments:
      method: The HTTP method used (i.e. 'GET' or 'POST').
      service: The main functionality requested.
      cmd: An optional, service-specific, command.
    """
    
    starttime = time.time()
    
    command = AccountCommand(
      request = self.request,
      response = self.response,
      method = method,
      service = service,
      cmd = cmd,
      session_id = self.session.get('session_id'),
      user_id = self.session.get('user_id'),
      dev_mode = DEVMODE
    )
    
    unihandler = UniHandler(handler_name = 'RkhskAccountInterface.__unihandler()', command = command, dev_mode = DEVMODE)
    
    unihandler.handle_request()
    
    unihandler.serve_response(loadtime = { 'loadTime': '%s ms' % str(int((time.time() - starttime)*1000)) })
  
  
  # /account/retrieve/
  def retrieve(self, command):
    """Method for retrieving account data to the reg. page's details view.
      
    Arguments:
      param_data['loginUsername']: The member's email address.
      param_data['loginCredentials']: One of the member's recent payment keys.
      
    Returns:
      A JSON object with pretty-print member data.
    """
    
    if not command.from_verified_admin():
      return { 'success': False, 'status_code': 403, 'body': False }
    
    command.set_action('user_details_retrieval')
    command.set_description('A user-initiated account details retrieval.')
    command.set_valid_keys([
      'firstName',
      'lastName',
      'lastApplicationFor',
      'admittedSemester',
      'email',
      'phone',
      'studentConfirmation'
    ])
    
    try:
      email = command.param_data['loginUsername'].lower()
      payment_key = command.param_data['loginCredentials'].upper()
      logging.info('RkhskAccountInterface.__retrieve().loginUsername: %s' % email)
      logging.info('RkhskAccountInterface.__retrieve().loginCredentials: %s' % email)
    except (KeyError):
      return {
        'success'    : False,
        'status_code': 400, # bad request
        'errors'     : { 'invalidCredentials': 'Credentials missing.' }
      }
    if re.search('^[A-Z]{5}$', payment_key) is not None and re.search('^[a-z_\-\.\d]+@[a-z_\-\.\d]+\.[a-z]{2,4}$', email, flags=re.I) is not None:
      membercon = PaymentKey.get_by_key_name(payment_key)
      if not membercon:
        return {
          'success'    : False,
          'status_code': 400, # bad request
          'errors'     : { 'invalidCredentials': 'No member match for submitted params.' }
        }
      
      member = membercon.memberRef
      if member and member.email == email:
        
        street_address = '%s %s%s' % (member.streetAddress, member.streetNumber, member.streetEntrance)
        if member.streetApartment:
          street_address += ' / lgh. %s' % member.streetApartment
        if member.streetFloor:
          street_address += ', %s' % member.streetFloor
        street_address += '<br>SE-%s  %s' % (member.zipCode, member.city)
        if member.streetCo:
          street_address = 'c/o %s<br>%s' % (member.streetCo, street_address)
        
        application_semester_history = '[ Inga anmälningar registrerade ]'
        for i in range(len(member.applicationSemesterHistory)):
          if i == 0:
            application_semester_history = ''
          else:
            application_semester_history += ', '
          application_semester_history += '%s (%s)' % (member.applicationSemesterHistory[i], member.applicationDateHistory[i].strftime('%y-%m-%d'))
        
        try:
          last_payment_recieved = member.lastPaymentRecieved.strftime('%Y-%m-%d')
        except (NameError, AttributeError):
          last_payment_recieved = '[ Inga betalningar registrerade ]'
        
        membership_history = '[ Tidigare medlemskap saknas ]'
        if (len(member.membershipHistory)):
          membership_history = member.membershipHistory[0]
          for i in range(len(member.membershipHistory)):
            if i == 0:
              membership_history = ''
            else:
              membership_history += ', ' 
            membership_history += member.membershipHistory[i]
        
        mecenat_history = '[ Tidigare registreringar saknas ]'
        for i in range(len(member.mecenatHistory)):
          if i == 0:
            mecenat_history = ''
          else:
            mecenat_history += ', '
          mecenat_history += member.mecenatHistory[i]
        
        ssco_history = '[ Tidigare registreringar saknas ]'
        for i in range(len(member.sscoHistory)):
          if i == 0:
            ssco_history = ''
          else:
            ssco_history += ', '
          ssco_history += member.sscoHistory[i]
        
        member_data = command.get_member_data(member)
        member_data.update({
          'lastPaymentRecieved'       : last_payment_recieved,
          'applicationSemesterHistory': application_semester_history,
          'sscoHistory'               : ssco_history,
          'mecenatHistory'            : mecenat_history,
          'membershipHistory'         : membership_history,
          'streetAddress'             : street_address
        })
        
        if member.sys_email_verified is False:
          member.set_email_verified()
          member.put()
        
        return {
          'success': True,
          'data'   : member_data
        }
    
    return { 'success': False, 'status_code': 400, 'body': False }
  
  
  # /account/register/
  def register(self, command):
    """Method for recieving new membership applications.
      
    Arguments:
      param_data['admittedSemester']: The member's admission semester.
      param_data['lastApplicationFor']: The target application semester.
      param_data['firstName']: The member's first name.
      param_data['lastName']: The member's last name.
      param_data['streetCo']: The member's c/o address (optional).
      param_data['streetAddress']: The member's street address.
      param_data['streetNumber']: The member's street address number.
      param_data['streetEntrance']: The member's street entrance (optional).
      param_data['streetApartment']: The member's apartment number (optional).
      param_data['streetFloor']: The member's building floor (optional).
      param_data['zipCode']: The member's zip code.
      param_data['city']: The member's city.
      param_data['country']: The member's country.
      param_data['email']: The member's email address.
      param_data['phone']: The member's phone number (optional).
      param_data['studentConfirmation']: The member's confirmation of currently
        studying at RKH.
      
    Returns:
      A JSON object with the registered member data.
    """
    
    if not command.from_verified_admin():
      return { 'success': False, 'status_code': 403, 'body': False }
    
    command.set_action('user_registration')
    command.set_description('A user-initiated registration.')
    command.set_valid_keys([
      'admittedSemester',
      'lastApplicationFor',
      'firstName',
      'lastName',
      'streetCo',
      'streetAddress',
      'streetNumber',
      'streetEntrance',
      'streetApartment',
      'streetFloor',
      'zipCode',
      'city',
      'country',
      'email',
      'phone',
      'studentConfirmation'
    ])
    
    if command.param_data['studentConfirmation']:
      command.param_data['studentConfirmation'] = True
    else:
      command.param_data['studentConfirmation'] = False
    command.param_data['email'] = command.param_data['email'].lower()
    command.param_data['admittedSemester'] = command.param_data['admittedSemester'].upper()
    
    time.sleep(2)
    
    logging.info('RkhskAccountInterface.__register().command.cmd: %s' % str(command.param_data))
    if not (command.param_data['admittedSemester'] and command.param_data['lastApplicationFor'] and command.param_data['personalId'] and command.param_data['firstName'] and command.param_data['lastName'] and command.param_data['streetAddress'] and command.param_data['streetNumber'] and command.param_data['zipCode'] and command.param_data['city'] and command.param_data['country'] and command.param_data['email'] and command.param_data['studentConfirmation']):
      return { 'success': False, 'status_code': 403, 'body': False }
    
    if RkhskMember.create_or_update(command):
      return {
        'success': True,
        'data'   : member_data
      }
    else:
      return { 'success': False, 'status_code': 403, 'body': False }
  
  
  # /account/load/
  def load(self, command):
    """Method for loading a personalized application form.
      
    Arguments:
      cmd: A member's unique accountPageUri attribute (through AccountCommand).
      
    Returns:
      An array containing member details.
    """
    
    if not command.from_verified_admin():
      return { 'success': False, 'status_code': 403, 'body': False }
    
    if len(command.cmd) == 30:
      command.set_action('user_details_loader')
      command.set_description('Load user details to the membership application form.')
      command.set_valid_keys([
        'firstName',
        'lastName',
        'streetCo',
        'streetAddress',
        'streetNumber',
        'streetEntrance',
        'streetApartment',
        'admittedSemester',
        'streetFloor',
        'zipCode',
        'city',
        'phone',
        'email'
      ])
      
      logging.info('RkhskAccountInterface.__load().command.cmd: %s' % command.cmd)
      if re.search('^[\da-z]{30}$', command.cmd, flags=re.I) is not None:
        account_link = AccountUri.get_by_key_name(command.cmd)
        if account_link is None:
          return { 'success': False, 'status_code': 403, 'body': False } # forbidden
        try:
          member = account_link.memberRef
        except:
          return {
            'success'    : False,
            'status_code': 500, # internal server error
            'errors'     : { 'system': 'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.' }
          }
        if member.sys_deleted:
          return {
            'success'    : False,
            'status_code': 400, # bad request
            'errors'     : { 'accountDeleted': 'The associated account has been deleted.' }
          }
        if member.sys_email_verified is False:
          member.set_email_verified()
          member.put()
        member_data = command.get_member_data(member)
        return {
          'success': True,
          'data'   : member_data
        }
    
    elif len(command.cmd) == 26:
      command.set_action('user_credentials_loader')
      command.set_description('Load user details to the membership application form.')
      command.set_valid_keys([
        'email',
        'lastPaymentKey',
      ])
      logging.info('RkhskAccountInterface.__load().command.cmd: %s' % command.cmd)
      if re.search('^[\da-z]{26}$', command.cmd, flags=re.I) is not None:
        account_link = SysTmpKey.get_by_key_name(command.cmd)
        if account_link is None or account_link.is_member_login_reminder() is False:
          return { 'success': False, 'status_code': 403, 'body': False }
        if account_link.is_expired():
          return {
            'success'    : False,
            'status_code': 403,
            'errors'     : { 'linkExpired': 'Link expired %s' % account_link.sys_expired.isoformat().split('.')[0] }
          }
        member = account_link.get_member()
        if member is None:
          logging.warning('Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.')
          return {
            'success'    : False,
            'status_code': 500,
            'errors'     : { 'system': 'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.' }
          }
        if member.sys_deleted:
          return {
            'success'    : False,
            'status_code': 400, # bad request
            'errors'     : { 'accountDeleted': 'The associated account has been deleted.' }
          }
        else:
          account_link.set_expired()
          if member.sys_email_verified is False:
            member.set_email_verified()
            member.put()
          member_data = command.get_member_data(member)
          return {
            'success': True,
            'data'   : { 'loginUsername': member_data['email'], 'loginCredentials': member_data['lastPaymentKey'] }
          }
    
    logging.info('RkhskAccountInterface.__load().command.cmd: Unrecognized command \'%s\'' % command.cmd)
    
    return { 'success': False, 'status_code': 403, 'body': False }
  
  
  # /account/resend/
  def resend(self, command):
    """Method for sending out reminders with login information.
      
    Arguments:
      param_data['loginUsername']: The members email adress.
      
    Returns:
      A confirmation of submission to task queue.
    """
    
    if not command.from_verified_admin():
      return { 'success': False, 'status_code': 403, 'body': False }
    
    command.set_action('user_login_reminder')
    command.set_description('Resend user login details.')
    command.set_valid_keys([])
    
    try:
      email = command.param_data['loginUsername'] #self.request.get('loginUsername', default_value = '')
      logging.info('RkhskAccountInterface.__resend().email: %s' % email)
    except (KeyError):
      logging.info('RkhskAccountInterface.__resend(): No loginUsername supplied.')
    if email:
      account_link = AccountEmail.get_by_key_name(email)
      if account_link and account_link.memberRef:
        member = command.get_member(account_link.memberRef)
        member.set_tmpkey('member_login_reminder')
        member.put()
        
        taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_member_details_reminder/',
          params = {
            'personal_id': member.personalId,
            'client_ip'  : command.client_ip
          },
          countdown = 5
        )
        
        return {
          'success': True,
          'data'   : { 'emailSent': 'TaskQueued' }
        }
    
    return { 'success': False, 'status_code': 400, 'body': False } # bad request
  


class AdminCommand(Command):
  """Wrapper class for the admin UI functions.
    
  Attributes:
    request_id: An optional identifier for channeled requests.
  """
  
  def __init__(self, request, response, method, service, cmd, request_id, session_id, user_id, dev_mode = False):
    """Initialization method for the command wrapper.
      
    Arguments:
      request: A WebOb-compatible request object.
      method: The HTTP method used (i.e. "GET" or "POST").
      service: The main functionality requested.
      cmd: An optional, service-specific, command.
      request_id: An optional identifier for channeled requests.
    """
    
    if dev_mode:
      logging.info('AdminCommand.__init__(request, response, method=\'%s\', service=\'%s\', cmd=\'%s\', request_id=\'%s\', session_id=\'%s\', user_id=\'%s\', dev_mode=%s)' % (str(method), str(service), str(cmd), str(request_id), str(session_id), str(user_id), str(dev_mode)))
    
    super(AccountCommand, self).__init__(RkhskMember, request, response, method, service, cmd, session_id, user_id, dev_mode, request_id = request_id, user_class = 'admin', interface = 'admin_interface')
    
    self.set_valid_keys() # defaults to match the dataset used by the admin's member grid
  
  
  def set_valid_keys(self, valid_keys = None):
    """Setter for applying the 'valid_keys' attribute.
      
    The 'valid_keys' attribute control which attributes of a union member will
    be available for update as well as what set of attributes are returned by
    self.get_member_data(member).
      
    Arguments:
      valid_keys: An array containing the attributes to work with.
    """
    
    if valid_keys is None:
      valid_keys = ['firstName', 'lastName', 'streetCo', 'streetAddress', 'streetNumber', 'streetEntrance', 'streetApartment', 'streetFloor', 'zipCode', 'city', 'email', 'phone', 'sys_administrator', 'sys_accounttype', 'lastPaymentKey', 'lastApplicationDate', 'lastApplicationFor', 'lastMembershipFor', 'lastPaymentRecieved', 'lastMecenatReg', 'lastSscoReg']
    if self.dev_mode:
      logging.info('AdminCommand.set_valid_keys(%s)' % str(valid_keys))
    super(AdminCommand, self).set_valid_keys(valid_keys = valid_keys)
  
  
  def save_session(self):
    """Method for updating the session trace and trigger the evaluation
      process for user interactions.
    """
    
    self.session.update_session_trace(
      action = self.action,
      interface = self.interface,
      status_code = self.response_data['status_code'],
      request_data = self.dumps()
    )
  
  
  def dumps(self):
    """Method for dumping the command data.
      
    Returns:
      A dict with the command attribute configuration.
    """
    
    instance_data = super(AdminCommand, self).dumps([
      'client_ip',
      'session_id',
      'method',
      'service',
      'cmd',
      'user_class',
      'interface',
      'action',
      'description',
      'date_format',
      'valid_keys',
      'json_data',
      'param_data',
      'payload',
      'request_id',
      'response_data'
    ])
    
    return instance_data
  


class RkhskAdminInterface(webapp2.RequestHandler):
  """The public interface for the admin UI at http://regga.rkh-sk.se/admin.
    
  The request handler routes all incomming request through self.__unihandler
    where a AdminCommand is set up and the appropriate method is called.
    
  The handler deploys secure cookies using the 'default' store and cookie
    expiration is controlled by the global variable ADMIN_COOKIE_EXPIRATION.
  """
  
  # session management
  def dispatch(self):
    # Get a session store for this request.
    self.session_store = sessions.get_store(request=self.request)
    
    try:
      # Dispatch the request.
      webapp2.RequestHandler.dispatch(self)
    finally:
      # Save all sessions.
      self.session_store.save_sessions(self.response)
  
  @webapp2.cached_property
  def session(self):
    # Returns a session using the default cookie key.
    #return self.session_store.get_session(name = 'admin', max_age = ADMIN_COOKIE_EXPIRATION, backend = 'securecookie')
    return self.session_store.get_session(name = 'default', backend = 'securecookie')
  
  
  # redirects
  def post(self, service = None, cmd = None, request_id = None):
    """Handler for redirecting HTTP-POST requests to the admin interface."""
    
    return self.__unihandler('POST', service, cmd, request_id)
  
  def get(self, service = None, cmd = None, request_id = None):
    """Handler for redirecting HTTP-GET requests to the admin interface."""
    
    return self.__unihandler('GET', service, cmd, request_id)
  
  def __unihandler(self, method, service, cmd, request_id):
    """Method for delegating requested functionality to the appropriate service.
      
    Arguments:
      method: The HTTP method used (i.e. 'GET' or 'POST').
      service: The main functionality requested.
      cmd: An optional, service-specific, command.
      request_id: An optional identifier for channeled requests.
    """
    
    starttime = time.time()
    
    command = AdminCommand(
      request = self.request,
      response = self.response,
      method = method,
      service = service,
      cmd = cmd,
      request_id = request_id,
      session_id = self.session.get('session'),
      user_id = self.session.get('admin'),
      dev_mode = DEVMODE
    )
    
    logging.info('command: %s' % str(command.dumps()))
    
    unihandler = UniHandler(handler_name = 'RkhskAdminInterface.__unihandler()', command = command, dev_mode = DEVMODE)
    
    unihandler.handle_request()
    
    unihandler.serve_response(loadtime = { 'loadTime': '%s ms' % str(int((time.time() - starttime)*1000)) })
  
  
  # /admin/update_members/
  def update_members(self, command):
    """Method for performing updates of union member data.
      
    Arguments:
      json_data: A JSON array passed through AdminCommand.
      
    Returns:
      A dict with success status and the updated members count.
    """
    
    command.set_action('admin_member_update')
    command.set_description('Member update event from the admin gridview.')
    command.set_valid_keys([
      'lastApplicationFor',
      'firstName',
      'lastName',
      'streetCo',
      'streetAddress',
      'streetNumber',
      'streetEntrance',
      'streetApartment',
      'streetFloor',    
      'zipCode',        
      'city',           
      'email',          
      'phone',          
      'lastPaymentRecieved',
      'lastApplicationDate',
      'sys_administrator',  
      'sys_accounttype'
    ])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    results = RkhskMember.create_or_update(command)
    if results:
      results.update({ 'success': True })
      return results
    else:
      return {
        'success'    : False,
        'status_code': 400 # bad request
      }
  
  
  # /admin/verify_payments/
  def verify_payments(self, command):
    """Method for controlling payment registration.
      
    Arguments:
      request_id: An unique identifier for the request (through AdminCommand).
      json_data: A JSON array passed through AdminCommand.
      
    Returns:
      Two channeled JSON arrays with matched and unmatched payments and a HTTP
        response with matched/unmatched count.
    """
    
    command.set_action('admin_payments_verification')
    command.set_description('Payment verification event from the admin payments input field.')
    command.set_valid_keys([
      'sys_accounttype',
      'sys_administrator',
      'lastApplicationFor',
      'personalId',
      'firstName',
      'lastName',
      'lastApplicationDate'
    ])
    command.set_date_format('%Y-%m-%d')
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    mismatched = []
    matched = []
    for payment in payments:
      payment['lastPaymentKey'] = payment['lastPaymentKey'].upper()
      paymentcon = PaymentKey.get_by_key_name(payment['lastPaymentKey'])
      if paymentcon is None:
        payment.update({ 'payment': '100 kr' })
        mismatched.append(payment)
      else:
        logging.info('matched %s' % paymentcon.memberRef.personalId)
        entry = {
          'lastPaymentRecieved': payment['lastPaymentRecieved'],
          'lastPaymentKey'     : payment['lastPaymentKey'],
          'payment'            : '100 kr'
        }
        entry.update(command.get_member_data(paymentcon.memberRef))
        matched.append(entry)
    logging.info('command.user_id: %s, command.request_id: %s' % (command.user_id, command.request_id))
    channel.send_message(command.user_id, json.dumps({
      'itemsRecognized'  : matched,
      'itemsUnidentified': mismatched,
      'requestId'        : command.request_id,
    }))
    
    return {
      'success': True,
      'data'   : { 'paymentsRecognized': len(matched), 'paymentsUnidentified': len(mismatched) }
    }
  
  
  # /admin/update_payments/
  def update_payments(self, command):
    """Method for performing the actual update of payments recieved, post-validation.
      
    Arguments:
      json_data: A JSON array passed through AdminCommand.
      
    Returns:
      A HTTP response with payments registered count.
    """
    
    command.set_action('admin_payments_update')
    command.set_description('Payment update event from the admin payments input field.')
    command.set_valid_keys([
      'lastPaymentRecieved',
      'lastPaymentKey',
      'personalId'
    ])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    result = RkhskMember.create_or_update(command)
    if result:
      result.update({ 'success': True })
      return result
    else:
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
  
  
  # /admin/read/
  def read_members(self, command):
    """Method for serving admins with account information.
      
    Arguments:
      param_data['start']: The query 'start' param passed through AdminCommand.
      param_data['limit']: The query 'limit' param passed through AdminCommand.
      param_data['page']: The query 'page' param passed through AdminCommand.
      param_data['sort']: The query 'sort' param passed through AdminCommand
        as a JSON array.
      param_data['filter']: The query 'filter' param passed through 
        AdminCommand as a JSON array.
      
    Returns:
      An array containing member details.
    """
    
    command.set_action('admin_member_read')
    command.set_description('Read members page by page.')
    command.set_valid_keys([
      'firstName',
      'lastName',
      'streetCo',
      'streetAddress',
      'streetNumber',
      'streetEntrance',
      'streetApartment',
      'streetFloor',
      'zipCode',
      'city',
      'email',
      'phone',
      'sys_administrator',
      'sys_accounttype',
      'lastPaymentKey',
      'lastApplicationDate',
      'lastApplicationFor',
      'lastMembershipFor',
      'lastPaymentRecieved',
      'lastMecenatReg',
      'lastSscoReg'
    ])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    try:
      page = command.param_data['page']
    except (KeyError):
      page = None
    
    try:
      start = command.param_data['start']
    except (KeyError):
      start = None
    
    try:
      limit = command.param_data['limit']
    except (KeyError):
      limit = None
    
    try:
      sort = json.loads(command.param_data['sort'])
    except (KeyError, ValueError):
      sort = None
    
    try:
      filter = json.loads(command.param_data['filter'])
    except (KeyError, ValueError):
      filter = None
    
    logging.info('RkhskAdminInterface.__read_members(page = %s, start = %s, limit = %s, sort = %s, filter = %s)' % (str(page), str(start), str(limit), str(sort), str(filter)))
    
    query = "SELECT * FROM RkhskMember "
    
    if filter:
      query += 'WHERE %s IN (%r, %r) ' % (filter[0].property, 'news', 'press')
    logging.info('RkhskAdminInterface.__read_members(page, start, limit, sort, filter).query (post-filter): %s' % query)
    
    sortorder = []
    for i in range(len(sort)):
      sortorder.append('%s %s ' % (sort[i]['property'], sort[i]['direction']))
    if sortorder:
      query += 'ORDER BY %s' % string.join(sortorder, '')
    logging.info('RkhskAdminInterface.__read_members(page, start, limit, sort, filter).query (post-sort): %s' % query)
    
    try:
      if not start:
        raise ValueError('\'start\' param missing')
      if not limit:
        raise ValueError('\'limit\' param missing')
      query += 'LIMIT %s,%s ' % (start, int(limit))
      logging.info('RkhskAdminInterface.__read_members(page, start, limit, sort, filter).query (post-limit): %s' % query)
    except (TypeError, ValueError):
      pass
    
    query = query.rstrip()
    logging.info('RkhskAdminInterface.__read_members(page, start, limit, sort, filter).query: %s' % query)
    memcache_src = False
    
    members = []
    
    # first try memcache
    if memcache.get('speedparams_list_is_dirty') is not True:
      memberlist = memcache.get('speedparams_%s' % query)
      if memberlist is not None:
        rawmembers = memcache.get_multi(memberlist, key_prefix='speedparams_')
        for i in range(len(memberlist)):
          try:
            members.append(rawmembers[memberlist[i]])
          except (KeyError):
            logging.info('exception thrown')
            member = command.get_member_data(memberlist[i])
            memberdata = member.dumps(details)
            memcache.set('speedparams_%s' % memberlist[i], memberdata)
            members.append(memberdata)
        
        memcache_src = True
    
    # if not sufficient, use gds
    if memcache_src is False:
      collection = db.GqlQuery(query)
      mapping = {}
      personal_ids = []
      for member in collection:
        data = member.dumps(details)
        members.append(data) 
        mapping[data['personalId']] = data
        personal_ids.append(data['personalId'])
      
      logging.info('memberlist: %s' % personal_ids)
      mapping['list_is_dirty'] = False
      mapping[query] = personal_ids
      
      memcache.set_multi(mapping, key_prefix='speedparams_')
    
    return {
      'success'    : True,
      'data'       : members,
      'membersRead': len(members),
      'memberCount': SysConfig.get_member_count(),
      'memcacheSrc': memcache_src,
    }
  
  
  # /admin/create/
  def create_members(self, command):
    """Method for validating and creating new members from the admin UI.
      
    The following fields are enforced when creating a new member: 'firstName',
    'lastName', 'lastApplicationFor', 'personalId', 'streetAddress',
    'streetNumber', 'zipCode', 'city', 'country' and 'studentConfirmation'.
      
    Arguments:
      param_data: A member details form passed through AdminCommand.
    """
    
    command.set_action('admin_member_create')
    command.set_description('Member created from the admin "Add new member" panel.')
    command.set_valid_keys([
      'personalId',
      'admittedSemester',
      'lastApplicationFor',
      'firstName',
      'lastName',
      'streetCo',
      'streetAddress',
      'streetNumber',
      'streetEntrance',
      'lastPaymentRecieved',
      'streetApartment',
      'streetFloor',
      'zipCode',
      'city',
      'country',
      'email',
      'phone',
      'studentConfirmation'
    ])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    if 'admittedSemester' in command.param_data:
      command.param_data['admittedSemester'] = command.param_data['admittedSemester'].upper()
    
    if not (command.param_data['lastApplicationFor'] and command.param_data['personalId'] and command.param_data['firstName'] and command.param_data['lastName'] and command.param_data['streetAddress'] and command.param_data['streetNumber'] and command.param_data['zipCode'] and command.param_data['city'] and command.param_data['country'] and command.param_data['studentConfirmation']):
      return {
        'success'    : False,
        'status_code': 400,
        'errors'     : { 'invalidParameters': 'An expected field was missing.' }
      }
    
    result = RkhskMember.create_or_update(command)
    
    if result:
      result.update({ 'success': True })
      return result
    else:
      return {
        'success'    : False,
        'status_code': 400, # bad request
        'errors'     : { 'system': 'RkhskMember.create_or_update(command) failed.' }
      }
  
  
  # /admin/login/
  def login(self, command):
    """Method for validating admin login credentials.
      
    This method will evaluate login credentials and update the list of active
      admins on a HTTP POST request. After the session has been opened the 
      admin will send recurrent HTTP GET requests to the method in order to 
      refresh the secure cookie and make sure the list of active admins is up-
      to-date.
      
    Arguments:
      param_data['loginUsername']: The admin's email (through AdminCommand).
      param_data['loginPassword']: The admin's password (through AdminCommand).
      method: The HTTP request method (i.e. 'GET' or 'POST').
    """
    
    def update_active_admins(personal_id):
      if personal_id == 'su':
        personal_id = '841021-7171'
      active = memcache.get(ACTIVE_ADMINS)
      if active is None:
        active = {}
      else:
        newdict = {}
        for admin in active:
          if (now - active[admin]).seconds < 3600:
            newdict.update({ admin: active[admin] })
          else:
            logging.info('RkhskAdminInterface.__login(command): Removed %s from memcache active admins.' % admin)
        active = newdict
      active.update({ personal_id: now })
      memcache.set(ACTIVE_ADMINS, active, 3600)
    
    
    command.set_action('admin_login')
    command.set_description('An admin login event.')
    command.set_valid_keys([])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    
    now = datetime.datetime.today()
    expires = now + datetime.timedelta(minutes=10)
    
    connected = memcache.get(CONNECTED_ADMINS)
    logging.info('RkhskAdminInterface.__login(command).connected_admins: %s' % str(connected))
    
    if command.method == 'GET':
      logging.info('RkhskAdminInterface.__login(command).user_id: %s' % command.user_id)
      if not command.from_verified_admin():
        return {
          'success'    : False,
          'status_code': 403,
          'body'       : False
        }
      else:
        update_active_admins(command.user_id)
        self.session['admin'] = command.user_id
        return {
          'success': True,
          'data'   : { 'expires': expires.isoformat().split('.')[0] }
        }
    
    try:
      email = command.param_data['loginUsername']
      password = command.param_data['loginPassword']
      logging.info('RkhskAdminInterface.__login(command).credentials: %s/%s' % (email, password))
      if len(email) < 7 or len(password) < 6:
        raise ValueError('Invalid parameters recieved')
    except (KeyError, ValueError):
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    
    # first, try su login
    if email == 'regga@rkh-sk.se' and password == 't3nr3tni':
      update_active_admins('su')
      self.session['admin'] = 'su'
      return {
        'success': True,
        'data'   : { 'expires': expires.isoformat().split('.')[0] }
      }
    
    # then, try admin login
    email_connector = AccountEmail.get_by_key_name(email)
    if email_connector is not None:
      admin = email_connector.memberRef
      if verify_admin(admin.personalId) and admin.sys_password == password and admin.email == email:
        update_active_admins(admin.personalId)
        self.session['admin'] = admin.personalId
        logentry = db.Text(json.dumps({
          'admin_login': now.isoformat().split('.')[0]
        }))
        admin.sys_activitylog.append(logentry)
        admin.put()
        return {
          'success': True,
          'data'   : { 'expires': expires.isoformat().split('.')[0] }
        }
    
    return {
      'success'    : False,
      'status_code': 403,
      'body'       : False
    }
  
  
  # /admin/logout/
  def logout(self, command):
    """Method for clearing the user cookies in order to close the session.
      
    Arguments:
      user_id: The user ID stored in the secure cookie (through AdminCommand).
      
    Returns:
      A redirect to the domain root.
    """
    
    command.set_action('admin_logout')
    command.set_description('An admin logout event.')
    command.set_valid_keys([])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    
    now = datetime.datetime.today().isoformat().split('.')[0]
    
    if command.user_id is not None:
      active_admins = memcache.get(ACTIVE_ADMINS)
      activitylog_msg = None
      if active_admins is None:
        active_admins = {}
      else:
        try:
          active_admins.pop(command.user_id)
          activitylog_msg = { 'admin_logout': now }
          logging.info('RkhskAdminInterface.__logout(command): Removed \'%s\' from memcache active admins.' % command.user_id)
        except (KeyError):
          logging.info('RkhskAdminInterface.__logout(command): User \'%s\' not found in memcache active admins.' % command.user_id)
      
      if not any(activitylog_msg):
        activitylog_msg.update({ 'admin_logout_dirty': now })
      
      memcache.set(ACTIVE_ADMINS, active_admins, 3600)
      
      user = RkhskMember.get_by_key_name(command.user_id)
      if user is not None:
        user.sys_activitylog.append(db.Text(json.dumps(activitylog_msg)))
        user.put()
    
    self.session.clear()
    
    return {
      'success' : True,
      'redirect': '/',
      'body'    : False
    }
  
  
  # /admin/passwd/<sys_tmpKey|reset>
  def passwd(self, command):
    """Method for requesting a password reset and/or setting an administrators'
      password.
      
    Arguments:
      cmd: A command for controlling the method's behavior, valid inputs are
        'reset' or the key name of a SysTmpKey instance. 
      param_data['loginUsername']: The admins username (through AdminCommand).
      param_data['loginPassword']: The admins password (through AdminCommand).
      param_data['cfrmLoginPassword']: A second password parameter in order to
        confirm the admins input (through AdminCommand).
      
    Returns:
      A redirect to the domain root.
    """
    
    command.set_action('admin_passwd')
    command.set_description('Admin password update.')
    command.set_valid_keys([
      'email',
      'sys_password'
    ])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    
    if command.cmd == 'reset':
      try:
        login_username = command.param_data['loginUsername']
      except (KeyError):
        return {
          'success'    : False,
          'status_code': 400,
          'body'       : False
        }
      member_connector = AccountEmail.get_by_key_name(login_username)
      if member_connector and member_connector.memberRef and member_connector.memberRef.sys_administrator is True:
        member = member_connector.memberRef
        member.set_tmpkey('admin_password_reset')
        member.put()
        
        taskqueue.add(
          queue_name = 'mailer'+str(random.randrange(4)),
          url = '/tasks/send_admin_password_reset_email/',
          params = {
            'personal_id': member.personalId,
            'client_ip'  : command.client_ip
          },
          countdown = 5
        )
        return {
          'success': True,
          'data'   : { 'resetSentTo': login_username }
        }
      else:
        return {
          'success'    : False,
          'status_code': 400,
          'body'       : False
        }
    elif len(param) != 26:
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    elif re.search('^[\da-z]{26}$', param, flags=re.I) is not None:
      account_link = SysTmpKey.get_by_key_name(param)
      if account_link is None or account_link.is_admin_password_reset() is False:
        return {
          'success'    : False,
          'status_code': 403,
          'body'       : False
        }
      if account_link.is_expired():
        return {
          'success'    : False,
          'status_code': 403,
          'errors'     : { 'linkExpired': 'Link expired %s' % account_link.sys_expired.isoformat().split('.')[0] }
        }
      member = account_link.get_member()
      if member is None:
        logging.warning('Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.')
        return {
          'success'    : False,
          'status_code': 500,
          'errors'     : { 'system': 'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.' }
        }
      if member.sys_administrator is not True:
        return {
          'success'    : False,
          'status_code': 403, # bad request
          'errors'     : { 'adminAccessDenied': 'The account does not have admin access configured.' }
        }
      if member.sys_deleted:
        return {
          'success'    : False,
          'status_code': 400, # bad request
          'errors'     : { 'accountDeleted': 'The associated account has been deleted.' }
        }
      
      if command.method == 'GET':
        return {
          'success': True,
          'data'   : { 'loginUsername': member.email }
        }
      else:
        try:
          login_username = command.param_data['loginUsername']
          login_password = command.param_data['loginPassword']
          cfrm_login_password = command.param_data['cfrmLoginPassword']
        except (KeyError):
          return {
            'success'    : False,
            'status_code': 400,
            'body'       : False
          }
        if login_username and login_password and cfrm_login_password and login_password == cfrm_login_password:
          if member.email == login_username and len(login_password) > 5:
            member.sys_modifieddate = datetime.datetime.today()
            member.sys_password = login_password
            changelog = {
              'sys_password'    : member.sys_password,
              'sys_modifieddate': member.sys_modifieddate.isoformat().split('.')[0]
            }
            if member.sys_email_verified is False:
              member.sys_email_verified = True
              changelog.update({
                'sys_email_verified': True
              })
            member.sys_changelog.append(db.Text(json.dumps(changelog)))
            member.put()
            account_link.set_expired()
            return {
              'success': True,
              'data'   : { 'passwordSetFor': member.email }
            }
        
        return {
          'success'    : False,
          'status_code': 400,
          'errors'     : { 'invalidParams': 'Required parameters are missing or invalid.' }
        }
  
  
  # /admin/mecenat/<get_sync_details|setup_sync_operation|download_csv|confirm_transfer>
  def mecenat(self, command):
    """Method for administrating a transfer of member data to Mecenat.
      
    Arguments:
      cmd: A command for controlling the method's behavior, valid inputs are
        'get_sync_details', 'setup_sync_operation', 'download_csv' and
        'confirm_transfer' (through AdminCommand).
      request_id: An unique identifier for the request (through AdminCommand).
      
    Returns:
      cmd='get_sync_details': A JSON object with mecenatUsername and -Password.
      cmd='setup_sync_operation': An channel broadcast and HTTP response
        containing number of members to sync and the associated request ID.
      cmd='download_csv': CSV-formated member data in compliance with the 
        recipients file specification – a file download. The download is also
        announced through a channel broadcast.
      cmd='confirm_transfer': A notification of successful transfer, by channel
        broadcast and a HTTP response. The response contain the number of
        members transfered and the request ID.
    """
    
    command.set_action('admin_mecenat_transfer')
    command.set_description('Member data transfer to Mecenat.')
    command.set_valid_keys([])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    if command.cmd == 'get_sync_details':
      item_count = db.GqlQuery("SELECT __key__ FROM MecenatPushTask WHERE sys_pushcomplete = FALSE AND targetSemester = \'%s\'" % SysConfig.get_current_semester()).count()
      login = SysConfig.get_mecenat_login()
      return {
        'success': True,
        'data'   : { 'mecenatSyncCount': item_count, 'mecenatUsername': login[0], 'mecenatPassword': login[1] }
      }
      
    elif command.cmd == 'setup_sync_operation':
      sync_items = MecenatPushTask.gql("WHERE sys_pushcomplete = FALSE AND targetSemester = \'%s\'" % SysConfig.get_current_semester())
      item_count = sync_items.count()
      csv_data = []
      keys = []
      for task in sync_items:
        csv_data.append(task.dumps(as_csv = True))
        keys.append(str(command.key()))
      csv_data = string.join(csv_data, '')
      memcache.set('mecenat_sync_operation_%s%s' % (command.user_id, command.request_id), { 'csv_data': csv_data, 'task_keys': keys }, 3600)
      taskqueue.add(
        queue_name = 'broadcaster',
        url = '/tasks/admin_broadcast/',
        params = {
          'broadcaster' : command.user_id,
          'timestamp'   : pickle.dumps(datetime.datetime.today()),
          'remote_event': 'mecenat_sync_initiated',
          'data'        : json.dumps({ 'mecenatSyncCount': item_count, 'requestId': command.request_id })
        }
      )
      return {
        'success': True,
        'data'   : { 'mecenatSyncCount': item_count, 'requestId': command.request_id }
      }
      
    elif command.cmd == 'download_csv':
      operation = memcache.get('mecenat_sync_operation_%s%s' % (command.user_id, command.request_id))
      if operation is None:
        return {
          'success'    : False,
          'status_code': 400,
          'body'       : False
        }
      else:
        taskqueue.add(
          queue_name = 'broadcaster',
          url = '/tasks/admin_broadcast/',
          params = {
            'broadcaster' : command.user_id,
            'timestamp'   : pickle.dumps(datetime.datetime.today()),
            'remote_event': 'mecenat_sync_data_downloaded',
            'data'        : json.dumps({ 'mecenatSyncCount': len(operation['task_keys']), 'requestId': command.request_id })
          }
        )
        return {
          'success': True,
          'headers': { 'content_type': 'text/csv', 'content_disposition': 'attachment; filename=%s_mecenat.csv' % datetime.date.today().strftime('%y%m%d') },
          'body'   : operation['csv_data']
        }
      
    elif command.cmd == 'confirm_transfer':
      operation = memcache.get('mecenat_sync_operation_%s%s' % (command.user_id, command.request_id))
      if operation is None:
        taskqueue.add(
          queue_name = 'broadcaster',
          url = '/tasks/admin_broadcast/',
          params = {
            'broadcaster' : command.user_id,
            'timestamp'   : pickle.dumps(datetime.datetime.today()),
            'remote_event': 'mecenat_sync_failed',
            'data'        : json.dumps({ 'requestId': command.request_id })
          }
        )
        return {
          'success'    : False,
          'status_code': 400,
          'body'       : False
        }
      else:
        task_entries = operation['task_keys']
        
        taskqueue.add(
          queue_name = 'broadcaster',
          url = '/tasks/admin_broadcast/',
          params = {
            'broadcaster' : command.user_id,
            'timestamp'   : pickle.dumps(datetime.datetime.today()),
            'remote_event': 'mecenat_sync_completed',
            'data'        : json.dumps({ 'mecenatSyncCount': len(task_entries), 'requestId': command.request_id })
          }
        )
        
        for task_key in task_entries:
          taskqueue.add(
            queue_name = 'mecenatupdater%s' % str(random.randrange(4)),
            url = '/tasks/save_mecenat_transfer/',
            params = {
              'key'      : task_key,
              'timestamp': timestamp,
              'updater'  : command.user_id
            }
          )
        
        return {
          'success': True,
          'data'   : { 'mecenatSyncCount': len(task_entries), 'requestId': command.request_id }
        }
    
    else:
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
  
  
  # /admin/ssco/<get_sync_details|setup_sync_operation|download_csv|confirm_transfer>
  def ssco(self, command):
    """Method for administrating a transfer of member data to SSCO.
      
    Arguments:
      cmd: A command for controlling the method's behavior, valid inputs are
        'get_sync_details', 'setup_sync_operation', 'download_csv' and
        'confirm_transfer' (through AdminCommand).
      request_id: An unique identifier for the request (through AdminCommand).
      
    Returns:
      cmd='get_sync_details': A JSON object with sscoUsername and sscoPassword.
      cmd='setup_sync_operation': An channel broadcast and HTTP response
        containing number of members to sync and the associated request ID.
      cmd='download_csv': CSV-formated member data in compliance with the 
        recipients file specification – a file download. The download is also
        announced through a channel broadcast.
      cmd='confirm_transfer': A notification of successful transfer, by channel
        broadcast and a HTTP response. The response contain the number of
        members transfered and the request ID.
    """
    
    command.set_action('admin_ssco_transfer')
    command.set_description('Member data transfer to SSCO.')
    command.set_valid_keys([])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    if command.cmd == 'get_sync_details':
      item_count = db.GqlQuery("SELECT __key__ FROM SscoPullTask WHERE sys_pullcomplete = FALSE AND targetSemester = \'%s\'" % SysConfig.get_current_semester()).count()
      login = SysConfig.get_ssco_login()
      return {
        'success': True,
        'data'   : { 'sscoSyncCount': item_count, 'sscoUsername': login[0], 'sscoPassword': login[1] }
      }
      
    elif command.cmd == 'setup_sync_operation':
      sync_items = SscoPullTask.gql("WHERE sys_pullcomplete = FALSE AND targetSemester = \'%s\'" % SysConfig.get_current_semester())
      item_count = sync_items.count()
      csv_data = []
      keys = []
      for task in sync_items:
        if not csv_data:
          csv_data.append(task.dumps(as_csv = True)['header'])
        csv_data.append(task.dumps(as_csv = True)['memberData'])
        keys.append(str(task.key()))
      csv_data = string.join(csv_data, '')
      
      memcache.set('ssco_sync_operation_%s%s' % (command.user_id, command.request_id), { 'csv_data': csv_data, 'task_keys': keys }, 3600)
      taskqueue.add(
        queue_name = 'broadcaster',
        url = '/tasks/admin_broadcast/',
        params = {
          'broadcaster' : command.user_id,
          'timestamp'   : pickle.dumps(datetime.datetime.today()),
          'remote_event': 'ssco_sync_initiated',
          'data'        : json.dumps({ 'sscoSyncCount': item_count, 'requestId': command.request_id })
        }
      )
      return {
        'success': True,
        'data'   : { 'sscoSyncCount': item_count, 'requestId': command.request_id }
      }
      
    elif command.cmd == 'download_csv':
      operation = memcache.get('ssco_sync_operation_%s%s' % (command.user_id, command.request_id))
      if operation is None:
        return {
          'success'    : False,
          'status_code': 400,
          'body'       : False
        }
      else:
        taskqueue.add(
          queue_name = 'broadcaster',
          url = '/tasks/admin_broadcast/',
          params = {
            'broadcaster' : command.user_id,
            'timestamp'   : pickle.dumps(datetime.datetime.today()),
            'remote_event': 'ssco_sync_data_downloaded',
            'data'        : json.dumps({ 'sscoSyncCount': len(operation['task_keys']), 'requestId': command.request_id })
          }
        )
        return {
          'success': True,
          'headers': { 'content_type': 'text/csv', 'content_disposition': 'attachment; filename=%s_ssco.csv' % datetime.date.today().strftime('%y%m%d') },
          'body'   : operation['csv_data']
        }
      
    elif command.cmd == 'confirm_transfer':
      operation = memcache.get('ssco_sync_operation_%s%s' % (command.user_id, command.request_id))
      if operation is None:
        taskqueue.add(
          queue_name = 'broadcaster',
          url = '/tasks/admin_broadcast/',
          params = {
            'broadcaster' : command.user_id,
            'timestamp'   : pickle.dumps(datetime.datetime.today()),
            'remote_event': 'ssco_sync_failed',
            'data'        : json.dumps({ 'requestId': command.request_id })
          }
        )
        return {
          'success'    : False,
          'status_code': 400,
          'body'       : False
        }
      else:
        task_entries = operation['task_keys']
        
        taskqueue.add(
          queue_name = 'broadcaster',
          url = '/tasks/admin_broadcast/',
          params = {
            'broadcaster' : command.user_id,
            'timestamp'   : pickle.dumps(datetime.datetime.today()),
            'remote_event': 'ssco_sync_completed',
            'data'        : json.dumps({ 'sscoSyncCount': len(task_entries), 'requestId': command.request_id })
          }
        )
        
        for task_key in task_entries:
          taskqueue.add(
            queue_name = 'sscoupdater%s' % str(random.randrange(4)),
            url = '/tasks/save_ssco_transfer/',
            params = {
              'key'      : task_key,
              'timestamp': timestamp,
              'updater'  : command.user_id
            }
          )
        
        return {
          'success': True,
          'data'   : { 'sscoSyncCount': len(task_entries), 'requestId': command.request_id }
        }
    
    else:
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
  
  
  # /admin/memcache/<flush_all|get_stats|admins>
  def memcache(self, command): # DEV
    """Method for inspecting select memcache entries, flushing and displaying
      usage stats.
      
    Arguments:
      cmd: A command for controlling the method's behavior, valid inputs are
        'flush_all', 'get_stats' and 'admins' (through AdminCommand).
      
    Returns:
      cmd='flush_all': Success.
      cmd='get_stats': A JSON object with key stats.
      cmd='admins': A JSON object with admins currently connected to the system
        and the current number of admin connections.
    """
    
    command.set_action('admin_dev_memcache_mgnt')
    command.set_description('Memcache management.')
    command.set_valid_keys([])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    if DEVMODE is not True:
      logging.info('AdminInterface.__memcache(cmd=\'%s\'): Method called with DEVMODE disabled.' % command.cmd)
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    if command.cmd == 'flush_all':
      memcache.flush_all()
      return {
        'success': True
      }
    
    elif command.cmd == 'get_stats':
      return {
        'success': True,
        'data'   : { 'memcacheStats': memcache.get_stats() }
      }
    
    elif command.cmd == 'admins':
      
      connection_data = []
      connections = memcache.get(CONNECTED_ADMINS)
      if connections is not None:
        connection_data = connections
      
      admin_data = {}
      admins = memcache.get(ACTIVE_ADMINS)
      if admins is not None:
        for admin in admins:
          admindata.update({ admin: admins[admin].isoformat().split('.')[0] })
      
      return {
        'success': True,
        'data'   : { 'adminsActive': admin_data, 'adminsConnected': connection_data }
      }
    
    else:
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
  
  
  # /admin/datastore/<flush_all|get_stats>
  def datastore(self, command): # DEV
    """Method for inspecting select datastore entries, flushing and displaying
      usage stats.
      
    Arguments:
      cmd: A command for controlling the method's behavior, valid inputs are
        'flush_all', 'get_stats' and 'admins' (through AdminCommand).
      
    Returns:
      cmd='flush_all': Success.
      cmd='get_stats': A JSON object with the number of entries in each of the
        base models.
    """
    
    command.set_action('admin_dev_datastore_mgnt')
    command.set_description('Datastore management.')
    command.set_valid_keys([])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
      
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    if DEVMODE is not True:
      logging.info('AdminInterface.__datastore(cmd=\'%s\'): Method called with DEVMODE disabled.' % command.cmd)
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    counts = {
      'RkhskMember'    : RkhskMember.all().count(),
      'InboundEmail'   : InboundEmail.all().count(),
      'SentEmail'      : InboundEmail.all().count(),
      'AccountEmail'   : AccountEmail.all().count(),
      'TempFile'       : TempFile.all().count(),
      'PaymentKey'     : PaymentKey.all().count(),
      'SysTmpKey'      : SysTmpKey.all().count(),
      'AccountUri'     : AccountUri.all().count(),
      'SscoPullTask'   : SscoPullTask.all().count(),
      'MecenatPushTask': MecenatPushTask.all().count()
    }
      
    if command.cmd == 'flush_all':
      db.delete(RkhskMember.all())
      db.delete(SentEmail.all())
      db.delete(InboundEmail.all())
      db.delete(AccountEmail.all())
      db.delete(TempFile.all())
      db.delete(PaymentKey.all())
      db.delete(SysTmpKey.all())
      db.delete(AccountUri.all())
      db.delete(SscoPullTask.all())
      db.delete(MecenatPushTask.all())
      return {
        'success': True,
        'data'   : { 'entriesDeleted': counts }
      }
      
    elif command.cmd == 'get_stats':
      return {
        'success': True,
        'data'   : { 'entriesStored': counts }
      }
      
    else:
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
  
  
  # /admin/dumps/<mecenat_push_tasks|mecenat_export|ssco_pull_tasks|ssco_export|members|owa_contacts>
  def dumps(self, command): # DEV
    """Method for dumping export tasks, a full members list and OWA contacts.
      
    Arguments:
      cmd: A command for controlling the method's behavior, valid inputs are
        'mecenat_push_tasks', 'mecenat_export', 'ssco_pull_tasks',
        'ssco_export', 'members' and 'owa_contacts' (through AdminCommand).
      
    Returns:
      cmd='mecenat_push_tasks': A JSON array containing all export tasks.
      cmd='mecenat_export': A CSV file download containing all exports for the
        current semester.
      cmd='ssco_pull_tasks': A JSON array containing all export tasks.
      cmd='ssco_export': A CSV file download containing all exports for the
        current semester.
      cmd='members': A JSON array containing all members in the database.
      cmd='owa_contacts': A JSON array all Outlook Web Access contacts stored
        in the system.
    """
    
    command.set_action('admin_dev_dumps')
    command.set_description('Dumping datastore entries.')
    command.set_valid_keys([])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    if DEVMODE is not True:
      logging.info('AdminInterface.__dumps(cmd=\'%s\'): Method called with DEVMODE disabled.' % command.cmd)
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
      
    if command.cmd == 'mecenat_push_tasks':
      task_data = []
      tasks = MecenatPushTask.all()
      for task in tasks:
        task_data.append(task.dumps(raw = True))
      
      return {
        'success': True,
        'data'   : { 'MecenatPushTasks': task_data }
      }
    
    elif command.cmd == 'mecenat_export':
      csv_data = []
      current_semester = SysConfig.get_current_semester()
      tasks = MecenatPushTask.all()
      for task in tasks:
        if task.targetSemester != current_semester:
          continue
        csv_data.append(task.dumps(as_csv = True))
      csv_data = string.join(csv_data, '')
      
      return {
        'success': True,
        'headers': { 'content_type': 'text/csv', 'content_disposition': 'attachment; filename=%s_mecenat.csv' % datetime.date.today().strftime('%y%m%d') },
        'body'   : csv_data
      }
    
    elif command.cmd == 'ssco_pull_tasks':
      task_data = []
      tasks = SscoPullTask.all()
      for task in tasks:
        task_data.append(task.dumps(raw = True))
      csv_data = string.join(csv_data, '')
      
      return {
        'success': True,
        'data'   : { 'SscoPullTasks': task_data }
      }
    
    elif command.cmd == 'ssco_export':
      csv_data = []
      current_semester = SysConfig.get_current_semester()
      tasks = SscoPullTask.all()
      for task_entry in tasks:
        if task.targetSemester != current_semester:
          continue
        if csv_data:
          csv_data.append(u'%s' % task.dumps(as_csv = True)['member_data'])
        else:
          csv_data.append(u'%s%s' % (task.dumps(as_csv = True)['header'], task.dumps()['member_data']))
      csv_data = string.join(csv_data, '')
      
      return {
        'success': True,
        'headers': { 'content_type': 'text/csv', 'content_disposition': 'attachment; filename=%s_ssco.csv' % datetime.date.today().strftime('%y%m%d') },
        'body'   : csv_data
      }
    
    elif command.cmd == 'members':
      member_data = []
      members = RkhskMember.all()
      for member in members:
        data = member.dumps()
        parsedlog = []
        for entry in data['sys_changelog']:
          parsedlog.append(json.loads(entry))
        data['sys_changelog'] = parsedlog
        member_data.append(data)
      
      return {
        'success': True,
        'data'   : { 'memberData': member_data }
      }
    
    elif command.cmd == 'owa_contacts':
      
      contact_data = []
      contacts = OwaUser.all()
      for contact in contacts:
        contact_data.append(contact.dumps())
      
      return {
        'success': True,
        'data'   : { 'OwaUsers': contact_data }
      }
    
    elif command.cmd == 'sysconfig':
      sysconfig = SysConfig.load()
      response_conf = {}
      for key, value in sysconfig.iteritems():
        if isinstance(value, datetime.datetime) or isinstance(value, datetime.date):
          response_conf.update({ key: value.isoformat().split('.')[0] })
        else:
          response_conf.update({ key: value })
      
      return {
        'success': True,
        'data'   : { 'SysConfig': response_conf }
      }
    
    elif command.cmd == 'payment_keys':
      key_data = []
      keys = PaymentKey.all()
      for key in keys:
        key_data.append(key.dumps())
      
      return {
        'success': True,
        'data'   : { 'PaymentKeys': key_data }
      }
    
    elif command.cmd == 'sys_tmp_keys':
      key_data = []
      keys = SysTmpKey.all()
      for key in keys:
        key_data.append(key.dumps())
      
      return {
        'success': True,
        'data'   : { 'SysTmpKeys': key_data }
      }
    
    elif command.cmd == 'account_uris':
      accounturi_data = []
      accounturis = AccountUri.all()
      for uri in accounturis:
        accounturi_data.append(uri.dumps())
      
      return {
        'success': True,
        'data'   : { 'AccountUris': accounturi_data }
      }
    
    elif command.cmd == 'account_emails':
      email_data = []
      addresses = AccountEmail.all()
      for address in addresses:
        email_data.append(address.dumps())
      
      return {
        'success': True,
        'data'   : { 'AccountEmails': email_data }
      }
    
    elif command.cmd == 'session_traces':
      session_trace_data = []
      session_traces = SessionTrace.all()
      for session_trace in session_traces:
        session_trace_data.append(session_trace.dumps())
      
      return {
        'success': True,
        'data'   : { 'SessionTraces': session_trace_data }
      }
    
    return {
      'success'    : False,
      'status_code': 400,
      'body'       : False
    }
    
  
  
  # /admin/trigger/<sync_to_mecenat|send_activation_notice>/
  def trigger(self, command): # DEV
    """Method for manually triggering processes during development.
      
    Arguments:
      cmd: A command for controlling the method's behavior, valid inputs
        are 'sync_to_mecenat' and 'send_activation_notice' (passed through
        AdminCommand).
      
    Returns:
      cmd='sync_to_mecenat': A success notice stating 'TaskQueued'.
      cmd='send_activation_notice': A JSON object with target member details.
    """
    
    command.set_action('admin_dev_triggers')
    command.set_description('Admin development triggers.')
    command.set_valid_keys([])
    
    if not command.is_valid_session():
      return {
        'success'    : False,
        'status_code': 400,
        'body'       : False
      }
    
    if not command.from_verified_admin():
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    if DEVMODE is not True:
      logging.info('AdminInterface.__trigger(cmd=\'%s\'): Method called with DEVMODE disabled.' % command.cmd)
      return {
        'success'    : False,
        'status_code': 403,
        'body'       : False
      }
    
    if command.cmd == 'sync_to_mecenat':
      taskqueue.add(
        queue_name = 'registrator',
        url = '/tasks/sync_to_mecenat/',
        params = {
          #'personal_id': '841021-7171',
          'sys_activator': command.user_id
        }
      )
      
      return {
        'success': True,
        'data'   : { 'syncToMecenat': 'TaskQueued' }
      }
    
    if command.cmd == 'send_member_registration_reminder':
      target_members = db.GqlQuery("SELECT * FROM RkhskMember WHERE lastApplicationFor = 'HT11'")
      personal_ids = []
      skipped = []
      for personal_id in target_members:
        member = RkhskMember.get_by_key_name(personal_id)
        if member is None or member.email is None:
          skipped.append(personal_id)
          continue
        personal_ids.append(personal_id)
        taskqueue.add(
          queue_name = 'mailer%s' % str(random.randrange(4)),
          url = '/tasks/send_member_registration_reminder/',
          params = {
            'personal_id': personal_id
          },
          countdown = random.randrange(15,90)
        )
      
      return {
        'success': True,
        'data'   : { 'sendActivationEmail': 'TaskQueued', 'targetMembers': sentpids, 'targetMemberCount': len(sentpids), 'skippedMembers': skipped }
      }
    
    return {
      'success'    : False,
      'status_code': 400,
      'body'       : False
    }
  


# TODO(mats.blomdahl@gmail.com): Prio 1 todo, fix override!
class AdminUploadCommand(AdminCommand):
  """Wrapper class for the admin UI upload function.
    
  Attributes:
    request_id: An optional identifier for channeled requests.
  """
  
  def __init__(self):
    raise NotImplementedError()
  


class RkhskAdminUpload(blobstore_handlers.BlobstoreUploadHandler):
  """The public interface for setting up URIs for blob uploads and handling
    submitted uploads.
    
  The interface deploys secure cookies using the 'default' store and cookie
    expiration is controlled by the global variable ADMIN_COOKIE_EXPIRATION.
  """
  
  # session management
  def dispatch(self):
    # Get a session store for this request.
    self.session_store = sessions.get_store(request = self.request)
    
    try:
      # Dispatch the request.
      webapp2.RequestHandler.dispatch(self)
    finally:
      #pass
      # Save all sessions.
      self.session_store.save_sessions(self.response)
  
  @webapp2.cached_property
  def session(self):
    # Returns a session using the default cookie key.
    return self.session_store.get_session(name = 'default', max_age = ADMIN_COOKIE_EXPIRATION, backend = 'securecookie')
  
  
  # /admin/upload/<payments|attachment>/<request_id>
  def get(self, filetype, request_id):
    """Method for creating upload URIs on-demand.
      
    Arguments:
      filetype: The 'filetype' param control how the blob will be processed
        once uploaded, the method currently accepts the values 'attachment' 
        and 'payments'.
      request_id: A unique request ID associated with the upload, used for 
        attaching client-side event handlers to the GAE channel service.
      
    Returns:
      A JSON object with a upload token (URI).s
    """
    
    command = AdminUploadCommand(
      request = self.request,
      response = self.response,
      method = 'GET',
      service = 'upload',
      cmd = filetype,
      request_id = request_id,
      session_id = self.session.get('session_id'),
      user_id = self.session.get('user_id'),
      dev_mode = DEVMODE
    )
    command.set_action('admin_upload')
    command.set_description('Create blob upload token.')
    
    if command.dev_mode:
      logging.info('RkhskAdminUpload.get(filetype=\'%s\', request_id=\'%s\').user_id: \'%s\'' % (filetype, request_id, str(command.user_id)))
    
    response_data = None
    
    if not command.from_verified_admin():
      response = {
        'success'    : False,
        'status_code': 403,
        'errors'     : { 'accessDenied': 'The user does not have sufficient privileges to call /admin/upload/%s/%s.' % (str(filetype), str(request_id)) }
      }
      self.response.set_status(403) # forbidden
    
    elif filetype != 'payments' and filetype != 'attachment':
      response_data = {
        'success'    : False,
        'status_code': 400,
        'errors'     : { 'invalidRequest': 'The call to /admin/upload/%s/%s does not match the interface specification.' % (str(filetype), str(request_id)) }
      }
      self.response.set_status(400) # bad request
    
    else:
      upload_token = blobstore.create_upload_url('/admin/upload/%s/%s' % (filetype, request_id))
      
      if command.dev_mode:
        logging.info('RkhskAdminUpload.get(filetype=\'%s\', request_id=\'%s\').upload_token: \'%s\'' % (filetype, request_id, upload_token))
      
      response_data = {
        'success'    : True,
        'status_code': 200,
        'data'       : { 'uploadToken': upload_token }
      }
      self.response.headers.add_header("Access-Control-Allow-Origin", "*")
      self.response.content_type = 'application/json'
      self.response.body = json.dumps(response_data)
    
    command.set_response(response_data)
    command.save_session()
  
  
  # TODO(mats.blomdahl@gmail.com): Implement processing for filetype 'attachment'.
  # /admin/upload/<payments|attachment>/<request_id>
  def post(self, filetype, request_id):
    """Method for recieving, processing and storing uploaded blobs.
      
    Arguments:
      filetype: The 'filetype' param control how an uploaded blob is processed,
        the method currently accepts the values 'attachment' and 'payments'.
      request_id: A unique request ID associated with the upload.
      
    Returns:
      A channeled message containing raw file information and the results from
        data processing.
    """
    
    command = AdminUploadCommand(
      request = self.request,
      response = self.response,
      method = 'POST',
      service = 'upload',
      cmd = filetype,
      request_id = request_id,
      session_id = self.session.get('session_id'),
      user_id = self.session.get('user_id'),
      dev_mode = DEVMODE
    )
    command.set_action('admin_upload')
    command.set_description('Handle submitted blob upload.')
    
    response_data = None
    
    if not command.from_verified_admin():
      response_data = {
        'success'    : False,
        'status_code': 403,
        'errors'     : { 'accessDenied': 'The user does not have sufficient privileges to call /admin/upload/%s/%s.' % (str(filetype), str(request_id)) }
      }
      command.set_response(response_data)
      command.save_session()
      self.response.set_status(403)
      return
    
    if command.dev_mode:
      logging.info('RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').user_id: \'%s\'' % (filetype, request_id, str(command.user_id)))
    
    try:
      if not any(self.get_uploads()) and command.dev_mode:
        logging.info("RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\'): Fatal error! No blobinfo.")
        
      upload_info = self.get_uploads("datafile")
      
      logging.info(upload_info[0].filename)
      logging.info(upload_info[0].content_type)
      logging.info(upload_info[0].size)
      
      temp_file = TempFile(
        key_name = str(upload_info[0].key()),
        fileKey = upload_info[0].key(),
        fileName = upload_info[0].filename,
        fileContentType = upload_info[0].content_type,
        fileSize = upload_info[0].size,
        fileCreation = upload_info[0].creation,
        fileType = filetype,
        fileCreatedBy = command.user_id
      )
      
      body = blobstore.fetch_data(upload_info[0].key(), 0, blobstore.MAX_BLOB_FETCH_SIZE-1)
      
      channel_data = {
        'fileName'       : upload_info[0].filename,
        'fileSize'       : upload_info[0].size,
        'fileContentType': upload_info[0].content_type,
        'fileCreation'   : upload_info[0].creation.isoformat(),
        'fileKey'        : str(upload_info[0].key()),
        'requestId'      : request_id,
        'userId'         : command.user_id
      }
      
      db.put(temp_file)
      
      if filetype == 'payments':
        channel_data.update(self.__parse_payments(body))
      
      elif filetype == 'attachment':
        response_data = {
          'success'    : False,
          'status_code': 400,
          'errors'     : { 'notImplemented': 'Upload of mail attachment is yet to be implemented.' }
        }
        self.response.set_status(400)
        return
      
      else:
        response_data = {
          'success'    : False,
          'status_code': 400,
          'errors'     : { 'invalidRequest': 'The call to /admin/upload/%s/%s does not match the interface specification.' % (str(filetype), str(request_id)) }
        }
        command.set_response(response_data)
        command.save_session()
        self.response.set_status(400)
        return
      
      channel.send_message(command.user_id, json.dumps(channel_data))
      response_data = {
        'success'    : True,
        'status_code': 200,
        'data'       : channel_data,
        'body'       : []
      }
      
    except blobstore.Error:
      logging.info("RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: blobstore.Error")
      response_data = {
        'success'    : False,
        'status_code': 500,
        'errors'     : { 'blobstoreError': 'The call to /admin/upload/%s/%s raised a blobstore.Error exception.' % (str(filetype), str(request_id)) }
      }
    
    except blobstore.BlobInfoParseError:
      logging.info("RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: blobstore.BlobInfoParseError")
      response_data = {
        'success'    : False,
        'status_code': 500,
        'errors'     : { 'blobInfoParseError': 'The call to /admin/upload/%s/%s raised a blobstore.BlobInfoParseError exception.' % (str(filetype), str(request_id)) }
      }
    
    except blobstore.BlobNotFoundError:
      logging.info("RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: blobstore.BlobNotFoundError")
      response_data = {
        'success'    : False,
        'status_code': 404,
        'errors'     : { 'blobNotFoundError': 'The call to /admin/upload/%s/%s raised a blobstore.BlobNotFoundError exception.' % (str(filetype), str(request_id)) }
      }
    
    except blobstore.InternalError:
      logging.info("RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: blobstore.InternalError")
      response_data = {
        'success'    : False,
        'status_code': 500,
        'errors'     : { 'blobstoreInternalError': 'The call to /admin/upload/%s/%s raised a blobstore.InternalError exception.' % (str(filetype), str(request_id)) }
      }
    
    except IndexError:
      logging.info("RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: IndexError")
      response_data = {
        'success'    : False,
        'status_code': 500,
        'errors'     : { 'blobstoreIndexError': 'The call to /admin/upload/%s/%s raised an IndexError exception.' % (str(filetype), str(request_id)) }
      }
    
    finally:
      command.set_response(response_data)
      command.save_session()
      
      if response_data['success']:
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        self.response.content_type = 'text/html'
        self.response.body = json.dumps(response_data.body)
      else:
        self.response.set_status(response_data['status_code'])
        self.response.content_type = 'application/javascript'
        self.response.body = json.dumps(response_data)
  
  
  # backend for processing bank statements submitted to /admin/upload/payments/<request_id>
  def __parse_payments(self, body):
    """Method for parsing and processing uploaded bank statements.
      
    This method will analyze exported bank statements of Nordea bank row by row
      in order to identify a few properties for each transaction; a) the amount
      of money recieved, b) transaction date and c) transaction reference.
      
    The transaction reference is cross-checked against the system's list of
      payment keys and – if unsuccessful – the personal identification numbers
      enlisted.
      
    Arguments:
      body: The raw blob body.
      
    Returns:
      A dict with the keys 'itemsUnidentified' and 'itemsRecognized', both of
        which contain lists of sub-dicts.
    """
    
    body = unicode(body, 'windows-1252') #'latin-1')
    lines = body.splitlines()
    items_recognized = []
    items_unidentified = []
    
    lowest_fee_accepted = SysConfig.get_membership_fee()
    first_date_accepted = SysConfig.get_first_payment_date()
    
    thisyear = str(first_date_accepted.year)
    today = datetime.date.today()
    
    for i in range(len(lines)):
      raw_dates = re.findall(thisyear+'\-\d{2}\-\d{2}', lines[i])
      verified_date = None
      for j in range(len(raw_dates)):
        try:
          verified_date = datetime.datetime.date(datetime.datetime.strptime(raw_dates[j], '%Y-%m-%d'))
        except (ValueError):
          continue
        if verified_date > today or verified_date < first_date_accepted:
          verified_date = None
        else:
          lines[i] = lines[i][re.search(raw_dates[j], lines[i]).end():]
          break
      if verified_date is None:
        continue
      
      raw_payments = re.findall('\s[^\-\w]\d{'+str(len(str(lowest_fee_accepted)))+'},\d{2}', lines[i])
      verified_payment = None
      exact_match = None
      for j in range(len(raw_payments)):
        verified_payment = int(raw_payments[j][2:len(str(lowest_fee_accepted))+2])
        if verified_payment == lowest_fee_accepted:
          exact_match = raw_payments[j]
          break
        else:
          verified_payment = None
      if verified_payment is None:
        continue
      elif exact_match is not None:
        end_of_payments = re.search(exact_match, lines[i]).end()
        lines[i] = lines[i][end_of_payments:]
        
      lines[i] = re.sub('\s', ' ', lines[i])
      while re.search('  ', lines[i]):
        lines[i] = re.sub('  ', ' ', lines[i])
        
      verified_user = None
      verified_keys = []
      raw_payment_keys = []
      for j in range(len(lines[i])-5):
        keys = re.findall('[A-Z]{5}', lines[i][j:])
        for k in range(len(keys)):
          if raw_payment_keys.count(keys[k]):
            continue
          else:
            raw_payment_keys.append(keys[k])
      for j in range(len(raw_payment_keys)):
        paymentkey = PaymentKey.get_by_key_name(raw_payment_keys[j])
        if paymentkey is None:
          verified_keys.append(paymentkey)
          continue
        verified_user = paymentkey.memberRef
        verified_keys = [raw_payment_keys[j]]
        break
      
      if verified_user is None:
        next_payment_keys = []
        for j in range(len(lines[i])-5):
          keys = re.findall('[a-z]{5}', lines[i][j:], flags=re.I)
          for k in range(len(keys)):
            if raw_payment_keys.count(keys[k].upper()) or next_payment_keys.count(keys[k]):
              continue
            else:
              next_payment_keys.append(keys[k])
        for j in range(len(next_payment_keys)):
          payment_key = PaymentKey.get_by_key_name(next_payment_keys[j].upper())
          if payment_key is None:
            continue
          verified_user = payment_key.memberRef
          verified_keys = [next_payment_keys[j].upper()]
          break
      
      numbersonly = re.sub('[^\d]', '', lines[i])
      verified_personal_id = None
      raw_personal_ids = []
      for j in range(len(numbersonly)-12):
        personal_ids = re.findall('19[4-9]\d0[1-9]0[1-9]\d{4}', numbersonly[j:]) # 19840203-nnnn
        personal_ids.extend(re.findall('19[4-9]\d1[0-2]0[1-9]\d{4}', numbersonly[j:])) # 19841003-nnnn
        personal_ids.extend(re.findall('19[4-9]\d0[1-9][1-3][0-9]\d{4}', numbersonly[j:])) # 19840203-nnnn
        personal_ids.extend(re.findall('19[4-9]\d1[0-2][1-3][0-9]\d{4}', numbersonly[j:])) # 19840203-nnnn
        for k in range(len(personal_ids)):
          if raw_personal_ids.count(personal_ids[k]):
            continue
          else:
            raw_personal_ids.append(personal_ids[k])
      
      for j in range(len(raw_personal_ids)):
        personal_id = '%s-%s' % (raw_personal_ids[j][2:8], raw_personal_ids[j][8:])
        raw_personal_ids[j] = raw_personal_ids[j][2:]
        user = RkhskMember.get_by_key_name(personal_id)
        if user is not None:
          verified_personal_id = personal_id
          break
      
      if verified_personal_id is None:
        next_personal_ids = []
        for j in range(len(numbersonly)-10):
          personal_ids = re.findall('[4-9]\d0[1-9]0[1-9]\d{4}', numbersonly[j:]) # 840203-nnnn
          personal_ids.extend(re.findall('[4-9]\d1[0-2]0[1-9]\d{4}', numbersonly[j:])) # 841003-nnnn
          personal_ids.extend(re.findall('[4-9]\d0[1-9][1-3][0-9]\d{4}', numbersonly[j:])) # 840203-nnnn
          personal_ids.extend(re.findall('[4-9]\d1[0-2][1-3][0-9]\d{4}', numbersonly[j:])) # 840203-nnnn
          for k in range(len(personal_ids)):
            if raw_personal_ids.count(personal_ids[k]) or next_personal_ids.count(personal_ids[k]):
              continue
            else:
              next_personal_ids.append(personal_ids[k])
        #logging.info('raw_personal_ids, secondary: %s' % next_personal_ids)
        for j in range(len(next_personal_ids)):
          personal_id = '%s-%s' % (next_personal_ids[j][:6], next_personal_ids[j][6:])
          user = RkhskMember.get_by_key_name(personal_id)
          if user is not None:
            verified_personal_id = personal_id
            break
      
      if verified_user is None and verified_personal_id is None:
        #logging.info('unidentified!')
        items_unidentified.append({
          'lastPaymentRecieved': verified_date.strftime('%Y-%m-%d'),
          'payment'            : '%s kr' % verified_payment,
          'message'            : lines[i]
        })
        #logging.info('unidentified: %s' % items_unidentified[-1])
      elif verified_user:
        #logging.info('recognized!')
        application_date = ''
        if hasattr('lastApplicationDate', verified_user):
          application_date = verified_user.lastApplicationDate.strftime('%Y-%m-%d')
        
        items_recognized.append({
          'lastPaymentRecieved': verified_date.strftime('%Y-%m-%d'),
          'payment'            : '%s kr' % verified_payment,
          'paymentKey'         : verified_keys[0],
          'firstName'          : verified_user.firstName,
          'lastName'           : verified_user.lastName,
          'personalId'         : verified_user.personal_id,
          'lastApplicationFor' : verified_user.lastApplicationFor,
          'lastApplicationDate': application_date
        })
      else:
        user = RkhskMember.get_by_key_name(verified_personal_id)
        application_date = ''
        if hasattr(user, 'lastApplicationDate'):
          application_date = user.lastApplicationDate.strftime('%Y-%m-%d')
        
        items_recognized.append({
          'lastPaymentRecieved': verified_date.strftime('%Y-%m-%d'),
          'payment'            : '%s kr' % verified_payment,
          'paymentKey'         : '',
          'firstName'          : user.firstName,
          'lastName'           : user.lastName,
          'personalId'         : user.personalId,
          'lastApplicationFor' : user.lastApplicationFor,
          'lastApplicationDate': application_date
        })
      
    return {
      'itemsRecognized'  : items_recognized,
      'itemsUnidentified': items_unidentified
    }
  


# TODO(mats.blomdahl@gmail.com): Expand functionality and REST capabilities of /admin/mail/ backend.
class RkhskInboundMail(webapp2.RequestHandler):
  """An interface for recieving inbound mail and, during development, dumping
    a JSON array with the recieved emails.
    
  The HTTP-GET handler deploys secure cookies using the 'default' store,
    expiration is controlled by the global variable ADMIN_COOKIE_EXPIRATION.
    
  The HTTP-POST handler is secured by App Engine's built-in authentication
    support through the app.yaml config.
  """
  
  # session management
  @webapp2.cached_property
  def session(self):
    # Returns a session using the default cookie key.
    return self.session_store.get_session(name = 'default', max_age = ADMIN_COOKIE_EXPIRATION, backend = 'securecookie')
  
  
  # /admin/mail
  def get(self, item):
    """Handler for dumping the emails stored in the 'InboundEmail' entries.
      
    Arguments:
      item: Currently not in use.
      
    Returns:
      A JSON array containing the emails in 'InboundEmail'.
    """
    
    self.session_store = sessions.get_store(request = self.request)
    if verify_admin(self.session.get('admin')) is False:
      return {
        'success'    : False,
        'status_code': 403, # forbidden
        'body'       : False
      }
    email_data = []
    emails = InboundEmail.all()
    for email in emails:
      email_data.append(email.dumps())
    response_data = {
      'data': { 'InboundEmails': email_data }
    }
    
    self.response.content_type = 'application/json'
    self.response.body = json.dumps(response_data)
    
    self.session_store.save_sessions(self.response)
  
  
  # /_ah/mail/
  def post(self):
    """Handler for redirecting HTTP-POST requests with inbound mail."""
    
    self.__receive(mail.InboundEmailMessage(self.request.body))
  
  def __receive(self, message):
    """Handler for recieving and storing inbound email to 'InboundEmail'.
      
    Arguments:
      message: The email message object.
    """
    
    logging.info("RkhskInboundMail.__recieve(message): Recieved a message from %r to %r." % (message.sender, message.to))
    
    recieved = InboundEmail()
    logging.info('message: %s' % dir(message))
    if hasattr(message, 'to'):
      recieved.to = unicode(message.to)
    if hasattr(message, 'sender'):
      recieved.sender = unicode(message.sender)
    if hasattr(message, 'cc'):
      recieved.cc = unicode(message.cc)
    if hasattr(message, 'date'):
      logging.info('RkhskInboundMail.__recieve(message): Date %s recieved.' % message.date)
      try:
        recieved.date = datetime.datetime.fromtimestamp(mktime_tz(parsedate_tz(message.date)))
      except:
        logging.info('RkhskInboundMail.__recieve(message): Faulty date format recieved, %r.' % message.date)
    if hasattr(message, 'reply_to'):
      recieved.reply_to = unicode(message.reply_to)
    
    if hasattr(message, 'bodies'):
      plaintext_bodies = message.bodies('text/plain')
      if hasattr(plaintext_bodies, '__iter__'):
        for content_type, body in plaintext_bodies:
          recieved.body_plaintext_type.append(str(content_type))
          recieved.body_plaintext_content.append(db.Text(body.decode()))
      
      html_bodies = message.bodies('text/html')
      if hasattr(html_bodies, '__iter__'):
        for content_type, body in html_bodies:
          recieved.body_html_type.append(str(content_type))
          recieved.body_html_content.append(db.Text(body.decode()))
    
    if hasattr(message, 'attachments'):
      for name, content in message.attachments:
        recieved.attachment_filenames.append(unicode(name))
        recieved.attachment_filecontents.append(db.Blob(content.payload))
    
    recieved.original = db.Blob(message.original.as_string())
    recieved.put()
  


class RkhskChannel(webapp2.RequestHandler):
  """The public interface for setting up channels as well as handling system 
    events such as successful connects and disconnects.
    
  The HTTP-GET handler deploys secure cookies using the 'default' store,
    expiration is controlled by the global variable ADMIN_COOKIE_EXPIRATION.
    
  The HTTP-POST handler is secured by App Engine's built-in authentication
    support through the app.yaml config.
  """
  
  # session management
  @webapp2.cached_property
  def session(self):
    # Returns a session using the default cookie key.
    return self.session_store.get_session(name = 'default', max_age = ADMIN_COOKIE_EXPIRATION, backend = 'securecookie')
  
  
  # /admin/channel/<create>
  def get(self, command):
    """Method for setting up a real-time channel to an authenticated user.
      
    The channel handler will be the user ID read from the clients secure
      cookie (typically a personal identification number).
      
    Arguments:
      command: Currently accepts only one value, 'create'.
      
    Returns:
      A JSON object with a 'channelToken' property used for connecting to the
        GAE channel service.
    """
    
    self.session_store = sessions.get_store(request = self.request)
    user_id = self.session.get('admin')
    
    logging.info('RkhskChannel.get(command=\'%s\').user_id: \'%s\'' % (command, str(user_id)))
    
    if user_id is None or verify_admin(user_id) is False or command != 'create':
      time.sleep(2+random.random())
      self.response.set_status(400) # bad request
    
    else:
      self.response.headers.add_header("Access-Control-Allow-Origin", "*")
      self.response.content_type = 'application/json'
      self.response.body = json.dumps({
        'success': True,
        'data'   : { 'channelToken': channel.create_channel(user_id) }
      })
    
    self.session_store.save_sessions(self.response)
  
  
  # /_ah/channel/<connected|disconnected>/
  def post(self, command):
    """Method for recieving connect/disconnect events from the GAE channel.
      
    The channel handler will be the user ID read from the clients secure
      cookie (typically a personal identification number).
      
    Arguments:
      command: GAE will submit a HTTP-POST to the application with one of the
        two commands 'connected' and 'disconnected'.
      from: A parameter from the channel service containing the clients unique
        client ID.
    """
    
    user_id = self.request.get('from')
    
    logging.info('RkhskChannel.post(command=\'%s\').user_id: \'%s\'' % (command, str(user_id)))
    
    connected = memcache.get(CONNECTED_ADMINS)
    if connected is None:
      connected = []
    else:
      try:
        index = connected.index(user_id)
        connected.pop(index)
      except (ValueError):
        pass
    
    if command == 'connected' and verify_admin(user_id):
      connected.append(user_id)
      taskqueue.add(
        queue_name = 'broadcaster',
        url = '/tasks/admin_broadcast/',
        params = {
          'broadcaster' : user_id,
          'timestamp'   : pickle.dumps(datetime.datetime.today()),
          'remote_event': 'admin_connected'
        }
      )
      
    elif command == 'disconnected':
      taskqueue.add(
        queue_name = 'broadcaster',
        url = '/tasks/admin_broadcast/',
        params = {
          'broadcaster' : user_id,
          'timestamp'   : pickle.dumps(datetime.datetime.today()),
          'remote_event': 'admin_disconnected'
        }
      )
    
    else:
      self.error(500)
      return
    
    memcache.set(CONNECTED_ADMINS, connected, 3600)
  


def handle_404(request, response, exception):
  logging.exception(exception)
  response.write('Oops! I could swear this page was here!')
  response.set_status(404)

def handle_500(request, response, exception):
  logging.exception(exception)
  response.write('A server error occurred!')
  response.set_status(500)


app = webapp2.WSGIApplication(routes = [
  (r'/account/(.+)/($|.+)', RkhskAccountInterface),
  (r'/_ah/mail/.+', RkhskInboundMail),
  (r'/tasks/(\w+)/', RkhskTasks),
  (r'/admin/channel/(\w+)', RkhskChannel),
  (r'/_ah/channel/(\w+)/', RkhskChannel),
  (r'/admin/mail/($|.+)', RkhskInboundMail),
  (r'/admin/upload/(\w+)/(\w+)', RkhskAdminUpload),
  (r'/admin/(\w+)/(\w+)/(\w+)', RkhskAdminInterface),
  (r'/admin/(\w+)/($|\w+)', RkhskAdminInterface),
  (r'/(admin|clear_cookies|frame\-r|frame\-a|m|d)', RkhskFrontpage),
  (r'/', RkhskFrontpage)
], config = config, debug = True)

app.error_handlers[404] = handle_404
app.error_handlers[500] = handle_500

def main():
  app.run()

if __name__ == '__main__':
  main()

