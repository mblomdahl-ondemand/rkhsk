#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   RKH-SK Members Datastore
# Author:  Mats Blomdahl
# Version: 2012-03-08
# 

"""rkhsk_ui module
  
Support for session management and wrapping inbound requests.
  
'poster.version' is a 3-tuple of integers representing the version number.
New releases of poster will always have a version number that compares greater
than an older version of poster.
"""

import rkhsk_ui.session_trace
import rkhsk_ui.command
import rkhsk_ui.unihandler

version = (0, 8, 1)
