#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   SessionTrace
# Author:  Mats Blomdahl
# Version: 2012-03-18

import logging
import pickle

from random import randrange
from datetime import datetime

from google.appengine.ext.ndb import Model, StringProperty, PickleProperty, JsonProperty, IntegerProperty, DateTimeProperty
from google.appengine.api import memcache

from rkhsk_model import SysConfig

class SessionTrace(Model):
  """A wrapper around the client sessions.
    
  The SessionTrace wrapper is designed to keep track of each user session in
    order to limit the effects of DDoS attacks, brute force hacking attempts
    and malicious system usage (e.g. sending dummy requests in order to drive
    resource usage costs through the roof).
    
  If the session resource usage limits is not honored, the session will be
    blocked for the number of seconds specified by the targets retry paramters'
    'min_backoff_seconds' attribute. For each attempt to submit requests within
    the blocking interval, the blocking will be exponentially prolonged untill
    the retry parameters' 'max_backoff_seconds' attribute is reached.
    
  Attributes:
    client_ip: The client's IP address (i.e. the WebOb request's 'remote_addr'
      attribute).
    session_id: The request's session ID; a cookie set at first visit. The 
      cookie never expires and is used for keeping clients organized to their
      respective session logs.
    user_id: The decoded user ID an admin will recieve on login.
    
    dev_mode: A boolean flag marking development mode. If True, logging will be
      suppressed.
    
    admin_login_200_trace_timestamp: A pickled list of successful interaction
      timestamps (value type is 'datetime.datetime').
    admin_login_200_trace_descriptions: A dumped JSON array with an entry
      for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    admin_login_403_trace_timestamp: A pickled list of timestamps for
      interactions that have resulted in a 403 ("forbidden") error.
    admin_login_403_trace_descriptions: A dumped JSON array with an entry
      for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    admin_login_0_trace_timestamp: A pickled list of timestamps for
      interactions resulting in the request being blocked.
    admin_login_0_trace_descriptions: A dumped JSON array with an entry
      for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    admin_login_retry_parameters: Retry limits configuration, a dict.
    admin_login_rpm_limit: Request per minute limitation, an integer.
    
    admin_interface_200_trace_timestamp: A pickled list of successful
      interaction timestamps (value type is 'datetime.datetime').
    admin_interface_200_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance)..
    admin_interface_403_trace_timestamp: A pickled list of timestamps for
      interactions that have resulted in a 403 ("forbidden") error.
    admin_interface_403_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    admin_interface_0_trace_timestamp: A pickled list of timestamps for
      interactions resulting in the request being blocked.
    admin_interface_0_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    admin_interface_retry_parameters: Retry limits configuration, a dict.
    admin_interface_rpm_limit: Request per minute limitation, an integer. 
    
    account_retrieve_200_trace_timestamp: A pickled list of successful
      interaction timestamps (value type is 'datetime.datetime').
    account_retrieve_200_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_retrieve_403_trace_timestamp: A pickled list of timestamps for
      interactions that have resulted in a 403 ("forbidden") error.
    account_retrieve_403_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_retrieve_0_trace_timestamp: A pickled list of timestamps for
      interactions resulting in the request being blocked.
    account_retrieve_0_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_retrieve_retry_parameters: Retry limits configuration, a dict.
    account_retrieve_rpm_limit: Request per minute limitation, an integer.
    
    account_load_200_trace_timestamp: A pickled list of successful interaction
      timestamps (value type is 'datetime.datetime').
    account_load_200_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_load_403_trace_timestamp: A pickled list of timestamps for
      interactions that have resulted in a 403 ("forbidden") error.
    account_load_403_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_load_0_trace_timestamp: A pickled list of timestamps for
      interactions resulting in the request being blocked.
    account_load_0_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_load_retry_parameters: Retry limits configuration, a dict.
    account_load_rpm_limit: Request per minute limitation, an integer.
    
    account_register_200_trace_timestamp: A pickled list of successful
      interaction timestamps (value type is 'datetime.datetime').
    account_register_200_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_register_403_trace_timestamp: A pickled list of timestamps for
      interactions that have resulted in a 403 ("forbidden") error.
    account_register_403_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_register_0_trace_timestamp: A pickled list of timestamps for
      interactions resulting in the request being blocked.
    account_register_0_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_register_retry_parameters: Retry limits configuration, a dict.
    account_register_rpm_limit: Request per minute limitation, an integer.
    
    account_resend_200_trace_timestamp: A pickled list of successful
      interaction timestamps (value type is 'datetime.datetime').
    account_resend_200_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_resend_403_trace_timestamp: A pickled list of timestamps for
      interactions that have resulted in a 403 ("forbidden") error.
    account_resend_403_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_resend_0_trace_timestamp: A pickled list of timestamps for
      interactions resulting in the request being blocked.
    account_resend_0_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_resend_retry_parameters: Retry limits configuration, a dict.
    account_resend_rpm_limit: Request per minute limitation, an integer.
    
    account_interface_200_trace_timestamp: A pickled list of successful
      interaction timestamps (value type is 'datetime.datetime').
    account_interface_200_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_interface_403_trace_timestamp: A pickled list of timestamps for
      interactions that have resulted in a 403 ("forbidden") error.
    account_interface_403_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_interface_0_trace_timestamp: A pickled list of timestamps for
      interactions resulting in the request being blocked.
    account_interface_0_trace_descriptions: A dumped JSON array with an
      entry for each of the timestamps. The entries contain JSON objects with
      request data (in general; the dict dumped by a Command instance).
    account_interface_retry_parameters: Retry limits configuration, a dict.
    account_interface_rpm_limit: Request per minute limitation, an integer.
    
    sys_createddate: First creation date for the session trace.
    sys_modifieddate: The last modified date.
    sys_saveddate: A timestamp for the most recent successful save operation.
  """
  
  client_ip = StringProperty()
  session_id = StringProperty()
  user_id = StringProperty()
  
  admin_login_200_trace_timestamps = PickleProperty(default = [])
  admin_login_200_trace_descriptions = JsonProperty(default = [])
  
  admin_login_403_trace_timestamps = PickleProperty(default = [])
  admin_login_403_trace_descriptions = JsonProperty(default = [])
  
  admin_login_0_trace_timestamps = PickleProperty(default = [])
  admin_login_0_trace_descriptions = JsonProperty(default = [])
  
  admin_login_retry_parameters = PickleProperty()
  admin_login_rpm_limit = IntegerProperty()
  
  
  admin_passwd_200_trace_timestamps = PickleProperty(default = [])
  admin_passwd_200_trace_descriptions = JsonProperty(default = [])
  
  admin_passwd_403_trace_timestamps = PickleProperty(default = [])
  admin_passwd_403_trace_descriptions = JsonProperty(default = [])
  
  admin_passwd_0_trace_timestamps = PickleProperty(default = [])
  admin_passwd_0_trace_descriptions = JsonProperty(default = [])
  
  admin_passwd_retry_parameters = PickleProperty()
  admin_passwd_rpm_limit = IntegerProperty()
  
  
  admin_interface_200_trace_timestamps = PickleProperty(default = [])
  admin_interface_200_trace_descriptions = JsonProperty(default = [])
  
  admin_interface_403_trace_timestamps = PickleProperty(default = [])
  admin_interface_403_trace_descriptions = JsonProperty(default = [])
  
  admin_interface_0_trace_timestamps = PickleProperty(default = [])
  admin_interface_0_trace_descriptions = JsonProperty(default = [])
  
  admin_interface_retry_parameters = PickleProperty()
  admin_interface_rpm_limit = IntegerProperty()
  
  
  account_retrieve_200_trace_timestamps = PickleProperty(default = [])
  account_retrieve_200_trace_descriptions = JsonProperty(default = [])
  
  account_retrieve_403_trace_timestamps = PickleProperty(default = [])
  account_retrieve_403_trace_descriptions = JsonProperty(default = [])
  
  account_retrieve_0_trace_timestamps = PickleProperty(default = [])
  account_retrieve_0_trace_descriptions = JsonProperty(default = [])
  
  account_retrieve_retry_parameters = PickleProperty()
  account_retrieve_rpm_limit = IntegerProperty()
  
  
  account_load_200_trace_timestamps = PickleProperty(default = [])
  account_load_200_trace_descriptions = JsonProperty(default = [])
  
  account_load_403_trace_timestamps = PickleProperty(default = [])
  account_load_403_trace_descriptions = JsonProperty(default = [])
  
  account_load_0_trace_timestamps = PickleProperty(default = [])
  account_load_0_trace_descriptions = JsonProperty(default = [])
  
  account_load_retry_parameters = PickleProperty()
  account_load_rpm_limit = IntegerProperty()
  
  
  account_register_200_trace_timestamps = PickleProperty(default = [])
  account_register_200_trace_descriptions = JsonProperty(default = [])
  
  account_register_403_trace_timestamps = PickleProperty(default = [])
  account_register_403_trace_descriptions = JsonProperty(default = [])
  
  account_register_0_trace_timestamps = PickleProperty(default = [])
  account_register_0_trace_descriptions = JsonProperty(default = [])
  
  account_register_retry_parameters = PickleProperty()
  account_register_rpm_limit = IntegerProperty()
  
  
  account_resend_200_trace_timestamps = PickleProperty(default = [])
  account_resend_200_trace_descriptions = JsonProperty(default = [])
  
  account_resend_403_trace_timestamps = PickleProperty(default = [])
  account_resend_403_trace_descriptions = JsonProperty(default = [])
  
  account_resend_0_trace_timestamps = PickleProperty(default = [])
  account_resend_0_trace_descriptions = JsonProperty(default = [])
  
  account_resend_retry_parameters = PickleProperty()
  account_resend_rpm_limit = IntegerProperty()
  
  
  account_interface_200_trace_timestamps = PickleProperty(default = [])
  account_interface_200_trace_descriptions = JsonProperty(default = [])
  
  account_interface_403_trace_timestamps = PickleProperty(default = [])
  account_interface_403_trace_descriptions = JsonProperty(default = [])
  
  account_interface_0_trace_timestamps = PickleProperty(default = [])
  account_interface_0_trace_descriptions = JsonProperty(default = [])
  
  account_interface_retry_parameters = PickleProperty()
  account_interface_rpm_limit = IntegerProperty()
  
  
  sys_createddate = DateTimeProperty()
  sys_modifieddate = DateTimeProperty()
  sys_saveddate = DateTimeProperty()
  
  
  # TODO(mats.blomdahl@gmail.com): Document!
  def __init__(self, client_ip, session_id = None, user_id = None, dev_mode = False):
    """ commentz """
    
    self.dev_mode = dev_mode
    
    if session_id is not None:
      logging.info('session id is: %s' % str(session_id))
      session = SessionTrace.get_by_id(session_id)
      if session is not None:
        if dev_mode:
          logging.info('SessionTrace.__init__(client_ip=\'%s\', session_id=\'%s\', user_id=\'%s\'): Instance successfully retrieved from Datastore.' % (str(client_ip), str(session_id), str(user_id)))
        self = session
        if self.set_client_ip(client_ip) or self.set_user_id(user_id):
          self.save()
        return
    
    now = datetime.today()
    
    super(SessionTrace, self).__init__(
      id = session_id,
      session_id = session_id,
      client_ip = client_ip,
      sys_createddate = now,
      sys_modifieddate = now
    )
    
    self.__set_retry_parameters('admin_login', {
      'min_backoff_seconds': 2,
      'max_backoff_seconds': 1800,
      'current_timeout_power': 1,
      'current_timeout_ends': None
    })
    self.admin_login_rpm_limit = 6
    
    self.__set_retry_parameters('admin_passwd', {
      'min_backoff_seconds': 2,
      'max_backoff_seconds': 1800,
      'current_timeout_power': 1,
      'current_timeout_ends': None
    })
    self.admin_passwd_rpm_limit = 6
    
    self.__set_retry_parameters('admin_interface', {
      'min_backoff_seconds': 2,
      'max_backoff_seconds': 60,
      'current_timeout_power': 1,
      'current_timeout_ends': None
    })
    self.admin_interface_rpm_limit = 120
    
    self.__set_retry_parameters('account_retrieve', {
      'min_backoff_seconds': 2,
      'max_backoff_seconds': 1800,
      'current_timeout_power': 1,
      'current_timeout_ends': None
    })
    self.account_retrieve_rpm_limit = 1
    
    self.__set_retry_parameters('account_load', {
      'min_backoff_seconds': 5,
      'max_backoff_seconds': 1800,
      'current_timeout_power': 1,
      'current_timeout_ends': None
    })
    self.account_load_rpm_limit = 6
    
    self.__set_retry_parameters('account_register', {
      'min_backoff_seconds': 10,
      'max_backoff_seconds': 1800,
      'current_timeout_power': 1,
      'current_timeout_ends': None
    })
    self.account_register_rpm_limit = 1
    
    self.__set_retry_parameters('account_resend', {
      'min_backoff_seconds': 10,
      'max_backoff_seconds': 1800,
      'current_timeout_power': 1,
      'current_timeout_ends': None
    })
    self.account_resend_rpm_limit = 2
    
    self.__set_retry_parameters('account_interface', {
      'min_backoff_seconds': 2,
      'max_backoff_seconds': 60,
      'current_timeout_power': 1,
      'current_timeout_ends': None
    })
    self.account_interface_rpm_limit = 15
    
    self.user_id = user_id
    
    if dev_mode:
      logging.info('SessionTrace.__init__(client_ip=\'%s\', session_id=\'%s\', user_id=\'%s\'): New instance created.' % (str(client_ip), str(session_id), str(user_id)))
    
    self.save()
    
    if dev_mode:
      logging.info('SessionTrace.dir(self): %s' % str(dir(self)))
  
  
  def __set_retry_parameters(self, attr_name, attr_dict):
    """Setter for pickling and setting retry parameters in order to 
      circumvent db.Expando attribute limitations.
      
    Arguments:
      attr_name: The attribute name (i.e. a 'action' or 'interface' name).
      attr_dict: The attribute's configuration dict.
    """
    
    setattr(self, '%s_retry_parameters' % attr_name, attr_dict)
  
  
  def __get_retry_parameters(self, attr_name):
    """Getter for retrieving and unpickling retry parameters in order to 
      circumvent db.Expando's attribute type limitations.
      
    Arguments:
      attr_name: The attribute name (i.e. a 'action' or 'interface' name).
      
    Returns:
      A configuration dict.
    """
    
    retry_params = getattr(self, '%s_retry_parameters' % attr_name, None)
    if retry_params is None:
      return retry_params
    else:
      return retry_params
  
  
  def verify_session(self, action, interface):
    """Method for verifying that a) the user session cookie is present and b)
      that the session is not currently blocked due to resource usage
      restrictions.
      
    Arguments:
      action: The request handler's 'action' attribute.
      interface: A fallback or general interface class descriptor for catching
        requests sent to handlers without specific resource usage limits.
    """
    
    logging.info('dumps: %s' % str(self.dumps()))
    
    if self.session_id is None:
      if self.dev_mode:
        logging.info('SessionTrace.verify_session(action=\'%s\', interface=\'%s\'): \'session_id\' attribute is undefined.' % (str(action), interface))
      return {
        'clear_to_recieve': False,
        'errors'          : { 'invalidAttributes': 'Session is blocked because the \'session_id\' attribute is undefined.' }
      }
    
    event_attr = None
    retry_params = None
    if action is not None:
      retry_params = self.__get_retry_parameters(action)
    if retry_params is None:
      retry_params = self.__get_retry_parameters(interface)
      if self.dev_mode:
        logging.info('SessionTrace.verify_session(action=\'%s\', interface=\'%s\'): Invalid action descriptor.' % (str(action), interface))
      if retry_params is None:
        if self.dev_mode:
          logging.info('SessionTrace.verify_session(action=\'%s\', interface=\'%s\'): Invalid interface descriptor.' % (str(action), interface))
        return False
      else:
        event_attr = interface
    else:
      event_attr = action
    
    now = datetime.today()
    if retry_params['current_timeout_ends'] is not None:
      if retry_params['current_timeout_ends'] > now:
        if self.dev_mode:
          logging.info('SessionTrace.verify_session(action=\'%s\', interface=\'%s\'): Invalid interface descriptor.' % (str(action), interface))
        return {
          'clear_to_recieve': False,
          'errors'          : { 'sessionBlocked': 'Session is blocked from triggering \'%s\' until %s' % (event_attr, retry_params['current_timeout_ends'].isoformat().split('.')[0]) }
        }
      else:
        retry_params['current_timeout_ends'] = None
        retry_params['current_timeout_power'] = 1
        self.__set_retry_parameters(event_attr, retry_params)
        self.save(put_to_datastore = False)
    
    return {
      'clear_to_recieve': True
    }
    
  
  
  def verify_as_admin(self):
    """Method for verifying admin privileges for the session's 'user_id' attribute."""
    
    if self.user_id is None:
      return False
    
    admins = SysConfig.get_sys_admins()
    
    if self.user_id in admins:
      return True
    else:
      return False
  
  
  def set_client_ip(self, client_ip):
    """Setter method for the 'client_ip' attribute.
      
    Arguments:
      client_ip: An IP address (the WebOb request attribute 'remote_addr').
    """
    
    if self.client_ip != client_ip:
      if self.dev_mode:
        logging.info('SessionTrace.set_client_ip(%s)' % client_ip)
      self.client_ip = client_ip
      return True
    else:
      if self.dev_mode:
        logging.info('SessionTrace.set_client_ip(%s): Set omitted/redundant.' % client_ip)
      return False
  
  
  def set_user_id(self, user_id):
    """Setter method for the 'user_id' attribute.
      
    Arguments:
      client_ip: A member key name (personal identification number), a string.
    """
    
    if self.user_id != user_id:
      if self.dev_mode:
        logging.info('SessionTrace.set_user_id(%s)' % user_id)
      self.user_id = user_id
      return True
    else:
      if self.dev_mode:
        logging.info('SessionTrace.set_user_id(%s): Set omitted/redundant.' % user_id)
      return False
  
  
  # TODO(mats.blomdahl@gmail.com): Implement smarter rpm limit calc!
  def update_session_trace(self, action, interface, status_code, request_data):
    """Method for updating the session trace and applying per-interface  
      resource usage limits.
      
    Arguments:
      action: The request handler's 'action' attribute.
      interface: A fallback or general interface class descriptor for catching
        requests sent to handlers without specific resource usage limits.
      status_code: The HTTP response status code, an integer.
      request_data: The parameters passed to the request handler, a dict.
    """
    
    if status_code == 400:
      status_code = 0
    
    event_attr = None
    if action is not None:
      event_attr = getattr(self, '%s_%s_trace_timestamps' % (action, str(status_code)))
    if event_attr is None:
      if self.dev_mode:
        logging.info('SessionTrace.update_session_trace(action=\'%s\', interface=\'%s\', status_code=%s, request_data=\'%s\'): Invalid action descriptor.' % (str(action), interface, str(status_code), str(request_data)))
      event_attr = getattr(self, '%s_%s_trace_timestamps' % (interface, str(status_code)))
      if event_attr is None:
        if self.dev_mode:
          logging.info('SessionTrace.update_session_trace(action=\'%s\', interface=\'%s\', status_code=%s, request_data=\'%s\'): Invalid interface descriptor.' % (str(action), interface, str(status_code), str(request_data)))
        return False
      else:
        event_attr = interface
    else:
      event_attr = action
    
    now = datetime.today()
    put_to_datastore = False
    
    def apply_retries_throttle(retry_params, now):
      if self.dev_mode:
        logging.info('SessionTrace.update_session_trace(action=\'%s\', interface=\'%s\', status_code=%s, request_data=\'%s\').apply_retries_throttle(retry_params=%s, now=%s)' % (str(action), interface, str(status_code), str(request_data), str(retry_params), now.isoformat().split('.')[0]))
      backoff_seconds = retry_params['min_backoff_seconds']**retry_params['current_timeout_power']
      if backoff_seconds > retry_params['max_backoff_seconds']:
        backoff_seconds = retry_params['max_backoff_seconds']
      retry_params['current_timeout_ends'] = now+datetime.timedelta(seconds = backoff_seconds)
      retry_params['current_timeout_power'] += 1
      
      return retry_params
    
    
    timestamps = getattr(self, '%s_%s_trace_timestamps' % (event_attr, str(status_code)))
    descriptions = getattr(self, '%s_%s_trace_descriptions' % (event_attr, str(status_code)))
    if self.dev_mode:
      logging.info('descriptions: %s' % str(descriptions))
      logging.info('timestamps: %s' % str(timestamps))
    
    if status_code == 200: # validate against rpm limit
      rpm_limit = getattr(self, '%s_rpm_limit' % event_attr)
      if any(timestamps):
        rpm = 60.0/(now - timestamps[-1]).total_seconds()
        if rpm > rpm_limit: # apply <event_attr>_retry_parameters
          retry_params = apply_retries_throttle(self.__get_retry_parameters(event_attr), now)
          self.__set_retry_parameters(event_attr, retry_params)
        else:
          put_to_datastore = True
    
    elif status_code == 403 or status_code == 0: # apply <event_attr>_retry_parameters
      retry_params = apply_retries_throttle(getattr(self, '%s_retry_parameters' % event_attr), now)
      self.__set_retry_parameters(event_attr, retry_params)
    
    timestamps.append(now)
    descriptions.append(request_data)
    if self.dev_mode:
      logging.info('descriptions: %s' % str(descriptions))
      logging.info('timestamps: %s' % str(timestamps))
    setattr(self, '%s_%s_trace_timestamps' % (event_attr, str(status_code)), timestamps)
    setattr(self, '%s_%s_trace_descriptions' % (event_attr, str(status_code)), descriptions)
    
    self.save(put_to_datastore = put_to_datastore)
  
  
  def dumps(self):
    """Method for dumping the class attributes.
      
    Returns:
      A JSON object with the instance's configured attributes.
    """
    
    try:
      sys_createddate = self.sys_createddate.isoformat().split('.')[0]
    except (AttributeError):
      sys_createddate = None
    
    try:
      sys_modifieddate = self.sys_modifieddate.isoformat().split('.')[0]
    except (AttributeError):
      sys_modifieddate = None
    
    try:
      sys_saveddate = self.sys_saveddate.isoformat().split('.')[0]
    except (AttributeError):
      sys_saveddate = None
    
    response = {
      'client_ip'                  : self.client_ip,
      'session_id'                 : self.session_id,
      'user_id'                    : self.user_id,
      'admin_login_200_trace'      : [],
      'admin_login_403_trace'      : [],
      'admin_login_0_trace'        : [],
      'admin_passwd_200_trace'     : [],
      'admin_passwd_403_trace'     : [],
      'admin_passwd_0_trace'       : [],
      'admin_interface_200_trace'  : [],
      'admin_interface_403_trace'  : [],
      'admin_interface_0_trace'    : [],
      'account_retrieve_200_trace' : [],
      'account_retrieve_403_trace' : [],
      'account_retrieve_0_trace'   : [],
      'account_load_200_trace'     : [],
      'account_load_403_trace'     : [],
      'account_load_0_trace'       : [],
      'account_register_200_trace' : [],
      'account_register_403_trace' : [],
      'account_register_0_trace'   : [],
      'account_resend_200_trace'   : [],
      'account_resend_403_trace'   : [],
      'account_resend_0_trace'     : [],
      'account_interface_200_trace': [],
      'account_interface_403_trace': [],
      'account_interface_0_trace'  : [],
      'sys_createddate'            : sys_createddate,
      'sys_modifieddate'           : sys_modifieddate,
      'sys_saveddate'              : sys_saveddate
    }
    
    for i in range(len(self.admin_login_200_trace_descriptions)):
      event = {
        'timestamp'                   : self.admin_login_200_trace_timestamps[i].isoformat().split('.')[0],
        'admin_login_rpm_limit'       : self.admin_login_rpm_limit,
        'admin_login_retry_parameters': self.admin_login_retry_parameters,
        'description'                 : self.admin_login_200_trace_descriptions[i]
      }
      response['admin_login_200_trace'].append(event)
    
    for i in range(len(self.admin_login_403_trace_descriptions)):
      event = {
        'timestamp'                   : self.admin_login_403_trace_timestamps[i].isoformat().split('.')[0],
        'admin_login_rpm_limit'       : self.admin_login_rpm_limit,
        'admin_login_retry_parameters': self.admin_login_retry_parameters,
        'description'                 : self.admin_login_403_trace_descriptions[i]
      }
      response['admin_login_403_trace'].append(event)
    
    for i in range(len(self.admin_login_0_trace_descriptions)):
      event = {
        'timestamp'                   : self.admin_login_0_trace_timestamps[i].isoformat().split('.')[0],
        'admin_login_rpm_limit'       : self.admin_login_rpm_limit,
        'admin_login_retry_parameters': self.admin_login_retry_parameters,
        'description'                 : self.admin_login_0_trace_descriptions[i]
      }
      response['admin_login_0_trace'].append(event)
    
    
    for i in range(len(self.admin_login_200_trace_descriptions)):
      event = {
        'timestamp'                    : self.admin_passwd_200_trace_timestamps[i].isoformat().split('.')[0],
        'admin_passwd_rpm_limit'       : self.admin_passwd_rpm_limit,
        'admin_passwd_retry_parameters': self.admin_passwd_retry_parameters,
        'description'                  : self.admin_passwd_200_trace_descriptions[i]
      }
      response['admin_passwd_200_trace'].append(event)
    
    for i in range(len(self.admin_passwd_403_trace_descriptions)):
      event = {
        'timestamp'                    : self.admin_passwd_403_trace_timestamps[i].isoformat().split('.')[0],
        'admin_passwd_rpm_limit'       : self.admin_passwd_rpm_limit,
        'admin_passwd_retry_parameters': self.admin_passwd_retry_parameters,
        'description'                  : self.admin_passwd_403_trace_descriptions[i]
      }
      response['admin_passwd_403_trace'].append(event)
    
    for i in range(len(self.admin_passwd_0_trace_descriptions)):
      event = {
        'timestamp'                    : self.admin_passwd_0_trace_timestamps[i].isoformat().split('.')[0],
        'admin_passwd_rpm_limit'       : self.admin_passwd_rpm_limit,
        'admin_passwd_retry_parameters': self.admin_passwd_retry_parameters,
        'description'                  : self.admin_passwd_0_trace_descriptions[i]
      }
      response['admin_passwd_0_trace'].append(event)
    
    
    for i in range(len(self.admin_interface_200_trace_descriptions)):
      event = {
        'timestamp'                       : self.admin_interface_200_trace_timestamps[i].isoformat().split('.')[0],
        'admin_interface_rpm_limit'       : self.admin_interface_rpm_limit,
        'admin_interface_retry_parameters': self.admin_interface_retry_parameters,
        'description'                     : self.admin_interface_200_trace_descriptions[i]
      }
      response['admin_interface_200_trace'].append(event)
    
    for i in range(len(self.admin_interface_0_trace_descriptions)):
      event = {
        'timestamp'                       : self.admin_interface_0_trace_timestamps[i].isoformat().split('.')[0],
        'admin_interface_rpm_limit'       : self.admin_interface_rpm_limit,
        'admin_interface_retry_parameters': self.admin_interface_retry_parameters,
        'description'                     : self.admin_interface_0_trace_descriptions[i]
      }
      response['admin_interface_400_trace'].append(event)
    
    for i in range(len(self.admin_interface_403_trace_descriptions)):
      event = {
        'timestamp'                       : self.admin_interface_403_trace_timestamps[i].isoformat().split('.')[0],
        'admin_interface_rpm_limit'       : self.admin_interface_rpm_limit,
        'admin_interface_retry_parameters': self.admin_interface_retry_parameters,
        'description'                     : self.admin_interface_403_trace_descriptions[i]
      }
      response['admin_interface_403_trace'].append(event)
    
    
    for i in range(len(self.account_retrieve_200_trace_descriptions)):
      event = {
        'timestamp'                        : self.account_retrieve_200_trace_timestamps[i].isoformat().split('.')[0],
        'account_retrieve_rpm_limit'       : self.account_retrieve_rpm_limit,
        'account_retrieve_retry_parameters': self.account_retrieve_retry_parameters,
        'description'                      : self.account_retrieve_200_trace_descriptions[i]
      }
      response['account_retrieve_200_trace'].append(event)
    
    for i in range(len(self.account_retrieve_403_trace_descriptions)):
      event = {
        'timestamp'                        : self.account_retrieve_403_trace_timestamps[i].isoformat().split('.')[0],
        'account_retrieve_rpm_limit'       : self.account_retrieve_rpm_limit,
        'account_retrieve_retry_parameters': self.account_retrieve_retry_parameters,
        'description'                      : self.account_retrieve_403_trace_descriptions[i]
      }
      response['account_retrieve_403_trace'].append(event)
    
    for i in range(len(self.account_retrieve_0_trace_descriptions)):
      event = {
        'timestamp'                        : self.account_retrieve_0_trace_timestamps[i].isoformat().split('.')[0],
        'account_retrieve_rpm_limit'       : self.account_retrieve_rpm_limit,
        'account_retrieve_retry_parameters': self.account_retrieve_retry_parameters,
        'description'                      : self.account_retrieve_0_trace_descriptions[i]
      }
      response['account_retrieve_0_trace'].append(event)
    
    
    for i in range(len(self.account_load_200_trace_descriptions)):
      event = {
        'timestamp'                    : self.account_load_200_trace_timestamps[i].isoformat().split('.')[0],
        'account_load_rpm_limit'       : self.account_load_rpm_limit,
        'account_load_retry_parameters': self.account_load_retry_parameters,
        'description'                  : self.account_load_200_trace_descriptions[i]
      }
      response['account_load_200_trace'].append(event)
    
    for i in range(len(self.account_load_403_trace_descriptions)):
      event = {
        'timestamp'                    : self.account_load_403_trace_timestamps[i].isoformat().split('.')[0],
        'account_load_rpm_limit'       : self.account_load_rpm_limit,
        'account_load_retry_parameters': self.account_load_retry_parameters,
        'description'                  : self.account_load_403_trace_descriptions[i]
      }
      response['account_load_403_trace'].append(event)
    
    for i in range(len(self.account_load_0_trace_descriptions)):
      event = {
        'timestamp'                    : self.account_load_0_trace_timestamps[i].isoformat().split('.')[0],
        'account_load_rpm_limit'       : self.account_load_rpm_limit,
        'account_load_retry_parameters': self.account_load_retry_parameters,
        'description'                  : self.account_load_0_trace_descriptions[i]
      }
      response['account_load_0_trace'].append(event)
    
    
    for i in range(len(self.account_register_200_trace_descriptions)):
      event = {
        'timestamp'                        : self.account_register_200_trace_timestamps[i].isoformat().split('.')[0],
        'account_register_rpm_limit'       : self.account_register_rpm_limit,
        'account_register_retry_parameters': self.account_register_retry_parameters,
        'description'                      : self.account_register_200_trace_descriptions[i]
      }
      response['account_register_200_trace'].append(event)
    
    for i in range(len(self.account_register_403_trace_descriptions)):
      event = {
        'timestamp'                        : self.account_register_403_trace_timestamps[i].isoformat().split('.')[0],
        'account_register_rpm_limit'       : self.account_register_rpm_limit,
        'account_register_retry_parameters': self.account_register_retry_parameters,
        'description'                      : self.account_register_403_trace_descriptions[i]
      }
      response['account_register_403_trace'].append(event)
    
    for i in range(len(self.account_register_0_trace_descriptions)):
      event = {
        'timestamp'                        : self.account_register_0_trace_timestamps[i].isoformat().split('.')[0],
        'account_register_rpm_limit'       : self.account_register_rpm_limit,
        'account_register_retry_parameters': self.account_register_retry_parameters,
        'description'                      : self.account_register_0_trace_descriptions[i]
      }
      response['account_register_0_trace'].append(event)
    
    
    for i in range(len(self.account_resend_200_trace_descriptions)):
      event = {
        'timestamp'                      : self.account_resend_200_trace_timestamps[i].isoformat().split('.')[0],
        'account_resend_rpm_limit'       : self.account_resend_rpm_limit,
        'account_resend_retry_parameters': self.account_resend_retry_parameters,
        'description'                    : self.account_resend_200_trace_descriptions[i]
      }
      response['account_resend_200_trace'].append(event)
    
    for i in range(len(self.account_resend_403_trace_descriptions)):
      event = {
        'timestamp'                      : self.account_resend_403_trace_timestamps[i].isoformat().split('.')[0],
        'account_resend_rpm_limit'       : self.account_resend_rpm_limit,
        'account_resend_retry_parameters': self.account_resend_retry_parameters,
        'description'                    : self.account_resend_403_trace_descriptions[i]
      }
      response['account_resend_403_trace'].append(event)
    
    for i in range(len(self.account_resend_0_trace_descriptions)):
      event = {
        'timestamp'                      : self.account_resend_0_trace_timestamps[i].isoformat().split('.')[0],
        'account_resend_rpm_limit'       : self.account_resend_rpm_limit,
        'account_resend_retry_parameters': self.account_resend_retry_parameters,
        'description'                    : self.account_resend_0_trace_descriptions[i]
      }
      response['account_resend_0_trace'].append(event)
    
    
    for i in range(len(self.account_interface_200_trace_descriptions)):
      event = {
        'timestamp'                         : self.account_interface_200_trace_timestamps[i].isoformat().split('.')[0],
        'account_interface_rpm_limit'       : self.account_interface_rpm_limit,
        'account_interface_retry_parameters': self.account_interface_retry_parameters,
        'description'                       : self.account_interface_200_trace_descriptions[i]
      }
      response['account_interface_200_trace'].append(event)
    
    for i in range(len(self.account_interface_0_trace_descriptions)):
      event = {
        'timestamp'                         : self.account_interface_0_trace_timestamps[i].isoformat().split('.')[0],
        'account_interface_rpm_limit'       : self.account_interface_rpm_limit,
        'account_interface_retry_parameters': self.account_interface_retry_parameters,
        'description'                       : self.account_interface_0_trace_descriptions[i]
      }
      response['account_interface_400_trace'].append(event)
    
    for i in range(len(self.account_interface_403_trace_descriptions)):
      event = {
        'timestamp'                         : self.account_interface_403_trace_timestamps[i].isoformat().split('.')[0],
        'account_interface_rpm_limit'       : self.account_interface_rpm_limit,
        'account_interface_retry_parameters': self.account_interface_retry_parameters,
        'description'                       : self.account_interface_403_trace_descriptions[i]
      }
      response['account_interface_403_trace'].append(event)
    
    
    return response
  
  
  def save(self):
    """Method for saving the session trace."""
    
    if self.session_id is None:
      if self.dev_mode:
        logging.info('SessionTrace.save(): Illegal call, \'session_id=%s\' is undefined.' % str(self.session_id))
      return False
    else:
      now = datetime.today()
      self.sys_modifieddate = now
      self.sys_saveddate = now
      self.put()
      if self.dev_mode:
        logging.info('SessionTrace.save(): Saved \'session_id=%s\' to NDS.' % self.session_id)
  
  
  # TODO(mats.blomdahl@gmail.com): Implement random! Document!
  @classmethod
  def generate_session_id(cls):
    """Foo
      
    """
    
    return 'foobarzss2z'+str(randrange(1000))+'bar'
  

