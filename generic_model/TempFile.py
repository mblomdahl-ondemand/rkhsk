#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   TempFile
# Author:  Mats Blomdahl
# Version: 2012-03-18

from google.appengine.ext.db import Model, StringProperty, IntegerProperty, DateTimeProperty
from google.appengine.ext.blobstore import BlobReferenceProperty

# TODO(mats.blomdahl@gmail.com): Update naming conventions. Transfer to NDB.
class TempFile(Model):
  """The standard representation of a temporary blob upload.
    
  Attributes:
    fileKey: A reference to the Blobstore entry containing the uploaded file.
    fileName: The file name of the upload, a string.
    fileContentType: The MIME type of the uploaded file, a string.
    fileType: The file type of the upload, a string.
    fileSize: An optional integer storing the file size (in bytes).
    fileCreation: Stores a datetime.datetime timestamp from the creation date.
    fileCreatedBy: An identifier string for the user submitting the upload.
  """
  
  fileKey = BlobReferenceProperty(required = True)
  fileName = StringProperty(required = True)
  fileContentType = StringProperty(required = True)
  fileType = StringProperty(required = True)
  fileSize = IntegerProperty()
  fileCreation = DateTimeProperty()
  fileCreatedBy = StringProperty()


