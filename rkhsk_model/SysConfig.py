#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   SysConfig
# Author:  Mats Blomdahl
# Version: 2012-03-18

import logging
import datetime
from google.appengine.ext.db import Expando, DateProperty, StringProperty, IntegerProperty, ListProperty, GqlQuery
from google.appengine.api import memcache

# TODO(mats.blomdahl@gmail.com): Transfer to NDB. Review attribute names.
class SysConfig(Expando):
  """A singleton config entry for system-wide settings.
    
  Attributes:
    sysconfig_keyname: The config's key_name.
    semester_spring_opening: A datetime.date instance storing the first date
      for accepting payments for the spring semester.
    semester_fall_closing: A datetime.date instance storing the end date for
      the fall semester. Used for evaluating semester transition intervals.
    
    semester_fall_opening: A datetime.date instance storing the first date
      for accepting payments for the fall semester.
    semester_spring_closing: A datetime.date instance storing the end date for
      the spring semester. Used for evaluating semester transition intervals.
    
    trans_semester: A string marking the semester currently in transition.
    current_semester: A string marking the current semester.
    
    mecenat_username: Login username for the Mecenat partnership, a string.
    mecenat_password: Login password for the Mecenat partnership, a string.
    
    ssco_username: Login username for the SSCO partnership, a string.
    ssco_password: Login password for the SSCO partnership, a string.
    
    membership_fee: The current membership fee per semester, an integer.
    
    sys_admins: A ListProperty instance which keeps an updated list of system
      administrators, referenced by of their RkhskMember instance key names
      (default = []).
    
    current_member_count: The currently active memberships count, an integer.
    full_member_registry_count: The total number of current members, unverified
      membership applicants and historic members, an integer.
  """
  
  sysconfig_keyname = "SYSCONFIG"
  
  semester_spring_opening = DateProperty(default = datetime.date(2012, 1, 1))
  semester_fall_closing = DateProperty(default = datetime.date(2012, 1, 31))
  
  semester_fall_opening = DateProperty(default = datetime.date(2012, 8, 1))
  semester_spring_closing = DateProperty(default = datetime.date(2012, 8, 31))
  
  trans_semester = StringProperty(default = 'HT11')
  current_semester = StringProperty(default = 'VT12')
  
  email_message_maxlength = IntegerProperty(default = 5000)
  email_inbox_size_limit = IntegerProperty(default = 10000000l)
  
  mecenat_username = StringProperty(default = 'roda')
  mecenat_password = StringProperty(default = 'ZzvNnbgh')
  
  ssco_username = StringProperty(default = u'Röda Korsets högskolas studentkår')
  ssco_password = StringProperty(default = u'röda123')
  
  membership_fee = IntegerProperty(default = 100)
  
  sys_admins = ListProperty(str, default = [])
  
  current_member_count = IntegerProperty(default = 0)
  full_member_registry_count = IntegerProperty(default = 0)
  
  @classmethod
  def get_membership_fee(cls):
    """Method for retrieving and returning the current membership fee."""
    
    return cls.__load()['membership_fee']
  
  
  @classmethod
  def get_current_member_count(cls):
    """Method for retrieving and returning the currently activated members'
      count.
    """
    
    return cls.__load()['current_member_count']
  
  
  @classmethod
  def get_full_member_registry_count(cls):
    """Method for retrieving and returning the full member registry count."""
    
    return cls.__load()['full_member_registry_count']
  
  
  @classmethod
  def get_mecenat_login(cls):
    """Method for retrieving and returning Mecenat login credentials.
      
    Returns:
      A 2-tuple with username and password for logging in to Mecenat.
    """
    
    sys_config_data = cls.__load()
    return (sys_config_data['mecenat_username'], sys_config_data['mecenat_password'])
  
  
  @classmethod
  def get_ssco_login(cls):
    """Method for retrieving and returning SSCO login credentials.
      
    Returns:
      A 2-tuple with username and password for logging in to SSCO.
    """
    
    sys_config_data = cls.__load()
    return (sys_config_data['ssco_username'], sys_config_data['ssco_password'])
  
  
  @classmethod
  def get_current_semester(cls):
    """Method for retrieving and returning the current semester."""
    
    return cls.__load()['current_semester']
  
  
  @classmethod
  def get_spring_semester_dateinterval(cls):
    """Method for retrieving and returning the spring semester date interval.
      
    Returns:
      A tuple with the opening date and closing date for the spring semester,
        both are datetime.date instances.
    """
    
    sys_config_data = cls.__load()
    return (sys_config_data['semester_spring_opening'], sys_config_data['semester_spring_closing'])
  
  
  @classmethod
  def get_fall_semester_dateinterval(cls):
    """Method for retrieving and returning the fall semester date interval.
      
    Returns:
      A tuple with the opening date and closing date for the fall semester,
        both are datetime.date instances.
    """
    
    sys_config_data = cls.__load()
    return (sys_config_data['semester_fall_opening'], sys_config_data['semester_fall_closing'])
  
  
  @classmethod
  def get_first_payment_date(cls):
    """Method for retrieving and returning the first date membership fees can
      be collected."""
    
    sys_config_data = cls.__load()
    if len(sys_config_data['trans_semester']): # we are transitioning...
      if sys_config_data['current_semester'][0:2] == 'VT' and sys_config_data['trans_semester'][0:2] == 'HT':
        return sys_config_data['semester_spring_opening']
      else:
        return sys_config_data['semester_fall_opening']
        
    else: # no longer transitioning.
      if sys_config_data['current_semester'][0:2] == 'VT':
        return sys_config_data['semester_spring_opening']
      else:
        return sys_config_data['semester_fall_opening']
  
  
  @classmethod
  def get_trans_semester(cls):
    """Method for retrieving and returning a semester in transition."""
    
    return cls.__load()['trans_semester']
  
  
  @classmethod
  def get_sys_admins(cls):
    """Method for retrieving and returning a list of system administrators."""
    
    return cls.__load()['sys_admins']
  
  
  @classmethod
  def __load(cls):
    """Method for loading the SysConfig singleton's data from Memcache or
      Datastore.
      
    If the data is not found in Memcache, the method will make an empty update
      call in order to retrieve the instance from Datastore and return the
      instance's data.
      
    Returns:
      A dict containing the SysConfig singleton's instance data.
    """
    
    sys_config_data = memcache.get('%s_dict' % cls.sysconfig_keyname)
    
    if sys_config_data is None:
      sys_config_data = cls.update()
    
    return sys_config_data
  
  
  @classmethod
  def update(cls, config_entry = None, **kwargs):
    """Method for updating the system-wide SysConfig settings and automatically
      calculating values for unspecified attributes.
      
    Arguments:
      config_entry: The singleton SysConfig instance to perform updates on
        (default = None).
      
      semester_spring_opening: A datetime.date instance with the first date for
        accepting payments to the spring semester.
      semester_fall_closing: A datetime.date instance with the end date for the
        fall semester. Used for evaluating semester transition intervals.
      
      semester_fall_opening: A datetime.date instance with the first date for
        accepting payments to the fall semester.
      semester_spring_closing: A datetime.date instance with the end date for
        the spring semester. Used for evaluating semester transition intervals.
      
      trans_semester: A string marking the semester currently in transition.
      current_semester: A string marking the current semester.
      
      mecenat_login: Login credentials to access the systems provided by the
        Mecenat partnership, a 2-tuple with the strings username and password.
      
      ssco_login: Login credentials to access the systems provided by the
        SSCO partnership, a 2-tuple with the strings username and password.
      
      membership_fee: The current membership fee per semester, an integer.
      
      sys_admins: Accepts a tuple with a command string ('refresh', 'add' or
        'remove') and – if 'add' or 'remove' – the admin's account key name.
      
      current_member_count: The currently active memberships count. Accepts
        relative values as a string (e.g. '+3' or '-1') and absolute values as
        an unsigned integer (e.g. 113).
      full_member_registry_count: The total number of current members,
        unverified, membership applicants and historic members. Accepts
        relative values as a string (e.g. '+3' or '-1') and absolute values as
        an unsigned integer (e.g. 113).
      
    Returns:
      A dict with the attributes and values of the updated config.
    """
    
    today = datetime.date.today()
    key_name = cls.sysconfig_keyname
    
    if config_entry:
      entry = config_entry
    else:
      entry = memcache.get('%s_instance' % key_name)
      if entry is None:
        entry = cls.get_by_key_name(key_name)
        if entry is None:
          entry = SysConfig(key_name = key_name)
    
    if 'semester_spring_opening' in kwargs:
      entry.semester_spring_opening = kwargs['semester_spring_opening']
    entry.semester_spring_opening = entry.semester_spring_opening.replace(today.year)
    
    if 'semester_fall_closing' in kwargs:
      entry.semester_fall_closing = kwargs['semester_fall_closing']
    entry.semester_fall_closing = entry.semester_fall_closing.replace(today.year)
    
    if 'semester_fall_opening' in kwargs:
      entry.semester_fall_opening = kwargs['semester_fall_opening']
    entry.semester_fall_opening = entry.semester_fall_opening.replace(today.year)
    
    if 'semester_spring_closing' in kwargs:
      entry.semester_spring_closing = kwargs['semester_spring_closing']
    entry.semester_spring_closing = entry.semester_spring_closing.replace(today.year)
    
    if 'current_semester' in kwargs:
      entry.current_semester = kwargs['current_semester']
    else:
      if today >= entry.semester_spring_opening:
        entry.current_semester = 'VT%s' % str(today.year)[2:4]
      if today >= entry.semester_fall_opening:
        entry.current_semester = 'HT%s' % str(today.year)[2:4]
    
    if 'trans_semester' in kwargs:
      entry.trans_semester = kwargs['trans_semester']
    else:
      semester = ""
      if entry.current_semester[:2] == 'HT' and today <= entry.semester_spring_closing:
        semester = 'VT%s' % str(today.year)[2:4]
      elif entry.current_semester[:2] == 'VT' and today <= entry.semester_fall_closing:
        semester = 'HT%s' % str(today.year)[2:4]
      entry.trans_semester = semester
    
    if 'full_member_registry_count' in kwargs:
      try:
        if kwargs['full_member_registry_count'][0] == '+': # incremental value
          entry.full_member_registry_count += int(kwargs['full_member_registry_count'][1:])
        elif kwargs['full_member_registry_count'][0] == '-': # decremental value
          entry.full_member_registry_count -= int(kwargs['full_member_registry_count'][1:])
        else: # absolute value
          entry.full_member_registry_count -= int(kwargs['full_member_registry_count'])
      except (ValueError):
        logging.info('SysConfig.update(full_member_registry_count=\'%s\'): Fatal error! Argument value raised ValueError.' % str(kwargs['full_member_registry_count']))
    else:
      entry.full_member_registry_count = RkhskMember.all(keys_only = True).count()
    
    if 'current_member_count' in kwargs and len(kwargs['current_member_count']) > 1:
      try:
        if kwargs['current_member_count'][0] == '+': # incremental value
          entry.current_member_count += int(kwargs['current_member_count'][1:])
        elif kwargs['current_member_count'][0] == '-': # decremental value
          entry.current_member_count -= int(kwargs['current_member_count'][1:])
        else: # absolute value
          entry.current_member_count -= int(kwargs['current_member_count'])
      except (ValueError):
        logging.info('SysConfig.update(current_member_count = \'%s\'): Fatal error! Argument value raised ValueError.' % str(kwargs['current_member_count']))
    else:
      entry.current_member_count = GqlQuery('SELECT __keys__ FROM RkhskMember WHERE lastMembershipFor = \'%s\'' % entry.current_semester).count()
    
    if 'mecenat_login' in kwargs:
      entry.mecenat_username = kwargs['mecenat_login'][0]
      entry.mecenat_password = kwargs['mecenat_login'][1]
    
    if 'ssco_login' in kwargs:
      entry.ssco_username = kwargs['ssco_login'][0]
      entry.ssco_password = kwargs['ssco_login'][1]
    
    if 'sys_admins' in kwargs:
      sys_admins_cmd = kwargs['sys_admins'][0]
      if sys_admins_cmd == 'refresh':
        admins = ['su']
        query = GqlQuery('SELECT * FROM RkhskMember WHERE sys_administrator = TRUE')
        for user in query:
          admins.append(user.personalId)
        enry.sys_admins = admins
      
      elif sys_admins_cmd == 'add':
        admin_key_name = kwargs['sys_admins'][1]
        entry.sys_admins.append(admin_key_name)
        if entry.sys_admins.count(admin_key_name) > 1:
          logging.info('SysConfig.update(sys_admins = (\'add\', \'%s\'): Fatal error! Account key name is already listed in SysConfig\'s \'sys_admins\' attribute.' % admin_key_name)
      
      elif sys_admins_cmd == 'remove':
        admin_key_name = kwargs['sys_admins'][1]
        try:
          entry.sys_admins.remove(admin_key_name)
        except (ValueError):
          logging.info('SysConfig.update(sys_admins = (\'remove\', \'%s\'): Fatal error! Account key name was not found in SysConfig\'s \'sys_admins\' attribute.' % admin_key_name)
    
    if 'membership_fee' in kwargs:
      entry.membership_fee = kwargs['membership_fee']
    
    updated_config = {
      'semester_spring_opening'   : entry.semester_spring_opening,
      'semester_fall_closing'     : entry.semester_fall_closing,
      'semester_fall_opening'     : entry.semester_fall_opening,
      'semester_spring_closing'   : entry.semester_spring_closing,
      'trans_semester'            : entry.trans_semester,
      'current_semester'          : entry.current_semester,
      'full_member_registry_count': entry.full_member_registry_count,
      'current_member_count'      : entry.current_member_count,
      'mecenat_username'          : entry.mecenat_username,
      'mecenat_password'          : entry.mecenat_password,
      'ssco_username'             : entry.ssco_username,
      'ssco_password'             : entry.ssco_password,
      'sys_admins'                : entry.sys_admins,
      'membership_fee'            : entry.membership_fee
    }
    
    memcache.set('%s_instance' % key_name, entry)
    memcache.set('%s_dict' % key_name, updated_config)
    
    entry.put()
    
    return updated_config
  


