#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   OwaUser
# Author:  Mats Blomdahl
# Version: 2012-03-16

from google.appengine.ext.db import Model, StringProperty, EmailProperty, ListProperty, DateTimeProperty, Text, Key

# TODO(mats.blomdahl@gmail.com): Evolve?
class OwaUser(Model):
  """An Outlook Web Access user copied from rkh.se Exchange server.
    
  Attributes:
    category: A user class string, e.g. 'Student' (default = '').
    firstName: The user's first name, a string (default = '').
    lastName: The user's last name, a string (default = '').
    email: The user's email address, a string.
    group: The user's group belonging, a string (default = ''). Not in use.
    phone: If available, a string with the user's phone number (default = '').
    office: For students, this attribute stores a string with the student's
      admission class and study group and for staff members the attribute
      defaults to 'RKH' (default = '').
    company: For students, this attribute stores the student's admission class,
      and for staff members the attribute defaults to 'RKH' (default = '').
    
    sys_changelog: A list of db.Text instances storing a JSON object for each
      set of modifications made post-creation (default = []).
    sys_createddate: A datetime.datetime instance storing a timestamp from when
      the instance was first written to Datastore.
    sys_modifieddate: A datetime.datetime instance storing a timestamp for the
      last modification date.
    sys_emailstouser: A list of db.Key references to emails sent to the user
      (default = []).
    sys_emailsfromuser: A list of db.Key references to emails recieved from the
      user (default = []).
  """
  
  category = StringProperty(default = '')
  firstName = StringProperty(default = '')
  lastName = StringProperty(default = '')
  email = EmailProperty()
  group = StringProperty(default = '')
  phone = StringProperty(default = '')
  office = StringProperty(default = '')
  company = StringProperty(default = '')
  
  sys_changelog = ListProperty(Text, default = [])
  sys_createddate = DateTimeProperty(auto_now_add = True)
  sys_modifieddate = DateTimeProperty(auto_now_add = True)
  sys_emailstouser = ListProperty(Key, default = [])
  sys_emailsfromuser = ListProperty(Key, default = [])
  
  def dumps(self):
    """Method for dumping instance data for the Outlook Web Access contact.
      
    Returns:
      A dict with the instance attributes and values.
    """
    
    return {
      'category'          : self.category,
      'firstName'         : self.firstName,
      'lastName'          : self.lastName,
      'email'             : self.email,
      'group'             : self.group,
      'phone'             : self.phone,
      'office'            : self.office,
      'company'           : self.company,
      'sys_changelog'     : self.sys_changelog,
      'sys_createddate'   : self.sys_createddate.isoformat().split('.')[0],
      'sys_modifieddate'  : self.sys_modifieddate.isoformat().split('.')[0],
      'sys_emailstouser'  : self.sys_emailstouser,
      'sys_emailsfromuser': self.sys_emailsfromuser
    }
  

