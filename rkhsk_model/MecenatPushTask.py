#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   MecenatPushTask
# Author:  Mats Blomdahl
# Version: 2012-03-15

import cgi
import webapp2 # web application framework
#from webapp2_extras import jinja2
from webapp2_extras.sessions import SessionStore
from webapp2_extras import sessions
import os
import re
import json
import logging
import random
import urllib
import pickle
import datetime
import time
import string
import locale
# email test start
#from webapp2.mail_handlers import InboundMailHandler

from email.utils import parsedate_tz, mktime_tz

from webob import Response, Request
# email test end
from google.appengine.api import mail
from google.appengine.api import taskqueue
from google.appengine.ext import db
from google.appengine.ext import ndb
from google.appengine.api import namespace_manager
from google.appengine.api import channel
from google.appengine.api import memcache
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import blobstore

# TODO(mats.blomdahl@gmail.com): Document.
class MecenatPushTask(db.Expando):
  """
  A registration task for Mecenat.
  """
  
  personalId = PersonalIdProperty(required = True)
  firstName = NameProperty(required = True)
  lastName = NameProperty(required = True)
  streetCo = OptNameProperty(default = '')
  streetAddress = NameProperty(required = True)
  streetNumber = StreetNumberProperty(required = True)
  streetEntrance = StreetEntranceProperty(default = '')
  streetApartment = StreetApartmentProperty(default = '')
  streetFloor = StreetFloorProperty(default = '')
  zipCode = ZipCodeProperty(required = True)
  city = NameProperty(required = True)
  country = db.StringProperty(default = 'Sverige', choices = ['Sverige'])
  
  memberRef = db.ReferenceProperty(required = True)
  targetSemester = SemesterProperty(default = '')
  
  sys_pushcomplete = db.BooleanProperty(default = False, required = True)
  sys_pushsuccesses = db.ListProperty(datetime.datetime, default = [])
  sys_pushfailures = db.ListProperty(datetime.datetime, default = [])
  sys_createddate = db.DateTimeProperty(auto_now_add = True)
  
  def dumps(self, raw = False, as_csv = False):
    """
    Method for translating the instance attributes to a dict or CSV row.
    """
    
    personal_id = self.personalId[:6]+self.personalId[7:]
    
    streetAddress = '%s %s%s' % (self.streetAddress, self.streetNumber, self.streetEntrance)
    if self.streetApartment:
      streetAddress += ' / lgh. %s' % self.streetApartment
    if self.streetFloor:
      streetAddress += ', %s' % self.streetFloor
    
    if raw is True:
      pushsuccesses = []
      for success in self.sys_pushsuccesses:
        pushsuccesses.append(success.isoformat().split('.')[0])
      
      pushfailures = []
      for failure in self.sys_pushfailures:
        pushfailures.append(success.isoformat().split('.')[0])
      
      return {
        'personalId': personal_id,
        'firstName': self.firstName,
        'lastName': self.lastName,
        'country': self.country,
        'city': self.city,
        'zipCode': self.zipCode,
        'streetAddress': streetAddress,
        'streetCo': self.streetCo,
        'lastName': self.lastName,
        'firstName': self.firstName,
        'memberRef': self.memberRef.personalId,
        'targetSemester': self.targetSemester,
        'sys_pullcomplete': self.sys_pushcomplete,
        'sys_pullsuccesses': pushsuccesses,
        'sys_pullfailures': pushfailures,
        'sys_createddate': self.sys_createddate.isoformat().split('.')[0]
      }
    
    elif as_csv:
      memberdata = u'%s;%s;%s;%s;%s;;%s;%s;;1;1;1;;;;;;;\n' % (personal_id, self.lastName, self.firstName, self.streetCo, streetAddress, self.zipCode, self.city)
      return memberdata.encode('windows-1252')
      
    else:
      return {
        'country': self.country,
        'city': self.city,
        'zipCode': self.zipCode,
        'streetAddress': streetAddress,
        'streetCo': self.streetCo,
        'lastName': self.lastName,
        'firstName': self.firstName,
        'personalId': personal_id
      }
  
  
  def save_success_state(self):
    """
    Propagates a successful push operation.
    """
    
    if any(self.sys_pushsuccesses) and self.sys_pushcomplete is False:
      self.memberRef.lastMecenatReg = self.targetSemester
      self.memberRef.mecenatHistory.append(self.targetSemester)
      self.memberRef.update_memcache()
      self.memberRef.put()
      self.sys_pushcomplete = True
      self.put()
    else:
      logging.warning('Illegal call to \'MecenatPushTask.save_success_state()\' for %s' % self.personalId)
  
  

