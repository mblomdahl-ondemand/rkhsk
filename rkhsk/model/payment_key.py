# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.PaymentKey
# Author:  Mats Blomdahl
# Version: 2012-08-27

from google.appengine.ext import db # foobar

# TODO(mats.blomdahl@gmail.com): Fix PaymentKey documentation!
# TODO(mats.blomdahl@gmail.com): Transfer PaymentKey kind to the NDB database!
class PaymentKey( db.Model ):
    """Payment keys for enabling rapid search and elimination of doublets.

    Attributes:
        paymentKey:
        memberRef:
        sys_createddate:
    """

    paymentKey = db.StringProperty( required=True )
    memberRef = db.ReferenceProperty( required=True )
    sys_createddate = db.DateTimeProperty( auto_now_add=True )

    def dumps(self):
        """Method for dumping registered Payment Keys.

        Returns:
            A dict ...
        """

        return {
            'paymentKey': self.paymentKey,
            'memberRef': self.memberRef.personalId,
            'sys_createddate': self.sys_createddate.isoformat( ).split( '.' )[ 0 ]
        }


