# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.SysTmpKey
# Author:  Mats Blomdahl
# Version: 2012-08-27

import datetime
from google.appengine.ext import db

# TODO(mats.blomdahl@gmail.com): Fix member.SysTmpKey documentation!
# TODO(mats.blomdahl@gmail.com): Transfer member.SysTmpKey kind to the NDB database!
class SysTmpKey( db.Model ):
    """URI for easy access to the member's account information view.

    Attributes:
        tmpKey:
        memberRef:
        useCase:

        sys_expired:
        sys_createddate:
        sys_changelog:
    """

    tmpKey = db.StringProperty( required=True )
    memberRef = db.ReferenceProperty( required=True )
    useCase = db.StringProperty( required=True, choices=[
        'member_login_reminder',
        'email_changed_verification',
        'admin_password_reset'
    ] )
    sys_expired = db.DateTimeProperty( default=None )
    sys_createddate = db.DateTimeProperty( auto_now_add=True )
    sys_changelog = db.ListProperty( db.Text, default=[ ] )

    def is_member_login_reminder(self):
        """Method for checking if SysTmpKey represents a member login reminder.

        Returns:
            ...
        """

        taskqueue.add( queue_name='systmpkeychangelogger', url='/tasks/apply_changelog_update/', params={
            'model': 'SysTmpKey',
            'key': str( self.key( ) ),
            'updates': json.dumps( {
                'testedFor': 'member_login_reminder',
                'getUseCase': self.useCase,
                'dateStamp': datetime.datetime.today( ).isoformat( ).split( '.' )[ 0 ]
            } )
        }, countdown=10 )

        if self.useCase == 'member_login_reminder':
            return True
        else:
            return False

    def is_email_changed_verification(self):
        """Method for checking if SysTmpKey represents a verification of email change.

        Returns:
            ...
        """

        taskqueue.add( queue_name='systmpkeychangelogger', url='/tasks/apply_changelog_update/', params={
            'model': 'SysTmpKey',
            'key': str( self.key( ) ),
            'updates': json.dumps( {
                'testedFor': 'email_changed_verification',
                'getUseCase': self.useCase,
                'dateStamp': datetime.datetime.today( ).isoformat( ).split( '.' )[ 0 ]
            } )
        }, countdown=10 )

        if self.useCase == 'email_changed_verification':
            return True
        else:
            return False

    def is_admin_password_reset(self):
        """Method for checking if SysTmpKey represents an admin password reset.

        Returns:
            ...
        """

        taskqueue.add( queue_name='systmpkeychangelogger', url='/tasks/apply_changelog_update/', params={
            'model': 'SysTmpKey',
            'key': str( self.key( ) ),
            'updates': json.dumps( {
                'testedFor': 'admin_password_reset',
                'getUseCase': self.useCase,
                'dateStamp': datetime.datetime.today( ).isoformat( ).split( '.' )[ 0 ]
            } )
        }, countdown=10 )

        if self.useCase == 'admin_password_reset':
            return True
        else:
            return False

    def is_expired(self):
        """Method for checking if SysTmpKey is expired.

        Returns:
            ...
        """

        try:
            expired = self.sys_expired.isoformat( ).split( '.' )[ 0 ]
        except (AttributeError):
            expired = None

        taskqueue.add( queue_name='systmpkeychangelogger', url='/tasks/apply_changelog_update/', params={
            'model': 'SysTmpKey',
            'key': str( self.key( ) ),
            'updates': json.dumps( {
                'testedFor': 'expired',
                'getKeyExpired': expired,
                'dateStamp': datetime.datetime.today( ).isoformat( ).split( '.' )[ 0 ]
            } )
        }, countdown=10 )

        if self.sys_expired:
            return True
        else:
            return False

    def set_expired(self):
        """Method for expiring the temporary key.

        Returns:
            ...
        """

        self.sys_expired = datetime.datetime.today( )
        self.put( )
        taskqueue.add( queue_name='systmpkeychangelogger', url='/tasks/apply_changelog_update/', params={
            'model': 'SysTmpKey',
            'key': str( self.key( ) ),
            'updates': json.dumps( {
                'setKeyExpired': self.sys_expired.isoformat( ).split( '.' )[ 0 ]
            } )
        }, countdown=10 )

    def get_member(self):
        """Method for retrieving and returning the associated member.

        Returns:
            ...
        """

        taskqueue.add( queue_name='systmpkeychangelogger', url='/tasks/apply_changelog_update/', params={
            'model': 'SysTmpKey',
            'key': str( self.key( ) ),
            'updates': json.dumps( {
                'getMember': self.memberRef.personalId,
                'dateStamp': datetime.datetime.today( ).isoformat( ).split( '.' )[ 0 ]
            } )
        }, countdown=10 )

        try:
            return self.memberRef
        except:
            return None

    def dumps(self):
        """Method for dumping temporary account information URI:s.

        Returns:
            ...
        """

        try:
            sys_expired = self.sys_expired.isoformat( ).split( '.' )[ 0 ]
        except (ValueError):
            sys_expired = None

        data = {
            'tmpKey': self.tmpKey,
            'memberRef': self.memberRef.personalId,
            'useCase': self.useCase,
            'sys_createddate': self.sys_createddate.isoformat( ).split( '.' )[ 0 ],
            'sys_expired': sys_expired
        }

        taskqueue.add( queue_name='systmpkeychangelogger', url='/tasks/apply_changelog_update/', params={
            'model': 'SysTmpKey',
            'key': str( self.key( ) ),
            'updates': json.dumps( {
                'dumpedData': data,
                'dateStamp': datetime.datetime.today( ).isoformat( ).split( '.' )[ 0 ]
            } )
        }, countdown=10 )

        return data
