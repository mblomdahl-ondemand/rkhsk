# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.SscoPullTask
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging, datetime

from google.appengine.ext import db

from ._member_property import _PersonalIdProperty, _SemesterProperty, _NameProperty, _PhoneProperty

# TODO(mats.blomdahl@gmail.com): Fix SscoPullTask documentation!
# TODO(mats.blomdahl@gmail.com): Transfer SscoPullTask kind to the NDB database!
class SscoPullTask( db.Expando ):
    """The datastore representation of SSCO registration task.

    Attributes:
        personalId:
        firstName:
        lastName:
        email:
        phone:
        memberRef:
        targetSemester:
        membershipStarts:
        membershipEnds:

        sys_pullcomplete:
        sys_pullsuccesses:
        sys_pullfailures:
        sys_createddate:

        accountPageUri: The members private account URI, a string (this is also the
            'key_name' attribute of the connector instance).
        memberRef: A ReferenceProperty connecting the email address to its owner.
        sys_createddate: A datestamp for the instance's creation date.
    """

    personalId = _PersonalIdProperty( required=True )
    firstName = _NameProperty( required=True )
    lastName = _NameProperty( required=True )
    email = db.EmailProperty( )
    phone = _PhoneProperty( default='' )

    memberRef = db.ReferenceProperty( required=True )
    targetSemester = _SemesterProperty( default='' )
    membershipStarts = db.DateProperty( required=True )
    membershipEnds = db.DateProperty( required=True )

    sys_pullcomplete = db.BooleanProperty( default=False, required=True )
    sys_pullsuccesses = db.ListProperty( datetime.datetime, default=[ ] )
    sys_pullfailures = db.ListProperty( datetime.datetime, default=[ ] )
    sys_createddate = db.DateTimeProperty( auto_now_add=True )

    def dumps(self, raw=False, as_csv=False, dev_mode=False):
        """Method for dumping the instance data into a dict or CSV row.

        Arguments:
            raw: A boolean flag used to dump the full set of instance attributes
                (default = False).
            as_csv: A boolean flag used to dump the data set and encoding (UTF-8)
                found in SSCO's service specification (default = False).
            dev_mode: A boolean flag marking development mode. If False, logging
                will be suppressed (default = False).

        Returns:
            A dict with the data set specified by the flags in the method's argument
                list.
        """

        if raw is True:
            pullsuccesses = [ ]
            for success in self.sys_pullsuccesses:
                pullsuccesses.append( success.isoformat( ).split( '.' )[ 0 ] )

            pullfailures = [ ]
            for failure in self.sys_pullfailures:
                pullfailures.append( failure.isoformat( ).split( '.' )[ 0 ] )

            instance_data = {
                'personalId': '19%s' % self.personalId,
                'firstName': self.firstName,
                'lastName': self.lastName,
                'email': self.email,
                'phone': self.phone,
                'memberRef': self.memberRef.personalId,
                'targetSemester': self.targetSemester,
                'membershipStarts': self.membershipStarts.isoformat( ),
                'membershipEnds': self.membershipEnds.isoformat( ),
                'sys_pullcomplete': self.sys_pullcomplete,
                'sys_pullsuccesses': pullsuccesses,
                'sys_pullfailures': pullfailures,
                'sys_createddate': self.sys_createddate.isoformat( ).split( '.' )[ 0 ]
            }

            if dev_mode:
                logging.info(
                    'SscoPullTask.dumps(raw=True, as_csv=False, dev_mode=True).instance_data: %s' % str(
                        instance_data ) )

            return instance_data

        elif as_csv is True:
            if self.email is None:
                email = ''
            else:
                email = self.email

            csv_data = {
                'data_header': u'Nytt personnr;Förnamn;Efternamn;Medlem fr.o.m.;Medlem t.o.m.;E-postadress;Mobilnr\n'
                .encode(
                    'utf-8' ),
                'member_data': u'19%s;%s;%s;%s;%s;%s;%s\n' % (
                    self.personalId, self.firstName, self.lastName, self.membershipStarts.isoformat( ),
                    self.membershipEnds.isoformat( ), email, self.phone).encode( 'utf-8' )
            }

            if dev_mode:
                logging.info(
                    'SscoPullTask.dumps(raw=False, as_csv=True, dev_mode=True).csv_data: %s' % str( csv_data ) )

            return csv_data

        else:
            member_data = {
                'personalId': '19%s' % self.personalId,
                'firstName': self.firstName,
                'lastName': self.lastName,
                'email': self.email,
                'phone': self.phone,
                'membershipStarts': self.membershipStarts.isoformat( ),
                'membershipEnds': self.membershipEnds.isoformat( )
            }

            if dev_mode:
                logging.info(
                    'SscoPullTask.dumps(raw=False, as_csv=False, dev_mode=True).member_data: %s' % str( member_data ) )

            return member_data


    def save_success_state(self, dev_mode=False):
        """Method for saving success state for a transferred member.

        Arguments:
            dev_mode: A boolean flag marking development mode. If False, logging will
                be suppressed.

        Returns:
            None
        """

        if any( self.sys_pullsuccesses ) and self.sys_pullcomplete is False:
            self.memberRef.lastSscoReg = self.targetSemester
            self.memberRef.sscoHistory.append( self.targetSemester )
            self.memberRef.update_memcache( )
            self.memberRef.put( )

            self.sys_pullcomplete = True
            self.put( )
            if dev_mode:
                logging.info(
                    'SscoPullTask.save_success_state(dev_mode=True): Registration task for \'personalId=%s\' successfully updated as \'completed\'.' % self.personalId )
        elif dev_mode:
            logging.info(
                'SscoPullTask.save_success_state(dev_mode=True): Illegal call to save \'personalId=%s\'.' % self.personalId )



