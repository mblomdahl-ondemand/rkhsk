# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.AccountUri
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging
from google.appengine.ext import db

# TODO(mats.blomdahl@gmail.com): Fix member.AccountUri documentation!
# TODO(mats.blomdahl@gmail.com): Transfer member.AccountUri kind to the NDB database!
# TODO(mats.blomdahl@gmail.com): Review and update member.AccountUri attribute names.
class AccountUri( db.Model ):
    """A private account URI connector for enabling fast search and easy
        prevention of doublet creation.

    Attributes:
        accountPageUri: The members private account URI, a string (this is also the
            'key_name' attribute of the connector instance).
        memberRef: A ReferenceProperty connecting the email address to its owner.
        sys_createddate: A datestamp for the instance's creation date.
    """

    accountPageUri = db.StringProperty( required=True )
    memberRef = db.ReferenceProperty( required=True )
    sys_createddate = db.DateTimeProperty( auto_now_add=True )

    def dumps(self, dev_mode=False):
        """Method for dumping an accounts unique and private URI.

        Arguments:
            dev_mode: A boolean flag marking development mode. If False, logging
                will be suppressed (default = False).
        """

        instance_data = {
            'accountPageUri': self.accountPageUri,
            'memberRef': self.memberRef.personalId,
            'sys_createddate': self.sys_createddate.isoformat( ).split( '.' )[ 0 ]
        }

        if dev_mode:
            logging.info( 'AccountUri.dumps(dev_mode=True): %s' % str( instance_data ) )

        return instance_data


