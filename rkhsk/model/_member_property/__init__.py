# -*- coding: utf-8 -*-
#
# Title:   Package 'rkhsk.model._member_property'
# Author:  Mats Blomdahl
# Version: 2012-09-10
#

"""rkhsk.model._member_property module

More documentation ...
"""

import re, datetime
from google.appengine.ext import db

class _CourseProperty( db.StringProperty ):
    """Custom property for validating an admission class attribute as compliant
        with the coding convention employed at The Red Cross University College.
    """

    data_type = str

    def validate(self, course):
        """Validator to make sure the admission class input is specified correctly.

        Arguments:
            course: A 4 char string coded as <['VK','HK']YY>, <'SPEC'> or an empty
                string.
        """

        course = super( _CourseProperty, self ).validate( course )
        this_year = str( datetime.datetime.today( ).year )
        if course != '' and re.search( '^(SPEC)|([HV]K0[6-9]$|^[HV]K1[0-' + this_year[ 3 ] + '])$', course ) is None:
            raise db.BadValueError(
                "Property %r must be of format '^(SPEC)|([HV]K0[6-9]$|^[HV]K1[0-'%s'])$', "
                "admission class %r is not." % (
                    self.name, this_year[ 3 ], course) )
        else:
            return course

class _ZipCodeProperty( db.StringProperty ):
    """Custom property for validating Swedish zip code attributes."""

    data_type = str

    def validate(self, zip_code):
        """Validator to make sure the zip code is a five-digit number.

        Arguments:
            zip_code: The five-digit zip code used Swedish postal addresses.
        """

        zip_code = super( _ZipCodeProperty, self ).validate( zip_code )
        if zip_code is None or re.search( '^\d{5}$', zip_code ) is None:
            raise db.BadValueError(
                "Property %r must be of format '^\d{5}$', zip code %r is not." % (self.name, str( zip_code )) )
        else:
            return zip_code


class _StreetNumberProperty( db.StringProperty ):
    """Custom property for validating a Swedish street number attribute."""

    data_type = str

    def validate(self, street_address):
        """Validator to make sure the address' street number is formatted correctly.

        Arguments:
            street_address: The postal address' street number (e.g. '23' or
              '125-127').
        """

        street_address = super( _StreetNumberProperty, self ).validate( street_address )
        if street_address is None or re.search( '^\d{1,3}$|^\d{1,3}\-\d{1,3}$', street_address ) is None:
            raise db.BadValueError(
                "Property %r must be of format '^\d{1,3}$|^\d{1,3}\-\d{1,3}$', street address %r is not." % (
                    self.name, str( street_address )) )
        else:
            return street_address


class _StreetFloorProperty( db.StringProperty ):
    """Custom property for validating a street floor attribute."""

    data_type = str

    def validate(self, street_floor):
        """Validator to make sure the apartment floor is formatted correctly.

        Arguments:
            street_floor: Valid options for the floor are 'N.B.' or '1 tr.', '2 tr.',
                ..., '99 tr.' or an empty string.
        """

        street_floor = super( _StreetFloorProperty, self ).validate( street_floor )
        if street_floor != '' and re.search( '^N\.B\.$|^[\d]{1,2} tr\.$', street_floor ) is None:
            raise db.BadValueError(
                "Property %r must be of format '^N\.B\.$|^[\d]{1,2} tr\.$', street floor %r is not." % (
                    self.name, str( street_floor )) )
        else:
            return street_floor


class _StreetEntranceProperty( db.StringProperty ):
    """Custom property for validating a street address entrance attribute."""

    data_type = str

    def validate(self, street_entrance):
        """Validator to make sure the street entrance is specified correctly.

        Arguments:
            street_entrance: A single upper-case alpha char or an empty string.
        """

        street_entrance = super( _StreetEntranceProperty, self ).validate( street_entrance )
        if street_entrance != '' and re.search( '^[A-Z]$', street_entrance ) is None:
            raise db.BadValueError( "Property %r must be of format '^[A-Z]$, street entrance %r is not." % (
                self.name, str( street_entrance )) )
        else:
            return street_entrance


class _StreetApartmentProperty( db.StringProperty ):
    """Custom property for validating an apartment number attribute."""

    data_type = str

    def validate(self, street_apartment):
        """Validator to make sure the street apartment number is specified
            correctly.

        Arguments:
            street_apartment: A 1-4 digits long code or an empty string.
        """

        street_apartment = super( _StreetApartmentProperty, self ).validate( street_apartment )
        if street_apartment != '' and re.search( '^[\d]{1,4}$', street_apartment ) is None:
            raise db.BadValueError( "Property %r must be of format '^[\d]{1,4}$', steet apartment number %r is not." % (
                self.name, str( street_apartment )) )
        else:
            return street_apartment


class _SemesterProperty( db.StringProperty ):
    """Custom property for validating a college semester attribute as compliant
        with the coding convention employed at The Red Cross University College.
    """

    data_type = str

    def validate(self, semester):
        """Validator to make sure the semester input is specified correctly.

        Arguments:
            semester: A 4 char string coded as <['VT','HT']YY> or an empty string.
        """

        semester = super( _SemesterProperty, self ).validate( semester )
        this_year = str( datetime.datetime.today( ).year )
        if semester != '' and re.search( '^[HV]T0[6-9]$|^[HV]T1[0-' + this_year[ 3 ] + ']$', semester ) is None:
            raise db.BadValueError(
                "Property %r must be of format '^[HV]T0[6-9]$|^[HV]T1[0-'%s']$', semester %r is not." % (
                    self.name, this_year[ 3 ], semester) )
        else:
            return semester


class _PhoneProperty( db.StringProperty ):
    """Custom property for validating a phone number attribute."""

    data_type = str

    def validate(self, phone_number):
        """Validator to make sure the phone number is specified correctly.

        Arguments:
            phone_number: A 7–18 char phone number.
        """

        phone_number = super( _PhoneProperty, self ).validate( phone_number )
        if phone_number != '' and re.search( '^[\+\d][\d -]{7,18}$', phone_number ) is None:
            raise db.BadValueError( "Property %r must be of format '^[\+\d][\d -]{7,18}$', phone number %r is not." % (
                self.name, phone_number) )
        else:
            return phone_number


class _PersonalIdProperty( db.StringProperty ):
    """Custom property for validating personal ID number attributes."""

    data_type = str

    def validate(self, personal_id):
        """Validator to make sure the personal ID number is specified
            correctly.

        Arguments:
            personal_id: A Swedish personal ID number formatted as YYMMDD-NNNN.
        """

        personal_id = super( _PersonalIdProperty, self ).validate( personal_id )
        if personal_id is None or re.search( '^\d{6}-[a-z\d]{4}$', personal_id, flags=re.I ) is None:
            raise db.BadValueError( "Property %r must be of format '^\d{6}-[\w\d]{4}$', personal ID %r is not." % (
                self.name, str( personal_id )) )
        else:
            return personal_id


class _OptNameProperty( db.StringProperty ):
    """Custom property for validating an optional name attribute."""

    data_type = unicode

    def validate(self, opt_name):
        """Validator to make sure the 'optional name' input is specified correctly.

        Arguments:
            opt_name: A 1-40 char name or an empty string.
        """

        opt_name = super( _OptNameProperty, self ).validate( opt_name )
        if opt_name != '' and re.search( u'^[a-z :éüÜÅÄÖåäö\-]{1,40}$', opt_name, flags=re.I ) is None:
            raise db.BadValueError(
                "Property %r must be of format '^[a-z :ÅÄÖåäö\-]{1,40}$', optional name %r is not." % (
                    self.name, opt_name) )
        else:
            return opt_name


class _NameProperty( db.StringProperty ):
    """Custom property for validating a member name attribute."""

    data_type = unicode

    def validate(self, name):
        """Validator to make sure the 'name' input is specified correctly.

        Arguments:
            name: A 1-40 char name or None.
        """

        name = super( _NameProperty, self ).validate( name )
        if name is None or re.search( u'^[a-z :éüÜÅÄÖåäö\-]{1,40}$', name, flags=re.I ) is None:
            raise db.BadValueError(
                "Property %r must be of format '^[a-z :ÅÄÖåäö\-]{1,40}$', name %r is not." % (self.name, name) )
        else:
            return name
