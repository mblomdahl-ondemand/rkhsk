# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.AccountEmail
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging
from google.appengine.ext import db

# TODO(mats.blomdahl@gmail.com): Fix member.AccountEmail documentation!
# TODO(mats.blomdahl@gmail.com): Transfer member.AccountEmail kind to the NDB database!
# TODO(mats.blomdahl@gmail.com): Review/bug-fix member.AccountEmail attribute coding!
class AccountEmail( db.Model ):
    """An account email connector for enabling fast search and easy prevention of
        doublet creation.

    Attributes:
        accountEmail: The email address for an account, a string (this is also the
            'key_name' attribute of the instance).
        memberRef: A ReferenceProperty connecting the email address to its owner.
        sys_createddate: A datestamp for the instance's creation date.
    """

    accountEmail = db.StringProperty( required=True )
    memberRef = db.ReferenceProperty( required=True )
    sys_createddate = db.DateTimeProperty( auto_now_add=True )

    def dumps(self, dev_mode=False):
        """Method for dumping account email connection data.

        Arguments:
            dev_mode: A boolean flag marking development mode. If False, logging
                will be suppressed (default = False).
        """

        instance_data = {
            'accountEmail': self.accountEmail,
            'memberRef': self.memberRef.personalId,
            'sys_createddate': self.sys_createddate.isoformat( ).split( '.' )[ 0 ]
        }

        if dev_mode:
            logging.info( 'AccountEmail.dumps(dev_mode=True): %s' % str( instance_data ) )

        return instance_data


