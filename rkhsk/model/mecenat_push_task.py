# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.MecenatPushTask
# Author:  Mats Blomdahl
# Version: 2012-08-27

import datetime

from google.appengine.ext import db

from ._member_property import _PersonalIdProperty, _SemesterProperty, _NameProperty, _OptNameProperty, _ZipCodeProperty, _StreetApartmentProperty, _StreetEntranceProperty, _StreetFloorProperty, _StreetNumberProperty

# TODO(mats.blomdahl@gmail.com): Fix MecenatPushTask documentation!
# TODO(mats.blomdahl@gmail.com): Transfer MecenatPushTask kind to the NDB database!
class MecenatPushTask( db.Expando ):
    """A registration task for Mecenat.

    Attributes:
        personalId:
        firstName:
        lastName:
        streetCo:
        streetAddress:
        streetNumber:
        streetEntrance:
        streetApartment:
        streetFloor:
        zipCode:
        city:
        country:

        memberRef:
        targetSemester:

        sys_pushcomplete:
        sys_pushsuccesses:
        sys_pushfailures:
        sys_createddate:
    """

    personalId = _PersonalIdProperty( required=True )
    firstName = _NameProperty( required=True )
    lastName = _NameProperty( required=True )
    streetCo = _OptNameProperty( default='' )
    streetAddress = _NameProperty( required=True )
    streetNumber = _StreetNumberProperty( required=True )
    streetEntrance = _StreetEntranceProperty( default='' )
    streetApartment = _StreetApartmentProperty( default='' )
    streetFloor = _StreetFloorProperty( default='' )
    zipCode = _ZipCodeProperty( required=True )
    city = _NameProperty( required=True )
    country = db.StringProperty( default='Sverige', choices=[ 'Sverige' ] )

    memberRef = db.ReferenceProperty( required=True )
    targetSemester = _SemesterProperty( default='' )

    sys_pushcomplete = db.BooleanProperty( default=False, required=True )
    sys_pushsuccesses = db.ListProperty( datetime.datetime, default=[ ] )
    sys_pushfailures = db.ListProperty( datetime.datetime, default=[ ] )
    sys_createddate = db.DateTimeProperty( auto_now_add=True )

    def dumps(self, raw=False, as_csv=False):
        """Method for translating the instance attributes to a dict or CSV row.

        Arguments:
            raw:
            as_csv:

        Returns:
            Foo.
        """

        personal_id = self.personalId[ :6 ] + self.personalId[ 7: ]

        streetAddress = '%s %s%s' % (self.streetAddress, self.streetNumber, self.streetEntrance)
        if self.streetApartment:
            streetAddress += ' / lgh. %s' % self.streetApartment
        if self.streetFloor:
            streetAddress += ', %s' % self.streetFloor

        if raw is True:
            pushsuccesses = [ ]
            for success in self.sys_pushsuccesses:
                pushsuccesses.append( success.isoformat( ).split( '.' )[ 0 ] )

            pushfailures = [ ]
            for failure in self.sys_pushfailures:
                pushfailures.append( failure.isoformat( ).split( '.' )[ 0 ] )

            return {
                'personalId': personal_id,
                'firstName': self.firstName,
                'lastName': self.lastName,
                'country': self.country,
                'city': self.city,
                'zipCode': self.zipCode,
                'streetAddress': streetAddress,
                'streetCo': self.streetCo,
                'memberRef': self.memberRef.personalId,
                'targetSemester': self.targetSemester,
                'sys_pullcomplete': self.sys_pushcomplete,
                'sys_pullsuccesses': pushsuccesses,
                'sys_pullfailures': pushfailures,
                'sys_createddate': self.sys_createddate.isoformat( ).split( '.' )[ 0 ]
            }

        elif as_csv:
            member_data = u'%s;%s;%s;%s;%s;;%s;%s;;1;1;1;;;;;;;\n' % (
                personal_id, self.lastName, self.firstName, self.streetCo, streetAddress, self.zipCode, self.city)
            return member_data.encode( 'windows-1252' )

        else:
            return {
                'country': self.country,
                'city': self.city,
                'zipCode': self.zipCode,
                'streetAddress': streetAddress,
                'streetCo': self.streetCo,
                'lastName': self.lastName,
                'firstName': self.firstName,
                'personalId': personal_id
            }

    def save_success_state(self):
        """Propagates a successful push operation.

        Returns:
            Foo.
        """

        if any( self.sys_pushsuccesses ) and self.sys_pushcomplete is False:
            self.memberRef.lastMecenatReg = self.targetSemester
            self.memberRef.mecenatHistory.append( self.targetSemester )
            self.memberRef.update_memcache( )
            self.memberRef.put( )
            self.sys_pushcomplete = True
            self.put( )
        else:
            logging.info( 'MecenatPushTask.save_success_state(): Illegal call for %s recieved.' % self.personalId )



