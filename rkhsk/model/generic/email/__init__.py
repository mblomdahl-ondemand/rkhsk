# -*- coding: utf-8 -*-
#
# Title:   Package 'rkhsk.model.generic.email'
# Author:  Mats Blomdahl
# Version: 2012-08-21
#

"""rkhsk.model.generic.email module

Support for session management and wrapping inbound requests. More documentation ...
"""

# TODO(mats.blomdahl@gmail.com): Fix documentation for rkhsk.model.generic.email package!
from .inbound_email import InboundEmail
from .sent_email import SentEmail

