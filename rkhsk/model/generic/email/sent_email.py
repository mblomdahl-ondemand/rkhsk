# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.generic.email.SentEmail
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging

from google.appengine.api import mail
from google.appengine.ext import db

# TODO(mats.blomdahl@gmail.com): Improve generic.email.SentEmail implementation!
# TODO(mats.blomdahl@gmail.com): Transfer generic.email.SentEmail kind to the NDB database!
# TODO(mats.blomdahl@gmail.com): Update generic.email.SentEmail attribute naming convention!
class SentEmail( db.Model ):
    """The internal representation of a sent email.

    Attributes:
        to: The recipient's name and email address, a string.
        sender: The sender's name and email address, a string.
        cc: Name and email address of any carbon copy recipients, a string.
        reply_to: Name and email address to reply to, a string.
        subject: Email subject line, a string.
        date: The parsed email date, a datetime.datetime instance.
        body_plaintext_type: Content type of the message plaintext body, a string.
        body_plaintext_content: The message plaintext payload, a list of db.Text instances.
        body_html_type: Content type of the message HTML body, a string.
        body_html_content: The message HTML payload, a list of db.Text instances.
        attachment_filenames: The email attachments' filenames, a list.
        attachment_filecontents: The email attachments' file content, a list of db.Blob instances.
        original: The raw email message object, a db.Blob instance.

        sys_sentby: The user ID (i.e. 'key_name' attribute) of the user initiating the mail-out, a string.
        sys_creator: Usually the originating client IP address.

        sys_starred: A boolean flag marking the email as starred (default = False).
        sys_starredby: The user ID (i.e. 'key_name' attribute) of the user marking the message as starred, a string.
        sys_read: A boolean flag marking the email as read (default = False).
        sys_readby: The user ID (i.e. 'key_name' attribute) of the user first to mark the email as read, a string.
        sys_deleted: A boolean flag marking the email as deleted (default = False).
        sys_deletedby: The user ID (i.e. 'key_name' attribute) of the user who deleted the message, a string.
        sys_changelog: A list of modifications applied to the email message since creation, a list of db.Text instances
            with JSON data (default = []).
        sys_createddate: A datetime.datetime timestamp for when the instance was first written to Datastore.
    """

    to = db.StringProperty( )
    sender = db.StringProperty( )
    subject = db.StringProperty( )
    cc = db.StringProperty( )
    reply_to = db.StringProperty( )
    date = db.DateTimeProperty( )
    body_plaintext_type = db.ListProperty( str, default=[ ] )
    body_plaintext_content = db.ListProperty( db.Text, default=[ ] )
    body_html_type = db.ListProperty( str, default=[ ] )
    body_html_content = db.ListProperty( db.Text, default=[ ] )
    attachment_filenames = db.ListProperty( unicode, default=[ ] )
    attachment_filecontents = db.ListProperty( db.Blob, default=[ ] )
    original = db.Blob( )

    sys_sentby = db.StringProperty( )
    sys_creator = db.StringProperty( )
    sys_starred = db.BooleanProperty( default=False )
    sys_starredby = db.StringProperty( )
    sys_read = db.BooleanProperty( default=False )
    sys_readby = db.StringProperty( )
    sys_deleted = db.BooleanProperty( default=False )
    sys_deletedby = db.StringProperty( )

    sys_changelog = db.ListProperty( db.Text, default=[ ] )
    sys_createddate = db.DateTimeProperty( auto_now_add=True )

    @classmethod
    def send_wrapper(cls, dev_mode=False, **kwargs):
        """Method for wrapping outgoing emails.

        Arguments:
            dev_mode: A boolean flag marking development mode. If False, logging will be suppressed (default = False).
            to: The recipient's name and email address, a string.
            sender: The sender's name and email address, a string.
            cc: Name and email address for carbon copy recipients, a string.
            bcc: Name and email address for blank carbon copy recipients, a string.
            reply_to: Name and email address for reply, a string.
            subject: Email subject line, a string.
            body_plaintext: The message plaintext payload, a string.
            body_html: The message HTML payload, a string.
            sys_sentby: The user ID (i.e. 'key_name' attribute) of the user initiating the mail-out, a string.
            sys_creator: Usually the originating client IP address.
        """

        if dev_mode:
            logging.info( 'SentEmail.send_wrapper(dev_mode=\'%s\').kwargs: %s' % (str( dev_mode ), str( kwargs )) )

        message = mail.EmailMessage(
            to=kwargs[ 'to' ],
            sender='Medlemsregistreringen (RKH-SK) <regga@rkh-sk.se>',
            #bcc='Mats Blomdahl <mats.blomdahl@gmail.com>; Nathalie Lindholm <nathalie.lindholm@rkh-sk.se>; Henrietta '\
            #    'Olestad <henrietta.olestad@rkh-sk.se',
            bcc='Mats Blomdahl <mats.blomdahl@gmail.com>; Henrietta Olestad <henrietta.olestad@rkh-sk.se',
            subject=kwargs[ 'subject' ]
        )
        if 'body_plaintext' in kwargs:
            message.body = kwargs[ 'body_plaintext' ]
        if 'body_html' in kwargs:
            message.html = kwargs[ 'body_html' ]
        if 'cc' in kwargs:
            message.cc = kwargs[ 'cc' ]

        message.send( )

        message_sent = cls(
            sender=message.sender,
            to=message.to,
            bcc=message.bcc,
            body_plaintext_type=[ 'text/plain' ],
            body_plaintext_content=[ db.Text( message.body ) ],
            body_html_type=[ 'text/html' ],
            body_html_content=[ db.Text( message.html ) ],
            sys_sentby=kwargs[ 'sys_sentby' ],
            sys_creator=kwargs[ 'sys_creator' ]
        )

        message_sent.put( )


