# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.generic.email.InboundEmail
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging
from google.appengine.ext import db

# TODO(mats.blomdahl@gmail.com): Transfer generic.email.InboundEmail kind to the NDB database!
class InboundEmail( db.Model ):
    """The internal representation of an inbound email.

    Attributes:
        to: The recipient's name and email address, a string.
        sender: The sender's name and email address, a string.
        cc: Name and email address of any carbon copy recipients, a string.
        reply_to: Name and email address to reply to, a string.
        subject: Email subject line, a string.
        date: The parsed email date, a datetime.datetime instance.
        body_plaintext_type: Content type of the message plaintext body, a string.
        body_plaintext_content: The message plaintext payload, a list of db.Text instances.
        body_html_type: Content type of the message HTML body, a string.
        body_html_content: The message HTML payload, a list of db.Text instances.
        attachment_filenames: The email attachments' filenames, a list.
        attachment_filecontents: The email attachments' file content, a list of db.Blob instances.
        original: The raw email message object, a db.Blob instance.

        sys_starred: A boolean flag marking the email as starred (default = False).
        sys_starredby: The user ID (i.e. 'key_name' attribute) of the user marking the message as starred, a string.
        sys_read: A boolean flag marking the email as read (default = False).
        sys_readby: The user ID (i.e. 'key_name' attribute) of the user first to mark the email as read, a string.
        sys_deleted: A boolean flag marking the email as deleted (default = False).
        sys_deletedby: The user ID (i.e. 'key_name' attribute) of the user who deleted the message, a string.
        sys_changelog: A list of modifications applied to the email message since creation, a list of db.Text instances
            with JSON data (default = []).
        sys_createddate: A datetime.datetime timestamp for when the instance was first written to Datastore.
    """

    to = db.StringProperty( default="" )
    sender = db.StringProperty( default="" )
    cc = db.StringProperty( default="" )
    reply_to = db.StringProperty( default="" )
    subject = db.StringProperty( default="" )
    date = db.DateTimeProperty( )
    body_plaintext_type = db.ListProperty( str, default=[ ] )
    body_plaintext_content = db.ListProperty( db.Text, default=[ ] )
    body_html_type = db.ListProperty( str, default=[ ] )
    body_html_content = db.ListProperty( db.Text, default=[ ] )
    attachment_filenames = db.ListProperty( unicode, default=[ ] )
    attachment_filecontents = db.ListProperty( db.Blob, default=[ ] )
    original = db.Blob()

    sys_starred = db.BooleanProperty( default=False )
    sys_starredby = db.StringProperty( )
    sys_read = db.BooleanProperty( default=False )
    sys_readby = db.StringProperty( )
    sys_deleted = db.BooleanProperty( default=False )
    sys_deletedby = db.StringProperty( )

    sys_changelog = db.ListProperty( db.Text, default=[ ] )
    sys_createddate = db.DateTimeProperty( auto_now_add=True )

    # TODO(mats.blomdahl@gmail.com): Perform unit testing!
    @classmethod
    def get_range(cls, offset=0, limit=1000, order='DESC', dev_mode=False):
        """Method for retrieving a range or 'page' of inbound emails.

        Arguments:
            offset: Number of emails to skip by, an integer (default = 0).
            limit: Max number of emails to fetch, an integer (default = 1000).
            order: Sort direction, valid parameters are 'ASC' and 'DESC' (default = 'DESC').
            dev_mode: A boolean flag marking development mode. If False logging will be suppressed.

        Returns:
            A list of dicts containing email data.
        """

        query = db.GqlQuery(
            "SELECT * FROM IncomingEmail ORDER BY sys_createddate %s LIMIT %s,%s" % (order, offset, limit) )

        if dev_mode:
            logging.info(
                'IncomingEmail.get_range(offset=\'%s\', limit=\'%s\', order=\'%s\', dev_mode=%s).count: %r' % (
                    str( offset ), str( limit ), order, str( dev_mode ), str( query.count( ) )) )

        result_data = [ ]

        for email in query:
            date = None
            if email.date is not None:
                date = email.date.replace( tzinfo=UTCTIME ).astimezone( SWETIME ).strftime( "%Y-%m-%dT%H:%M:%S (%Z)" )
            result_data.append( {
                'to': email.to,
                'sender': email.sender,
                'subject': email.subject,
                'cc': email.cc,
                'replyTo': email.reply_to,
                'date': date,
                'plaintextType': email.body_plaintext_type,
                'plaintextBody': email.body_plaintext_content,
                'htmlType': email.body_html_type,
                'htmlBody': email.body_html_content,
                'attachments': email.attachment_filenames,
                'starred': email.sys_starred,
                'starredBy': email.sys_starredby,
                'read': email.sys_read,
                'readBy': email.sys_readby,
                'deleted': email.sys_deleted,
                'deletedBy': email.sys_deletedby
            } )

            if dev_mode:
                logging.info(
                    'IncomingEmail.get_range(offset=\'%s\', limit=\'%s\', order=\'%s\', dev_mode=%s).date: %r' % (
                        str( offset ), str( limit ), order, str( dev_mode ), str( result_data[ -1 ][ 'date' ] )) )

        return result_data


