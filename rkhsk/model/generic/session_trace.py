# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.generic.SessionTrace
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging, random, datetime

from google.appengine.ext import ndb

import rkhsk.model.sys_config

# TODO(mats.blomdahl@gmail.com): Review model.generic.SessionTraceModel implementation!
# TODO(mats.blomdahl@gmail.com): Rename the model.generic.SessionTraceModel in accordance with other classes!
class SessionTraceModel( ndb.Model ):
    """The standard Datastore representation of a client session.

    The SessionTraceModel class stores user session data in order to limit the
        effects of DDoS attacks, brute force hacking attempts and malicious use
        cases (e.g. sending dummy requests in order to drive resource usage costs
        through the roof).

    Attributes:
        client_ip: The client's IP address (i.e. the WebOb request's 'remote_addr'
            attribute).
        session_id: The request's session ID; a cookie set at first visit. The
            cookie never expires and is used for keeping clients organized to their
            respective session logs.
        user_id: The decoded user ID an admin will receive on login.

        admin_login_200_trace_timestamp: A pickled list of successful interaction
            timestamps (value type is 'datetime.datetime').
        admin_login_200_trace_descriptions: A dumped JSON array with an entry
            for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        admin_login_403_trace_timestamp: A pickled list of timestamps for
            interactions that have resulted in a 403 ("forbidden") error.
        admin_login_403_trace_descriptions: A dumped JSON array with an entry
            for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        admin_login_0_trace_timestamp: A pickled list of timestamps for
            interactions resulting in the request being blocked.
        admin_login_0_trace_descriptions: A dumped JSON array with an entry
            for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        admin_login_retry_parameters: Retry limits configuration, a dict.
        admin_login_rpm_limit: Request per minute limitation, an integer.

        admin_interface_200_trace_timestamp: A pickled list of successful
            interaction timestamps (value type is 'datetime.datetime').
        admin_interface_200_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance)..
        admin_interface_403_trace_timestamp: A pickled list of timestamps for
            interactions that have resulted in a 403 ("forbidden") error.
        admin_interface_403_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        admin_interface_0_trace_timestamp: A pickled list of timestamps for
            interactions resulting in the request being blocked.
        admin_interface_0_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        admin_interface_retry_parameters: Retry limits configuration, a dict.
        admin_interface_rpm_limit: Request per minute limitation, an integer.

        account_retrieve_200_trace_timestamp: A pickled list of successful
            interaction timestamps (value type is 'datetime.datetime').
        account_retrieve_200_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_retrieve_403_trace_timestamp: A pickled list of timestamps for
            interactions that have resulted in a 403 ("forbidden") error.
        account_retrieve_403_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_retrieve_0_trace_timestamp: A pickled list of timestamps for
            interactions resulting in the request being blocked.
        account_retrieve_0_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_retrieve_retry_parameters: Retry limits configuration, a dict.
        account_retrieve_rpm_limit: Request per minute limitation, an integer.

        account_load_200_trace_timestamp: A pickled list of successful interaction
            timestamps (value type is 'datetime.datetime').
        account_load_200_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_load_403_trace_timestamp: A pickled list of timestamps for
            interactions that have resulted in a 403 ("forbidden") error.
        account_load_403_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_load_0_trace_timestamp: A pickled list of timestamps for
            interactions resulting in the request being blocked.
        account_load_0_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_load_retry_parameters: Retry limits configuration, a dict.
        account_load_rpm_limit: Request per minute limitation, an integer.

        account_register_200_trace_timestamp: A pickled list of successful
            interaction timestamps (value type is 'datetime.datetime').
        account_register_200_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_register_403_trace_timestamp: A pickled list of timestamps for
            interactions that have resulted in a 403 ("forbidden") error.
        account_register_403_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_register_0_trace_timestamp: A pickled list of timestamps for
            interactions resulting in the request being blocked.
        account_register_0_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_register_retry_parameters: Retry limits configuration, a dict.
        account_register_rpm_limit: Request per minute limitation, an integer.

        account_resend_200_trace_timestamp: A pickled list of successful
            interaction timestamps (value type is 'datetime.datetime').
        account_resend_200_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_resend_403_trace_timestamp: A pickled list of timestamps for
            interactions that have resulted in a 403 ("forbidden") error.
        account_resend_403_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_resend_0_trace_timestamp: A pickled list of timestamps for
            interactions resulting in the request being blocked.
        account_resend_0_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_resend_retry_parameters: Retry limits configuration, a dict.
        account_resend_rpm_limit: Request per minute limitation, an integer.

        account_interface_200_trace_timestamp: A pickled list of successful
            interaction timestamps (value type is 'datetime.datetime').
        account_interface_200_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_interface_403_trace_timestamp: A pickled list of timestamps for
            interactions that have resulted in a 403 ("forbidden") error.
        account_interface_403_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_interface_0_trace_timestamp: A pickled list of timestamps for
            interactions resulting in the request being blocked.
        account_interface_0_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        account_interface_retry_parameters: Retry limits configuration, a dict.
        account_interface_rpm_limit: Request per minute limitation, an integer.

        webapp_interface_200_trace_timestamp: A pickled list of successful
            interaction timestamps (value type is 'datetime.datetime').
        webapp_interface_200_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        webapp_interface_403_trace_timestamp: A pickled list of timestamps for
            interactions that have resulted in a 403 ("forbidden") error.
        webapp_interface_403_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        webapp_interface_0_trace_timestamp: A pickled list of timestamps for
            interactions resulting in the request being blocked.
        webapp_interface_0_trace_descriptions: A dumped JSON array with an
            entry for each of the timestamps. The entries contain JSON objects with
            request data (in general; the dict dumped by a Command instance).
        webapp_interface_retry_parameters: Retry limits configuration, a dict.
        webapp_interface_rpm_limit: Request per minute limitation, an integer.

        sys_createddate: First creation date for the session trace.
        sys_modifieddate: The last modified date.
    """

    client_ip = ndb.StringProperty( )
    session_id = ndb.StringProperty( )
    user_id = ndb.StringProperty( )

    admin_login_200_trace_timestamps = ndb.PickleProperty( default=[ ] )
    admin_login_200_trace_descriptions = ndb.JsonProperty( default=[ ] )

    admin_login_403_trace_timestamps = ndb.PickleProperty( default=[ ] )
    admin_login_403_trace_descriptions = ndb.JsonProperty( default=[ ] )

    admin_login_0_trace_timestamps = ndb.PickleProperty( default=[ ] )
    admin_login_0_trace_descriptions = ndb.JsonProperty( default=[ ] )

    admin_login_retry_parameters = ndb.PickleProperty( )
    admin_login_rpm_limit = ndb.IntegerProperty( )

    admin_passwd_200_trace_timestamps = ndb.PickleProperty( default=[ ] )
    admin_passwd_200_trace_descriptions = ndb.JsonProperty( default=[ ] )

    admin_passwd_403_trace_timestamps = ndb.PickleProperty( default=[ ] )
    admin_passwd_403_trace_descriptions = ndb.JsonProperty( default=[ ] )

    admin_passwd_0_trace_timestamps = ndb.PickleProperty( default=[ ] )
    admin_passwd_0_trace_descriptions = ndb.JsonProperty( default=[ ] )

    admin_passwd_retry_parameters = ndb.PickleProperty( )
    admin_passwd_rpm_limit = ndb.IntegerProperty( )

    admin_interface_200_trace_timestamps = ndb.PickleProperty( default=[ ] )
    admin_interface_200_trace_descriptions = ndb.JsonProperty( default=[ ] )

    admin_interface_403_trace_timestamps = ndb.PickleProperty( default=[ ] )
    admin_interface_403_trace_descriptions = ndb.JsonProperty( default=[ ] )

    admin_interface_0_trace_timestamps = ndb.PickleProperty( default=[ ] )
    admin_interface_0_trace_descriptions = ndb.JsonProperty( default=[ ] )

    admin_interface_retry_parameters = ndb.PickleProperty( )
    admin_interface_rpm_limit = ndb.IntegerProperty( )

    account_retrieve_200_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_retrieve_200_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_retrieve_403_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_retrieve_403_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_retrieve_0_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_retrieve_0_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_retrieve_retry_parameter = ndb.PickleProperty( )
    account_retrieve_rpm_limit = ndb.IntegerProperty( )

    account_load_200_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_load_200_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_load_403_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_load_403_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_load_0_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_load_0_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_load_retry_parameters = ndb.PickleProperty( )
    account_load_rpm_limit = ndb.IntegerProperty( )

    account_register_200_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_register_200_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_register_403_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_register_403_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_register_0_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_register_0_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_register_retry_parameters = ndb.PickleProperty( )
    account_register_rpm_limit = ndb.IntegerProperty( )

    account_resend_200_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_resend_200_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_resend_403_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_resend_403_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_resend_0_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_resend_0_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_resend_retry_parameters = ndb.PickleProperty( )
    account_resend_rpm_limit = ndb.IntegerProperty( )

    account_interface_200_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_interface_200_trace_description = ndb.JsonProperty( default=[ ] )

    account_interface_403_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_interface_403_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_interface_0_trace_timestamps = ndb.PickleProperty( default=[ ] )
    account_interface_0_trace_descriptions = ndb.JsonProperty( default=[ ] )

    account_interface_retry_parameters = ndb.PickleProperty( )
    account_interface_rpm_limit = ndb.IntegerProperty( )

    webapp_interface_200_trace_timestamps = ndb.PickleProperty( default=[ ] )
    webapp_interface_200_trace_descriptions = ndb.JsonProperty( default=[ ] )

    webapp_interface_403_trace_timestamps = ndb.PickleProperty( default=[ ] )
    webapp_interface_403_trace_descriptions = ndb.JsonProperty( default=[ ] )

    webapp_interface_0_trace_timestamps = ndb.PickleProperty( default=[ ] )
    webapp_interface_0_trace_descriptions = ndb.JsonProperty( default=[ ] )

    webapp_interface_retry_parameters = ndb.PickleProperty( )
    webapp_interface_rpm_limit = ndb.IntegerProperty( )

    sys_createddate = ndb.DateTimeProperty( )
    sys_modifieddate = ndb.DateTimeProperty( )

    def set_client_ip(self, client_ip):
        """Setter method for the 'client_ip' attribute.

        Arguments:
            client_ip: An IP address (the WebOb request attribute 'remote_addr').
        """

        if self.client_ip != client_ip:
            #if self.dev_mode:
            #  logging.info('SessionTrace.set_client_ip(%s)' % client_ip)
            self.client_ip = client_ip
            return True
        else:
            #if self.dev_mode:
            #  logging.info('SessionTrace.set_client_ip(%s): Set omitted/redundant.' % client_ip)
            return False


    def set_user_id(self, user_id):
        """Setter method for the 'user_id' attribute.

        Arguments:
            client_ip: A member key name (personal identification number), a string.
        """

        if self.user_id != user_id:
            #if self.dev_mode:
            #  logging.info('SessionTrace.set_user_id(%s)' % user_id)
            self.user_id = user_id
            return True
        else:
            #if self.dev_mode:
            #  logging.info('SessionTrace.set_user_id(%s): Set omitted/redundant.' % user_id)
            return False


    def set_retry_parameters(self, attr_name, attr_dict):
        """Setter for pickling and setting retry parameters.

        Arguments:
            attr_name: The attribute name (i.e. a 'action' or 'interface' name).
            attr_dict: The attribute's configuration dict.
        """

        setattr( self, '%s_retry_parameters' % attr_name, attr_dict )


    def get_retry_parameters(self, attr_name):
        """Getter for retrieving and unpickling retry parameters.

        Arguments:
            attr_name: The attribute name (i.e. a 'action' or 'interface' name).

        Returns:
            A configuration dict.
        """

        #logging.info('get_retry_parameters for %s' % attr_name)
        #logging.info('get_retry_parameters().dir(self): %s' % str(dir(self)))
        retry_params = getattr( self, '%s_retry_parameters' % attr_name, None )
        if retry_params is None:
            return None
        else:
            return retry_params


    def set_trace_descriptions(self, event_attr, status_code, attr_list):
        """Setter for pickling and setting retry parameters.

        Arguments:
            event_attr: The attribute name (i.e. a 'action' or 'interface' name).
            status_code: The HTTP response status code.
            attr_list: A list of dicts storing event descriptions to be saved to
                the Datastore.
        """

        setattr( self, '%s_%s_trace_descriptions' % (event_attr, str( status_code )), attr_list )


    def get_trace_descriptions(self, event_attr, status_code):
        """Getter for retrieving and unpickling the event history descriptions.

        Arguments:
            event_attr: The attribute name (i.e. a 'action' or 'interface' name).
            status_code: The HTTP response status code.

        Returns:
            A list of dicts, each describing an event.
        """

        trace_descriptions = getattr( self, '%s_%s_trace_descriptions' % (event_attr, str( status_code )) )
        if trace_descriptions is None:
            return None
        else:
            return trace_descriptions


    def set_trace_timestamps(self, event_attr, status_code, attr_list):
        """Setter for pickling and setting retry parameters.

        Arguments:
            event_attr: The attribute name (i.e. a 'action' or 'interface' name).
            status_code: The HTTP response status code.
            attr_list: A list of datetime.datetime instances to be saved to the
                Datastore.
        """

        setattr( self, '%s_%s_trace_timestamps' % (event_attr, str( status_code )), attr_list )


    def get_trace_timestamps(self, event_attr, status_code):
        """Getter for retrieving and unpickling the event history timestamps.

        Arguments:
            event_attr: The attribute name (i.e. a 'action' or 'interface' name).
            status_code: The HTTP response status code.

        Returns:
            A list of datetime.datetime timestamps.
        """

        #logging.info('%s_%s_trace_timestamps' % (event_attr, str(status_code)))
        trace_descriptions = getattr( self, '%s_%s_trace_timestamps' % (event_attr, str( status_code )), None )
        if trace_descriptions is None:
            return None
        else:
            return trace_descriptions


    def get_rpm_limit(self, event_attr):
        """Getter for retrieving and unpickling the event history timestamps.

        Arguments:
            event_attr: The attribute name (i.e. a 'action' or 'interface' name).

        Returns:
            An integer.
        """

        rpm_limit = getattr( self, '%s_rpm_limit' % event_attr )
        if rpm_limit is None:
            return None
        else:
            return rpm_limit


    def apply_default_config(self):
        """Setter method for applying the default retry parameters and limits."""

        self.set_retry_parameters( 'admin_login', {
            'min_backoff_seconds': 2,
            'max_backoff_seconds': 1800,
            'current_timeout_power': 1,
            'current_timeout_ends': None
        } )
        self.admin_login_rpm_limit = 6

        self.set_retry_parameters( 'admin_passwd', {
            'min_backoff_seconds': 2,
            'max_backoff_seconds': 1800,
            'current_timeout_power': 1,
            'current_timeout_ends': None
        } )
        self.admin_passwd_rpm_limit = 6

        self.set_retry_parameters( 'admin_interface', {
            'min_backoff_seconds': 2,
            'max_backoff_seconds': 60,
            'current_timeout_power': 1,
            'current_timeout_ends': None
        } )
        self.admin_interface_rpm_limit = 120

        self.set_retry_parameters( 'account_retrieve', {
            'min_backoff_seconds': 2,
            'max_backoff_seconds': 1800,
            'current_timeout_power': 1,
            'current_timeout_ends': None
        } )
        self.account_retrieve_rpm_limit = 1

        self.set_retry_parameters( 'account_load', {
            'min_backoff_seconds': 5,
            'max_backoff_seconds': 1800,
            'current_timeout_power': 1,
            'current_timeout_ends': None
        } )
        self.account_load_rpm_limit = 6

        self.set_retry_parameters( 'account_register', {
            'min_backoff_seconds': 10,
            'max_backoff_seconds': 1800,
            'current_timeout_power': 1,
            'current_timeout_ends': None
        } )
        self.account_register_rpm_limit = 1

        self.set_retry_parameters( 'account_resend', {
            'min_backoff_seconds': 10,
            'max_backoff_seconds': 1800,
            'current_timeout_power': 1,
            'current_timeout_ends': None
        } )
        self.account_resend_rpm_limit = 2

        self.set_retry_parameters( 'account_interface', {
            'min_backoff_seconds': 2,
            'max_backoff_seconds': 60,
            'current_timeout_power': 1,
            'current_timeout_ends': None
        } )
        self.account_interface_rpm_limit = 15

        self.set_retry_parameters( 'webapp_interface', {
            'min_backoff_seconds': 2,
            'max_backoff_seconds': 60,
            'current_timeout_power': 1,
            'current_timeout_ends': None
        } )
        self.webapp_interface_rpm_limit = 15


    def dumps(self):
        """Method for dumping the class attributes.

        Returns:
            A JSON object with the instance's configured attributes.
        """

        sys_createddate = self.sys_createddate.isoformat( ).split( '.' )[ 0 ]
        sys_modifieddate = self.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
        client_ip = self.client_ip
        session_id = self.session_id
        user_id = self.user_id

        response = {
            'client_ip': client_ip,
            'session_id': session_id,
            'user_id': user_id,
            'admin_login_200_trace': [ ],
            'admin_login_403_trace': [ ],
            'admin_login_0_trace': [ ],
            'admin_passwd_200_trace': [ ],
            'admin_passwd_403_trace': [ ],
            'admin_passwd_0_trace': [ ],
            'admin_interface_200_trace': [ ],
            'admin_interface_403_trace': [ ],
            'admin_interface_0_trace': [ ],
            'account_retrieve_200_trace': [ ],
            'account_retrieve_403_trace': [ ],
            'account_retrieve_0_trace': [ ],
            'account_load_200_trace': [ ],
            'account_load_403_trace': [ ],
            'account_load_0_trace': [ ],
            'account_register_200_trace': [ ],
            'account_register_403_trace': [ ],
            'account_register_0_trace': [ ],
            'account_resend_200_trace': [ ],
            'account_resend_403_trace': [ ],
            'account_resend_0_trace': [ ],
            'account_interface_200_trace': [ ],
            'account_interface_403_trace': [ ],
            'account_interface_0_trace': [ ],
            'webapp_interface_200_trace': [ ],
            'webapp_interface_403_trace': [ ],
            'webapp_interface_0_trace': [ ],
            'sys_createddate': sys_createddate,
            'sys_modifieddate': sys_modifieddate,
            }

        for i in range( len( self.admin_login_200_trace_descriptions ) ):
            event = {
                'timestamp': self.admin_login_200_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'admin_login_rpm_limit': self.admin_login_rpm_limit,
                'admin_login_retry_parameters': self.admin_login_retry_parameters,
                'description': self.admin_login_200_trace_descriptions[ i ]
            }
            response[ 'admin_login_200_trace' ].append( event )

        for i in range( len( self.admin_login_403_trace_descriptions ) ):
            event = {
                'timestamp': self.admin_login_403_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'admin_login_rpm_limit': self.admin_login_rpm_limit,
                'admin_login_retry_parameters': self.admin_login_retry_parameters,
                'description': self.admin_login_403_trace_descriptions[ i ]
            }
            response[ 'admin_login_403_trace' ].append( event )

        for i in range( len( self.admin_login_0_trace_descriptions ) ):
            event = {
                'timestamp': self.admin_login_0_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'admin_login_rpm_limit': self.admin_login_rpm_limit,
                'admin_login_retry_parameters': self.admin_login_retry_parameters,
                'description': self.admin_login_0_trace_descriptions[ i ]
            }
            response[ 'admin_login_0_trace' ].append( event )

        for i in range( len( self.admin_login_200_trace_descriptions ) ):
            event = {
                'timestamp': self.admin_passwd_200_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'admin_passwd_rpm_limit': self.admin_passwd_rpm_limit,
                'admin_passwd_retry_parameters': self.admin_passwd_retry_parameters,
                'description': self.admin_passwd_200_trace_descriptions[ i ]
            }
            response[ 'admin_passwd_200_trace' ].append( event )

        for i in range( len( self.admin_passwd_403_trace_descriptions ) ):
            event = {
                'timestamp': self.admin_passwd_403_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'admin_passwd_rpm_limit': self.admin_passwd_rpm_limit,
                'admin_passwd_retry_parameters': self.admin_passwd_retry_parameters,
                'description': self.admin_passwd_403_trace_descriptions[ i ]
            }
            response[ 'admin_passwd_403_trace' ].append( event )

        for i in range( len( self.admin_passwd_0_trace_descriptions ) ):
            event = {
                'timestamp': self.admin_passwd_0_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'admin_passwd_rpm_limit': self.admin_passwd_rpm_limit,
                'admin_passwd_retry_parameters': self.admin_passwd_retry_parameters,
                'description': self.admin_passwd_0_trace_descriptions[ i ]
            }
            response[ 'admin_passwd_0_trace' ].append( event )

        for i in range( len( self.admin_interface_200_trace_descriptions ) ):
            event = {
                'timestamp': self.admin_interface_200_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'admin_interface_rpm_limit': self.admin_interface_rpm_limit,
                'admin_interface_retry_parameters': self.admin_interface_retry_parameters,
                'description': self.admin_interface_200_trace_descriptions[ i ]
            }
            response[ 'admin_interface_200_trace' ].append( event )

        for i in range( len( self.admin_interface_0_trace_descriptions ) ):
            event = {
                'timestamp': self.admin_interface_0_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'admin_interface_rpm_limit': self.admin_interface_rpm_limit,
                'admin_interface_retry_parameters': self.admin_interface_retry_parameters,
                'description': self.admin_interface_0_trace_descriptions[ i ]
            }
            response[ 'admin_interface_400_trace' ].append( event )

        for i in range( len( self.admin_interface_403_trace_descriptions ) ):
            event = {
                'timestamp': self.admin_interface_403_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'admin_interface_rpm_limit': self.admin_interface_rpm_limit,
                'admin_interface_retry_parameters': self.admin_interface_retry_parameters,
                'description': self.admin_interface_403_trace_descriptions[ i ]
            }
            response[ 'admin_interface_403_trace' ].append( event )

        for i in range( len( self.account_retrieve_200_trace_descriptions ) ):
            event = {
                'timestamp': self.account_retrieve_200_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_retrieve_rpm_limit': self.account_retrieve_rpm_limit,
                'account_retrieve_retry_parameters': self.account_retrieve_retry_parameters,
                'description': self.account_retrieve_200_trace_descriptions[ i ]
            }
            response[ 'account_retrieve_200_trace' ].append( event )

        for i in range( len( self.account_retrieve_403_trace_descriptions ) ):
            event = {
                'timestamp': self.account_retrieve_403_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_retrieve_rpm_limit': self.account_retrieve_rpm_limit,
                'account_retrieve_retry_parameters': self.account_retrieve_retry_parameters,
                'description': self.account_retrieve_403_trace_descriptions[ i ]
            }
            response[ 'account_retrieve_403_trace' ].append( event )

        for i in range( len( self.account_retrieve_0_trace_descriptions ) ):
            event = {
                'timestamp': self.account_retrieve_0_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_retrieve_rpm_limit': self.account_retrieve_rpm_limit,
                'account_retrieve_retry_parameters': self.account_retrieve_retry_parameters,
                'description': self.account_retrieve_0_trace_descriptions[ i ]
            }
            response[ 'account_retrieve_0_trace' ].append( event )

        for i in range( len( self.account_load_200_trace_descriptions ) ):
            event = {
                'timestamp': self.account_load_200_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_load_rpm_limit': self.account_load_rpm_limit,
                'account_load_retry_parameters': self.account_load_retry_parameters,
                'description': self.account_load_200_trace_descriptions[ i ]
            }
            response[ 'account_load_200_trace' ].append( event )

        for i in range( len( self.account_load_403_trace_descriptions ) ):
            event = {
                'timestamp': self.account_load_403_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_load_rpm_limit': self.account_load_rpm_limit,
                'account_load_retry_parameters': self.account_load_retry_parameters,
                'description': self.account_load_403_trace_descriptions[ i ]
            }
            response[ 'account_load_403_trace' ].append( event )

        for i in range( len( self.account_load_0_trace_descriptions ) ):
            event = {
                'timestamp': self.account_load_0_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_load_rpm_limit': self.account_load_rpm_limit,
                'account_load_retry_parameters': self.account_load_retry_parameters,
                'description': self.account_load_0_trace_descriptions[ i ]
            }
            response[ 'account_load_0_trace' ].append( event )

        for i in range( len( self.account_register_200_trace_descriptions ) ):
            event = {
                'timestamp': self.account_register_200_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_register_rpm_limit': self.account_register_rpm_limit,
                'account_register_retry_parameters': self.account_register_retry_parameters,
                'description': self.account_register_200_trace_descriptions[ i ]
            }
            response[ 'account_register_200_trace' ].append( event )

        for i in range( len( self.account_register_403_trace_descriptions ) ):
            event = {
                'timestamp': self.account_register_403_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_register_rpm_limit': self.account_register_rpm_limit,
                'account_register_retry_parameters': self.account_register_retry_parameters,
                'description': self.account_register_403_trace_descriptions[ i ]
            }
            response[ 'account_register_403_trace' ].append( event )

        for i in range( len( self.account_register_0_trace_descriptions ) ):
            event = {
                'timestamp': self.account_register_0_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_register_rpm_limit': self.account_register_rpm_limit,
                'account_register_retry_parameters': self.account_register_retry_parameters,
                'description': self.account_register_0_trace_descriptions[ i ]
            }
            response[ 'account_register_0_trace' ].append( event )

        for i in range( len( self.account_resend_200_trace_descriptions ) ):
            event = {
                'timestamp': self.account_resend_200_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_resend_rpm_limit': self.account_resend_rpm_limit,
                'account_resend_retry_parameters': self.account_resend_retry_parameters,
                'description': self.account_resend_200_trace_descriptions[ i ]
            }
            response[ 'account_resend_200_trace' ].append( event )

        for i in range( len( self.account_resend_403_trace_descriptions ) ):
            event = {
                'timestamp': self.account_resend_403_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_resend_rpm_limit': self.account_resend_rpm_limit,
                'account_resend_retry_parameters': self.account_resend_retry_parameters,
                'description': self.account_resend_403_trace_descriptions[ i ]
            }
            response[ 'account_resend_403_trace' ].append( event )

        for i in range( len( self.account_resend_0_trace_descriptions ) ):
            event = {
                'timestamp': self.account_resend_0_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_resend_rpm_limit': self.account_resend_rpm_limit,
                'account_resend_retry_parameters': self.account_resend_retry_parameters,
                'description': self.account_resend_0_trace_descriptions[ i ]
            }
            response[ 'account_resend_0_trace' ].append( event )

        for i in range( len( self.account_interface_200_trace_descriptions ) ):
            event = {
                'timestamp': self.account_interface_200_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_interface_rpm_limit': self.account_interface_rpm_limit,
                'account_interface_retry_parameters': self.account_interface_retry_parameters,
                'description': self.account_interface_200_trace_descriptions[ i ]
            }
            response[ 'account_interface_200_trace' ].append( event )

        for i in range( len( self.account_interface_0_trace_descriptions ) ):
            event = {
                'timestamp': self.account_interface_0_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_interface_rpm_limit': self.account_interface_rpm_limit,
                'account_interface_retry_parameters': self.account_interface_retry_parameters,
                'description': self.account_interface_0_trace_descriptions[ i ]
            }
            response[ 'account_interface_400_trace' ].append( event )

        for i in range( len( self.account_interface_403_trace_descriptions ) ):
            event = {
                'timestamp': self.account_interface_403_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'account_interface_rpm_limit': self.account_interface_rpm_limit,
                'account_interface_retry_parameters': self.account_interface_retry_parameters,
                'description': self.account_interface_403_trace_descriptions[ i ]
            }
            response[ 'account_interface_403_trace' ].append( event )

        for i in range( len( self.webapp_interface_200_trace_descriptions ) ):
            event = {
                'timestamp': self.webapp_interface_200_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'webapp_interface_rpm_limit': self.webapp_interface_rpm_limit,
                'webapp_interface_retry_parameters': self.webapp_interface_retry_parameters,
                'description': self.webapp_interface_200_trace_descriptions[ i ]
            }
            response[ 'webapp_interface_200_trace' ].append( event )

        for i in range( len( self.webapp_interface_0_trace_descriptions ) ):
            event = {
                'timestamp': self.webapp_interface_0_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'webapp_interface_rpm_limit': self.webapp_interface_rpm_limit,
                'webapp_interface_retry_parameters': self.webapp_interface_retry_parameters,
                'description': self.webapp_interface_0_trace_descriptions[ i ]
            }
            response[ 'webapp_interface_400_trace' ].append( event )

        for i in range( len( self.webapp_interface_403_trace_descriptions ) ):
            event = {
                'timestamp': self.webapp_interface_403_trace_timestamps[ i ].isoformat( ).split( '.' )[ 0 ],
                'webapp_interface_rpm_limit': self.webapp_interface_rpm_limit,
                'webapp_interface_retry_parameters': self.webapp_interface_retry_parameters,
                'description': self.webapp_interface_403_trace_descriptions[ i ]
            }
            response[ 'webapp_interface_403_trace' ].append( event )

        return response


    def save(self):
        """Method for saving the session trace."""

        self.sys_modifieddate = datetime.datetime.today( )
        self.put( )
        return True


class SessionTrace( object ):
    """A wrapper around the client sessions.

    The SessionTrace wrapper provides an interface for reading and updating the
        SessionTraceModel instances designed to keep track of each user session in
        order to limit the effects of DDoS attacks, brute force hacking attempts
        and malicious system usage (e.g. sending dummy requests in order to drive
        resource usage costs through the roof).

    If the session resource usage limits is not honored, the session will be
        blocked for the number of seconds specified by the targets retry paramters'
        'min_backoff_seconds' attribute. For each attempt to submit requests within
        the blocking interval, the blocking will be exponentially prolonged untill
        the retry parameters' 'max_backoff_seconds' attribute is reached.

    Attributes:
        client_ip: The client's IP address (i.e. the WebOb request's 'remote_addr'
            attribute).
        session_id: The request's session ID; a cookie set at first visit. The
            cookie never expires and is used for keeping clients organized to their
            respective session logs.
        user_id: The decoded user ID an admin will receive on login.

        dev_mode: A boolean flag marking development mode. If True, logging will be
            suppressed.
    """


    def __init__(self, client_ip=None, session_id=None, user_id=None, dev_mode=True):
        """Initialization method for the session trace wrapper.

        Arguments:
            client_ip: The client's IP address (i.e. the WebOb request's 'remote_addr'
                attribute).
            session_id: The request's session ID; a cookie set at first visit. The
                cookie never expires and is used for keeping clients organized to their
                respective session logs.
            user_id: The decoded user ID an admin will receive on login.

            dev_mode: A boolean flag marking development mode. If True, logging will be
                suppressed.
        """

        self.dev_mode = dev_mode
        self.session_id = session_id

        if session_id is None:
            #raise ValueError
            return

        session_key = ndb.Key( 'SessionTraceModel', session_id )
        session_trace = session_key.get( )
        if session_trace:
            if dev_mode:
                logging.info(
                    'SessionTrace.__init__(client_ip=\'%s\', session_id=\'%s\', '
                    'user_id=\'%s\'): Instance successfully retrieved from Datastore.' % (
                        str( client_ip ), str( session_id ), str( user_id )) )
                if session_trace.set_client_ip( client_ip ) or session_trace.set_user_id( user_id ):
                    if dev_mode:
                        logging.info(
                            'SessionTrace.__init__(client_ip=\'%s\', session_id=\'%s\', '
                            'user_id=\'%s\'): \'client_ip\' or \'user_id\' properties caused an update.' % (
                                str( client_ip ), str( session_id ), str( user_id )) )
                    session_trace.save( )
                elif dev_mode:
                    logging.info(
                        'SessionTrace.__init__(client_ip=\'%s\', session_id=\'%s\', '
                        'user_id=\'%s\'): \'client_ip\' and \'user_id\' property update omitted.' % (
                            str( client_ip ), str( session_id ), str( user_id )) )
        else:
            if dev_mode:
                logging.info(
                    'SessionTrace.__init__(client_ip=\'%s\', session_id=\'%s\', user_id=\'%s\'): New instance created'
                    '.' % (
                        str( client_ip ), str( session_id ), str( user_id )) )

            now = datetime.datetime.today( )
            session_trace = SessionTraceModel(
                key=session_key,
                session_id=session_id,
                client_ip=client_ip,
                user_id=user_id,
                sys_createddate=now,
                sys_modifieddate=now
            )
            session_trace.apply_default_config( )
            session_trace.save( )


    def verify_session(self, action, interface):
        """Method for verifying that a) the user session cookie is present and b)
            that the session is not currently blocked due to resource usage
            restrictions.

        Arguments:
            action: The request handler's 'action' attribute.
            interface: A fallback or general interface class descriptor for catching
                requests sent to handlers without specific resource usage limits.
        """

        session_id = self.session_id
        dev_mode = self.dev_mode

        if session_id is None:
            if dev_mode:
                logging.info(
                    'SessionTrace.verify_session(action=\'%s\', interface=\'%s\'): \'session_id\' attribute is '
                    'undefined.' % (
                        str( action ), interface) )
            return {
                'clear_to_receive': False,
                'errors': {'invalidAttributes': 'Session is blocked because the \'session_id\' attribute is undefined.'}
            }

        session_trace = ndb.Key( 'SessionTraceModel', session_id ).get( )
        if session_trace is None:
            if dev_mode:
                logging.info(
                    'SessionTrace.verify_session(action=\'%s\', interface=\'%s\'): \'session_id\' attribute is '
                    'invalid.' % (
                        str( action ), interface) )
            return {
                'clear_to_receive': False,
                'errors': {'invalidAttributes': 'Session is blocked because the \'session_id\' attribute is invalid.'}
            }

        event_attr = None
        retry_params = None
        if action is not None:
            retry_params = session_trace.get_retry_parameters( action )
        if retry_params is None:
            if dev_mode:
                logging.info(
                    'SessionTrace.verify_session(action=\'%s\', interface=\'%s\'): Invalid action descriptor.' % (
                        str( action ), interface) )
            retry_params = session_trace.get_retry_parameters( interface )
            if retry_params is None:
                if dev_mode:
                    logging.info(
                        'SessionTrace.verify_session(action=\'%s\', interface=\'%s\'): Invalid interface descriptor.'
                        % (
                            str( action ), interface) )
                return False
            else:
                event_attr = interface
        else:
            event_attr = action

        now = datetime.datetime.today( )
        if retry_params[ 'current_timeout_ends' ] is not None:
            if retry_params[ 'current_timeout_ends' ] > now:
                if dev_mode:
                    logging.info(
                        'SessionTrace.verify_session(action=\'%s\', interface=\'%s\'): Invalid interface descriptor.'
                        % (
                            str( action ), interface) )
                return {
                    'clear_to_receive': False,
                    'errors': {'sessionBlocked': 'Session is blocked from triggering \'%s\' until %s' % (
                        event_attr, retry_params[ 'current_timeout_ends' ].isoformat( ).split( '.' )[ 0 ])}
                }
            else:
                retry_params[ 'current_timeout_ends' ] = None
                retry_params[ 'current_timeout_power' ] = 1
                session_trace.set_retry_parameters( event_attr, retry_params )
                session_trace.save( )

        return {
            'clear_to_receive': True
        }


    def verify_as_admin(self):
        """Method for verifying admin privileges for the session's 'user_id' attribute."""

        session_id = self.session_id
        dev_mode = self.dev_mode
        session_trace = ndb.Key( 'SessionTraceModel', session_id ).get( )

        if session_trace is None:
            if dev_mode:
                logging.info(
                    'SessionTrace.verify_as_admin(): \'session_id\' attribute is invalid (\'%s\').' % str(
                        session_id ) )
            return False
        elif session_trace.user_id is None:
            if dev_mode:
                logging.info(
                    'SessionTrace.verify_as_admin(): \'user_id\' attribute is undefined, admin verification failed.' )
            return False
        else:
            admins = rkhsk.model.sys_config.SysConfig.get_sys_admins( )
            if session_trace.user_id in admins:
                if dev_mode:
                    logging.info(
                        'SessionTrace.verify_as_admin(): \'user_id\' successfully verified as admin (\'%s\').' %
                        session_trace.user_id )
                return True
            else:
                if dev_mode:
                    logging.info(
                        'SessionTrace.verify_as_admin(): \'user_id\' attribute not matched to admin pool (\'%s\').' %
                        session_trace.user_id )
                return False


    # TODO(mats.blomdahl@gmail.com): Implement smarter rpm limit calc!
    def update_session_trace(self, action, interface, status_code, request_data):
        """Method for updating the session trace and applying per-interface
            resource usage limits.

        Arguments:
            action: The request handler's 'action' attribute.
            interface: A fallback or general interface class descriptor for catching
                requests sent to handlers without specific resource usage limits.
            status_code: The HTTP response status code, an integer.
            request_data: The parameters passed to the request handler, a dict.
        """

        session_id = self.session_id
        dev_mode = self.dev_mode
        if session_id is None:
            if dev_mode:
                logging.info(
                    'SessionTrace.update_session_trace(action=\'%s\', interface=\'%s\', status_code=%s, '
                    'request_data=\'%s\'): \'session_id\' attribute is undefined.' % (
                        str( action ), interface, str( status_code ), str( request_data )) )
                #raise ValueError
            return

        session_trace = ndb.Key( 'SessionTraceModel', session_id ).get( )
        if session_trace is None:
            if dev_mode:
                logging.info(
                    'SessionTrace.update_session_trace(action=\'%s\', interface=\'%s\', status_code=%s, request_data=\'%s\'): \'session_id\' attribute is invalid (\'%s\').' % (
                        str( action ), interface, str( status_code ), str( request_data ), session_id) )
            raise ValueError

        if status_code == 400:
            status_code = 0

        event_attr = None
        if action is not None:
            event_attr = session_trace.get_trace_timestamps( action, status_code )
        if event_attr is None:
            if dev_mode:
                logging.info(
                    'SessionTrace.update_session_trace(action=\'%s\', interface=\'%s\', status_code=%s, request_data=\'%s\'): Invalid action descriptor.' % (
                        str( action ), interface, str( status_code ), str( request_data )) )
            event_attr = session_trace.get_trace_timestamps( interface, status_code )
            if event_attr is None:
                if dev_mode:
                    logging.info(
                        'SessionTrace.update_session_trace(action=\'%s\', interface=\'%s\', status_code=%s, request_data=\'%s\'): Invalid interface descriptor.' % (
                            str( action ), interface, str( status_code ), str( request_data )) )
                return False
            else:
                event_attr = interface
        else:
            event_attr = action

        now = datetime.datetime.today( )

        def apply_retries_throttle(retry_params, now):
            if dev_mode:
                logging.info(
                    'SessionTrace.update_session_trace(action=\'%s\', interface=\'%s\', status_code=%s, request_data=\'%s\').apply_retries_throttle(retry_params=%s, now=%s)' % (
                        str( action ), interface, str( status_code ), str( request_data ), str( retry_params ),
                        now.isoformat( ).split( '.' )[ 0 ]) )
            backoff_seconds = retry_params[ 'min_backoff_seconds' ] ** retry_params[ 'current_timeout_power' ]
            if backoff_seconds > retry_params[ 'max_backoff_seconds' ]:
                backoff_seconds = retry_params[ 'max_backoff_seconds' ]
            retry_params[ 'current_timeout_ends' ] = now + datetime.timedelta( seconds=backoff_seconds )
            retry_params[ 'current_timeout_power' ] += 1

            return retry_params


        timestamps = session_trace.get_trace_timestamps( event_attr, status_code )
        descriptions = getattr( session_trace, '%s_%s_trace_descriptions' % (event_attr, str( status_code )) )
        if dev_mode:
            logging.info( 'descriptions: %s' % str( descriptions ) )
            logging.info( 'timestamps: %s' % str( timestamps ) )

        if status_code == 200: # validate against rpm limit
            if any( timestamps ):
                rpm_limit = session_trace.get_rpm_limit( event_attr )
                rpm = 60.0 / (now - timestamps[ -1 ]).total_seconds( )
                if rpm > rpm_limit: # apply <event_attr>_retry_parameters
                    retry_params = apply_retries_throttle( session_trace.get_retry_parameters( event_attr ), now )
                    session_trace.set_retry_parameters( event_attr, retry_params )

        elif status_code == 403 or status_code == 0: # apply <event_attr>_retry_parameters
            retry_params = apply_retries_throttle( getattr( session_trace, '%s_retry_parameters' % event_attr ), now )
            session_trace.set_retry_parameters( event_attr, retry_params )

        timestamps.append( now )
        descriptions.append( request_data )
        if dev_mode:
            logging.info( 'descriptions: %s' % str( descriptions ) )
            logging.info( 'timestamps: %s' % str( timestamps ) )

        session_trace.set_trace_timestamps( event_attr, status_code, timestamps )
        session_trace.set_trace_descriptions( event_attr, status_code, descriptions )

        session_trace.save( )


    # TODO(mats.blomdahl@gmail.com): Implement random! Document!
    @classmethod
    def generate_session_id(cls):
        """Foo

        """

        return 'foobarzss2z' + str( random.randrange( 1000 ) ) + 'bar'


