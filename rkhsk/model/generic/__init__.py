# -*- coding: utf-8 -*-
#
# Title:   Package 'rkhsk.model.generic'
# Author:  Mats Blomdahl
# Version: 2012-08-21
#

"""rkhsk.model.generic module

Support for session management and wrapping inbound requests. More documentation ...
"""

# TODO(mats.blomdahl@gmail.com): Fix documentation for rkhsk.model.generic package!
from .session_trace import SessionTrace
from .temp_file import TempFile
