# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.generic.TempFile
# Author:  Mats Blomdahl
# Version: 2012-08-27

from google.appengine.ext import db
from google.appengine.ext import blobstore

# TODO(mats.blomdahl@gmail.com): Update model.generic.TempFile naming conventions!
# TODO(mats.blomdahl@gmail.com): Transfer model.generic.TempFile kind to the NDB database!
class TempFile( db.Model ):
    """The standard representation of a temporary blob upload.

    Attributes:
        fileKey: A reference to the Blobstore entry containing the uploaded file.
        fileName: The file name of the upload, a string.
        fileContentType: The MIME type of the uploaded file, a string.
        fileType: The file type of the upload, a string.
        fileSize: An optional integer storing the file size (in bytes).
        fileCreation: Stores a datetime.datetime timestamp from the creation date.
        fileCreatedBy: An identifier string for the user submitting the upload.
    """

    fileKey = blobstore.BlobReferenceProperty( required=True )
    fileName = db.StringProperty( required=True )
    fileContentType = db.StringProperty( required=True )
    fileType = db.StringProperty( required=True )
    fileSize = db.IntegerProperty( )
    fileCreation = db.DateTimeProperty( )
    fileCreatedBy = db.StringProperty( )


