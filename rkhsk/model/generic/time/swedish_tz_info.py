# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.generic.time.SwedishTzInfo
# Author:  Mats Blomdahl
# Version: 2012-08-27

import datetime

class SwedishTzInfo( datetime.tzinfo ):
    """A time zone info subclass for working with Swedish time.

    Attributes:
        dst_start: The start date for Daylight Saving Time [DST] adjustment.
        dst_end: The end date for Daylight Saving Time [DST] adjustment.
        dst_dst_interval_uninitialized: A boolean flag marking initialization status for the 'dst_start' and 'dst_end'
            attributes.
    """

    def __init__(self):
        """Init method for setting the default value of attribute 'dst_interval_uninitialized' to True.

        Returns:
            None
        """

        self.dst_interval_uninitialized = True


    def utcoffset(self, test_dt):
        """Getter for the time zone's calculated offset from UTC time at the specified date.

        Arguments:
            test_dt: A datetime.datetime instance used to evaluate the time zone's current offset.

        Returns:
            A datetime.timedelta instance configured with the calculated DST offset.
        """

        return datetime.timedelta( hours=1 ) + self.dst( test_dt )


    def _first_sunday(self, test_dt):
        """Getter for retrieving the date of the first sunday after the specified date.

        Arguments:
            test_dt: A datetime.datetime instance used to evaluate the date of the next sunday.

        Returns:
            A datetime.datetime instance with the date of the first sunday after test_dt.
        """

        return test_dt + datetime.timedelta( days=(6 - test_dt.weekday( )) )


    def dst(self, test_dt):
        """Getter for the Daylight Saving Time [DST] offset at the specified date.

        Arguments:
            test_dt: A datetime.datetime instance used to evaluate the time zone's current offset.

        Returns:
            A datetime.timedelta instance configured with the calculated DST offset.
        """

        if self.dst_interval_uninitialized:
            self.dst_start = self._first_sunday(
                datetime( test_dt.year, 3, 25, 1 ) ) # 01:00 (UTC), the last sunday in mars.
            self.dst_end = self._first_sunday(
                datetime( test_dt.year, 10, 25, 1 ) ) # 01:00 (UTC), the last sunday in october.
            self.dst_interval_uninitialized = False
            logging.info( "SwedishTzInfo.dst(test_dt=\'%s\').dst_start: %s" % (
                test_dt.isoformat( ).split( '.' )[ 0 ], self.dst_start.isoformat( ).split( '.' )[ 0 ]) )
            logging.info( "SwedishTzInfo.dst(test_dt=\'%s\').dst_end: %s" % (
                test_dt.isoformat( ).split( '.' )[ 0 ], self.dst_end.isoformat( ).split( '.' )[ 0 ]) )

        if self.dst_start <= test_dt.replace( tzinfo=None ) < self.dst_end:
            logging.info( "SwedishTzInfo.dst(test_dt=\'%s\').timedelta: 1 h" % test_dt.isoformat( ).split( '.' )[ 0 ] )
            return datetime.timedelta( hours=1 )
        else:
            logging.info( "SwedishTzInfo.dst(test_dt=\'%s\').timedelta: 0 h" % test_dt.isoformat( ).split( '.' )[ 0 ] )
            return datetime.timedelta( hours=0 )


    def tzname(self, test_dt):
        """Method for returning the name of the time zone.

        Arguments:
            test_dt: A datetime.datetime instance for the target time zone.

        Returns:
            A string name of the time zone.
        """

        if self.dst( test_dt ) == datetime.timedelta( hours=0 ):
            return "STD-SWE"
        else:
            return "DST-SWE"


