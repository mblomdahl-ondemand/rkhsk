# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.generic.time.UtcTzInfo
# Author:  Mats Blomdahl
# Version: 2012-08-27

import datetime

class UtcTzInfo( datetime.tzinfo ):
    """A time zone info subclass for working with UTC time."""

    def utcoffset(self, test_dt):
        """Getter for the time zone's calculated offset from UTC time at the
            specified date.

        Arguments:
            test_dt: A datetime instance used to evaluate the time zone's current
                offset.

        Returns:
            A datetime.timedelta instance configured with the calculated DST offset.
        """

        return datetime.timedelta( hours=0 )


    def _first_sunday(self, test_dt):
        """Getter for retrieving the date of the first sunday after the specified
            date.

        Arguments:
            test_dt: A datetime instance used to evaluate the date of the next
                sunday.

        Returns:
            A datetime instance with the date of the first sunday after test_dt.
        """

        return test_dt + datetime.timedelta( days=(6 - test_dt.weekday( )) )


    def dst(self, test_dt):
        """Getter for the Daylight Saving Time [DST] offset at the specified date.

        Arguments:
            test_dt: A datetime instance used to evaluate the time zone's current
                offset.

        Returns:
            A datetime.timedelta instance configured with the calculated DST offset.
        """
        return datetime.timedelta( hours=0 )


    def tzname(self, test_dt):
        return "STD-UTC"


