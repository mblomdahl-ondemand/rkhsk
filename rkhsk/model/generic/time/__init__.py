# -*- coding: utf-8 -*-
#
# Title:   Package 'rkhsk.model.generic.time'
# Author:  Mats Blomdahl
# Version: 2012-08-21
#

"""rkhsk.model.generic.time module

Support for session management and wrapping inbound requests. More documentation ...
"""

# TODO(mats.blomdahl@gmail.com): Fix documentation for rkhsk.model.generic.time package!
from .swedish_tz_info import SwedishTzInfo
from .utc_tz_info import UtcTzInfo
