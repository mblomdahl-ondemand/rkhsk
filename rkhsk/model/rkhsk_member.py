# -*- coding: utf-8 -*-
#
# Title:   rkhsk.model.RkhskMember
# Author:  Mats Blomdahl
# Version: 2012-08-22

import re, json, logging, random, pickle, string, datetime

from google.appengine.ext import db
from google.appengine.api import taskqueue, memcache # TODO(evelina.bisell@gmail.com): Ta och fixa mina klasser, vet inte vad jag tänkte på där...

# TODO(mats.blomdahl@gmail.com): Rewrite implementation. Document.
#import . as member
#import rkhsk.model.member as member
#from . import property as model.property

from .account_email import AccountEmail
from .account_uri import AccountUri
from .payment_key import PaymentKey
import sys_config
from .mecenat_push_task import MecenatPushTask
from .ssco_pull_task import SscoPullTask
from .sys_tmpkey import SysTmpKey
from ._member_property import _CourseProperty, _PersonalIdProperty, _SemesterProperty, _NameProperty, _OptNameProperty, _PhoneProperty, _ZipCodeProperty, _StreetApartmentProperty, _StreetEntranceProperty, _StreetFloorProperty, _StreetNumberProperty
from rkhsk.model.generic import SessionTrace, TempFile
from rkhsk.model.generic.email import InboundEmail, SentEmail


class RkhskMember( db.Expando ):
    """The standard representation of a student union member (or admin).

    Attributes:
        lastApplicationFor:
        lastPaymentKey: Senaste "personliga koden" för någon som har anmält sig till studentkåren.
        lastApplicationDate:
        lastPaymentRecieved:
        lastMecenatReg:
        lastSscoReg:
        lastMembershipFor:

        applicationSemesterHistory: A list of historic memberships (e.g. ['HT10', 'VT11']).
        applicationDateHistory:

        paymentKeyHistory:
        paymentRecievedHistory:

        membershipHistory:
        mecenatHistory:
        sscoHistory:

        accountPageUri:

        # Member information
        admittedSemester:
        personalId:
        firstName: The member's first name (a string).
        lastName: The member's last name (also a string).
        streetCo:
        streetAddress:
        streetNumber:
        streetEntrance:
        streetApartment:
        streetFloor:
        zipCode:
        city:
        country:
        email:
        phone:
        studentConfirmation:

        # System information
        sys_email_verified:
        sys_trans_email:
        sys_tmpkey:
        sys_administrator:
        sys_su:
        sys_accounttype:
        sys_password:
        sys_deleted:
        sys_changelog:
        sys_activitylog:
        sys_createddate:
        sys_modifieddate:
        sys_emailstouser:
        sys_emailsfromuser:

        accountPageUri: The members private account URI, a string (this is also the
            'key_name' attribute of the connector instance).
        memberRef: A ReferenceProperty connecting the email address to its owner.
        sys_createddate: A date-stamp for the instance's creation date.
        """

    # Administrative
    lastApplicationFor = _SemesterProperty( default='' )
    lastPaymentKey = db.StringProperty( default='' )
    lastApplicationDate = db.DateTimeProperty( indexed=True )
    lastPaymentRecieved = db.DateTimeProperty( indexed=True )
    lastMecenatReg = _SemesterProperty( default='' )
    lastSscoReg = _SemesterProperty( default='' )
    lastMembershipFor = _SemesterProperty( default='' )

    applicationSemesterHistory = db.ListProperty( str, default=[ ] )
    applicationDateHistory = db.ListProperty( datetime.datetime, default=[ ] )

    paymentKeyHistory = db.ListProperty( str, default=[ ] )
    paymentRecievedHistory = db.ListProperty( datetime.datetime, default=[ ] )

    membershipHistory = db.ListProperty( str, default=[ ] )
    mecenatHistory = db.ListProperty( str, default=[ ] )
    sscoHistory = db.ListProperty( str, default=[ ] )

    accountPageUri = db.StringProperty( default='' )

    # Member information
    admittedSemester = _CourseProperty( default='' )
    personalId = _PersonalIdProperty( required=True, indexed=True )
    firstName = _NameProperty( required=True )
    lastName = _NameProperty( required=True )
    streetCo = _OptNameProperty( default='' )
    streetAddress = _NameProperty( required=True )
    streetNumber = _StreetNumberProperty( required=True )
    streetEntrance = _StreetEntranceProperty( default='' )
    streetApartment = _StreetApartmentProperty( default='' )
    streetFloor = _StreetFloorProperty( default='' )
    zipCode = _ZipCodeProperty( required=True, default='' )
    city = _NameProperty( required=True, default='' )
    country = db.StringProperty( default='Sverige', choices=[ 'Sverige' ] )
    email = db.EmailProperty( default=None )
    phone = _PhoneProperty( default='' )
    studentConfirmation = db.BooleanProperty( default=True )

    # System information
    sys_email_verified = db.BooleanProperty( default=False )
    sys_trans_email = db.EmailProperty( default=None )
    sys_tmpkey = db.StringProperty( default='' )
    sys_administrator = db.BooleanProperty( default=False )
    sys_su = db.BooleanProperty( default=False )
    sys_accounttype = db.StringProperty( default='' )
    sys_password = db.StringProperty( default=None )
    sys_deleted = db.BooleanProperty( default=False, indexed=True )
    sys_changelog = db.ListProperty( db.Text, default=[ ] )
    sys_activitylog = db.ListProperty( db.Text, default=[ ] )
    sys_createddate = db.DateTimeProperty( auto_now_add=True )
    sys_modifieddate = db.DateTimeProperty( auto_now=True )
    sys_emailstouser = db.ListProperty( db.Key, default=[ ] )
    sys_emailsfromuser = db.ListProperty( db.Key, default=[ ] )

    def update_memcache(self):
        """Method for updating the memcache member representation.

        Returns:
            Foo. Blaha blaha.
        """

        olddata = memcache.get( 'speedparams_%s' % self.personalId )
        if olddata is None:
            return False
        else:
            memcache.set( 'speedparams_%s' % self.personalId, self.dumps( {'validKeys': olddata.keys( )} ) )

    def set_tmpkey(self, use_case='member_login_reminder'):
        """Method for setting the members' sys_tmpkey property.

        Arguments:
            use_case:

        Returns:
            Foo.
        """

        self.sys_tmpkey = RkhskMember.generate_key( sys_tmpkey='create' )
        self.sys_modifieddate = datetime.datetime.today( )
        self.sys_changelog.append( db.Text( json.dumps( {
            'sys_tmpkey': {
                'key_name': self.sys_tmpkey,
                'useCase': use_case
            },
            'sys_modifieddate': self.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
        } ) ) )

        def txn_store_sysTmpKey(owner, tmp_key, use_case):
            """Safe payment key creation

            Arguments:
                owner:
                tmp_key:
                use_case:
            """

            SysTmpKey(
                key_name=tmp_key,
                tmpKey=tmp_key,
                memberRef=owner,
                useCase=use_case
            ).put( )

        db.run_in_transaction( txn_store_sysTmpKey, self, self.sys_tmpkey, use_case )


    def set_admin_access(self, sys_activator):
        """Upgrade member to system administrator.

        Arguments:
            sys_activator:
        """

        self.sys_password = None
        self.set_tmpkey( 'admin_password_reset' )

        taskqueue.add(
            queue_name='mailer' + str( random.randrange( 4 ) ),
            url='/tasks/send_admin_activation_email/',
            params={
                'personalId': self.personalId,
                'sys_activator': sys_activator
            },
            countdown=5
        )

    def remove_admin_access(self, sys_deactivator):
        """Downgrade member to ordinary member.

        Arguments:
            sys_deactivator:
        """

        taskqueue.add(
            queue_name='mailer' + str( random.randrange( 4 ) ),
            url='/tasks/send_admin_deactivation_email/',
            params={
                'personalId': self.personalId,
                'sys_deactivator': sys_deactivator
            },
            countdown=5
        )

    def set_email_verified(self):
        """Foo. Bar."""

        self.sys_email_verified = True
        self.sys_modifieddate = datetime.datetime.today( )
        self.sys_changelog.append( db.Text( json.dumps( {
            'sys_email_verified': True,
            'sys_modifieddate': self.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
        } ) ) )

    def update_admin_email(self, email, updater):
        """Instance method for updating a user's email address.

        Arguments:
            email:
            updater:
        """

        if self.sys_administrator is True and updater != 'su' and updater != self.personalId:
            return False

        def txn_add_email(owner, new_email):
            """Safe email setter.

            Arguments:
                new_email:
            """

            AccountEmail(
                key_name=new_email,
                accountEmail=new_email,
                memberRef=owner
            ).put( )


        if email is None: # remove email
            if self.email is not None:
                old_email_connection = AccountEmail.get_by_key_name( self.email )
                if old_email_connection is not None:
                    db.delete( old_email_connection )
            self.email = None

        elif self.email is None and email is not None: # add email
            self.email = email
            db.run_in_transaction( txn_add_email, self, self.email )

        elif self.email is not None and email is not None: # change email
            old_email_connection = AccountEmail.get_by_key_name( self.email )
            if old_email_connection is not None:
                db.delete( old_email_connection )
            self.email = email
            db.run_in_transaction( txn_add_email, self, self.email )

    def update_member_email(self, email, updater):
        """Set email

        Arguments:
            email:
            updater:
        """

        pass

    def dumps(self, valid_keys=None):
        """Method for translating the instance attributes to dict.

        Arguments:
            details:

        Returns:
            Foo.
        """

        response = {}
        if valid_keys is None:
            valid_keys = [
                'sscoHistory',
                'mecenatHistory',
                'membershipHistory',
                'paymentRecievedHistory',
                'paymentKeyHistory',
                'applicationDateHistory',
                'applicationSemesterHistory',
                'lastMembershipFor',
                'lastSscoReg',
                'lastMecenatReg',
                'lastPaymentRecieved',
                'lastApplicationDate',
                'lastPaymentKey',
                'lastApplicationFor',
                'studentConfirmation',
                'phone',
                'email',
                'country',
                'city',
                'zipCode',
                'streetFloor',
                'streetApartment',
                'streetEntrance',
                'streetNumber',
                'streetAddress',
                'streetCo',
                'lastName',
                'firstName',
                'personalId',
                'admittedSemester',
                'sys_emailsfromuser:',
                'sys_emailstouser:',
                'sys_modifieddate:',
                'sys_createddate',
                'sys_changelog',
                'sys_deleted',
                'sys_password',
                'sys_accounttype',
                'sys_su',
                'sys_administrator',
                'sys_tmpkey',
                'sys_trans_email',
                'sys_email_verified'
            ]
            for key in valid_keys:
                if hasattr( self, key ):
                    value = getattr( self, key )
                    if isinstance( value, list ):
                        dumparray = [ ]
                        for i in range( len( value ) ):
                            if isinstance( value[ i ], datetime.datetime ):
                                dumparray.append( value[ i ].strftime( '%Y-%m-%d' ) )
                            else:
                                dumparray.append( value[ i ] )
                        response.update( {str( key ): dumparray} )
                    elif isinstance( value, datetime.datetime ):
                        response.update( {str( key ): value.strftime( '%Y-%m-%d' )} )
                    else:
                        response.update( {str( key ): value} )

        else:
            response.update( {'personalId': self.personalId} )
            for key in valid_keys:
                if hasattr( self, key ):
                    value = getattr( self, key )
                    if isinstance( value, list ):
                        dump_array = [ ]
                        for i in range( len( value ) ):
                            if isinstance( value[ i ], datetime.datetime ):
                                dump_array.append( value[ i ].strftime( '%Y-%m-%d' ) )
                            else:
                                dump_array.append( value[ i ] )
                        response.update( {str( key ): dump_array} )
                    elif isinstance( value, datetime.datetime ):
                        response.update( {str( key ): datetime.datetime.strftime( value, '%Y-%m-%d' )} )
                    else:
                        response.update( {str( key ): value} )

        #logging.info('dumping: %s', response)

        return response

    @classmethod
    def verify_payment(cls, entry, **kwargs):
        """Method for verifying a recieved membership fee payment.

        Arguments:
            entry:
            kwargs:

        Returns:
            foo
        """

        return True # for now TODO

    @classmethod
    def generate_key(cls, **kwargs):
        """Method for generating a unique random key used by various subsystems.

        Arguments:
            kwargs:

        Returns:
            foo
        """

        if 'paymentKey' in kwargs:
            payment_key = None
            while True:
                payment_key = ''
                for i in range( 5 ):
                    payment_key += chr( random.randint( ord( 'A' ), ord( 'Z' ) ) )
                if PaymentKey.get_by_key_name( payment_key ) is None:
                    break
            return payment_key

        if 'accountPageUri' in kwargs:
            account_uri = None
            AZ_range = [ ord( 'A' ), ord( 'Z' ) ]
            az_range = [ ord( 'a' ), ord( 'z' ) ]
            d_range = [ ord( '0' ), ord( '9' ) ]
            chr_sum = AZ_range[ 1 ] - AZ_range[ 0 ] + az_range[ 1 ] - az_range[ 0 ] + d_range[ 1 ] - d_range[ 0 ]
            AZ_p = float( AZ_range[ 1 ] - AZ_range[ 0 ] ) / chr_sum
            az_p = AZ_p + (float( az_range[ 1 ] - az_range[ 0 ] ) / chr_sum)
            d_p = az_p + (float( d_range[ 1 ] - d_range[ 0 ] ) / chr_sum)

            while True:
                account_uri = ''
                for i in range( 30 ):
                    target_range = random.random( )
                    if target_range < AZ_p:
                        target_range = AZ_range
                    elif target_range < az_p:
                        target_range = az_range
                    else:
                        target_range = d_range
                    account_uri += chr( random.randint( target_range[ 0 ], target_range[ 1 ] ) )

                if AccountUri.get_by_key_name( account_uri ) is None:
                    break

            return account_uri

        if 'sys_tmpkey' in kwargs:
            sys_tmpkey = None
            AZ_range = [ ord( 'A' ), ord( 'Z' ) ]
            az_range = [ ord( 'a' ), ord( 'z' ) ]
            d_range = [ ord( '0' ), ord( '9' ) ]
            chr_sum = AZ_range[ 1 ] - AZ_range[ 0 ] + az_range[ 1 ] - az_range[ 0 ] + d_range[ 1 ] - d_range[ 0 ]
            AZ_p = float( AZ_range[ 1 ] - AZ_range[ 0 ] ) / chr_sum
            az_p = AZ_p + (float( az_range[ 1 ] - az_range[ 0 ] ) / chr_sum)
            d_p = az_p + (float( d_range[ 1 ] - d_range[ 0 ] ) / chr_sum)

            while True:
                sys_tmpkey = ''
                for i in range( 26 ):
                    target_range = random.random( )
                    if target_range < AZ_p:
                        target_range = AZ_range
                    elif target_range < az_p:
                        target_range = az_range
                    else:
                        target_range = d_range
                    sys_tmpkey += chr( random.randint( target_range[ 0 ], target_range[ 1 ] ) )

                if SysTmpKey.get_by_key_name( sys_tmpkey ) is None:
                    break

            return sys_tmpkey

    @classmethod
    def create_or_update(cls, command):
        """Create or update database entries.

        Arguments:
            details:
            dataset:
        """

        func = getattr( cls, '_action_%s' % command.action, None )

        if func is None:
            logging.warning(
                'Hacking attempt??? Invalid action (%s) called on RkhskMember.create_or_update() from IP address %s.'
                % (
                    command.action, command.client_ip) )
            return False

        # "the big picture"
        personal_ids = [ ]
        updated_members = [ ]
        futures = []
        memcache_is_dirty = False
        added_members_count = 0
        updated_members_count = 0

        for dataset in command.payload:

            logging.info( 'creating/updating: \'%s\', count: %s' % (dataset, len( command.payload )) )

            # first, basic personal id verification
            personal_id = dataset[ 'personalId' ]
            if re.search( '^\d{6}-[a-z\d]{4}$', personal_id, flags=re.I ) is None:
                logging.warning( 'Hacking attempt??? Invalid personal ID (%s) recieved from IP address %s.' % (
                    personal_id, command.client_ip) )
                return False
            else:
                personal_id = personal_id.split( '-' )
                try:
                    datetime.datetime.strptime( personal_id[ 0 ], '%y%m%d' )
                    personal_id = string.join( personal_id, '-' )
                except (ValueError):
                    logging.warning( 'Hacking attempt??? Invalid personal ID (%r) recieved from IP address %s.' % (
                        string.join( personal_id, '-' ), command.client_ip) )
                    return False

            # basic email address verification
            if 'email' in dataset.keys() and dataset[ 'email' ]:
                old_account_connection = AccountEmail.get_by_key_name( dataset[ 'email' ] )
                if old_account_connection is not None:
                    try:
                        if (old_account_connection.memberRef.personalId != personal_id):
                            logging.warning(
                                'Hacking attempt??? Request to add existing email address (%s) to member %s recieved '
                                'from IP address %s.' % (
                                    dataset[ 'email' ], personal_id, command.client_ip) )
                            return False
                    except:
                        # except what?? => broken reference
                        db.delete( old_account_connection )

            task_data = {
                'member': RkhskMember.get_by_key_name( personal_id ),
                'dataset': dataset,
                'user_id': command.user_id,
                'client_ip': command.client_ip,
                'valid_keys': command.valid_keys,
                'changelog': {},
                'memcache_dirty': False,
                'memcache_update': False,
                'changed': False,
                'added': False
            }
            results = func( task_data )

            if not results:
                return False

            if results[ 'changed' ] or results[ 'added' ]:
                member = results[ 'member' ]
                member.sys_changelog.append( db.Text( json.dumps( results[ 'changelog' ] ) ) )
                if results[ 'memcache_dirty' ]:
                    memcache_is_dirty = True
                if results[ 'memcache_update' ]:
                    if not member.update_memcache( ):
                        memcache_is_dirty = True

                remote_event = 'member_updated'
                if results[ 'added' ]:
                    added_members_count += 1
                    remote_event = 'member_created'
                else:
                    updated_members_count += 1
                updated_members.append( member )

                taskqueue.add(
                    queue_name='broadcaster',
                    url='/tasks/admin_broadcast/',
                    params={
                        'broadcaster': command.user_id,
                        'timestamp': pickle.dumps( datetime.datetime.today( ) ),
                        'remote_event': remote_event,
                        'data': json.dumps( member.dumps( {
                            'valid_keys': [
                                'lastApplicationFor',
                                'firstName',
                                'lastName',
                                'streetCo',
                                'streetAddress',
                                'streetNumber',
                                'streetEntrance',
                                'streetApartment',
                                'streetFloor',
                                'zipCode',
                                'city',
                                'email',
                                'phone',
                                'lastPaymentRecieved',
                                'lastApplicationDate',
                                'sys_administrator',
                                'sys_accounttype',
                                'lastSscoReg',
                                'lastMecenatReg',
                                'lastPaymentKey'
                            ]
                        } ) )
                    } )
                if len( updated_members ) > 20:
                    futures.append(db.put_async( updated_members ) )
                    updated_members = [ ]

        # commit
        if any( updated_members ):
            futures.append(db.put_async( updated_members ) )

        for future in futures:
            future.get_result()

        if memcache_is_dirty:
            memcache.set( 'speedparams_list_is_dirty', True )

        sys_config.SysConfig.update( ) #full_member_registry_count='+%s' % added_members_count )

        return {
            'members_updated': updated_members_count,
            'members_created': added_members_count
        }

    @classmethod
    def _action_user_registration(cls, task_data):
        """Method for inserting new members by the public registration interface.

        Arguments:
            task_data:

        Returns:
            Foo
        """

        member = task_data[ 'member' ]
        dataset = task_data[ 'dataset' ]
        valid_keys = task_data[ 'valid_keys' ]
        changelog = {}
        memcache_dirty = task_data[ 'memcache_dirty' ]
        memcache_update = task_data[ 'memcache_update' ]
        changed = task_data[ 'changed' ]
        added = task_data[ 'added' ]

        if member: # update

            for key in valid_keys:
                if key == 'personalId':
                    continue
                elif getattr( member, key ) != dataset[ key ]: # change detected
                    if key == 'email':
                        if dataset[ 'email' ] == '':
                            # discard
                            pass

                        elif member.email is None or member.sys_email_verified is False:
                            # perform silent email update
                            changelog.update( {'emailDeleted': member.email, 'emailAdded': dataset[ 'email' ]} )
                            if member.email is not None:
                                old_email_connection = AccountEmail.get_by_key_name( member.email )
                                if old_email_connection is not None:
                                    db.delete( old_email_connection )
                            member.email = dataset[ 'email' ]

                            def txn_swap_email(owner, new_email):
                                """Safe email update

                                Arguments:
                                    owner:
                                    new_email:
                                """

                                member.AccountEmail(
                                    key_name=new_email,
                                    accountEmail=new_email,
                                    memberRef=owner
                                ).put( )

                            db.run_in_transaction( txn_swap_email, member, dataset[ 'email' ] )

                        else:
                            # require change verification
                            changelog.update(
                                    {'oldVerifiedEmail': member.email, 'newUnverifiedEmail': dataset[ 'email' ]} )
                            member.sys_trans_email = dataset[ 'email' ]
                            # todo: queue email notification

                        continue

                    elif key == 'lastApplicationFor':
                        # update lastApplicationFor and related history
                        lastApplicationDate = member.lastApplicationDate
                        if lastApplicationDate is not None:
                            lastApplicationDate = lastApplicationDate.isoformat( ).split( '.' )[ 0 ]
                        applicationDateHistory = [ ]
                        if any( member.applicationDateHistory ):
                            for datestamp in member.applicationDateHistory:
                                applicationDateHistory.append( datestamp.isoformat( ).split( '.' )[ 0 ] )
                        changelog.update( {
                            'lastApplicationFor': member.lastApplicationFor,
                            'lastApplicationDate': lastApplicationDate,
                            'applicationSemesterHistory': member.applicationSemesterHistory,
                            'lastPaymentKey': member.lastPaymentKey,
                            'paymentKeyHistory': member.paymentKeyHistory,
                            'applicationDateHistory': applicationDateHistory
                        } )
                        member.lastApplicationFor = dataset[ 'lastApplicationFor' ]
                        member.applicationSemesterHistory.append( member.lastApplicationFor )
                        member.lastApplicationDate = datetime.datetime.today( )
                        member.applicationDateHistory.append( datetime.datetime.today( ) )

                        # update lastPaymentKey, related history and key listing
                        member.lastPaymentKey = cls.generate_key( paymentKey='refresh' )
                        member.paymentKeyHistory.append( member.lastPaymentKey )

                        def txn_store_key(owner, key_name):
                            PaymentKey(
                                key_name=key_name,
                                paymentKey=key_name,
                                memberRef=owner
                            ).put( )

                        db.run_in_transaction( txn_store_key, member, member.lastPaymentKey )

                        memcache_dirty = True

                        continue

                    changelog.update( {str( key ): dataset[ key ]} )
                    setattr( member, key, dataset[ key ] )

                else:
                    pass
                    #logging.info('Unchanged parameter %s sent to action_user_registration()' % key)

            # finishing update
            if any( changelog ):
                member.sys_modifieddate = datetime.datetime.today( )
                changelog.update( {'sys_modifieddate': member.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]} )
                memcache_updated = True
                changed = True

        else: # create
            member = RkhskMember(
                key_name=dataset[ 'personalId' ],
                personalId=dataset[ 'personalId' ],
                lastApplicationFor=dataset[ 'lastApplicationFor' ],
                applicationSemesterHistory=[ dataset[ 'lastApplicationFor' ] ],
                admittedSemester=dataset[ 'admittedSemester' ],
                firstName=dataset[ 'firstName' ],
                lastName=dataset[ 'lastName' ],
                streetCo=dataset[ 'streetCo' ],
                streetAddress=dataset[ 'streetAddress' ],
                streetNumber=dataset[ 'streetNumber' ],
                streetEntrance=dataset[ 'streetEntrance' ],
                streetApartment=dataset[ 'streetApartment' ],
                streetFloor=dataset[ 'streetFloor' ],
                zipCode=dataset[ 'zipCode' ],
                city=dataset[ 'city' ],
                email=dataset[ 'email' ],
                country=dataset[ 'country' ],
                phone=dataset[ 'phone' ],
                studentConfirmation=dataset[ 'studentConfirmation' ],
                sys_accounttype='application pending',
                accountPageUri=cls.generate_key( accountPageUri='create' ),
                lastPaymentKey=cls.generate_key( paymentKey='create' )
            )

            logging.info( '##### lastappfor \'%s\'' % type( member.lastApplicationDate ).__name__ )

            member.lastApplicationDate = datetime.datetime.today( )
            #logging.info('##### lastappfor \'%s\'' % member.lastApplicationDate)
            member.applicationDateHistory.append( member.lastApplicationDate )
            logging.info( 'appdate: %s' % member.applicationDateHistory )
            member.paymentKeyHistory.append( member.lastPaymentKey )

            changelog.update( member.dumps( ) )

            def txn_store_paymentKey(owner, key_name):
                """Safe payment key creation

                Arguments:
                    owner:
                    key_name:
                """

                PaymentKey(
                    key_name=key_name,
                    paymentKey=key_name,
                    memberRef=owner
                ).put( )

            db.run_in_transaction( txn_store_paymentKey, member, member.lastPaymentKey )

            def txn_store_accountPageUri(owner, page_uri):
                """Safe payment key creation

                Arguments:
                    owner:
                    key_name:
                """

                AccountUri(
                    key_name=page_uri,
                    accountPageUri=page_uri,
                    memberRef=owner
                ).put( )

            db.run_in_transaction( txn_store_accountPageUri, member, member.accountPageUri )

            def txn_store_email(owner, email):
                """Store account email securely

                Arguments:
                    owner:
                    email:
                """

                AccountEmail(
                    key_name=email,
                    accountEmail=email,
                    memberRef=owner
                ).put( )

            db.run_in_transaction( txn_store_email, member, member.email )

            #  if details['action'] == 'user_registration':
            #    OutgoingEmail.send_registration_notice(
            #      firstName = entry.firstName,
            #      lastName = entry.lastName,
            #      email = entry.email,
            #      applicationFor = entry.lastApplicationFor,
            #      paymentKey = entry.lastPaymentKey,
            #      accountPageUri = entry.accountPageUri,
            #      sentBy = 'Mecenat-teamet',
            #      sys_creator = properties_log['sys_creator']
            #    )

            memcache_dirty = True
            memcache_updated = True
            added = True

        task_data.update( {
            'member': member,
            'dataset': None,
            'changelog': changelog,
            'memcache_dirty': memcache_dirty,
            'memcache_update': memcache_update,
            'changed': changed,
            'added': added
        } )
        #logging.info('problemmemberd: %s' % task_data['member'].values())

        if member.email:
            taskqueue.add(
                queue_name='mailer' + str( random.randrange( 4 ) ),
                url='/tasks/send_member_registration_notice/',
                params={
                    'personal_id': member.personalId,
                    'sys_registrator': member.personalId,
                    'client_ip': task_data['client_ip'],
                    'user_initiated': True
                },
                countdown=5
            )

        return task_data

    @classmethod
    def _action_admin_member_create(cls, task_data):
        """Method for executing various admin updates, from the interface grid as well as batch uploads.

        Arguments:
            task_data:

        Returns:
            Foo.
        """

        return cls._action_admin_member_update(task_data)

    @classmethod
    def _action_admin_payments_update(cls, task_data):
        """foofoo

        Arguments:
            task_data:

        Returns:
            Foo.
        """

        pass

    @classmethod
    def _action_admin_member_update(cls, task_data):
        """Method for executing various admin updates from the interface grid.

        Arguments:
            task_data:

        Returns:
            Foo.
        """

        member = task_data[ 'member' ]
        dataset = task_data[ 'dataset' ]
        valid_keys = task_data[ 'valid_keys' ]
        user_id = task_data[ 'user_id' ]
        changelog = {}
        memcache_dirty = task_data[ 'memcache_dirty' ]
        memcache_update = task_data[ 'memcache_update' ]
        changed = task_data[ 'changed' ]
        added = task_data[ 'added' ]

        if member is None: # create

            if 'lastApplicationDate' in dataset:
                lastApplicationDate = datetime.datetime.strptime( dataset[ 'lastApplicationDate' ], '%Y-%m-%d' )
            else:
                lastApplicationDate = datetime.datetime.today( )

            member = RkhskMember(
                key_name=dataset[ 'personalId' ],
                personalId=dataset[ 'personalId' ],
                firstName=dataset[ 'firstName' ],
                lastName=dataset[ 'lastName' ],
                streetAddress=dataset[ 'streetAddress' ],
                streetNumber=dataset[ 'streetNumber' ],
                zipCode=dataset[ 'zipCode' ],
                city=dataset[ 'city' ],
                lastApplicationDate=lastApplicationDate,
                accountPageUri=cls.generate_key( accountPageUri='create' ),
                lastPaymentKey=cls.generate_key( paymentKey='create' ),
                sys_accounttype='application pending'
            )

            member.applicationDateHistory.append( member.lastApplicationDate )
            member.paymentKeyHistory.append( member.lastPaymentKey )

            def txn_store_accountPageUri(owner, page_uri):
                """Safe payment key creation

                Arguments:
                    owner:
                    page_uri:
                """

                AccountUri(
                    key_name=page_uri,
                    accountPageUri=page_uri,
                    memberRef=owner
                ).put( )

            db.run_in_transaction( txn_store_accountPageUri, member, member.accountPageUri )

            def txn_store_paymentKey(owner, payment_key):
                """Safe payment key creation

                Arguments:
                    owner:
                    payment_key:
                """

                PaymentKey(
                    key_name=payment_key,
                    paymentKey=payment_key,
                    memberRef=owner
                ).put( )

            db.run_in_transaction( txn_store_paymentKey, member, member.lastPaymentKey )

            #  if details['action'] == 'user_registration':
            #    OutgoingEmail.send_registration_notice(
            #      firstName = entry.firstName,
            #      lastName = entry.lastName,
            #      email = entry.email,
            #      applicationFor = entry.lastApplicationFor,
            #      paymentKey = entry.lastPaymentKey,
            #      accountPageUri = entry.accountPageUri,
            #      sentBy = 'Mecenat-teamet',
            #      sys_creator = properties_log['sys_creator']
            #    )

            #taskqueue.add(
            #  queue_name = 'mailer'+(random.randrange(4)),
            #  url = '/tasks/send_member_registration_notice/',
            #  params = {
            #    'personalId': member.personalId,
            #    'sys_activator': task_data['request_details']['userId'],
            #    'ipAddress': task_data['request_details']['ipAddress'],
            #    'userInitiated': False
            #  }
            #)

            memcache_dirty = True
            memcache_updated = True
            added = True

            changelog.update( {'sys_createddate': member.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]} )

        for key in valid_keys:
            if not key in dataset:
                continue
                # making sure comparasion completes and input value is writeable
            newvalue = dataset[ key ]
            logvalue = dataset[ key ]
            oldvalue = getattr( member, key )
            if isinstance( oldvalue, datetime.datetime ):
                logging.info( 'newdatetimevalue: %s, attr key: %s' % (str( newvalue ), key) )
                oldvalue = oldvalue.strftime( '%Y-%m-%d' ) # datetime.datetime => YYYY-MM-DD
                newvalue = datetime.datetime.strptime( newvalue, '%Y-%m-%d' ) # YYYY-MM-DD => datetime.datetime
                logvalue = newvalue.isoformat( ).split( '.' )[ 0 ]
            elif isinstance( oldvalue, list ):
                continue
            elif oldvalue is None:
                try:
                    newvalue = datetime.datetime.strptime( newvalue, '%Y-%m-%d' )
                except (TypeError, ValueError):
                    pass

            if newvalue == '':
                #for i in range(len(oldvalue)):
                #  if isinstance(oldvalue, datetime.datetime):
                newvalue = None

            if dataset[ key ] != oldvalue:
                logging.info( 'MODIFIED: %s (dataset[key]) != %s (oldvalue)' % (dataset[ key ], oldvalue) )
                if key == 'personalId':
                    continue
                elif key == 'email':
                    old_email = member.email
                    if member.update_admin_email( email=newvalue, updater=user_id ) is True:
                        changelog.update( {'emailDeleted': old_email, 'emailAdded': newvalue} )
                    else:
                        changelog.update( {'emailDeleted': None, 'emailAdded': None} )
                else:
                    changelog.update( {key: logvalue} )
                    #logging.info('set attr %s to %s for %s' % (key, newvalue, member.personalId))
                    if newvalue is None:
                        try:
                            setattr( member, key, newvalue )
                        except:
                            setattr( member, key, '' )
                    else:
                        setattr( member, key, newvalue )

            else:
                pass
                #logging.info('Unchanged parameter %s sent to action_user_registration()' % key)

        # finishing update
        if any( changelog ):
            memcache_update = True

            today = datetime.datetime.today( )
            current_semester = sys_config.SysConfig.get_current_semester( )
            firstpaymentdate = sys_config.SysConfig.get_first_payment_date( )
            changelogrefresh = {}

            logging.info( 'changelog: %s' % str( changelog ) )
            #logging.info('changelog: %s' % changelog)
            #logging.info('task_data: %s' % task_data)
            if 'sys_administrator' in changelog:
                if changelog[ 'sys_administrator' ]:
                    member.set_admin_access( sys_activator=user_id )
                else:
                    member.remove_admin_access( sys_deactivator=user_id )

            if 'lastApplicationFor' in changelog or 'lastApplicationDate' in changelog:
                if not 'lastApplicationDate' in changelog and member.lastApplicationFor == current_semester:
                    logging.info( 'lastAppDate not in changelog' )
                    lastApplicationDate = None
                    if lastApplicationDate is not None:
                        lastApplicationDate = lastApplicationDate.isoformat( ).split( '.' )[ 0 ]
                    applicationDateHistory = [ ]
                    if any( member.applicationDateHistory ):
                        for datestamp in member.applicationDateHistory:
                            applicationDateHistory.append( datestamp.isoformat( ).split( '.' )[ 0 ] )
                    changelogrefresh.update( {
                        'lastApplicationDate': lastApplicationDate,
                        'applicationDateHistory': applicationDateHistory
                    } )
                    member.lastApplicationDate = today
                    member.applicationDateHistory.append( today )
                    memcache_dirty = True

                elif member.lastApplicationDate > today:
                    return False

                elif not 'lastApplicationFor' in changelog:
                    logging.info( 'lastAppFor not in changelog' )

                    if member.lastApplicationDate.date( ) >= firstpaymentdate:
                        changelogrefresh.update( {
                            'lastApplicationFor': member.lastApplicationFor,
                            'applicationSemesterHistory': member.applicationSemesterHistory
                        } )
                        member.lastApplicationFor = current_semester
                        member.applicationSemesterHistory.append( member.lastApplicationFor )

            if 'lastPaymentRecieved' in changelog:
                paymentRecievedHistory = [ ]
                if any( member.paymentRecievedHistory ):
                    for datestamp in member.paymentRecievedHistory:
                        paymentRecievedHistory.append( datestamp.isoformat( ).split( '.' )[ 0 ] )
                changelogrefresh.update(
                        {'paymentRecievedHistory': paymentRecievedHistory, 'sys_accounttype': 'verified member'} )
                if member.lastPaymentRecieved:
                    member.paymentRecievedHistory.append( member.lastPaymentRecieved )
                    member.sys_accounttype = 'verified member'

            if member.lastApplicationFor == current_semester and current_semester != member.lastMembershipFor:
                if isinstance( member.lastPaymentRecieved, datetime.datetime ): # try to register payment
                    logging.info( 'lastPaymentKey not datetime.datetime, instead: %s' % str( member.lastPaymentRecieved ) )
                    member.sys_accounttype = 'verified member'
                    if member.lastPaymentRecieved > today:
                        return False
                    elif member.lastPaymentRecieved.date( ) > firstpaymentdate:
                        changelogrefresh.update( {
                            'lastMembershipFor': member.lastMembershipFor,
                            'membershipHistory': member.membershipHistory
                        } )
                        member.lastMembershipFor = current_semester
                        member.membershipHistory.append( current_semester )
                else: # create new paymentkey
                    changelogrefresh.update( {
                        'lastPaymentKey': member.lastPaymentKey,
                        'paymentKeyHistory': member.paymentKeyHistory
                    } )

                    member.lastPaymentKey = cls.generate_key( paymentKey='refresh' )
                    member.paymentKeyHistory.append( member.lastPaymentKey )

                    def txn_store_key(owner, key_name):
                        """Safe payment key creation

                        Arguments:
                            owner:
                            key_name:
                        """

                        PaymentKey(
                            key_name=key_name,
                            paymentKey=key_name,
                            memberRef=owner
                        ).put( )

                    db.run_in_transaction( txn_store_key, member, member.lastPaymentKey )

            if member.lastMembershipFor == current_semester and member.lastSscoReg != current_semester:
                # add to upload list
                logging.info( 'ADDED %s TO REG-SSCO LIST' % member.personalId )
                changelogrefresh.update( {
                    'lastSscoReg': member.lastSscoReg,
                    'sscoHistory': member.sscoHistory
                } )

                if current_semester[ 0 ] == 'V':
                    dateinterval = sys_config.SysConfig.get_spring_semester_dateinterval( )
                else:
                    dateinterval = sys_config.SysConfig.get_fall_semester_dateinterval( )

                def txn_pull_task(owner, target_semester, startdate, enddate):
                    """Safe pull task creation

                        Arguments:
                            owner:
                            target_semester:
                            start_date:
                            end_date:
                        """

                    if SscoPullTask.get_by_key_name( '%s_%s' % (owner.personalId, target_semester) ) is None:
                        SscoPullTask(
                            key_name='%s_%s' % (owner.personalId, target_semester),
                            personalId=owner.personalId,
                            firstName=owner.firstName,
                            lastName=owner.lastName,
                            email=owner.email,
                            phone=owner.phone,
                            membershipStarts=startdate,
                            membershipEnds=enddate,
                            memberRef=owner,
                            targetSemester=target_semester
                        ).put( )

                db.run_in_transaction( txn_pull_task, member, current_semester, dateinterval[ 0 ], dateinterval[ 1 ] )

            if member.lastMembershipFor == current_semester and member.lastMecenatReg != current_semester:
                # add to upload list
                logging.info( 'ADDED %s TO REG-MECENAT LIST' % member.personalId )
                changelogrefresh.update( {
                    'lastMecenatReg': member.lastMecenatReg,
                    'mecenatHistory': member.mecenatHistory
                } )

                def txn_push_task(owner, target_semester):
                    """Safe push task creation

                    Arguments:
                        owner:
                        target_semester:
                    """

                    if MecenatPushTask.get_by_key_name( '%s_%s' % (owner.personalId, target_semester) ) is None:
                        MecenatPushTask(
                            key_name='%s_%s' % (owner.personalId, target_semester),
                            personalId=owner.personalId,
                            firstName=owner.firstName,
                            lastName=owner.lastName,
                            streetCo=owner.streetCo,
                            streetAddress=owner.streetAddress,
                            streetNumber=owner.streetNumber,
                            streetEntrance=owner.streetEntrance,
                            streetApartment=owner.streetApartment,
                            streetFloor=owner.streetFloor,
                            zipCode=owner.zipCode,
                            city=owner.city,
                            country=owner.country,
                            memberRef=owner,
                            targetSemester=target_semester
                        ).put( )

                db.run_in_transaction( txn_push_task, member, current_semester )

            changelog.update( changelogrefresh )

            member.sys_modifieddate = datetime.datetime.today( )
            changelog.update( {'sys_modifieddate': member.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]} )
            changed = True

        task_data.update( {
            'member': member,
            'dataset': None,
            'changelog': changelog,
            'memcache_dirty': memcache_dirty,
            'memcache_update': memcache_update,
            'changed': changed,
            'added': added
        } )
        #logging.info('problemmemberd: %s' % task_data['member'])
        return task_data


