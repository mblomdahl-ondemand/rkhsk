# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.sys.TaskInterface
# Author:  Mats Blomdahl
# Version: 2012-03-08

import webapp2 # web application framework
#from webapp2_extras import jinja2
import json
import logging
import pickle
import datetime
# email test start
#from webapp2.mail_handlers import InboundMailHandler

# email test end
from google.appengine.ext import db
from google.appengine.api import channel
from google.appengine.api import memcache

from rkhsk.model import  SscoPullTask, SysConfig, RkhskMember, SysTmpKey, MecenatPushTask
from rkhsk.model.generic.email import SentEmail


COMPATIBILITY_PLAINTEXT = u'Kompatibilitetsinfo: RKH-SK:s system har fullt stöd för öppna webbstandarder och fungerar'\
                          u' i alla moderna webbläsare, inkl. Android och iOS (iPhone). Om du använder en gammal '\
                          u'version av Internet Explorer som inte implementerar webbstandarder så kommer du uppmanas '\
                          u'att aktivera Chrome Frame. Processen tar c:a 30 sekunder och därefter kommer du kunna dra'\
                          u' nytta av alla moderna webbapplikationer, trots att du använder legacy IE.'

COMPATIBILITY_HTML = u'<u>Kompatibilitetsinfo</u>: RKH-SK:s system har fullt stöd för <a href="http://www.whatwg'\
                     u'.org/" target="_blank">öppna webbstandarder</a> och fungerar i alla moderna webbläsare, '\
                     u'inkl. Android och iOS (iPhone). Om du använder en gammal version av Internet Explorer som inte'\
                     u' implementerar webbstandarder så kommer du uppmanas att aktivera <a href="http://www.google'\
                     u'.com/chromeframe" target="_blank">Chrome Frame</a>. Processen tar c:a 30 sekunder och därefter'\
                     u' kommer du kunna dra nytta av alla moderna webbapplikationer, '\
                     u'trots att du använder <i>legacy IE</i>.'

# TODO(mats.blomdahl@gmail.com): Document. Restructure into a handler that takes 2 args, first of which is the queue, second the actual command (share more code!)
class TaskInterface( webapp2.RequestHandler ):
    """The private interface for queueing tasks such as mail-outs and large sets
        of updates.

    The request handler is secured by App Engine's built-in authentication
        configured in the app.yaml config.
    """

    # /tasks/<task_name>/<cmd>
    def post(self, task_name, cmd=None):
        """Handler for the TaskQueue service.

        The handler redirects queued tasks to the appropriate handler method.

        Arguments:
            task: The task identifier, a string.
        """

        func = getattr( self, '__%s' % task_name, None )
        if func is not None:
            if func( ) is False:
                self.error( 404 )


    # /tasks/send_admin_activation_email/
    def __send_admin_activation_email(self):
        """A task that sends out an admin activation email to the user specified
            by the 'personal_id' param.

        This task belong to the queue group 'mailer' which contains the individual
            queues mailer0, mailer1, mailer2 and mailer3.

        To use:
        >>> taskqueue.add(
                queue_name = 'mailer'+str(random.randrange(4)),
                url = '/tasks/send_admin_activation_email/',
                params = {
                    'personal_id'  : member.personalId,
                    'sys_activator': task.user_id
                }
            )

        Arguments (i.e. HTTP-POST params):
            personal_id: The recipient member's key name/personal id, a string.
            sys_activator: The admin initiating the activation process, a string.
        """

        logging.info( 'received send_activation_email' )
        logging.info( 'received args: %s' % self.request.arguments( ) )

        recipient = RkhskMember.get_by_key_name( self.request.get( 'personal_id' ) )
        if recipient is None:
            logging.info( 'failed, reason: member not found.' )
            return False

        sys_activator = self.request.get( 'sys_activator' )
        sender = sys_activator
        if sender != 'su':
            sender = RkhskMember.get_by_key_name( sender )
            if sender is None:
                logging.info( 'failed, reason: member not found.' )
                return False
            sender = '%s %s (%s)' % (sender.firstName, sender.lastName, sender.email)

        plaintext = (
            u'%s,\n'
            u'\n'
            u'Du har tilldelats administratörsprivilegier för RKH-SK:s medlemsdatabas, '
            u'rättigheterna har tilldelats dig av %s. Om du inte hör till studentkårens funktionärer och tänker att '
            u'rättigheterna kanske tilldelats dig av misstag – skriv omedelbart tillbaka och påtala misstaget!\n'
            u'\n'
            u'För att komma igång:\n'
            u'1. Välj ett lösenord via http://regga.rkh-sk.se/admin#%s (Obs! Länken är temporär och kommer inte kunna'
            u' användas mer än en gång.)\n'
            u'2. Logga in i medlemsdatabasen via http://regga.rkh-sk.se/admin med din e-postadress och det lösenord '
            u'du valt.\n'
            u'\n'
            u'%s\n'
            u'\n'
            u'\n'
            u'Varma hälsningar,\n'
            u'\n'
            u'Medlemsdatabasen\n'
            u'Röda Korsets Högskolas Studentkår\n'
            u'' % (recipient.firstName, sender, recipient.sys_tmpkey, COMPATIBILITY_PLAINTEXT)
            )

        html = (
            u'<p style="font-size: 12px;">%s,<br></p>'
            u'<p style="font-size: 12px;">Du har tilldelats administratörsprivilegier för RKH-SK:s medlemsdatabas, '
            u'rättigheterna har tilldelats dig av %s. Om du inte hör till studentkårens funktionärer och tänker att '
            u'rättigheterna kanske tilldelats dig av misstag – skriv omedelbart tillbaka och påtala misstaget!<br></p>'
            u'<p style="font-size: 12px;"><u>För att komma igång</u>:<br><ol style="font-size: 12px;">'
            u'<li>Välj ett lösenord via <a href="http://regga.rkh-sk.se/admin#%s" target="_blank">regga.rkh-sk'
            u'.se/admin#%s</a> (Obs! Länken är temporär och kommer inte kunna användas mer än en gång.)</li>'
            u'<li>Logga in i medlemsdatabasen via <a href="http://regga.rkh-sk.se/admin" target="_blank">regga.rkh-sk'
            u'.se/admin</a> med din e-postadress och det lösenord du valt.</li></ol></p>'
            u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
            u'<p style="font-size: 12px;">Varma hälsningar,<br>'
            u'<br>'
            u'Medlemsdatabasen<br>'
            u'Röda Korsets Högskolas Studentkår<br></p>' % (
                recipient.firstName, sender, recipient.sys_tmpkey, recipient.sys_tmpkey, COMPATIBILITY_HTML)
            )

        sentmail = sent_email.send_wrapper(
            to='%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
            subject=u'Du har utsetts till administratör av RKH-SK:s medlemsdatabas!',
            body_plaintext=plaintext,
            body_html=html,
            sys_sentby=sender,
            sys_creator=sys_activator
        )

        if sentmail is False:
            logging.info( 'failed, reason: mail not sent.' )
            return False
        else:
            logging.info( 'success! mail sent' )

            recipient.sys_emailstouser.append( sentmail.key( ) )
            recipient.sys_modifieddate = datetime.today( )
            changelog = {
                'action': 'Sent Admin Activation Notice',
                'reminderSentTo': recipient.email,
                'sys_activator': sys_activator,
                'sys_modifieddate': recipient.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
            }
            recipient.sys_changelog.append( db.Text( json.dumps( changelog ) ) )
            recipient.put( )

        sys_config.update( sys_admins=('add', recipient.personalId) )


    # /tasks/send_admin_deactivation_email/
    def __send_admin_deactivation_email(self):
        """A task that sends out an admin deactivation email to the user specified
        by the 'personal_id' param.

        This task belong to the queue group 'mailer' which contains the individual
            queues mailer0, mailer1, mailer2 and mailer3.

        To use:
        >>> taskqueue.add(
                queue_name = 'mailer'+str(random.randrange(4)),
                url = '/tasks/send_admin_deactivation_email/',
                params = {
                    'personal_id'    : member.personalId,
                    'sys_deactivator': task.user_id
                }
            )

        Arguments (i.e. HTTP-POST params):
            personal_id: The recipient member's key name/personal id, a string.
            sys_deactivator: The admin initiating the activation process, a string.
        """

        logging.info( 'received send_deactivation_email' )
        logging.info( 'received args: %s' % self.request.arguments( ) )

        recipient = RkhskMember.get_by_key_name( self.request.get( 'personal_id' ) )
        if recipient is None:
            logging.info( 'failed, reason: recipient not found.' )
            return False

        sys_deactivator = self.request.get( 'sys_deactivator' )
        sender = sys_deactivator
        if sender != 'su':
            sender = RkhskMember.get_by_key_name( sender )
            if sender is None:
                logging.info( 'failed, reason: sender not found.' )
                return False
            sender = '%s %s (%s)' % (sender.firstName, sender.lastName, sender.email)

        plaintext = (
            u'%s,\n'
            u'\n'
            u'Dina administratörsprivilegier i RKH-SK:s medlemsdatabas har upphävts. Administratörsrättigheterna har '
            u'tagits bort av %s. Om den här ändringen var oväntad och du tror att det är ett misstag – skriv '
            u'omedelbart tillbaka och påtala misstaget!\n'
            u'\n'
            u'\n'
            u'Varma hälsningar,\n'
            u'\n'
            u'Medlemsdatabasen\n'
            u'Röda Korsets Högskolas Studentkår\n'
            u'' % (recipient.firstName, sender)
            )

        html = (
            u'<p style="font-size: 12px;">%s,<br></p>'
            u'<p style="font-size: 12px;">Dina administratörsprivilegier i RKH-SK:s medlemsdatabas har upphävts. '
            u'Administratörsrättigheterna har tagits bort av %s. Om den här ändringen var oväntad och du tror att det'
            u' är ett misstag – skriv omedelbart tillbaka och påtala misstaget!<br><br></p>'
            u'<p style="font-size: 12px;">Varma hälsningar,<br>'
            u'<br>'
            u'Medlemsdatabasen<br>'
            u'Röda Korsets Högskolas Studentkår<br></p>' % (recipient.firstName, sender)
            )

        sentmail = sent_email.send_wrapper(
            to='%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
            subject=u'Du har avregistrerats som administratör för RKH-SK:s medlemsdatabas!',
            body_plaintext=plaintext,
            body_html=html,
            sys_sentby=sender,
            sys_creator=sys_deactivator
        )

        if sentmail is False:
            logging.info( 'failed, reason: sendmail failed.' )
            return False
        else:
            logging.info( 'success!, reason: sendmail sent.' )
            recipient.sys_emailstouser.append( sentmail.key( ) )
            recipient.sys_modifieddate = datetime.today( )
            changelog = {
                'action': 'Sent Admin Deactivation Notice',
                'reminderSentTo': recipient.email,
                'sys_deactivator': sys_deactivator,
                'sys_modifieddate': recipient.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
            }
            recipient.sys_changelog.append( db.Text( json.dumps( changelog ) ) )
            recipient.put( )

        sys_config.update( sys_admins=('remove', recipient.personalId) )


    # /tasks/send_admin_password_reset_email/
    def __send_admin_password_reset_email(self):
        """A task that sends out an temporary link by email in order for an admin
            to reset his/her login password.

        This task belong to the queue group 'mailer' which contains the individual
            queues mailer0, mailer1, mailer2 and mailer3.

        To use:
        >>> taskqueue.add(
                queue_name = 'mailer'+str(random.randrange(4)),
                url = '/tasks/send_admin_password_reset_email/',
                params = {
                    'personal_id': member.personalId,
                    'client_ip'  : task.client_ip
                }
            )

        Arguments (i.e. HTTP-POST params):
            personal_id: The recipient member's key name/personal id, a string.
            client_ip: The client IP address initiating the password reset, a string.
        """

        logging.info( 'received send_admin_password_reset_email' )
        logging.info( 'received args: %s' % self.request.arguments( ) )

        recipient = RkhskMember.get_by_key_name( self.request.get( 'personal_id' ) )
        if recipient is None:
            logging.info( 'failed, reason: member not found.' )
            return False

        client_ip = self.request.get( 'client_ip' )

        plaintext = (
            u'%s,\n'
            u'\n'
            u'Här följer en länk som du kan använda för att återställa ditt administratörslösenord i RKH-SK:s '
            u'medlemsdatabas. Om du inte känner igen att du skulle ha begärt en återställning av lösenordet så kan du'
            u' helt bortse från detta mail (begäran om återställning mottags från IP-adress %s).\n'
            u'\n'
            u'Du återställer lösenordet via http://regga.rkh-sk.se/admin#%s\n'
            u'\n'
            u'%s\n'
            u'\n'
            u'\n'
            u'Varma hälsningar,\n'
            u'\n'
            u'Medlemsdatabasen\n'
            u'Röda Korsets Högskolas Studentkår\n'
            u'' % (recipient.firstName, client_ip, recipient.sys_tmpkey, COMPATIBILITY_PLAINTEXT)
            )

        html = (
            u'<p style="font-size: 12px;">%s,<br></p>'
            u'<p style="font-size: 12px;">Här följer en länk som du kan använda för att återställa ditt '
            u'administratörslösenord i RKH-SK:s medlemsdatabas. Om du inte känner igen att du skulle ha begärt en '
            u'återställning av lösenordet så kan du helt bortse från detta mail (begäran om återställning mottags '
            u'från ip-adress %s).<br></p>'
            u'<p style="font-size: 12px;">Du återställer lösenordet via <a href="http://regga.rkh-sk.se/admin#%s" '
            u'target="_blank">regga.rkh-sk.se/admin#%s</a><br></p>'
            u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
            u'<p style="font-size: 12px;">Varma hälsningar,<br>'
            u'<br>'
            u'Medlemsdatabasen<br>'
            u'Röda Korsets Högskolas Studentkår</p>'
            u'<br>' % (recipient.firstName, client_ip, recipient.sys_tmpkey, recipient.sys_tmpkey, COMPATIBILITY_HTML)
            )

        sentmail = sent_email.send_wrapper(
            to='%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
            subject=u'Återställning av administratörslösenord för RKH-SK:s medlemsdatabas',
            body_plaintext=plaintext,
            body_html=html,
            sys_sentby=client_ip,
            sys_creator=recipient.email
        )

        if sentmail is False:
            logging.info( 'failed, reason: mail not sent.' )
            return False
        else:
            logging.info( 'success! mail sent' )

            recipient.sys_emailstouser.append( sentmail.key( ) )
            recipient.sys_modifieddate = datetime.today( )
            changelog = {
                'action': 'Sent Admin Password Reset',
                'resetSentTo': recipient.email,
                'sys_origin_client_ip': client_ip,
                'sys_modifieddate': recipient.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
            }
            recipient.sys_changelog.append( db.Text( json.dumps( changelog ) ) )
            recipient.put( )


    # /tasks/send_member_registration_notice/
    def __send_member_registration_notice(self):
        """A task that sends out a member application/registration notice.

        This task belong to the queue group 'mailer' which contains the individual
            queues mailer0, mailer1, mailer2 and mailer3.

        To use:
        >>> taskqueue.add(
                queue_name = 'mailer'+str(random.randrange(4)),
                url = '/tasks/send_admin_password_reset_email/',
                params = {
                    'personal_id'    : member.personalId,
                    'sys_registrator': task.user_id,
                    'client_ip'      : task.client_ip,
                    'user_initated'  : False
                }
            )

        Arguments (i.e. HTTP-POST params):
            personal_id: The recipient member's key name/personal id, a string.
            client_ip: The client IP address initiating the password reset, a string.
            user_initated: A flag marking the registration as user initiated (bool).
            sys_registrator: The registrator's key name (if not user initiated).
        """

        recipient = RkhskMember.get_by_key_name( self.request.get( 'personal_id' ) )
        if recipient is None:
            return False

        client_ip = self.request.get( 'client_ip', default_value=None )
        sys_registrator = self.request.get( 'sys_registrator', default_value=None )
        user_id = self.request.get( 'user_id', default_value=None )

        if self.request.get( 'user_initiated' ):
            operatorline = u'Du får det här mailet som en bekräftelse på din registrering i RKH-SK:s databas. '
        else:
            operatorline = u'Du får det här mailet som en bekräftelse på att RKH-SK:s funktionärer registrerat din '\
                           u'medlemsansökan till studentkåren. '

        plaintext_body = (
            u'%s,\n'
            u'\n'
            u'%sFör att ditt medlemskap för termin %s ska aktiveras måste medlemsavgiften (100 kr) betalas in och '
            u'registreras korrekt i systemet.\n'
            u'\n'
            u'Medlemsavgiften kan antingen betalas in till studentkårens PlusGiro, 126 98 26-2, '
            u'eller kontant i kårens studentexpedition tisdagar & torsdagar kl. 12-13. Oavsett betalningssätt är det '
            u'av högsta vikt att du uppger din personliga kod som referens – annars riskerar du att ditt ärende '
            u'"faller mellan stolarna" och att du får vänta.\n'
            u'\n'
            u'DINA INBETALNINGSUPPGIFTER\n'
            u'Referens & personlig kod: %s\n'
            u'Medlemsavgift: 100 kr\n'
            u'PlusGiro: 126 98 26-2\n'
            u'\n'
            u'För att se status för din medlemsansökan och vilka uppgifter vi har registrerade för dig:\n'
            u'1. Återvänd till registreringssidan http://regga.rkh-sk.se och välj fliken "Ärendestatus"\n'
            u'2. Ange e-postadress och personlig kod ("%s" resp. "%s") och klicka på "Visa ärendestatus"\n'
            u'\n'
            u'%s\n'
            u'\n'
            u'\n'
            u'Varma hälsningar,\n'
            u'\n'
            u'%s\n'
            u'Röda Korsets Högskolas Studentkår\n'
            u'' % (
                recipient.firstName, operatorline, recipient.lastApplicationFor, recipient.lastPaymentKey,
                recipient.email,
                recipient.lastPaymentKey, COMPATIBILITY_PLAINTEXT, 'Medlemsregistreringen')
            )

        html_body = (
            u'<p style="font-size: 12px;">%s,<br></p>'
            u'<p style="font-size: 12px;">%sFör att ditt medlemskap för termin %s ska aktiveras måste medlemsavgiften'
            u' (100 kr) betalas in och registreras korrekt i systemet.<br></p>'
            u'<p style="font-size: 12px;">Medlemsavgiften kan antingen betalas in till studentkårens PlusGiro, '
            u'126 98 26-2, eller kontant i kårens studentexpedition tisdagar & torsdagar kl. 12-13. Oavsett '
            u'betalningssätt är det <b>av högsta vikt att du uppger din personliga kod som referens</b> – annars '
            u'riskerar du att ditt ärende "faller mellan stolarna" och att du får vänta.<br></p>'
            u'<p style="font-size: 12px;"><u>Dina inbetalningsuppgifter</u>:<br>'
            u'Referens & personlig kod: %s<br>'
            u'Medlemsavgift: 100 kr<br>'
            u'PlusGiro: 126 98 26-2<br></p>'
            u'<p style="font-size: 12px;">För att se status för din medlemsansökan och vilka uppgifter vi har '
            u'registrerade för dig:<ol style="font-size: 12px;"><li>Återvänd till registreringssidan <a '
            u'href="http://regga.rkh-sk.se" target="_blank">regga.rkh-sk.se</a> och välj fliken "Ärendestatus"</li>'
            u'<li>Ange e-postadress och personlig kod ("%s" resp. "%s") och klicka på "Visa ärendestatus"</li></ol></p>'
            u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
            u'<p style="font-size: 12px;">Varma hälsningar,<br>'
            u'<br>'
            u'%s<br>'
            u'Röda Korsets Högskolas Studentkår<br>'
            u'<br></p>' % (
                recipient.firstName, operatorline, recipient.lastApplicationFor, recipient.lastPaymentKey,
                recipient.email,
                recipient.lastPaymentKey, COMPATIBILITY_HTML, 'Medlemsregistreringen')
            )

        sentmail = SentEmail.send_wrapper(
            to='%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
            subject=u'Registreringsbekräftelse från RKH-SK:s medlemsdatabas',
            body_plaintext=plaintext_body,
            body_html=html_body,
            sys_sentby=client_ip,
            sys_creator=user_id
        )

        if sentmail is None:
            return False
        else:
            recipient.sys_emailstouser.append( sentmail.key( ) )
            recipient.sys_modifieddate = datetime.today( )
            changelog = {
                'action': 'Sent Account Registration Notice Email',
                'registration_notice_recipient': recipient.email,
                'sys_origin_client_ip': client_ip,
                'sys_modifieddate': recipient.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
            }
            recipient.sys_changelog.append( db.Text( json.dumps( changelog ) ) )
            recipient.put( )


    # /tasks/send_member_details_reminder/
    def __send_member_details_reminder(self):
        """A task that sends out a reminder with member account details.

        This task belong to the queue group 'mailer' which contains the individual
            queues mailer0, mailer1, mailer2 and mailer3.

        To use:
        >>> taskqueue.add(
                queue_name = 'mailer'+str(random.randrange(4)),
                url = '/tasks/send_admin_password_reset_email/',
                params = {
                    'personal_id': member.personalId,
                    'client_ip'  : task.client_ip,
                }
            )

        Arguments (i.e. HTTP-POST params):
            personal_id: The recipient member's key name/personal id, a string.
            client_ip: The client IP address initiating the password reset, a string.
        """

        recipient = RkhskMember.get_by_key_name( self.request.get( 'personal_id' ) )
        if recipient is None:
            return False

        client_ip = self.request.get( 'client_ip' )

        def txn_store_sysTmpKey(owner, tmpkey):
            SysTmpKey(
                key_name=tmpkey,
                tmpKey=tmpkey,
                memberRef=owner,
                useCase='member_login_reminder'
            ).put( )

        db.run_in_transaction( txn_store_sysTmpKey, recipient, recipient.sys_tmpkey )

        plaintext = (
            u'%s,\n'
            u'\n'
            u'Det här är en påminnelse med dina personliga inloggningsuppgifter till RKH-SK:s medlemssida, '
            u'http://regga.rkh-sk.se. Om du inte känner igen att du skulle ha begärt en påminnelse så är det '
            u'förmodligen någon annan student (IP-adress: %s) som råkat ange fel adress och du kan helt bortse från '
            u'innehållet i det här mailet.\n'
            u'\n'
            u'INLOGGNINGSUPPGIFTER\n'
            u'E-postadress: %s\n'
            u'Personlig kod: %s\n'
            u'\n'
            u'Personlig adress, direkt-inloggning: http://regga.rkh-sk.se/#%s (Obs: Länken är temporär och du kommer '
            u'inte kunna använda den mer än en gång.)\n'
            u'\n'
            u'\n'
            u'BETALNINGSINFORMATION\n'
            u'Om du ännu inte betalat in din medlemsavgift för denna termin så finns två alternativ att tillgå:\n'
            u'1) Inbetalning till studentkårens PlusGiro, 126 98 26-2, eller\n'
            u'2) Kontant betalning hos kårens studentexpedition tisdagar & torsdagar kl. 12-13.\n'
            u'\n'
            u'Avgiften är 100 kr per termin och oavsett betalningssätt är det av högsta vikt att du uppger din '
            u'personliga kod som referens – annars riskerar du att ditt ärende "faller mellan stolarna".\n'
            u'\n'
            u'%s\n'
            u'\n'
            u'\n'
            u'Varma hälsningar,\n'
            u'\n'
            u'Medlemsregistreringen\n'
            u'Röda Korsets Högskolas Studentkår\n'
            u'' % (recipient.firstName, client_ip, recipient.email, recipient.lastPaymentKey, recipient.sys_tmpkey,
                   COMPATIBILITY_PLAINTEXT)
            )

        html = (
            u'<p style="font-size: 12px;">%s,<br></p>'
            u'<p style="font-size: 12px;">Det här är en påminnelse med dina personliga inloggningsuppgifter till '
            u'RKH-SK:s medlemssida, <a href="http://regga.rkh-sk.se" target="_blank">regga.rkh-sk.se</a>. Om du inte '
            u'känner igen att du skulle ha begärt en påminnelse så är det förmodligen någon annan student (IP: %s) '
            u'som råkat ange fel adress och du kan helt bortse från innehållet i det här mailet.<br></p>'
            u'<p style="font-size: 12px;"><u>Inloggningsuppgifter</u>:<br>'
            u'E-postadress: %s<br>'
            u'Personlig kod: %s<br></p>'
            u'<p style="font-size: 12px;">Personlig adress, direkt-inloggning: <a href="http://regga.rkh-sk.se/#%s" '
            u'target="_blank">regga.rkh-sk.se/#%s</a> (Obs: Länken är temporär och du kommer inte kunna använda den '
            u'mer än en gång.)<br><br></p>'
            u'<p style="font-size: 12px;"><u>Betalningsinformation</u>:<br>'
            u'Om du ännu inte betalat in din medlemsavgift för denna termin så finns två alternativ att tillgå:'
            u'<ol style="font-size: 12px;"><li>Inbetalning till studentkårens PlusGiro, 126 98 26-2, eller</li>'
            u'<li>Kontant betalning hos kårens studentexpedition tisdagar & torsdagar kl. 12-13.</li></ol>'
            u'Avgiften är 100 kr per termin och oavsett betalningssätt är det <b>av högsta vikt att du uppger din '
            u'personliga kod som referens</b> – annars riskerar du att ditt ärende "faller mellan stolarna".<br></p>'
            u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
            u'<p style="font-size: 12px;">Varma hälsningar,<br>'
            u'<br>'
            u'Medlemsregistreringen<br>'
            u'Röda Korsets Högskolas Studentkår<br></p>'
            u'' % (recipient.firstName, client_ip, recipient.email, recipient.lastPaymentKey, recipient.sys_tmpkey,
                   recipient.sys_tmpkey, COMPATIBILITY_HTML)
            )

        sentmail = sent_email.send_wrapper(
            to='%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
            subject=u'Inloggningsuppgifter till RKH-SK:s medlemssida',
            body_plaintext=plaintext,
            body_html=html,
            sys_sentby=recipient.personalId,
            sys_creator=client_ip
        )

        if sentmail is False:
            return False
        else:
            recipient.sys_emailstouser.append( sentmail.key( ) )
            recipient.sys_modifieddate = datetime.today( )
            changelog = {
                'action': 'Sent Account Details Reminder Email',
                'reminderSentTo': recipient.email,
                'sys_origin_client_ip': client_ip,
                'sys_modifieddate': recipient.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
            }
            recipient.sys_changelog.append( db.Text( json.dumps( changelog ) ) )
            recipient.put( )


    # TODO(mats.blomdahl@gmail.com): Implementation + unit tests!
    # /tasks/send_member_registration_reminder/
    def __send_member_registration_reminder(self):
        """A task that sends out a reminder to old members in order to inspire them
            to renew their memberships.s

        This task belong to the queue group 'mailer' which contains the individual
          queues mailer0, mailer1, mailer2 and mailer3.

        To use:
        >>> taskqueue.add(
                queue_name = 'mailer'+str(random.randrange(4)),
                url = '/tasks/send_admin_password_reset_email/',
                params = {
                    'personal_id': member.personalId
                }
            )

        Arguments (i.e. HTTP-POST params):
            personal_id: The recipient member's key name/personal id, a string.
        """

        pass


    # /tasks/send_member_activation_notice/
    def __send_member_activation_notice(self):
        """A task that sends out membership activation notifications.

        This task belong to the queue group 'mailer' which contains the individual
            queues mailer0, mailer1, mailer2 and mailer3.

        To use:
        >>> taskqueue.add(
                queue_name = 'mailer'+str(random.randrange(4)),
                url = '/tasks/send_member_activation_notice/',
                params = {
                    'personal_id': member.personalId,
                }
            )

        Arguments (i.e. HTTP-POST params):
            personal_id: The recipient member's key name/personal id, a string.
        """

        recipient = RkhskMember.get_by_key_name( self.request.get( 'personal_id' ) )
        if recipient is None:
            return False

        plaintext = (
            u'%s,\n'
            u'\n'
            u'Du får det här mailet som en bekräftelse på att studentkårens funktionärer har registrerat din '
            u'medlemsansökan till termin %s i RKH-SK:s medlemsdatabas, http://regga.rkh-sk.se. När det blir dags att '
            u'förnya ditt medlemskap för nästa termin återvänder du helt enkelt till http://regga.rkh-sk.se och '
            u'skickar in dina uppgifter pånytt, varpå du erhåller en ny personlig kod kopplad till din ansökan.\n'
            u'\n'
            u'INLOGGNINGSUPPGIFTER FÖR %s\n'
            u'E-postadress: %s\n'
            u'Personlig kod: %s\n'
            u'\n'
            u'För att kontrollera att din inbetalning av medlemsavgiften registrerats och se ärendestatus för din '
            u'medlemsansökan (inkl. Mecenat/SSSB) så räcker det att utföra två enkla steg:\n'
            u'1. Besök registreringssidan http://regga.rkh-sk.se och välj fliken "Ärendestatus"\n'
            u'2. Ange e-postadress och personlig kod ("%s" resp. "%s") och klicka på "Visa ärendestatus"\n'
            u'\n'
            u'%s\n'
            u'\n'
            u'\n'
            u'Varma hälsningar,\n'
            u'\n'
            u'Medlemsregistreringen\n'
            u'Röda Korsets Högskolas Studentkår\n'
            u'' % (recipient.firstName, recipient.lastApplicationFor, recipient.lastApplicationFor, recipient.email,
                   recipient.lastPaymentKey, recipient.email, recipient.lastPaymentKey, COMPATIBILITY_PLAINTEXT)
            )

        html = (
            u'<p style="font-size: 12px;">%s,<br></p>'
            u'<p style="font-size: 12px;">Du får det här mailet som en bekräftelse på att studentkårens funktionärer '
            u'har registrerat din medlemsansökan till termin %s i RKH-SK:s medlemsdatabas, '
            u'<a href="http://regga.rkh-sk.se" target="_blank">regga.rkh-sk.se</a>. När det blir dags att förnya ditt'
            u' medlemskap för nästa termin återvänder du helt enkelt till <a href="http://regga.rkh-sk.se" '
            u'target="_blank">regga.rkh-sk.se</a> och skickar in dina uppgifter pånytt, '
            u'varpå du erhåller en ny personlig kod kopplad till din ansökan.<br></p>'
            u'<p style="font-size: 12px;"><u>Inloggningsuppgifter för %s</u>:<br>'
            u'E-postadress: %s<br>'
            u'Personlig kod: %s<br></p>'
            u'<p style="font-size: 12px;">För att kontrollera att din inbetalning av medlemsavgiften registrerats och'
            u' se ärendestatus för din medlemsansökan (inkl. Mecenat/SSSB) så räcker det att utföra två enkla steg:'
            u'<ol style="font-size: 12px;"><li>Besök registreringssidan http://regga.rkh-sk.se och välj fliken '
            u'"Ärendestatus"</li>'
            u'<li>Ange e-postadress och personlig kod ("%s" resp. "%s") och klicka på "Visa ärendestatus"</li></ol></p>'
            u'<p style="font-size: 11px; color: grey;">%s<br><br></p>'
            u'<p style="font-size: 12px;">Varma hälsningar,<br>'
            u'<br>'
            u'Medlemsregistreringen<br>'
            u'Röda Korsets Högskolas Studentkår<br></p>'
            u'' % (recipient.firstName, recipient.lastApplicationFor, recipient.lastApplicationFor, recipient.email,
                   recipient.lastPaymentKey, recipient.email, recipient.lastPaymentKey, COMPATIBILITY_HTML)
            )

        sentmail = sent_email.send_wrapper(
            to='%s %s <%s>' % (recipient.firstName, recipient.lastName, recipient.email),
            subject=u'Registreringsbekräftelse för RKH-SK:s medlemsdatabas',
            body_plaintext=plaintext,
            body_html=html,
            sys_sentby='su',
            sys_creator='su'
        )

        if sentmail is False:
            return False
        else:
            recipient.sys_emailstouser.append( sentmail.key( ) )
            recipient.sys_modifieddate = datetime.today( )
            changelog = {
                'action': 'Sent Account Activation Notice',
                'reminderSentTo': recipient.email,
                'sys_notifier': 'su',
                'sys_modifieddate': recipient.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
            }
            recipient.sys_changelog.append( db.Text( json.dumps( changelog ) ) )
            recipient.put( )


    # /tasks/admin_broadcast/
    def __admin_broadcast(self):
        """A task that performs a broadcast to the admins currently connected.

        This task belong to the queue 'broadcast' and distribute messages to
            connected admins through App Engine's channel service.

        To use:
        >>> taskqueue.add(
                queue_name = 'broadcaster',
                url = '/tasks/admin_broadcast/',
                params = {
                    'broadcaster' : task.user_id,
                    'timestamp'   : pickle.dumps(datetime.today()),
                    'remote_event': <event name>,
                    'data'        : json.dumps(data)
                }
            )

        Arguments (i.e. HTTP-POST params):
            broadcaster: The broadcaster's key name/personal id, a string.
            timestamp: A pickled datetime instance (UTC).
            remote_event: The event to be broadcasted, one of: 'admin_connected',
                'admin_disconnected', 'member_created', 'member_updated',
                'mecenat_sync_failed', 'mecenat_sync_completed',
                'mecenat_sync_data_downloaded', 'mecenat_sync_initiated',
                'ssco_sync_failed', 'ssco_sync_completed', 'ssco_sync_data_downloaded'
                and 'ssco_sync_initiated'.
            data: Semi-optional payload for the broadcast, as JSON.
        """

        remote_events = [ 'admin_connected', 'admin_disconnected', 'member_created', 'member_updated',
                          'mecenat_sync_failed', 'mecenat_sync_completed', 'mecenat_sync_data_downloaded',
                          'mecenat_sync_initiated', 'ssco_sync_failed', 'ssco_sync_completed',
                          'ssco_sync_data_downloaded', 'ssco_sync_initiated' ]

        broadcaster = self.request.get( 'broadcaster', default_value=None )
        broadcast = {}
        if broadcaster == 'su':
            broadcast.update( {
                'sender': broadcaster,
                'senderUsername': 'mats.blomdahl@gmail.com'
            } )
        elif broadcaster == 'anonymous':
            broadcast.update( {
                'sender': broadcaster,
                'senderUsername': None
            } )
        else:
            member = RkhskMember.get_by_key_name( broadcaster )
            if member is None:
                logging.warning( 'admin_broadcast: Fake broadcast sent from user \'%s\'. Aborting...' % broadcaster )
                return False
            broadcast[ 'sender' ] = u'%s %s' % (member.firstName, member.lastName)
            broadcast[ 'senderUsername' ] = member.email

        timestamp = str( self.request.get( 'timestamp', default_value=None ) )
        try:
            timestamp = pickle.loads( timestamp )
        except:
            logging.warning(
                'admin_broadcast: Broken task (missing timestamp) sent from user \'%s\' (timestamp=%s). Aborting...' % (
                    broadcaster, str( timestamp )) )
            return False

        remote_event = self.request.get( 'remote_event', default_value=None )
        if remote_event is None or remote_event in remote_events is False:
            logging.warning(
                'admin_broadcast: Broken task (missing remote_event) sent from user \'%s\' (remote_event=%s). '
                'Aborting...' % (
                    broadcaster, str( remote_event )) )
            return False


        #message = self.request.get('message', default_value = None)

        broadcast.update( {
            'timestamp': timestamp.isoformat( ).split( '.' )[ 0 ],
            'remoteEvent': remote_event
        } )
        if remote_event == 'member_created' or remote_event == 'member_updated':
            data = self.request.get( 'data', default_value=None )
            if data is None:
                logging.warning(
                    'admin_broadcast: Broken task, \'category=%s\' but missing \'data\' param sent from user \'%s\'. '
                    'Aborting...' % (
                        remote_event, broadcaster) )
                return False
            else:
                broadcast.update( {
                    'data': json.loads( data )
                } )
        elif remote_event == 'admin_disconnected':
            pass
        elif remote_event == 'admin_connected':
            pass
        elif remote_event == 'mecenat_sync_initiated' or remote_event == 'mecenat_sync_data_downloaded' or\
             remote_event == 'mecenat_sync_completed' or remote_event == 'mecenat_sync_failed':
            data = self.request.get( 'data', default_value=None )
            if data is None:
                logging.warning(
                    'admin_broadcast: Broken task, \'category=%s\' but missing \'data\' param sent from user \'%s\'. '
                    'Aborting...' % (
                        remote_event, broadcaster) )
                return False
            else:
                broadcast.update( json.loads( data ) )
        elif remote_event == 'ssco_sync_initiated' or remote_event == 'ssco_sync_data_downloaded' or remote_event == 'ssco_sync_completed' or remote_event == 'ssco_sync_failed':
            data = self.request.get( 'data', default_value=None )
            if data is None:
                logging.warning(
                    'admin_broadcast: Broken task, \'category=%s\' but missing \'data\' param sent from user \'%s\'. Aborting...' % (
                        remote_event, broadcaster) )
                return False
            else:
                broadcast.update( json.loads( data ) )

        json_broadcast = json.dumps( broadcast )
        recipients = memcache.get( CONNECTED_ADMINS )
        if recipients is not None:
            for recipient in recipients:
                try:
                    channel.send_message( recipient, json_broadcast )
                except:
                    logging.warning(
                        'admin_broadcast: Message recipient \'%s\' unavailable (message=%s). Working on...' % (
                            recipient, message) )


    # /tasks/save_mecenat_transfer/
    def __save_mecenat_transfer(self):
        """A task that saves successful transfer-to-Mecenat operations to the
            datastore.

        This task belong to the queue group 'mecenatupdater' which contains the
            individual queues mecenatupdater0, mecenatupdater1, mecenatupdater2 and
            mecenatupdater3.

        To use:
        >>> taskqueue.add(
                queue_name = 'mecenatupdater'+str(random.randrange(4)),
                url = '/tasks/save_mecenat_transfer/',
                params = {
                    'key'      : str(task.key()),
                    'timestamp': pickle.dumps(datetime.today()),
                    'updater'  : task.user_id
                }
            )

        Arguments (i.e. HTTP-POST params):
            key: The datastore task descriptor for the update performed, a string.
            timestamp: A pickled datetime instance (UTC).
            updater: The updating admin's key name (i.e. personal ID), a string.
        """

        key = self.request.get( 'key', default_value=None )
        timestamp = pickle.loads( str( self.request.get( 'timestamp' ) ) )
        updater = self.request.get( 'updater', default_value=None )

        if key is None:
            logging.info( 'save_mecenat_transfer: \'key\' parameter is None. Aborting...' )
            return False
        task = mecenat_push_task.get( key )
        if task is None:
            logging.info( 'save_mecenat_transfer: No task resolved to \'key\' parameter. Aborting...' )
            return False

        logging.info( 'save_mecenat_transfer: Successfully added Mecenat transfer for member %s, updated by %s.' % (
            task.personalId, updater) )
        task.sys_pushsuccesses.append( timestamp )
        task.save_success_state( )


    # /tasks/save_mecenat_transfer/
    def __save_ssco_transfer(self):
        """A task that saves successful transfer-to-SSCO operations to the
            datastore.

        This task belong to the queue group 'sscoupdater' which contains the
            individual queues sscoupdater0, sscoupdater1, sscoupdater2 and
            sscoupdater3.

        To use:
        >>> taskqueue.add(
                queue_name = 'sscoupdater'+str(random.randrange(4)),
                url = '/tasks/save_ssco_transfer/',
                params = {
                    'key'      : str(task.key()),
                    'timestamp': pickle.dumps(datetime.today()),
                    'updater'  : task.user_id
                }
            )

        Arguments (i.e. HTTP-POST params):
            key: The datastore task descriptor for the update performed, a string.
            timestamp: A pickled datetime instance (UTC).
            updater: The updating admin's key name (i.e. personal ID), a string.
        """

        key = self.request.get( 'key', default_value=None )
        timestamp = pickle.loads( str( self.request.get( 'timestamp' ) ) )
        updater = self.request.get( 'updater', default_value=None )

        if key is None:
            logging.info( 'save_ssco_transfer: \'key\' parameter is None. Aborting...' )
            return False
        task = ssco_pull_task.get( key )
        if task is None:
            logging.info( 'save_ssco_transfer: No task resolved to \'key\' parameter. Aborting...' )
            return False

        logging.info( 'save_ssco_transfer: Successfully added SSCO transfer for member %s, updated by %s.' % (
            task.personalId, updater) )
        task.sys_pullsuccesses.append( timestamp )
        task.save_success_state( )


    # /tasks/store_session_trace/
    def __store_session_trace(self):
        """A task that queues rapid session trace save operations.

        This task belong to the queue group 'sessiontrace' which contains the
            individual queues 'sessiontrace0', 'sessiontrace1', ..., 'sessiontrace9'.

        To use:
        >>> taskqueue.add(
                queue_name = 'sessiontrace'+random.randrage(10),
                url = '/tasks/store_session_trace/',
                params = {
                    'key_name' : str(session_trace_instance.key_name),
                    'timestamp': pickle.dumps(datetime.today())
                },
                countdown = 60
            )


        Arguments (i.e. HTTP-POST params):
            key_name: The instance key name, a string.
            timestamp: A pickled datetime instance (UTC).
        """

        key_name = self.request.get( 'key_name', default_value=None )
        if key_name is not None:
            session_trace = memcache.get( 'session%s' % key_name )
            if session_trace is not None:
                session_trace.save( )
            else:
                logging.info(
                    'RkhskTasks.__store_session_trace(): Task execution failed! Memcache object unavailable.' )
        else:
            logging.info( 'RkhskTasks.__store_session_trace(): Task execution failed! No key supplied.' )
            return False


    # /tasks/apply_changelog_update/
    def __apply_changelog_update(self):
        """A task that queues rapid changelog updates to select model instances.

        This task belong to the queue group 'changelogger' which contains the
            individual queues 'systmpkeychangelogger' and 'rkhskmemberchangelogger'.

        To use:
        >>> taskqueue.add(
                queue_name = 'systmpkeychangelogger',
                url = '/tasks/apply_changelog_update/',
                params = {
                    'key'      : str(model_instance.key()),
                    'timestamp': pickle.dumps(datetime.today()),
                    'updates'  : json.dumps(data)
                },
                countdown = 10
            )


        Arguments (i.e. HTTP-POST params):
            key: The datastore task descriptor for the update performed, a string.
            timestamp: A pickled datetime instance (UTC).
            updates: The updates to be applied, as JSON.
        """

        supported_models = [ 'SysTmpKey', 'RkhskMember' ]
        model = self.request.get( 'model', default_value=None )
        key = self.request.get( 'key', default_value=None )
        updates = self.request.get( 'updates', default_value=None )

        if model in supported_models:
            model = eval( model )
            instance = model.get( key )
            instance.sys_changelog.append( db.Text( updates ) )
            instance.put( )
        else:
            return False

