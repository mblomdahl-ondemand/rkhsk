# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.sys.EmailInterface
# Author:  Mats Blomdahl
# Version: 2012-03-08

import webapp2
from webapp2_extras import sessions
import json
import logging
import datetime
from email.utils import parsedate_tz, mktime_tz
from google.appengine.api import mail
from google.appengine.ext import db


# TODO(mats.blomdahl@gmail.com): Expand functionality and REST capabilities of /admin/mail/ backend.
from rkhsk.model.generic.email import inbound_email

class EmailInterface( webapp2.RequestHandler ):
    """An interface for receiving inbound mail and, during development, dumping
        a JSON array with the received emails.

    The HTTP-GET handler deploys secure cookies using the 'default' store,
        expiration is controlled by the global variable ADMIN_COOKIE_EXPIRATION.

    The HTTP-POST handler is secured by App Engine's built-in authentication
        support through the app.yaml config.
    """

    # session management
    @webapp2.cached_property
    def session(self):
        # Returns a session using the default cookie key.
        _ADMIN_COOKIE_EXPIRATION = self.app.config.get( '_ADMIN_COOKIE_EXPIRATION' )
        return self.session_store.get_session( name='default', max_age=_ADMIN_COOKIE_EXPIRATION,
                                               backend='securecookie' )


    # /admin/mail
    def get(self, item):
        """Handler for dumping the emails stored in the 'InboundEmail' entries.

        Arguments:
            item: Currently not in use.

        Returns:
            A JSON array containing the emails in 'InboundEmail'.
        """

        self.session_store = sessions.get_store( request=self.request )
        if verify_admin( self.session.get( 'admin' ) ) is False:
            return {
                'success': False,
                'status_code': 403, # forbidden
                'body': False
            }
        email_data = [ ]
        emails = inbound_email.all( )
        for email in emails:
            email_data.append( email.dumps( ) )
        response_data = {
            'data': {'InboundEmails': email_data}
        }

        self.response.content_type = 'application/json'
        self.response.body = json.dumps( response_data )

        self.session_store.save_sessions( self.response )


    # /_ah/mail/
    def post(self, email_address):
        """Handler for redirecting HTTP-POST requests with inbound mail."""

        self.__receive( mail.InboundEmailMessage( self.request.body ) )

    def __receive(self, message):
        """Handler for recieving and storing inbound email to 'InboundEmail'.

        Arguments:
            message: The email message object.
        """

        logging.info(
            "RkhskInboundEmail.__recieve(message): Recieved a message from %r to %r." % (message.sender, message.to) )

        received = inbound_email( )
        logging.info( 'message: %s' % dir( message ) )
        if hasattr( message, 'to' ):
            received.to = unicode( message.to )
        if hasattr( message, 'sender' ):
            received.sender = unicode( message.sender )
        if hasattr( message, 'cc' ):
            received.cc = unicode( message.cc )
        if hasattr( message, 'date' ):
            logging.info( 'RkhskInboundEmail.__recieve(message): Date %s received.' % message.date )
            try:
                received.date = datetime.fromtimestamp( mktime_tz( parsedate_tz( message.date ) ) )
            except:
                logging.info( 'RkhskInboundEmail.__recieve(message): Faulty date format received, %r.' % message.date )
        if hasattr( message, 'reply_to' ):
            received.reply_to = unicode( message.reply_to )

        if hasattr( message, 'bodies' ):
            plaintext_bodies = message.bodies( 'text/plain' )
            if hasattr( plaintext_bodies, '__iter__' ):
                for content_type, body in plaintext_bodies:
                    received.body_plaintext_type.append( str( content_type ) )
                    received.body_plaintext_content.append( db.Text( body.decode( ) ) )

            html_bodies = message.bodies( 'text/html' )
            if hasattr( html_bodies, '__iter__' ):
                for content_type, body in html_bodies:
                    received.body_html_type.append( str( content_type ) )
                    received.body_html_content.append( db.Text( body.decode( ) ) )

        if hasattr( message, 'attachments' ):
            for name, content in message.attachments:
                received.attachment_filenames.append( unicode( name ) )
                received.attachment_filecontents.append( db.Blob( content.payload ) )

        received.original = db.Blob( message.original.as_string( ) )
        received.put( )
