# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.sys.ChannelInterface
# Author:  Mats Blomdahl
# Version: 2012-03-08

import webapp2
from webapp2_extras import sessions
import json
import logging
import random
import pickle
import datetime
import time
from google.appengine.api import taskqueue
from google.appengine.api import channel
from google.appengine.api import memcache

from rkhsk.model import SysConfig


class ChannelInterface( webapp2.RequestHandler ):
    """The public interface for setting up channels as well as handling system
        events such as successful connects and disconnects.

    The HTTP-GET handler deploys secure cookies using the 'default' store,
        expiration is controlled by the global variable ADMIN_COOKIE_EXPIRATION.

    The HTTP-POST handler is secured by App Engine's built-in authentication
        support through the app.yaml config.
    """

    # session management
    @webapp2.cached_property

    def session(self):
        # Returns a session using the default cookie key.
        _ADMIN_COOKIE_EXPIRATION = self.app.config.get( '_ADMIN_COOKIE_EXPIRATION' )
        return self.session_store.get_session( name='default', max_age=_ADMIN_COOKIE_EXPIRATION,
                                               backend='securecookie' )


    # /_ah/channel/<connected|disconnected>/
    def post(self, command):
        """Method for receiving connect/disconnect events from the GAE channel.

        The channel handler will be the user ID read from the clients secure
            cookie (typically a personal identification number).

        Arguments:
            command: GAE will submit a HTTP-POST to the application with one of the
                two commands 'connected' and 'disconnected'.
            from: A parameter from the channel service containing the clients unique
                client ID.
        """

        self._CONNECTED_ADMINS = self.app.config.get( '_CONNECTED_ADMINS' )
        user_id = self.request.get( 'from' )

        logging.info( 'RkhskChannel.post(command=\'%s\').user_id: \'%s\'' % (command, str( user_id )) )

        connected = memcache.get( self._CONNECTED_ADMINS )
        if connected is None:
            connected = [ ]
        else:
            try:
                index = connected.index( user_id )
                connected.pop( index )
            except (ValueError):
                pass

        if command == 'connected' and SysConfig.verify_as_admin( user_id ):
            connected.append( user_id )
            taskqueue.add(
                queue_name='broadcaster',
                url='/tasks/admin_broadcast/',
                params={
                    'broadcaster': user_id,
                    'timestamp': pickle.dumps( datetime.datetime.today( ) ),
                    'remote_event': 'admin_connected'
                }
            )

        elif command == 'disconnected':
            taskqueue.add(
                queue_name='broadcaster',
                url='/tasks/admin_broadcast/',
                params={
                    'broadcaster': user_id,
                    'timestamp': pickle.dumps( datetime.datetime.today( ) ),
                    'remote_event': 'admin_disconnected'
                }
            )

        else:
            self.error( 500 )
            return

        memcache.set( self._CONNECTED_ADMINS, connected, 3600 )
