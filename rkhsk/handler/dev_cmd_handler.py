# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.DevCmdHandler
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging, webapp2

from webapp2_extras import sessions

# TODO(mats.blomdahl@gmail.com): Review need of handler.DevCmdHandler!
class DevCmdHandler( webapp2.RequestHandler ):
    """A request handler providing short-hand access to trigger special tasks."""

    # session management
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store( request=self.request )

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch( self )
        finally:
            # Save all sessions.
            self.session_store.save_sessions( self.response )

    @webapp2.cached_property
    def session(self):
        # Returns a session using the default cookie key.
        return self.session_store.get_session( name='default', backend='securecookie' )


    # /clear_cookies
    def get(self, param):
        """Request handler for serving the web application.

        Arguments:
            param: Supported values are '' (implies path '/') and 'admin' (implies
                path '/admin').
        """

        logging.info( 'DevCmdHandler hit!' )

        if param == 'clear_cookies':
            self.response.write( 'session cookies cleared' )
            self.session.clear( )
        else:
            self.response.write( 'unknown dev command' )
            self.error( 404 )
        return


