# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.admin.AdminApiInterface
# Author:  Mats Blomdahl
# Version: 2012-08-27

import re, json, logging, pickle, string

from random import randrange
from webapp2_extras import sessions
from webapp2 import RequestHandler, cached_property
from time import time
from datetime import datetime, timedelta, date

from google.appengine.api import taskqueue, blobstore
from google.appengine.ext import db
from google.appengine.api import channel
from google.appengine.api import memcache

from rkhsk.handler.admin import AdminCommand
from rkhsk.handler.generic import UniHandler

from rkhsk.model import  SscoPullTask, OwaUser, SysConfig
from rkhsk.model import MecenatPushTask
from rkhsk.model.generic import TempFile, SessionTrace
from rkhsk.model.generic.email import SentEmail, InboundEmail
from rkhsk.model import RkhskMember, AccountEmail, AccountUri, PaymentKey, SysTmpKey

class AdminApiInterface( RequestHandler ):
    """The public interface for the admin UI at http://regga.rkh-sk.se/admin.

    The request handler routes all incoming request through self.__unihandler
        where a AdminCommand is set up and the appropriate method is called.

    The handler deploys secure cookies using the 'default' store and cookie
        expiration is controlled by the global variable ADMIN_COOKIE_EXPIRATION.
    """

    # Session Management
    def dispatch(self):
        """Get a session store for this request.

        Returns:
            None
        """

        self.session_store = sessions.get_store( request=self.request )

        try:
            # Dispatch the request.
            RequestHandler.dispatch( self )
        finally:
            # Save all sessions.
            self.session_store.save_sessions( self.response )

    @cached_property
    def session(self):
        """Returns a session using the default cookie key.

        Returns:
            Foo.
        """

        _ADMIN_COOKIE_EXPIRATION = self.app.config.get( '_ADMIN_COOKIE_EXPIRATION' )
        return self.session_store.get_session( name='default', max_age=_ADMIN_COOKIE_EXPIRATION,
                                               backend='securecookie' )

    # Redirects
    def post(self, service=None, cmd=None, request_id=None):
        """Handler for redirecting HTTP-POST requests to the admin interface.

        Arguments:
            service:
            cmd:
            request_id:

        Returns:
            Foo.
        """

        return self.__unihandler( 'POST', service, cmd, request_id )

    def get(self, service=None, cmd=None, request_id=None):
        """Handler for redirecting HTTP-GET requests to the admin interface.

        Arguments:
            service:
            cmd:
            request_id:

        Returns:
            Foo.
        """

        return self.__unihandler( 'GET', service, cmd, request_id )

    # Interface: /admin/<service>/<cmd>/<request_id>
    def __unihandler(self, method, service, cmd, request_id):
        """Method for delegating requested functionality to the appropriate service.

        Arguments:
            method: The HTTP method used (i.e. 'GET' or 'POST').
            service: The main functionality requested.
            cmd: An optional, service-specific, command.
            request_id: An optional identifier for channeled requests.
        """

        starttime = time( )

        _DEVMODE = self.app.config.get( '_DEVMODE' )

        command = AdminCommand(
            handler_instance=self,
            method=method,
            service=service,
            cmd=cmd,
            request_id=request_id,
            session_id=self.session.get( 'session' ),
            user_id=self.session.get( 'admin' ),
            dev_mode=_DEVMODE
        )

        logging.info( 'command: %s' % str( command.dumps( ) ) )

        unihandler = UniHandler( handler_name='RkhskAdminInterface.__unihandler()', command=command, dev_mode=_DEVMODE )

        unihandler.handle_request( )

        unihandler.serve_response( loadtime={'loadTime': '%s ms' % str( int( (time( ) - starttime) * 1000 ) )} )

    # Interface: /admin/update_members/
    def update(self, command):
        """Method for performing updates of union member data.

        Arguments:
            json_data: A JSON array passed through AdminCommand.

        Returns:
            A dict with success status and the updated members count.
        """

        command.set_action( 'admin_member_update' )
        command.set_description( 'Member update event from the admin gridview.' )
        command.set_valid_keys( [
            'lastApplicationFor',
            'firstName',
            'lastName',
            'streetCo',
            'streetAddress',
            'streetNumber',
            'streetEntrance',
            'streetApartment',
            'streetFloor',
            'zipCode',
            'city',
            'email',
            'phone',
            'lastPaymentRecieved',
            'lastApplicationDate',
            'sys_administrator',
            'sys_accounttype'
        ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        results = RkhskMember.create_or_update( command )
        if results:
            results.update( {'success': True} )
            return results
        else:
            return {
                'success': False,
                'status_code': 400 # bad request
            }

    # Interface: /admin/upload/<payments|attachment>/<request_id>
    def upload(self, command):
        """Method for controlling payment registration.

        Arguments:
            command.request_id: An unique identifier for the request (through AdminCommand).
            command.payload: A JSON array passed through AdminCommand.

        Returns:
            An upload token.
        """

        command.set_action( 'admin_upload_ticket' )
        command.set_description( 'Create blob upload token.' )
        command.set_valid_keys( [
            'sys_accounttype',
            'sys_administrator',
            'lastApplicationFor',
            'personalId',
            'firstName',
            'lastName',
            'lastApplicationDate'
        ] )
        command.set_date_format( '%Y-%m-%d' )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'errors': {
                    'accessDenied': 'The user does not have sufficient privileges to call /admin/upload/%s/%s.' % (
                        str( command.cmd  ), str( command.request_id ))}
            }

        if not command.cmd in ['payments', 'attachment']:
            return {
                'success': False,
                'status_code': 400,
                'errors': {
                    'invalidRequest': 'The call to /admin/upload/%s/%s does not match the interface specification.' % (
                        str( command.cmd ), str( command.request_id ))}
            }

        upload_token = blobstore.create_upload_url( '/admin/upload/%s/%s' % (command.cmd, command.request_id) )

        return {
            'success': True,
            'status_code': 200,
            'data': {'uploadToken': upload_token}
        }



    # Interface: /admin/verify_payments/
    def verify_payments(self, command):
        """Method for controlling payment registration.

        Arguments:
            request_id: An unique identifier for the request (through AdminCommand).
            json_data: A JSON array passed through AdminCommand.

        Returns:
            Two channeled JSON arrays with matched and unmatched payments and a HTTP
                response with matched/unmatched count.
        """

        command.set_action( 'admin_payments_verification' )
        command.set_description( 'Payment verification event from the admin payments input field.' )
        command.set_valid_keys( [
            'sys_accounttype',
            'sys_administrator',
            'lastApplicationFor',
            'personalId',
            'firstName',
            'lastName',
            'lastApplicationDate'
        ] )
        command.set_date_format( '%Y-%m-%d' )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if type(command.payload) is not list:
            command.payload = [command.payload]
        mismatched = [ ]
        matched = [ ]

        for payment in command.payload:
            payment[ 'lastPaymentKey' ] = payment[ 'lastPaymentKey' ].upper( )
            paymentcon = PaymentKey.get_by_key_name( payment[ 'lastPaymentKey' ] )
            if paymentcon is None:
                payment.update( {'payment': '100 kr'} )
                mismatched.append( payment )
            else:
                logging.info( 'matched %s' % paymentcon.memberRef.personalId )
                entry = {
                    'lastPaymentRecieved': payment[ 'lastPaymentRecieved' ],
                    'lastPaymentKey': payment[ 'lastPaymentKey' ],
                    'payment': '100 kr'
                }
                entry.update( command.get_member_data( paymentcon.memberRef ) )
                matched.append( entry )
        logging.info( 'command.user_id: %s, command.request_id: %s' % (command.user_id, command.request_id) )
        channel.send_message( command.user_id, json.dumps( {
            'itemsRecognized': matched,
            'itemsUnidentified': mismatched,
            'requestId': command.request_id,
            } ) )

        return {
            'success': True,
            'data': {'paymentsRecognized': len( matched ), 'paymentsUnidentified': len( mismatched )}
        }

    # Interface: /admin/update_payments/
    def update_payments(self, command):
        """Method for performing the actual update of payments recieved, post-validation.

        Arguments:
            json_data: A JSON array passed through AdminCommand.

        Returns:
            A HTTP response with payments registered count.
        """

        command.set_action( 'admin_payments_update' )
        command.set_description( 'Payment update event from the admin payments input field.' )
        command.set_valid_keys( [
            'lastPaymentRecieved',
            'lastPaymentKey',
            'personalId'
        ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        result = RkhskMember.create_or_update( command )
        if result:
            result.update( {'success': True} )
            return result
        else:
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

    # Interface: /admin/read/
    def read(self, command):
        """Method for serving admins with account information.

        Arguments:
            param_data['start']: The query 'start' param passed through AdminCommand.
            param_data['limit']: The query 'limit' param passed through AdminCommand.
            param_data['page']: The query 'page' param passed through AdminCommand.
            param_data['sort']: The query 'sort' param passed through AdminCommand
                as a JSON array.
            param_data['filter']: The query 'filter' param passed through
                AdminCommand as a JSON array.

        Returns:
            An array containing member details.
        """

        command.set_action( 'admin_member_read' )
        command.set_description( 'Read members page by page.' )
        command.set_valid_keys( [
            'firstName',
            'lastName',
            'streetCo',
            'streetAddress',
            'streetNumber',
            'streetEntrance',
            'streetApartment',
            'streetFloor',
            'zipCode',
            'city',
            'email',
            'phone',
            'sys_administrator',
            'sys_accounttype',
            'lastPaymentKey',
            'lastApplicationDate',
            'lastApplicationFor',
            'lastMembershipFor',
            'lastPaymentRecieved',
            'lastMecenatReg',
            'lastSscoReg'
        ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        try:
            page = command.payload[ 'page' ]
        except (KeyError):
            page = None

        try:
            start = command.payload[ 'start' ]
        except (KeyError):
            start = None

        try:
            limit = command.payload[ 'limit' ]
        except (KeyError):
            limit = None

        try:
            sort = command.payload[ 'sort' ]
        except (KeyError):
            sort = None

        try:
            filter = command.payload[ 'filter' ]
        except (KeyError):
            filter = None

        logging.info(
            'RkhskAdminInterface.__read_members(page = %s, start = %s, limit = %s, sort = %s, filter = %s)' % (
                str( page ), str( start ), str( limit ), str( sort ), str( filter )) )

        query = "SELECT * FROM RkhskMember "

        if filter:
            query += 'WHERE %s IN (%r, %r) ' % (filter[ 0 ].property, 'news', 'press')
        logging.info(
            'RkhskAdminInterface.__read_members(page, start, limit, sort, filter).query (post-filter): %s' % query )

        sortorder = [ ]
        for i in range( len( sort ) ):
            sortorder.append( '%s %s ' % (sort[ i ][ 'property' ], sort[ i ][ 'direction' ]) )
        if sortorder:
            query += 'ORDER BY %s' % string.join( sortorder, '' )
        logging.info(
            'RkhskAdminInterface.__read_members(page, start, limit, sort, filter).query (post-sort): %s' % query )

        try:
            if not start:
                raise ValueError( '\'start\' param missing' )
            if not limit:
                raise ValueError( '\'limit\' param missing' )
            if not page:
                raise ValueError( '\'page\' param missing' )
            query += 'LIMIT %i,%i ' % (page*limit, int( limit ))
            logging.info(
                'RkhskAdminInterface.__read_members(page, start, limit, sort, filter).query (post-limit): %s' % query )
        except (TypeError, ValueError):
            pass

        query = query.rstrip( )
        logging.info( 'RkhskAdminInterface.__read_members(page, start, limit, sort, filter).query: %s' % query )
        memcache_src = False

        members = [ ]

        # first try memcache
        if memcache.get( 'speedparams_list_is_dirty' ) is not True:
            memberlist = memcache.get( 'speedparams_%s' % query )
            if memberlist is not None:
                rawmembers = memcache.get_multi( memberlist, key_prefix='speedparams_' )
                for i in range( len( memberlist ) ):
                    try:
                        members.append( rawmembers[ memberlist[ i ] ] )
                    except (KeyError):
                        logging.info( 'exception thrown' )
                        member = command.get_member_data( memberlist[ i ] )
                        memberdata = member.dumps( command.valid_keys )
                        memcache.set( 'speedparams_%s' % memberlist[ i ], memberdata )
                        members.append( memberdata )

                memcache_src = True

        # if not sufficient, use gds
        if memcache_src is False:
            collection = db.GqlQuery( query )
            mapping = {}
            personal_ids = [ ]
            for member in collection:
                data = member.dumps( command.valid_keys )
                members.append( data )
                mapping[ data[ 'personalId' ] ] = data
                personal_ids.append( data[ 'personalId' ] )

            logging.info( 'memberlist: %s' % personal_ids )
            mapping[ 'list_is_dirty' ] = False
            mapping[ query ] = personal_ids

            memcache.set_multi( mapping, key_prefix='speedparams_' )

        return {
            'success': True,
            'data': members,
            'membersRead': len( members ),
            'memberCount': SysConfig.get_current_member_count( ),
            'memcacheSrc': memcache_src,
        }

    # Interface: /admin/create/
    def create(self, command):
        """Method for validating and creating new members from the admin UI.

        The following fields are enforced when creating a new member: 'firstName',
            'lastName', 'lastApplicationFor', 'personalId', 'streetAddress',
            'streetNumber', 'zipCode', 'city', 'country' and 'studentConfirmation'.

        Arguments:
            param_data: A member details form passed through AdminCommand.
        """

        command.set_action( 'admin_member_create' )
        command.set_description( 'Member created from the admin "Add new member" panel.' )
        command.set_valid_keys( [
            'personalId',
            'admittedSemester',
            'lastApplicationFor',
            'firstName',
            'lastName',
            'streetCo',
            'streetAddress',
            'streetNumber',
            'streetEntrance',
            'lastPaymentRecieved',
            'streetApartment',
            'streetFloor',
            'zipCode',
            'city',
            'country',
            'email',
            'phone',
            'studentConfirmation'
        ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.payload[ 'studentConfirmation' ]:
            command.payload[ 'studentConfirmation' ] = True
        else:
            command.payload[ 'studentConfirmation' ] = False

        if 'email' in command.payload:
            command.payload[ 'email' ] = command.payload[ 'email' ].lower( )

        if 'admittedSemester' in command.param_data:
            command.param_data[ 'admittedSemester' ] = command.param_data[ 'admittedSemester' ].upper( )

        command.payload['streetNumber'] = str(command.payload['streetNumber'])
        command.payload['zipCode'] = str(command.payload['zipCode'])
        command.payload['streetApartment'] = str(command.payload['streetApartment'])

        if command.payload[ 'phone']:
            command.payload[ 'phone' ] = str(command.payload[ 'phone' ])

        required_fields = [
            'lastApplicationFor',
            'personalId',
            'firstName',
            'lastName',
            'streetAddress',
            'streetNumber',
            'zipCode',
            'city',
            'country',
            'studentConfirmation'
        ]

        for field in required_fields:
            if not command.payload[field]:
                return {
                    'success': False,
                    'status_code': 400,
                    'errors': {'invalid_arguments': 'One or more required arguments are missing.'}
                }

        member_data = command.payload
        command.payload = [command.payload]

        result = RkhskMember.create_or_update( command )

        if result:
            result.update( {'success': True, 'member_data': member_data} )
            return result
        else:
            return {
                'success': False,
                'status_code': 400, # bad request
                'errors': {'system': 'RkhskMember.create_or_update(command) failed.'}
            }


    # /admin/channel/<create>
    def channel(self, command):
        """Method for setting up a real-time channel to an authenticated user.

        The channel handler will be the user ID read from the clients secure
            cookie (typically a personal identification number).

        Arguments:
            comman.cmd: Currently accepts only one value, 'create'.

        Returns:
            A JSON object with a 'channelToken' property used for connecting to the
                GAE channel service.
        """

        command.set_action( 'admin_channel_create' )
        command.set_description( 'GAE Channel created.' )


        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.cmd == 'create':
            return {
                'success': True,
                'data': {'channel_token': channel.create_channel( command.user_id ) }
            }


    # Interface: /admin/login/
    def login(self, command):
        """Method for validating admin login credentials.

        This method will evaluate login credentials and update the list of active
            admins on a HTTP POST request. After the session has been opened the
            admin will send recurrent HTTP GET requests to the method in order to
            refresh the secure cookie and make sure the list of active admins is up-
            to-date.

        Arguments:
            param_data['loginUsername']: The admin's email (through AdminCommand).
            param_data['loginPassword']: The admin's password (through AdminCommand).
            method: The HTTP request method (i.e. 'GET' or 'POST').
        """

        _CONNECTED_ADMINS = self.app.config.get( '_CONNECTED_ADMINS' )

        def update_connected_admins(personal_id):
            if personal_id == 'su':
                personal_id = '841021-7171'
            connected = memcache.get( _CONNECTED_ADMINS )
            if connected is None:
                connected = {}
            else:
                newdict = {}
                for admin in connected:
                    if (now - connected[ admin ]).seconds < 3600:
                        newdict.update( {admin: connected[ admin ]} )
                    else:
                        logging.info(
                            'RkhskAdminInterface.__login(command): Removed %s from memcache connected admins.' % admin )
                connected = newdict
            connected.update( {personal_id: now} )
            memcache.set( _CONNECTED_ADMINS, connected, 3600 )


        command.set_action( 'admin_login' )
        command.set_description( 'An admin login event.' )
        command.set_valid_keys( [ ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'errors': {'invalidSession': 'The originating session is invalid'}
            }

        now = datetime.today( )
        expires = now + timedelta( minutes=10 )

        #connected = memcache.get(CONNECTED_ADMINS)
        #logging.info('RkhskAdminInterface.__login(command).connected_admins: %s' % str(connected))

        if command.method == 'GET':
            logging.info( 'RkhskAdminInterface.__login(command).user_id: %s' % command.user_id )
            if not command.from_verified_admin( ):
                return {
                    'success': False,
                    'status_code': 403,
                    'errors': {'invalidCredentials': 'Invalid login credentials recieved.'}
                }
            else:
                update_connected_admins( command.user_id )
                self.session[ 'admin' ] = command.user_id
                return {
                    'success': True,
                    'data': {'cookieExpires': expires.isoformat( ).split( '.' )[ 0 ]}
                }

        try:
            email = command.param_data[ 'loginUsername' ]
            password = command.param_data[ 'loginPassword' ]
            logging.info( 'RkhskAdminInterface.__login(command).credentials: %s/%s' % (email, password) )
            if len( email ) < 7 or len( password ) < 6:
                raise ValueError( 'Invalid parameters recieved' )
        except (KeyError, ValueError):
            return {
                'success': False,
                'status_code': 400,
                'errors': {'malformedCredentials': 'Malformed login credentials recieved.'}
            }

        # first, try su login
        if email == 'regga@rkh-sk.se' and password == 't3nr3tni':
            update_connected_admins( 'su' )
            SysConfig.update( sys_admins=('refresh', 'su') )
            self.session[ 'admin' ] = 'su'
            return {
                'success': True,
                'data': {'cookieExpires': expires.isoformat( ).split( '.' )[ 0 ]}
            }

        # then, try admin login
        email_connector = AccountEmail.get_by_key_name( email )
        if email_connector is not None:
            admin = email_connector.memberRef
            if SysConfig.verify_as_admin(
                admin.personalId ) and admin.sys_password == password and admin.email == email:
                update_connected_admins( admin.personalId )
                SysConfig.update( sys_admins=('refresh', admin.personalId) )
                self.session[ 'admin' ] = admin.personalId
                log_entry = db.Text( json.dumps( {
                    'admin_login': now.isoformat( ).split( '.' )[ 0 ]
                } ) )
                admin.sys_activitylog.append( log_entry )
                admin.put( )
                return {
                    'success': True,
                    'data': {'cookieExpires': expires.isoformat( ).split( '.' )[ 0 ]}
                }

        return {
            'success': False,
            'status_code': 403,
            'errors': {'invalidCredentials': 'Invalid login credentials recieved.'}
        }

    # Interface: /admin/logout/
    def logout(self, command):
        """Method for clearing the user cookies in order to close the session.

        Arguments:
            user_id: The user ID stored in the secure cookie (through AdminCommand).

        Returns:
            A redirect to the domain root.
        """

        _CONNECTED_ADMINS = self.app.config.get( '_CONNECTED_ADMINS' )

        command.set_action( 'admin_logout' )
        command.set_description( 'An admin logout event.' )
        command.set_valid_keys( [ ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

        now = datetime.today( ).isoformat( ).split( '.' )[ 0 ]

        if command.user_id is not None:
            connected_admins = memcache.get( _CONNECTED_ADMINS )
            activitylog_msg = None
            if connected_admins is None:
                connected_admins = {}
            else:
                try:
                    connected_admins.pop( command.user_id )
                    logging.info(
                        'RkhskAdminInterface.__logout(command): Removed \'%s\' from memcache connected admins.' %
                        command.user_id )
                    activitylog_msg = {'admin_logout': now}

                except (KeyError):
                    logging.info(
                        'RkhskAdminInterface.__logout(command): User \'%s\' not found in memcache connected admins.'
                        % command.user_id )

            if not activitylog_msg:
                activitylog_msg = {'admin_logout_dirty': now}

            memcache.set( _CONNECTED_ADMINS, connected_admins, 3600 )

            user = RkhskMember.get_by_key_name( command.user_id )
            if user is not None:
                user.sys_activitylog.append( db.Text( json.dumps( activitylog_msg ) ) )
                user.put( )

        self.session.clear( )

        return {
            'success': True,
            'redirect': '/',
            'body': False
        }

    # Interface: /admin/passwd/<sys_tmpKey|reset>
    def passwd(self, command):
        """Method for requesting a password reset and/or setting an administrators'
            password.

        Arguments:
            cmd: A command for controlling the method's behavior, valid inputs are
                'reset' or the key name of a SysTmpKey instance.
            param_data['loginUsername']: The admins username (through AdminCommand).
            param_data['loginPassword']: The admins password (through AdminCommand).
            param_data['cfrmLoginPassword']: A second password parameter in order to
                confirm the admins input (through AdminCommand).

        Returns:
            A redirect to the domain root.
        """

        command.set_action( 'admin_passwd' )
        command.set_description( 'Admin password update.' )
        command.set_valid_keys( [
            'email',
            'sys_password'
        ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

        if command.cmd == 'reset':
            try:
                login_username = command.param_data[ 'loginUsername' ]
            except (KeyError):
                return {
                    'success': False,
                    'status_code': 400,
                    'body': False
                }
            member_connector = AccountEmail.get_by_key_name( login_username )
            if member_connector and member_connector.memberRef and member_connector.memberRef.sys_administrator is True:
                member = member_connector.memberRef
                member.set_tmpkey( 'admin_password_reset' )
                member.put( )

                taskqueue.add(
                    queue_name='mailer' + str( randrange( 4 ) ),
                    url='/tasks/send_admin_password_reset_email/',
                    params={
                        'personal_id': member.personalId,
                        'client_ip': command.client_ip
                    },
                    countdown=5
                )
                return {
                    'success': True,
                    'data': {'resetSentTo': login_username}
                }
            else:
                return {
                    'success': False,
                    'status_code': 400,
                    'body': False
                }
        elif len( param ) != 26:
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        elif re.search( '^[\da-z]{26}$', param, flags=re.I ) is not None:
            account_link = SysTmpKey.get_by_key_name( param )
            if account_link is None or account_link.is_admin_password_reset( ) is False:
                return {
                    'success': False,
                    'status_code': 403,
                    'body': False
                }
            if account_link.is_expired( ):
                return {
                    'success': False,
                    'status_code': 403,
                    'errors': {
                        'linkExpired': 'Link expired %s' % account_link.sys_expired.isoformat( ).split( '.' )[ 0 ]}
                }
            member = account_link.get_member( )
            if member is None:
                logging.warning( 'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.' )
                return {
                    'success': False,
                    'status_code': 500,
                    'errors': {
                        'system': 'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.'}
                }
            if member.sys_administrator is not True:
                return {
                    'success': False,
                    'status_code': 403, # bad request
                    'errors': {'adminAccessDenied': 'The account does not have admin access configured.'}
                }
            if member.sys_deleted:
                return {
                    'success': False,
                    'status_code': 400, # bad request
                    'errors': {'accountDeleted': 'The associated account has been deleted.'}
                }

            if command.method == 'GET':
                return {
                    'success': True,
                    'data': {'loginUsername': member.email}
                }
            else:
                try:
                    login_username = command.param_data[ 'loginUsername' ]
                    login_password = command.param_data[ 'loginPassword' ]
                    cfrm_login_password = command.param_data[ 'cfrmLoginPassword' ]
                except (KeyError):
                    return {
                        'success': False,
                        'status_code': 400,
                        'body': False
                    }
                if login_username and login_password and cfrm_login_password and login_password == cfrm_login_password:
                    if member.email == login_username and len( login_password ) > 5:
                        member.sys_modifieddate = datetime.today( )
                        member.sys_password = login_password
                        changelog = {
                            'sys_password': member.sys_password,
                            'sys_modifieddate': member.sys_modifieddate.isoformat( ).split( '.' )[ 0 ]
                        }
                        if member.sys_email_verified is False:
                            member.sys_email_verified = True
                            changelog.update( {
                                'sys_email_verified': True
                            } )
                        member.sys_changelog.append( db.Text( json.dumps( changelog ) ) )
                        member.put( )
                        account_link.set_expired( )
                        return {
                            'success': True,
                            'data': {'passwordSetFor': member.email}
                        }

                return {
                    'success': False,
                    'status_code': 400,
                    'errors': {'invalidParams': 'Required parameters are missing or invalid.'}
                }

    # Interface: /admin/mecenat/<get_sync_details|setup_sync_operation|download_csv|confirm_transfer>
    def mecenat(self, command):
        """Method for administrating a transfer of member data to Mecenat.

        Arguments:
            cmd: A command for controlling the method's behavior, valid inputs are
                'get_sync_details', 'setup_sync_operation', 'download_csv' and
                'confirm_transfer' (through AdminCommand).
            request_id: An unique identifier for the request (through AdminCommand).

        Returns:
            cmd='get_sync_details': A JSON object with mecenatUsername and -Password.
            cmd='setup_sync_operation': An channel broadcast and HTTP response
                containing number of members to sync and the associated request ID.
            cmd='download_csv': CSV-formated member data in compliance with the
                recipients file specification – a file download. The download is also
                announced through a channel broadcast.
            cmd='confirm_transfer': A notification of successful transfer, by channel
                broadcast and a HTTP response. The response contain the number of
                members transfered and the request ID.
        """

        command.set_action( 'admin_mecenat_transfer' )
        command.set_description( 'Member data transfer to Mecenat.' )
        command.set_valid_keys( [ ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.cmd == 'get_sync_details':
            item_count = db.GqlQuery(
                "SELECT __key__ FROM MecenatPushTask WHERE sys_pushcomplete = FALSE AND targetSemester = \'%s\'" %
                SysConfig.get_current_semester( ) ).count( )
            login = SysConfig.get_mecenat_login( )
            return {
                'success': True,
                'data': {'mecenatSyncCount': item_count, 'mecenatUsername': login[ 0 ], 'mecenatPassword': login[ 1 ]}
            }

        elif command.cmd == 'setup_sync_operation':
            sync_items = MecenatPushTask.gql(
                "WHERE sys_pushcomplete = FALSE AND targetSemester = \'%s\'" % SysConfig.get_current_semester( ) )
            item_count = sync_items.count( )
            csv_data = [ ]
            keys = [ ]
            for task in sync_items:
                csv_data.append( task.dumps( as_csv=True ) )
                keys.append( str( command.key( ) ) )
            csv_data = string.join( csv_data, '' )
            memcache.set( 'mecenat_sync_operation_%s%s' % (command.user_id, command.request_id),
                    {'csv_data': csv_data, 'task_keys': keys}, 3600 )
            taskqueue.add(
                queue_name='broadcaster',
                url='/tasks/admin_broadcast/',
                params={
                    'broadcaster': command.user_id,
                    'timestamp': pickle.dumps( datetime.today( ) ),
                    'remote_event': 'mecenat_sync_initiated',
                    'data': json.dumps( {'mecenatSyncCount': item_count, 'requestId': command.request_id} )
                }
            )
            return {
                'success': True,
                'data': {'mecenatSyncCount': item_count, 'requestId': command.request_id}
            }

        elif command.cmd == 'download_csv':
            operation = memcache.get( 'mecenat_sync_operation_%s%s' % (command.user_id, command.request_id) )
            if operation is None:
                return {
                    'success': False,
                    'status_code': 400,
                    'body': False
                }
            else:
                taskqueue.add(
                    queue_name='broadcaster',
                    url='/tasks/admin_broadcast/',
                    params={
                        'broadcaster': command.user_id,
                        'timestamp': pickle.dumps( datetime.today( ) ),
                        'remote_event': 'mecenat_sync_data_downloaded',
                        'data': json.dumps(
                                {'mecenatSyncCount': len( operation[ 'task_keys' ] ), 'requestId': command.request_id} )
                    }
                )
                return {
                    'success': True,
                    'headers': {'content_type': 'text/csv',
                                'content_disposition': 'attachment; filename=%s_mecenat.csv' % date.today( ).strftime(
                                    '%y%m%d' )},
                    'body': operation[ 'csv_data' ]
                }

        elif command.cmd == 'confirm_transfer':
            operation = memcache.get( 'mecenat_sync_operation_%s%s' % (command.user_id, command.request_id) )
            if operation is None:
                taskqueue.add(
                    queue_name='broadcaster',
                    url='/tasks/admin_broadcast/',
                    params={
                        'broadcaster': command.user_id,
                        'timestamp': pickle.dumps( datetime.today( ) ),
                        'remote_event': 'mecenat_sync_failed',
                        'data': json.dumps( {'requestId': command.request_id} )
                    }
                )
                return {
                    'success': False,
                    'status_code': 400,
                    'body': False
                }
            else:
                task_entries = operation[ 'task_keys' ]

                taskqueue.add(
                    queue_name='broadcaster',
                    url='/tasks/admin_broadcast/',
                    params={
                        'broadcaster': command.user_id,
                        'timestamp': pickle.dumps( datetime.today( ) ),
                        'remote_event': 'mecenat_sync_completed',
                        'data': json.dumps( {'mecenatSyncCount': len( task_entries ), 'requestId': command.request_id} )
                    }
                )

                for task_key in task_entries:
                    taskqueue.add(
                        queue_name='mecenatupdater%s' % str( randrange( 4 ) ),
                        url='/tasks/save_mecenat_transfer/',
                        params={
                            'key': task_key,
                            'timestamp': timestamp,
                            'updater': command.user_id
                        }
                    )

                return {
                    'success': True,
                    'data': {'mecenatSyncCount': len( task_entries ), 'requestId': command.request_id}
                }

        else:
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

    # Interface: /admin/ssco/<get_sync_details|setup_sync_operation|download_csv|confirm_transfer>
    def ssco(self, command):
        """Method for administrating a transfer of member data to SSCO.

        Arguments:
            cmd: A command for controlling the method's behavior, valid inputs are
                'get_sync_details', 'setup_sync_operation', 'download_csv' and
                'confirm_transfer' (through AdminCommand).
            request_id: An unique identifier for the request (through AdminCommand).

        Returns:
            cmd='get_sync_details': A JSON object with sscoUsername and sscoPassword.
            cmd='setup_sync_operation': An channel broadcast and HTTP response
                containing number of members to sync and the associated request ID.
            cmd='download_csv': CSV-formated member data in compliance with the
                recipients file specification – a file download. The download is also
                announced through a channel broadcast.
            cmd='confirm_transfer': A notification of successful transfer, by channel
                broadcast and a HTTP response. The response contain the number of
                members transfered and the request ID.
        """

        command.set_action( 'admin_ssco_transfer' )
        command.set_description( 'Member data transfer to SSCO.' )
        command.set_valid_keys( [ ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.cmd == 'get_sync_details':
            item_count = db.GqlQuery(
                "SELECT __key__ FROM SscoPullTask WHERE sys_pullcomplete = FALSE AND targetSemester = \'%s\'" %
                SysConfig.get_current_semester( ) ).count( )
            login = SysConfig.get_ssco_login( )
            return {
                'success': True,
                'data': {'sscoSyncCount': item_count, 'sscoUsername': login[ 0 ], 'sscoPassword': login[ 1 ]}
            }

        elif command.cmd == 'setup_sync_operation':
            sync_items = SscoPullTask.gql(
                "WHERE sys_pullcomplete = FALSE AND targetSemester = \'%s\'" % SysConfig.get_current_semester( ) )
            item_count = sync_items.count( )
            csv_data = [ ]
            keys = [ ]
            for task in sync_items:
                if not csv_data:
                    csv_data.append( task.dumps( as_csv=True )[ 'header' ] )
                csv_data.append( task.dumps( as_csv=True )[ 'memberData' ] )
                keys.append( str( task.key( ) ) )
            csv_data = string.join( csv_data, '' )

            memcache.set( 'ssco_sync_operation_%s%s' % (command.user_id, command.request_id),
                    {'csv_data': csv_data, 'task_keys': keys}, 3600 )
            taskqueue.add(
                queue_name='broadcaster',
                url='/tasks/admin_broadcast/',
                params={
                    'broadcaster': command.user_id,
                    'timestamp': pickle.dumps( datetime.today( ) ),
                    'remote_event': 'ssco_sync_initiated',
                    'data': json.dumps( {'sscoSyncCount': item_count, 'requestId': command.request_id} )
                }
            )
            return {
                'success': True,
                'data': {'sscoSyncCount': item_count, 'requestId': command.request_id}
            }

        elif command.cmd == 'download_csv':
            operation = memcache.get( 'ssco_sync_operation_%s%s' % (command.user_id, command.request_id) )
            if operation is None:
                return {
                    'success': False,
                    'status_code': 400,
                    'body': False
                }
            else:
                taskqueue.add(
                    queue_name='broadcaster',
                    url='/tasks/admin_broadcast/',
                    params={
                        'broadcaster': command.user_id,
                        'timestamp': pickle.dumps( datetime.today( ) ),
                        'remote_event': 'ssco_sync_data_downloaded',
                        'data': json.dumps(
                                {'sscoSyncCount': len( operation[ 'task_keys' ] ), 'requestId': command.request_id} )
                    }
                )
                return {
                    'success': True,
                    'headers': {'content_type': 'text/csv',
                                'content_disposition': 'attachment; filename=%s_ssco.csv' % date.today( ).strftime(
                                    '%y%m%d' )},
                    'body': operation[ 'csv_data' ]
                }

        elif command.cmd == 'confirm_transfer':
            operation = memcache.get( 'ssco_sync_operation_%s%s' % (command.user_id, command.request_id) )
            if operation is None:
                taskqueue.add(
                    queue_name='broadcaster',
                    url='/tasks/admin_broadcast/',
                    params={
                        'broadcaster': command.user_id,
                        'timestamp': pickle.dumps( datetime.today( ) ),
                        'remote_event': 'ssco_sync_failed',
                        'data': json.dumps( {'requestId': command.request_id} )
                    }
                )
                return {
                    'success': False,
                    'status_code': 400,
                    'body': False
                }
            else:
                task_entries = operation[ 'task_keys' ]

                taskqueue.add(
                    queue_name='broadcaster',
                    url='/tasks/admin_broadcast/',
                    params={
                        'broadcaster': command.user_id,
                        'timestamp': pickle.dumps( datetime.today( ) ),
                        'remote_event': 'ssco_sync_completed',
                        'data': json.dumps( {'sscoSyncCount': len( task_entries ), 'requestId': command.request_id} )
                    }
                )

                for task_key in task_entries:
                    taskqueue.add(
                        queue_name='sscoupdater%s' % str( randrange( 4 ) ),
                        url='/tasks/save_ssco_transfer/',
                        params={
                            'key': task_key,
                            'timestamp': timestamp,
                            'updater': command.user_id
                        }
                    )

                return {
                    'success': True,
                    'data': {'sscoSyncCount': len( task_entries ), 'requestId': command.request_id}
                }

        else:
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

    # Interface: /admin/memcache/<flush_all|get_stats|admins>
    def memcache(self, command): # DEV
        """Method for inspecting select memcache entries, flushing and displaying
            usage stats.

        Arguments:
            cmd: A command for controlling the method's behavior, valid inputs are
                'flush_all', 'get_stats' and 'admins' (through AdminCommand).

        Returns:
            cmd='flush_all': Success.
            cmd='get_stats': A JSON object with key stats.
            cmd='admins': A JSON object with admins currently connected to the system
                and the current number of admin connections.
        """

        command.set_action( 'admin_dev_memcache_mgnt' )
        command.set_description( 'Memcache management.' )
        command.set_valid_keys( [ ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }
        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.dev_mode is not True:
            logging.info( 'AdminApiInterface.__memcache(cmd=\'%s\'): Method called with DEVMODE disabled.' % command.cmd )
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.cmd == 'flush_all':
            memcache.flush_all( )
            return {
                'success': True
            }

        elif command.cmd == 'get_stats':
            return {
                'success': True,
                'data': {'memcacheStats': memcache.get_stats( )}
            }

        elif command.cmd == 'admins':
            connection_data = [ ]
            #connections = memcache.get(CONNECTED_ADMINS)
            #if connections is not None:
            #  connection_data = connections

            admin_data = {}
            connected_admins = memcache.get( _CONNECTED_ADMINS )
            if connected_admins is not None:
                for admin in connected_admins:
                    admindata.update( {admin: connected_admins[ admin ].isoformat( ).split( '.' )[ 0 ]} )

            return {
                'success': True,
                'data': {'adminsConnected': connected_admins}
            }

        else:
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

    # Interface: /admin/datastore/<flush_all|get_stats>
    def datastore(self, command): # DEV
        """Method for inspecting select datastore entries, flushing and displaying
            usage stats.

        Arguments:
            cmd: A command for controlling the method's behavior, valid inputs are
                'flush_all', 'get_stats' and 'admins' (through AdminCommand).

        Returns:
            cmd='flush_all': Success.
            cmd='get_stats': A JSON object with the number of entries in each of the
                base models.
        """

        command.set_action( 'admin_dev_datastore_mgnt' )
        command.set_description( 'Datastore management.' )
        command.set_valid_keys( [ ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.dev_mode is not True:
            logging.info( 'AdminApiInterface.__datastore(cmd=\'%s\'): Method called with DEVMODE disabled.' % command.cmd )
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        counts = {
            'RkhskMember': RkhskMember.all( ).count( ),
            'InboundEmail': InboundEmail.all( ).count( ),
            'SentEmail': SentEmail.all( ).count( ),
            'AccountEmail': AccountEmail.all( ).count( ),
            'TempFile': TempFile.all( ).count( ),
            'PaymentKey': PaymentKey.all( ).count( ),
            'SysTmpKey': SysTmpKey.all( ).count( ),
            'AccountUri': AccountUri.all( ).count( ),
            'SscoPullTask': SscoPullTask.all( ).count( ),
            'MecenatPushTask': MecenatPushTask.all( ).count( )
        }

        if command.cmd == 'flush_all':
            db.delete( RkhskMember.all( ) )
            db.delete( SentEmail.all( ) )
            db.delete( InboundEmail.all( ) )
            db.delete( AccountEmail.all( ) )
            db.delete( TempFile.all( ) )
            db.delete( PaymentKey.all( ) )
            db.delete( SysTmpKey.all( ) )
            db.delete( AccountUri.all( ) )
            db.delete( SscoPullTask.all( ) )
            db.delete( MecenatPushTask.all( ) )
            return {
                'success': True,
                'data': {'entriesDeleted': counts}
            }

        elif command.cmd == 'get_stats':
            return {
                'success': True,
                'data': {'entriesStored': counts}
            }

        else:
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

    # Interface: /admin/dumps/<mecenat_push_tasks|mecenat_export|ssco_pull_tasks|ssco_export|members|owa_contacts>
    def dumps(self, command): # DEV
        """Method for dumping export tasks, a full members list and OWA contacts.

        Arguments:
            cmd: A command for controlling the method's behavior, valid inputs are
                'mecenat_push_tasks', 'mecenat_export', 'ssco_pull_tasks',
                'ssco_export', 'members' and 'owa_contacts' (through AdminCommand).

        Returns:
            cmd='mecenat_push_tasks': A JSON array containing all export tasks.
            cmd='mecenat_export': A CSV file download containing all exports for the
                current semester.
            cmd='ssco_pull_tasks': A JSON array containing all export tasks.
            cmd='ssco_export': A CSV file download containing all exports for the
                current semester.
            cmd='members': A JSON array containing all members in the database.
            cmd='owa_contacts': A JSON array all Outlook Web Access contacts stored
                in the system.
        """

        command.set_action( 'admin_dev_dumps' )
        command.set_description( 'Dumping datastore entries.' )
        command.set_valid_keys( [ ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.dev_mode is not True:
            logging.info( 'AdminApiInterface.__dumps(cmd=\'%s\'): Method called with DEVMODE disabled.' % command.cmd )
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.cmd == 'mecenat_push_tasks':
            task_data = [ ]
            tasks = MecenatPushTask.all( )
            for task in tasks:
                task_data.append( task.dumps( raw=True ) )

            return {
                'success': True,
                'data': {'MecenatPushTasks': task_data}
            }

        elif command.cmd == 'mecenat_export':
            csv_data = [ ]
            current_semester = SysConfig.get_current_semester( )
            tasks = MecenatPushTask.all( )
            for task in tasks:
                if task.targetSemester != current_semester:
                    continue
                csv_data.append( task.dumps( as_csv=True ) )
            csv_data = string.join( csv_data, '' )

            return {
                'success': True,
                'headers': {'content_type': 'text/csv',
                            'content_disposition': 'attachment; filename=%s_mecenat.csv' % date.today( ).strftime(
                                '%y%m%d' )},
                'body': csv_data
            }

        elif command.cmd == 'ssco_pull_tasks':
            task_data = [ ]
            tasks = SscoPullTask.all( )
            for task in tasks:
                task_data.append( task.dumps( raw=True ) )
            csv_data = string.join( task_data, '' )

            return {
                'success': True,
                'data': {'SscoPullTasks': task_data}
            }

        elif command.cmd == 'ssco_export':
            csv_data = [ ]
            current_semester = SysConfig.get_current_semester( )
            tasks = SscoPullTask.all( )
            for task_entry in tasks:
                if task_entry.targetSemester != current_semester:
                    continue
                if csv_data:
                    csv_data.append( u'%s' % task_entry.dumps( as_csv=True )[ 'member_data' ] )
                else:
                    csv_data.append( u'%s%s' % (task_entry.dumps( as_csv=True )[ 'header' ], task_entry.dumps( )[ 'member_data' ]) )
            csv_data = string.join( csv_data, '' )

            return {
                'success': True,
                'headers': {'content_type': 'text/csv',
                            'content_disposition': 'attachment; filename=%s_ssco.csv' % date.today( ).strftime(
                                '%y%m%d' )}
                ,
                'body': csv_data
            }

        elif command.cmd == 'members':
            member_data = [ ]
            members = RkhskMember.all( )
            for member in members:
                data = member.dumps( )
                parsedlog = [ ]
                for entry in data[ 'sys_changelog' ]:
                    parsedlog.append( json.loads( entry ) )
                data[ 'sys_changelog' ] = parsedlog
                member_data.append( data )

            return {
                'success': True,
                'data': {'memberData': member_data}
            }

        elif command.cmd == 'owa_contacts':
            contact_data = [ ]
            contacts = OwaUser.all( )
            for contact in contacts:
                contact_data.append( contact.dumps( ) )

            return {
                'success': True,
                'data': {'OwaUsers': contact_data}
            }

        elif command.cmd == 'sysconfig':
            sysconfig = SysConfig.load( )
            response_conf = {}
            for key, value in sysconfig.iteritems( ):
                if isinstance( value, datetime ) or isinstance( value, date ):
                    response_conf.update( {key: value.isoformat( ).split( '.' )[ 0 ]} )
                else:
                    response_conf.update( {key: value} )

            return {
                'success': True,
                'data': {'SysConfig': response_conf}
            }

        elif command.cmd == 'payment_keys':
            key_data = [ ]
            keys = PaymentKey.all( )
            for key in keys:
                key_data.append( key.dumps( ) )

            return {
                'success': True,
                'data': {'PaymentKeys': key_data}
            }

        elif command.cmd == 'sys_tmp_keys':
            key_data = [ ]
            keys = SysTmpKey.all( )
            for key in keys:
                key_data.append( key.dumps( ) )

            return {
                'success': True,
                'data': {'SysTmpKeys': key_data}
            }

        elif command.cmd == 'account_uris':
            accounturi_data = [ ]
            accounturis = AccountUri.all( )
            for uri in accounturis:
                accounturi_data.append( uri.dumps( ) )

            return {
                'success': True,
                'data': {'AccountUris': accounturi_data}
            }

        elif command.cmd == 'account_emails':
            email_data = [ ]
            addresses = AccountEmail.all( )
            for address in addresses:
                email_data.append( address.dumps( ) )

            return {
                'success': True,
                'data': {'AccountEmails': email_data}
            }

        elif command.cmd == 'session_traces':
            session_trace_data = [ ]
            session_traces = SessionTrace.all( )
            for session_trace in session_traces:
                session_trace_data.append( session_trace.dumps( ) )

            return {
                'success': True,
                'data': {'SessionTraces': session_trace_data}
            }

        return {
            'success': False,
            'status_code': 400,
            'body': False
        }

    # Interface: /admin/trigger/<sync_to_mecenat|send_activation_notice>/
    def trigger(self, command): # DEV
        """Method for manually triggering processes during development.

        Arguments:
            cmd: A command for controlling the method's behavior, valid inputs
                are 'sync_to_mecenat' and 'send_activation_notice' (passed through
                AdminCommand).

        Returns:
            cmd='sync_to_mecenat': A success notice stating 'TaskQueued'.
            cmd='send_activation_notice': A JSON object with target member details.
        """

        command.set_action( 'admin_dev_triggers' )
        command.set_description( 'Admin development triggers.' )
        command.set_valid_keys( [ ] )

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 400,
                'body': False
            }

        if not command.from_verified_admin( ):
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.dev_mode is not True:
            logging.info( 'AdminApiInterface.__trigger(cmd=\'%s\'): Method called with DEVMODE disabled.' % command.cmd )
            return {
                'success': False,
                'status_code': 403,
                'body': False
            }

        if command.cmd == 'sync_to_mecenat':
            taskqueue.add(
                queue_name='registrator',
                url='/tasks/sync_to_mecenat/',
                params={
                    #'personal_id': '841021-7171',
                    'sys_activator': command.user_id
                }
            )

            return {
                'success': True,
                'data': {'syncToMecenat': 'TaskQueued'}
            }

        if command.cmd == 'send_member_registration_reminder':
            target_members = db.GqlQuery( "SELECT * FROM RkhskMember WHERE lastApplicationFor = 'HT11'" )
            personal_ids = [ ]
            skipped = [ ]
            for personal_id in target_members:
                member = RkhskMember.get_by_key_name( personal_id )
                if member is None or member.email is None:
                    skipped.append( personal_id )
                    continue
                personal_ids.append( personal_id )
                taskqueue.add(
                    queue_name='mailer%s' % str( randrange( 4 ) ),
                    url='/tasks/send_member_registration_reminder/',
                    params={
                        'personal_id': personal_id
                    },
                    countdown=randrange( 15, 90 )
                )

            return {
                'success': True,
                'data': {'sendActivationEmail': 'TaskQueued', 'targetMembers': sentpids,
                         'targetMemberCount': len( sentpids ), 'skippedMembers': skipped}
            }

        return {
            'success': False,
            'status_code': 400,
            'body': False
        }
