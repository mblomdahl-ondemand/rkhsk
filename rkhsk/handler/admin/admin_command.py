# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.admin.AdminCommand
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging

from rkhsk.handler.generic import Command
from rkhsk.model import RkhskMember


# TODO(mats.blomdahl@gmail.com): Improve handler.admin.AdminCommand implementation!
class AdminCommand( Command ):
    # TODO(evelina.bisell@gmail.com): Skriv en fin och pedagogisk kommentar! Den här klassen gör si och så. Använder argumenten foo och bar som är ... ... ... Fråga om något är konstigt! Sötnos.
    """Wrapper class for commands recieved from the admins' user interface.

    Attributes:
        request_id: An optional identifier for channeled requests.
    """

    def __init__(self, handler_instance, method, service, cmd, request_id, session_id, user_id, dev_mode=False):
        """Initialization method for the command wrapper.

        Arguments:
            request: A WebOb-compatible request object.
            method: The HTTP method used (i.e. "GET" or "POST").
            service: The main functionality requested.
            cmd: An optional, service-specific, command.
            request_id: An optional identifier for channeled requests.
        """

        if dev_mode:
            logging.info(
                'AdminCommand.__init__(account_source=RkhskMember, handler_instance, method=\'%s\', service=\'%s\', '
                'cmd=\'%s\', request_id=\'%s\', session_id=\'%s\', user_id=\'%s\', dev_mode=%s)' % (
                    str( method ), str( service ), str( cmd ), str( request_id ), str( session_id ), str( user_id ),
                    str( dev_mode )) )
        super( AdminCommand, self ).__init__( account_source=RkhskMember, handler_instance=handler_instance,
                                              method=method,
                                              service=service, cmd=cmd, session_id=session_id, user_id=user_id,
                                              dev_mode=dev_mode, request_id=request_id, user_class='admin',
                                              interface='admin_interface' )

        self.set_valid_keys( ) # defaults to match the dataset used by the admin's member grid


    def set_valid_keys(self, valid_keys=None):
        """Setter for applying the 'valid_keys' attribute.

        The 'valid_keys' attribute control which attributes of a union member will
            be available for update as well as what set of attributes are returned by
            self.get_member_data(member).

        Arguments:
            valid_keys: An array containing the attributes to work with.
        """

        if valid_keys is None:
            valid_keys = [ 'firstName', 'lastName', 'streetCo', 'streetAddress', 'streetNumber', 'streetEntrance',
                           'streetApartment', 'streetFloor', 'zipCode', 'city', 'email', 'phone', 'sys_administrator',
                           'sys_accounttype', 'lastPaymentKey', 'lastApplicationDate', 'lastApplicationFor',
                           'lastMembershipFor', 'lastPaymentRecieved', 'lastMecenatReg', 'lastSscoReg' ]
        if self.dev_mode:
            logging.info( 'AdminCommand.set_valid_keys(%s)' % str( valid_keys ) )
        super( AdminCommand, self ).set_valid_keys( valid_keys=valid_keys )


    def save_session(self):
        """Method for updating the session trace and trigger the evaluation
            process for user interactions.
        """

        self.session.update_session_trace(
            action=self.action,
            interface=self.interface,
            status_code=self.response_data[ 'status_code' ],
            request_data=self.dumps( )
        )


    def dumps(self):
        """Method for dumping the command data.

        Returns:
            A dict with the command attribute configuration.
        """

        instance_data = super( AdminCommand, self ).dumps( [
            'client_ip',
            'session_id',
            'method',
            'service',
            'cmd',
            'user_class',
            'interface',
            'action',
            'description',
            'date_format',
            'valid_keys',
            'json_data',
            'param_data',
            'payload',
            'request_id',
            'response_data'
        ] )

        return instance_data



