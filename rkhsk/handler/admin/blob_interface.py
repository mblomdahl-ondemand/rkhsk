# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.sys.BlobInterface
# Author:  Mats Blomdahl
# Version: 2012-03-08

import webapp2
from webapp2_extras import sessions
import re
import json
import logging
import datetime
from google.appengine.ext import db
from google.appengine.api import channel
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import blobstore

from rkhsk.handler.admin import AdminCommand

from rkhsk.model import   sys_config
from rkhsk.model.generic import temp_file
from rkhsk.model import RkhskMember, PaymentKey

class BlobInterface( blobstore_handlers.BlobstoreUploadHandler ):
    """The public interface for setting up URIs for blob uploads and handling
        submitted uploads.

    The interface deploys secure cookies using the 'default' store and cookie
        expiration is controlled by the global variable ADMIN_COOKIE_EXPIRATION.
    """

    # session management
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store( request=self.request )

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch( self )
        finally:
            #pass
            # Save all sessions.
            self.session_store.save_sessions( self.response )

    @webapp2.cached_property
    def session(self):
        # Returns a session using the default cookie key.
        _ADMIN_COOKIE_EXPIRATION = self.app.config.get( '_ADMIN_COOKIE_EXPIRATION' )
        return self.session_store.get_session( name='default', max_age=_ADMIN_COOKIE_EXPIRATION,
                                               backend='securecookie' )


    # /admin/<upload|serve>/<payments|attachment>/<request_id>
    def get(self, service, filetype, request_id):
        """Method for creating upload URIs on-demand.

        Arguments:
            filetype: The 'filetype' param control how the blob will be processed
                once uploaded, the method currently accepts the values 'attachment'
                and 'payments'.
            request_id: A unique request ID associated with the upload, used for
                attaching client-side event handlers to the GAE channel service.

        Returns:
            A JSON object with a upload token (URI).
        """

        self._DEVMODE = self.app.config.get( '_DEVMODE' )

        command = AdminCommand(
            handler_instance=self,
            method='GET',
            service=service,
            cmd=filetype,
            request_id=request_id,
            session_id=self.session.get( 'session' ),
            user_id=self.session.get( 'admin' ),
            dev_mode=self._DEVMODE
        )
        command.set_action( 'admin_%s' % service )
        if service == 'upload':
            command.set_description( 'Create blob upload token.' )
        elif service == 'serve':
            command.set_description( 'Serve Blobstore data.' )

        # TODO(mats.blomdahl@gmail.com): Complete GET /admin/serve/ implementation.
        if command.dev_mode:
            logging.info( 'RkhskAdminUpload.get(filetype=\'%s\', request_id=\'%s\').user_id: \'%s\'' % (
                filetype, request_id, str( command.user_id )) )

        response_data = None

        if not command.from_verified_admin( ):
            response = {
                'success': False,
                'status_code': 403,
                'errors': {
                    'accessDenied': 'The user does not have sufficient privileges to call /admin/upload/%s/%s.' % (
                        str( filetype ), str( request_id ))}
            }
            self.response.set_status( 403 ) # forbidden

        elif filetype != 'payments' and filetype != 'attachment':
            response_data = {
                'success': False,
                'status_code': 400,
                'errors': {
                    'invalidRequest': 'The call to /admin/upload/%s/%s does not match the interface specification.' % (
                        str( filetype ), str( request_id ))}
            }
            self.response.set_status( 400 ) # bad request

        else:
            upload_token = blobstore.create_upload_url( '/admin/upload/%s/%s' % (filetype, request_id) )

            if command.dev_mode:
                logging.info( 'RkhskAdminUpload.get(filetype=\'%s\', request_id=\'%s\').upload_token: \'%s\'' % (
                    filetype, request_id, upload_token) )

            response_data = {
                'success': True,
                'status_code': 200,
                'data': {'uploadToken': upload_token}
            }
            self.response.headers.add_header( "Access-Control-Allow-Origin", "*" )
            self.response.content_type = 'application/json'
            self.response.body = json.dumps( response_data )

        command.set_response_data( response_data )
        command.save_session( )


    # TODO(mats.blomdahl@gmail.com): Implement processing for filetype 'attachment'.
    # /admin/upload/<payments|attachment>/<request_id>
    def post(self, service, filetype, request_id):
        """Method for recieving, processing and storing uploaded blobs.

    Arguments:
      filetype: The 'filetype' param control how an uploaded blob is processed,
        the method currently accepts the values 'attachment' and 'payments'.
      request_id: A unique request ID associated with the upload.

    Returns:
      A channeled message containing raw file information and the results from
        data processing.
    """

        self._DEVMODE = self.app.config.get( '_DEVMODE' )

        command = AdminCommand(
            handler_instance=self,
            method='POST',
            service=service,
            cmd=filetype,
            request_id=request_id,
            session_id=self.session.get( 'session_id' ),
            user_id=self.session.get( 'user_id' ),
            dev_mode=self._DEVMODE
        )

        command.set_action( 'admin_%' % service )
        if service == 'upload':
            command.set_description( 'Handle submitted blob upload.' )
        else:
            command.set_description( 'Serve Blobstore data.' )

        # TODO(mats.blomdahl@gmail.com): Complete POST /admin/serve/ implementation.

        response_data = None

        if not command.from_verified_admin( ):
            response_data = {
                'success': False,
                'status_code': 403,
                'errors': {
                    'accessDenied': 'The user does not have sufficient privileges to call /admin/upload/%s/%s.' % (
                        str( filetype ), str( request_id ))}
            }
            command.set_response( response_data )
            command.save_session( )
            self.response.set_status( 403 )
            return

        if command.dev_mode:
            logging.info( 'RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').user_id: \'%s\'' % (
                filetype, request_id, str( command.user_id )) )

        try:
            if not any( self.get_uploads( ) ) and command.dev_mode:
                logging.info( "RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\'): Fatal error! No blobinfo." )

            upload_info = self.get_uploads( "datafile" )

            logging.info( upload_info[ 0 ].filename )
            logging.info( upload_info[ 0 ].content_type )
            logging.info( upload_info[ 0 ].size )

            temp_file = temp_file(
                key_name=str( upload_info[ 0 ].key( ) ),
                fileKey=upload_info[ 0 ].key( ),
                fileName=upload_info[ 0 ].filename,
                fileContentType=upload_info[ 0 ].content_type,
                fileSize=upload_info[ 0 ].size,
                fileCreation=upload_info[ 0 ].creation,
                fileType=filetype,
                fileCreatedBy=command.user_id
            )

            body = blobstore.fetch_data( upload_info[ 0 ].key( ), 0, blobstore.MAX_BLOB_FETCH_SIZE - 1 )

            channel_data = {
                'fileName': upload_info[ 0 ].filename,
                'fileSize': upload_info[ 0 ].size,
                'fileContentType': upload_info[ 0 ].content_type,
                'fileCreation': upload_info[ 0 ].creation.isoformat( ),
                'fileKey': str( upload_info[ 0 ].key( ) ),
                'requestId': request_id,
                'userId': command.user_id
            }

            db.put( temp_file )

            if filetype == 'payments':
                channel_data.update( self.__parse_payments( body ) )

            elif filetype == 'attachment':
                response_data = {
                    'success': False,
                    'status_code': 400,
                    'errors': {'notImplemented': 'Upload of mail attachment is yet to be implemented.'}
                }
                self.response.set_status( 400 )
                return

            else:
                response_data = {
                    'success': False,
                    'status_code': 400,
                    'errors': {
                        'invalidRequest': 'The call to /admin/upload/%s/%s does not match the interface specification.' % (
                            str( filetype ), str( request_id ))}
                }
                command.set_response( response_data )
                command.save_session( )
                self.response.set_status( 400 )
                return

            channel.send_message( command.user_id, json.dumps( channel_data ) )
            response_data = {
                'success': True,
                'status_code': 200,
                'data': channel_data,
                'body': [ ]
            }

        except blobstore.Error:
            logging.info( "RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: blobstore.Error" )
            response_data = {
                'success': False,
                'status_code': 500,
                'errors': {'blobstoreError': 'The call to /admin/upload/%s/%s raised a blobstore.Error exception.' % (
                    str( filetype ), str( request_id ))}
            }

        except blobstore.BlobInfoParseError:
            logging.info(
                "RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: blobstore.BlobInfoParseError" )
            response_data = {
                'success': False,
                'status_code': 500,
                'errors': {
                    'blobInfoParseError': 'The call to /admin/upload/%s/%s raised a blobstore.BlobInfoParseError exception.' % (
                        str( filetype ), str( request_id ))}
            }

        except blobstore.BlobNotFoundError:
            logging.info(
                "RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: blobstore.BlobNotFoundError" )
            response_data = {
                'success': False,
                'status_code': 404,
                'errors': {
                    'blobNotFoundError': 'The call to /admin/upload/%s/%s raised a blobstore.BlobNotFoundError exception.' % (
                        str( filetype ), str( request_id ))}
            }

        except blobstore.InternalError:
            logging.info(
                "RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: blobstore.InternalError" )
            response_data = {
                'success': False,
                'status_code': 500,
                'errors': {
                    'blobstoreInternalError': 'The call to /admin/upload/%s/%s raised a blobstore.InternalError exception.' % (
                        str( filetype ), str( request_id ))}
            }

        except IndexError:
            logging.info( "RkhskAdminUpload.post(filetype=\'%s\', request_id=\'%s\').exception: IndexError" )
            response_data = {
                'success': False,
                'status_code': 500,
                'errors': {'blobstoreIndexError': 'The call to /admin/upload/%s/%s raised an IndexError exception.' % (
                    str( filetype ), str( request_id ))}
            }

        finally:
            command.set_response( response_data )
            command.save_session( )

            if response_data[ 'success' ]:
                self.response.headers.add_header( "Access-Control-Allow-Origin", "*" )
                self.response.content_type = 'text/html'
                self.response.body = json.dumps( response_data.body )
            else:
                self.response.set_status( response_data[ 'status_code' ] )
                self.response.content_type = 'application/javascript'
                self.response.body = json.dumps( response_data )


    # backend for processing bank statements submitted to /admin/upload/payments/<request_id>
    def __parse_payments(self, body):
        """Method for parsing and processing uploaded bank statements.

    This method will analyze exported bank statements of Nordea bank row by row
      in order to identify a few properties for each transaction; a) the amount
      of money recieved, b) transaction date and c) transaction reference.

    The transaction reference is cross-checked against the system's list of
      payment keys and – if unsuccessful – the personal identification numbers
      enlisted.

    Arguments:
      body: The raw blob body.

    Returns:
      A dict with the keys 'itemsUnidentified' and 'itemsRecognized', both of
        which contain lists of sub-dicts.
    """

        body = unicode( body, 'windows-1252' ) #'latin-1')
        lines = body.splitlines( )
        items_recognized = [ ]
        items_unidentified = [ ]

        lowest_fee_accepted = sys_config.get_membership_fee( )
        first_date_accepted = sys_config.get_first_payment_date( )

        thisyear = str( first_date_accepted.year )
        today = datetime.date.today( )

        for i in range( len( lines ) ):
            raw_dates = re.findall( thisyear + '\-\d{2}\-\d{2}', lines[ i ] )
            verified_date = None
            for j in range( len( raw_dates ) ):
                try:
                    verified_date = datetime.datetime.date( datetime.datetime.strptime( raw_dates[ j ], '%Y-%m-%d' ) )
                except (ValueError):
                    continue
                if verified_date > today or verified_date < first_date_accepted:
                    verified_date = None
                else:
                    lines[ i ] = lines[ i ][ re.search( raw_dates[ j ], lines[ i ] ).end( ): ]
                    break
            if verified_date is None:
                continue

            raw_payments = re.findall( '\s[^\-\w]\d{' + str( len( str( lowest_fee_accepted ) ) ) + '},\d{2}',
                                       lines[ i ] )
            verified_payment = None
            exact_match = None
            for j in range( len( raw_payments ) ):
                verified_payment = int( raw_payments[ j ][ 2:len( str( lowest_fee_accepted ) ) + 2 ] )
                if verified_payment == lowest_fee_accepted:
                    exact_match = raw_payments[ j ]
                    break
                else:
                    verified_payment = None
            if verified_payment is None:
                continue
            elif exact_match is not None:
                end_of_payments = re.search( exact_match, lines[ i ] ).end( )
                lines[ i ] = lines[ i ][ end_of_payments: ]

            lines[ i ] = re.sub( '\s', ' ', lines[ i ] )
            while re.search( '  ', lines[ i ] ):
                lines[ i ] = re.sub( '  ', ' ', lines[ i ] )

            verified_user = None
            verified_keys = [ ]
            raw_payment_keys = [ ]
            for j in range( len( lines[ i ] ) - 5 ):
                keys = re.findall( '[A-Z]{5}', lines[ i ][ j: ] )
                for k in range( len( keys ) ):
                    if raw_payment_keys.count( keys[ k ] ):
                        continue
                    else:
                        raw_payment_keys.append( keys[ k ] )
            for j in range( len( raw_payment_keys ) ):
                paymentkey = PaymentKey.get_by_key_name( raw_payment_keys[ j ] )
                if paymentkey is None:
                    verified_keys.append( paymentkey )
                    continue
                verified_user = paymentkey.memberRef
                verified_keys = [ raw_payment_keys[ j ] ]
                break

            if verified_user is None:
                next_payment_keys = [ ]
                for j in range( len( lines[ i ] ) - 5 ):
                    keys = re.findall( '[a-z]{5}', lines[ i ][ j: ], flags=re.I )
                    for k in range( len( keys ) ):
                        if raw_payment_keys.count( keys[ k ].upper( ) ) or next_payment_keys.count( keys[ k ] ):
                            continue
                        else:
                            next_payment_keys.append( keys[ k ] )
                for j in range( len( next_payment_keys ) ):
                    payment_key = PaymentKey.get_by_key_name( next_payment_keys[ j ].upper( ) )
                    if payment_key is None:
                        continue
                    verified_user = payment_key.memberRef
                    verified_keys = [ next_payment_keys[ j ].upper( ) ]
                    break

            numbersonly = re.sub( '[^\d]', '', lines[ i ] )
            verified_personal_id = None
            raw_personal_ids = [ ]
            for j in range( len( numbersonly ) - 12 ):
                personal_ids = re.findall( '19[4-9]\d0[1-9]0[1-9]\d{4}', numbersonly[ j: ] ) # 19840203-nnnn
                personal_ids.extend( re.findall( '19[4-9]\d1[0-2]0[1-9]\d{4}', numbersonly[ j: ] ) ) # 19841003-nnnn
                personal_ids.extend( re.findall( '19[4-9]\d0[1-9][1-3][0-9]\d{4}', numbersonly[ j: ] ) ) # 19840203-nnnn
                personal_ids.extend( re.findall( '19[4-9]\d1[0-2][1-3][0-9]\d{4}', numbersonly[ j: ] ) ) # 19840203-nnnn
                for k in range( len( personal_ids ) ):
                    if raw_personal_ids.count( personal_ids[ k ] ):
                        continue
                    else:
                        raw_personal_ids.append( personal_ids[ k ] )

            for j in range( len( raw_personal_ids ) ):
                personal_id = '%s-%s' % (raw_personal_ids[ j ][ 2:8 ], raw_personal_ids[ j ][ 8: ])
                raw_personal_ids[ j ] = raw_personal_ids[ j ][ 2: ]
                user = RkhskMember.get_by_key_name( personal_id )
                if user is not None:
                    verified_personal_id = personal_id
                    break

            if verified_personal_id is None:
                next_personal_ids = [ ]
                for j in range( len( numbersonly ) - 10 ):
                    personal_ids = re.findall( '[4-9]\d0[1-9]0[1-9]\d{4}', numbersonly[ j: ] ) # 840203-nnnn
                    personal_ids.extend( re.findall( '[4-9]\d1[0-2]0[1-9]\d{4}', numbersonly[ j: ] ) ) # 841003-nnnn
                    personal_ids.extend( re.findall( '[4-9]\d0[1-9][1-3][0-9]\d{4}', numbersonly[ j: ] ) ) # 840203-nnnn
                    personal_ids.extend( re.findall( '[4-9]\d1[0-2][1-3][0-9]\d{4}', numbersonly[ j: ] ) ) # 840203-nnnn
                    for k in range( len( personal_ids ) ):
                        if raw_personal_ids.count( personal_ids[ k ] ) or next_personal_ids.count( personal_ids[ k ] ):
                            continue
                        else:
                            next_personal_ids.append( personal_ids[ k ] )
                            #logging.info('raw_personal_ids, secondary: %s' % next_personal_ids)
                for j in range( len( next_personal_ids ) ):
                    personal_id = '%s-%s' % (next_personal_ids[ j ][ :6 ], next_personal_ids[ j ][ 6: ])
                    user = RkhskMember.get_by_key_name( personal_id )
                    if user is not None:
                        verified_personal_id = personal_id
                        break

            if verified_user is None and verified_personal_id is None:
                #logging.info('unidentified!')
                items_unidentified.append( {
                    'lastPaymentRecieved': verified_date.strftime( '%Y-%m-%d' ),
                    'payment': '%s kr' % verified_payment,
                    'message': lines[ i ]
                } )
                #logging.info('unidentified: %s' % items_unidentified[-1])
            elif verified_user:
                #logging.info('recognized!')
                application_date = ''
                if hasattr( 'lastApplicationDate', verified_user ):
                    application_date = verified_user.lastApplicationDate.strftime( '%Y-%m-%d' )

                items_recognized.append( {
                    'lastPaymentRecieved': verified_date.strftime( '%Y-%m-%d' ),
                    'payment': '%s kr' % verified_payment,
                    'paymentKey': verified_keys[ 0 ],
                    'firstName': verified_user.firstName,
                    'lastName': verified_user.lastName,
                    'personalId': verified_user.personal_id,
                    'lastApplicationFor': verified_user.lastApplicationFor,
                    'lastApplicationDate': application_date
                } )
            else:
                user = RkhskMember.get_by_key_name( verified_personal_id )
                application_date = ''
                if hasattr( user, 'lastApplicationDate' ):
                    application_date = user.lastApplicationDate.strftime( '%Y-%m-%d' )

                items_recognized.append( {
                    'lastPaymentRecieved': verified_date.strftime( '%Y-%m-%d' ),
                    'payment': '%s kr' % verified_payment,
                    'paymentKey': '',
                    'firstName': user.firstName,
                    'lastName': user.lastName,
                    'personalId': user.personalId,
                    'lastApplicationFor': user.lastApplicationFor,
                    'lastApplicationDate': application_date
                } )

        return {
            'itemsRecognized': items_recognized,
            'itemsUnidentified': items_unidentified
        }

