# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.admin.AdminFrontPageInterface
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging, time, webapp2
from webapp2_extras import sessions

from rkhsk.handler.generic import UniHandler
from rkhsk.handler.public import FrontPageCommand
from rkhsk.model.generic import SessionTrace

# TODO(mats.blomdahl@gmail.com): Improve handler.admin.AdminFrontPageInterface documentation!
class AdminFrontPageInterface( webapp2.RequestHandler ):
    """The public interface for retrieving the administration page at located at
        http://regga.rkh-sk.se/admin

    The interface deploys secure cookies using the 'default' store and cookie
        expiration is controlled by the global variable SESSION_DEFAULT_EXPIRATION.
    """

    # Session Management
    def dispatch(self):
        """Get a session store for this request

        Returns:
            Foo.
        """

        self.session_store = sessions.get_store( request=self.request )

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch( self )
        finally:
            # Save all sessions.
            self.session_store.save_sessions( self.response )

    @webapp2.cached_property
    def session(self):
        """Returns a session using the default cookie key.

        Returns:
            Foo.
        """

        _SESSION_DEFAULT_EXPIRATION = self.app.config.get('_SESSION_DEFAULT_EXPIRATION')
        return self.session_store.get_session( name='default', max_age=_SESSION_DEFAULT_EXPIRATION, backend='securecookie' )

    # Interface: /<service>
    def get(self, service):
        """Request handler for serving the web application.

        The request handler will also ensure that the client's session cookie is
            set. The session cookie is will be required by any and all subsystems
            within this solution.

        Arguments:
            service: Supported values are 'admin', 'app_am', 'app_ad'.

        Returns:
            None
        """

        starttime = time.time( )

        _DEVMODE = self.app.config.get( '_DEVMODE' )

        session_id = self.session.get( 'session' )
        if session_id is None:
            session_id = SessionTrace.generate_session_id( )
            self.session[ 'session' ] = session_id

        command = FrontPageCommand(
            handler_instance=self,
            method='GET',
            service=service,
            session_id=session_id,
            user_id=self.session.get( 'user_id' ),
            dev_mode=_DEVMODE
        )

        unihandler = UniHandler( handler_name='RkhskAdminInterface.__unihandler()', command=command, dev_mode=_DEVMODE )

        unihandler.handle_request( )

        unihandler.serve_response( loadtime={'loadTime': '%s ms' % str( int( (time.time( ) - starttime) * 1000 ) )} )

    # Interface: /admin
    def admin(self, command):
        """Method for serving the default web application administration interface.

        Selection of mobile or desktop version of the application is controlled by
            the command's 'client_is_mobile' method.

        Arguments:
            command:

        Returns:
            Foo.
        """

        if command.client_is_mobile( ):
            if False and command.dev_mode:
                page = 'app/admin_mobile/app.html'
            else:
                page = 'app/admin_mobile/app.html'
        else:
            if False and command.dev_mode:
                page = 'app/admin_desktop/app.html'
            else:
                page = 'app/admin_desktop/app.html'

        page_content = open( page, 'r' )

        response_data = {
            'success': True,
            'headers': {'content_type': 'text/html'},
            'data': page_content.read( ).replace('app.js', 'app_ad.js')
        }

        page_content.close( )

        return response_data

    # Interface: /app_am
    def app_am(self, command):
        """Method for serving the web application administration interface with the
            application's mobile version enforced.
        """

        if self.dev_app:
            page = 'index-am-dev.html'
        else:
            page = 'index-am.html'
        page_content = open( page, 'r' )
        response_data = {
            'success': True,
            'headers': {'content_type': 'text/html'},
            'data': page_content.read( )
        }
        #self.response.write(page_content.read())
        page_content.close( )
        return response_data

    # Interface: /app_ad
    def app_ad(self, command):
        """Method for serving the web application administration interface with the
            application's desktop version enforced.
        """

        if self.dev_app:
            page = 'index-ad-dev.html'
        else:
            page = 'index-ad.html'
        page_content = open( page, 'r' )
        response_data = {
            'success': True,
            'headers': {'content_type': 'text/html'},
            'data': page_content.read( )
        }
        #self.response.write(page_content.read())
        page_content.close( )
        return response_data

