# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.user.AccountCommand
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging

from rkhsk.handler.generic import Command
from rkhsk.model import RkhskMember


# TODO(mats.blomdahl@gmail.com): Improve handler.user.AccountCommand implementation!
class AccountCommand( Command ):
    """Wrapper class for commands recieved from the members' registration page."""

    def __init__(self, handler_instance, method, service, cmd, session_id, user_id, dev_mode=False):
        """Initialization method for the command wrapper.

        Arguments:
            request: A WebOb-compatible request object.
            method: The HTTP method used (i.e. "GET" or "POST").
            service: The main functionality requested.
            cmd: An optional, service-specific, command.
        """

        if dev_mode:
            logging.info(
                'AccountCommand.__init__(account_source=RkhskMember, handler_instance, method=\'%s\', service=\'%s\','
                ' cmd=\'%s\', session_id=\'%s\', user_id=\'%s\', dev_mode=%s)' % (
                    str( method ), str( service ), str( cmd ), str( session_id ), str( user_id ), str( dev_mode )) )
        super( AccountCommand, self ).__init__( account_source=RkhskMember, handler_instance=handler_instance,
                                                method=method, service=service, cmd=cmd, session_id=session_id,
                                                user_id=user_id, dev_mode=dev_mode, user_class='member',
                                                interface='account_interface' )

        self.set_valid_keys( ) # defaults to match the dataset used by the registration form


    def set_valid_keys(self, valid_keys=None):
        """Setter for applying the 'valid_keys' attribute.

        The 'valid_keys' attribute control which attributes of a union member will
            be available for update as well as what set of attributes are returned by
            self.get_member_data(member).

        Arguments:
            valid_keys: An array containing the attributes to work with.
        """

        if valid_keys is None:
            valid_keys = [ 'firstName', 'lastName', 'streetCo', 'streetAddress', 'streetNumber', 'streetEntrance',
                           'streetApartment', 'streetFloor', 'zipCode', 'city', 'email', 'phone', 'lastApplicationFor' ]
        if self.dev_mode:
            logging.info( 'AccountCommand.set_valid_keys(%s)' % str( valid_keys ) )
        super( AccountCommand, self ).set_valid_keys( valid_keys=valid_keys )


    def save_session(self):
        """Method for updating the session trace and trigger the evaluation
            process for user interactions.
        """

        self.session.update_session_trace(
            action=self.action,
            interface=self.interface,
            status_code=self.response_data[ 'status_code' ],
            request_data=self.dumps( )
        )


    def dumps(self):
        """Method for dumping the command data.

        Returns:
            A dict with the command attribute configuration.
        """

        instance_data = super( AccountCommand, self ).dumps( [
            'client_ip',
            'session_id',
            'method',
            'service',
            'cmd',
            'user_class',
            'interface',
            'action',
            'description',
            'date_format',
            'valid_keys',
            'json_data',
            'param_data',
            'response_data'
        ] )

        return instance_data


