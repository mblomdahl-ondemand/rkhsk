# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.user.AccountApiInterface
# Author:  Mats Blomdahl
# Version: 2012-03-18

import logging, random, time, re, webapp2

from webapp2_extras import sessions

from google.appengine.api import taskqueue

from rkhsk.handler.generic import UniHandler
from rkhsk.handler.user import AccountCommand

from rkhsk.model import RkhskMember, PaymentKey, AccountEmail, AccountUri, SysTmpKey


# TODO(mats.blomdahl@gmail.com): Improve handler.user.AccountApiInterface documentation!
class AccountApiInterface( webapp2.RequestHandler ):
    """The API interface for the web application's registration page served at
        http://regga.rkh-sk.se/

    The request handler routes all incoming request through self.__unihandler
        where a command is set up and the appropriate method is called.

    The handler deploys secure cookies using the 'account' store and expiration
        is controlled by the global variable SESSION_DEFAULT_EXPIRATION.
    """

    # Session Management
    def dispatch(self):
        """Get a session store for this request.

        Returns:
            None
        """

        self.session_store = sessions.get_store( request=self.request )

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch( self )
        finally:
            # Save all sessions.
            self.session_store.save_sessions( self.response )

    @webapp2.cached_property
    def session(self):
        """Returns a session using the default cookie key.

        Returns:
            Foo.
        """

        _SESSION_DEFAULT_EXPIRATION = self.app.config.get('_SESSION_DEFAULT_EXPIRATION')
        return self.session_store.get_session( name='default', max_age=_SESSION_DEFAULT_EXPIRATION,
                                               backend='securecookie' )

    # Redirects
    def get(self, service, cmd):
        """Handler for redirecting HTTP-GET requests to the member interface.

        Arguments:
            service:
            cmd:

        Returns:
            Foo.
        """

        return self.__unihandler( 'GET', service, cmd )

    def post(self, service, cmd):
        """Handler for redirecting HTTP-POST requests to the member interface.

        Arguments:
            service:
            cmd:

        Returns:
            Foo.
        """

        return self.__unihandler( 'POST', service, cmd )

    # Interface: /account/<service>/<cmd>
    def __unihandler(self, method, service, cmd):
        """Method for delegating requested functionality to the appropriate service.

        Arguments:
            method: The HTTP method used (i.e. 'GET' or 'POST').
            service: The main functionality requested.
            cmd: An optional, service-specific, command.

        Returns:
            Foo.
        """

        starttime = time.time( )

        _DEVMODE = self.app.config.get( '_DEVMODE' )

        command = AccountCommand(
            handler_instance=self,
            method=method,
            service=service,
            cmd=cmd,
            session_id=self.session.get( 'session_id' ),
            user_id=self.session.get( 'user_id' ),
            dev_mode=_DEVMODE
        )

        unihandler = UniHandler( handler_name='AccountApiInterface.__unihandler()', command=command,
                                 dev_mode=_DEVMODE )

        unihandler.handle_request( )

        unihandler.serve_response( loadtime={'loadTime': '%s ms' % str( int( (time.time( ) - starttime) * 1000 ) )} )


    # Interface: /account/retrieve/
    def retrieve(self, command):
        """Method for retrieving account data to the details/status view on the web
            application's registration page.

        Arguments:
            command.payload['loginUsername']: The member's email address (passed through
                user.AccountCommand).
            command.payload['loginCredentials']: One of the member's recent payment keys
                (passed through user.AccountCommand).

        Returns:
            A JSON object with pretty-print member data.
        """

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 403,
                'errors': {'invalidSession': 'The user\'s session is invalid.'}
            }

        command.set_action( 'user_details_retrieval' )
        command.set_description( 'A user-initiated account details retrieval command.' )
        command.set_valid_keys( [
            'firstName',
            'lastName',
            'lastApplicationFor',
            'admittedSemester',
            'email',
            'phone',
            'studentConfirmation'
        ] )

        try:
            email = command.payload[ 'loginUsername' ].lower( )
            payment_key = command.payload[ 'loginCredentials' ].upper( )
            logging.info( 'AccountApiInterface.__retrieve().loginUsername: %s' % email )
            logging.info( 'AccountApiInterface.__retrieve().loginCredentials: %s' % payment_key )
        except (KeyError):
            return {
                'success': False,
                'status_code': 400,
                'errors': {'invalidArguments': 'Required arguments are missing.'}
            }

        if re.search( '^[A-Z]{5}$', payment_key ) is not None and re.search( '^[a-z_\-\.\d]+@[a-z_\-\.\d]+\.[a-z]{2,4}$'
                                                                             ,
                                                                             email, flags=re.I ) is not None:
            member_connector = PaymentKey.get_by_key_name( payment_key )
            if not member_connector: # PaymentKey instance not found
                return {
                    'success': False,
                    'status_code': 403,
                    'errors': {'invalidCredentials': 'Submitted credentials does not match a registered member.'}
                }

            member = member_connector.memberRef
            if member and member.email == email: # Compile a JSON response object
                street_address = '%s %s%s' % (member.streetAddress, member.streetNumber, member.streetEntrance)
                if member.streetApartment:
                    street_address += ' / lgh. %s' % member.streetApartment
                if member.streetFloor:
                    street_address += ', %s' % member.streetFloor
                street_address += '<br>SE-%s  %s' % (member.zipCode, member.city)
                if member.streetCo:
                    street_address = 'c/o %s<br>%s' % (member.streetCo, street_address)

                application_semester_history = '[ Inga anmälningar registrerade ]'
                for i in range( len( member.applicationSemesterHistory ) ):
                    if i == 0:
                        application_semester_history = ''
                    else:
                        application_semester_history += ', '
                    application_semester_history += '%s (%s)' % (
                        member.applicationSemesterHistory[ i ],
                        member.applicationDateHistory[ i ].strftime( '%y-%m-%d' ))

                try:
                    last_payment_recieved = member.lastPaymentRecieved.strftime( '%Y-%m-%d' )
                except (NameError, AttributeError):
                    last_payment_recieved = '[ Inga betalningar registrerade ]'

                membership_history = '[ Tidigare medlemskap saknas ]'
                if (len( member.membershipHistory )):
                    membership_history = member.membershipHistory[ 0 ]
                    for i in range( len( member.membershipHistory ) ):
                        if i == 0:
                            membership_history = ''
                        else:
                            membership_history += ', '
                        membership_history += member.membershipHistory[ i ]

                mecenat_history = '[ Tidigare registreringar saknas ]'
                for i in range( len( member.mecenatHistory ) ):
                    if i == 0:
                        mecenat_history = ''
                    else:
                        mecenat_history += ', '
                    mecenat_history += member.mecenatHistory[ i ]

                ssco_history = '[ Tidigare registreringar saknas ]'
                for i in range( len( member.sscoHistory ) ):
                    if i == 0:
                        ssco_history = ''
                    else:
                        ssco_history += ', '
                    ssco_history += member.sscoHistory[ i ]

                member_data = command.get_account_data( member )
                member_data.update( {
                    'lastPaymentRecieved': last_payment_recieved,
                    'applicationSemesterHistory': application_semester_history,
                    'sscoHistory': ssco_history,
                    'mecenatHistory': mecenat_history,
                    'membershipHistory': membership_history,
                    'streetAddress': street_address
                } )

                if member.sys_email_verified is False:
                    member.set_email_verified( )
                    member.put( )

                return {
                    'success': True,
                    'data': member_data
                }

        return {
            'success': False,
            'status_code': 403,
            'errors': {'invalidCredentials': 'Submitted credentials does not match a registered member.'}
        }


    # Interface: /account/register/
    def register(self, command):
        """Method for receiving new membership applications.

        Arguments:
            command.payload['admittedSemester']: The member's admission semester.
            command.payload['lastApplicationFor']: The target application semester.
            command.payload['firstName']: The member's first name.
            command.payload['lastName']: The member's last name.
            command.payload['streetCo']: The member's c/o address (optional).
            command.payload['streetAddress']: The member's street address.
            command.payload['streetNumber']: The member's street address number.
            command.payload['streetEntrance']: The member's street entrance (optional).
            command.payload['streetApartment']: The member's apartment number (optional).
            command.payload['streetFloor']: The member's building floor (optional).
            command.payload['zipCode']: The member's zip code.
            command.payload['city']: The member's city.
            command.payload['country']: The member's country.
            command.payload['email']: The member's email address.
            command.payload['phone']: The member's phone number (optional).
            command.payload['studentConfirmation']: The member's confirmation of currently
                studying at RKH.

        Returns:
            A JSON object with the registered member data.
        """

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 403,
                'errors': {'invalidSession': 'The user\'s session is invalid.'}
            }

        command.set_action( 'user_registration' )
        command.set_description( 'A user-initiated registration request.' )
        command.set_valid_keys( [
            'admittedSemester',
            'lastApplicationFor',
            'firstName',
            'lastName',
            'streetCo',
            'streetAddress',
            'streetNumber',
            'streetEntrance',
            'streetApartment',
            'streetFloor',
            'zipCode',
            'city',
            'country',
            'email',
            'phone',
            'studentConfirmation'
        ] )

        if command.payload[ 'studentConfirmation' ]:
            command.payload[ 'studentConfirmation' ] = True
        else:
            command.payload[ 'studentConfirmation' ] = False
        command.payload[ 'email' ] = command.payload[ 'email' ].lower( )
        command.payload[ 'admittedSemester' ] = command.payload[ 'admittedSemester' ].upper( )
        command.payload['streetNumber'] = str(command.payload['streetNumber'])
        command.payload['zipCode'] = str(command.payload['zipCode'])
        command.payload['streetApartment'] = str(command.payload['streetApartment'])
        if command.payload['phone']:
            command.payload['phone'] = str(command.payload['phone'])

        logging.info( 'AccountApiInterface.__register().command.cmd: %s' % str( command.payload ) )

        required_fields = [
            'admittedSemester',
            'lastApplicationFor',
            'personalId',
            'firstName',
            'lastName',
            'streetAddress',
            'streetNumber',
            'zipCode',
            'city',
            'country',
            'email',
            'studentConfirmation'
        ]

        for field in required_fields:
            if not command.payload[field]:
                return {
                    'success': False,
                    'status_code': 400,
                    'errors': {'invalid_arguments': 'One or more required arguments are missing.'}
                }

        member_data = command.payload
        command.payload = [command.payload]

        if RkhskMember.create_or_update( command ):
            return {
                'success': True,
                'data': [member_data]
            }

        else:
            return {
                'success': False,
                'status_code': 403,
                'errors': {'invalid_request': 'The submitted registration form contained an invalid dataset.'}
            }


    # Interface: /account/load/
    def load(self, command):
        """Method for loading a personalized application form.

        Arguments:
            cmd: A member's unique accountPageUri attribute (through user.AccountCommand).

        Returns:
            An array containing member details.
        """

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 403,
                'errors': {'invalidSession': 'The user\'s session is invalid.'}
            }

        if len( command.cmd ) == 30:
            command.set_action( 'user_details_loader' )
            command.set_description( 'Load user details to the membership application form.' )
            command.set_valid_keys( [
                'firstName',
                'lastName',
                'streetCo',
                'streetAddress',
                'streetNumber',
                'streetEntrance',
                'streetApartment',
                'admittedSemester',
                'streetFloor',
                'zipCode',
                'city',
                'phone',
                'email'
            ] )

            logging.info( 'AccountApiInterface.__load().command.cmd: %s' % command.cmd )
            if re.search( '^[\da-z]{30}$', command.cmd, flags=re.I ) is not None:
                account_link = AccountUri.get_by_key_name( command.cmd )
                if account_link is None:
                    return {'success': False, 'status_code': 403, 'body': False} # forbidden
                try:
                    member = account_link.memberRef
                except:
                    return {
                        'success': False,
                        'status_code': 500, # internal server error
                        'errors': {
                            'system': 'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this '
                                      'message.'}
                    }
                if member.sys_deleted:
                    return {
                        'success': False,
                        'status_code': 400, # bad request
                        'errors': {'accountDeleted': 'The associated account has been deleted.'}
                    }
                if member.sys_email_verified is False:
                    member.set_email_verified( )
                    member.put( )
                member_data = command.get_account_data( member )
                return {
                    'success': True,
                    'data': member_data
                }

        elif len( command.cmd ) == 26:
            command.set_action( 'user_credentials_loader' )
            command.set_description( 'Load user details to the membership application form.' )
            command.set_valid_keys( [
                'email',
                'lastPaymentKey'
            ] )

            logging.info( 'AccountApiInterface.__load().command.cmd: %s' % command.cmd )
            if re.search( '^[\da-z]{26}$', command.cmd, flags=re.I ) is not None:
                account_link = SysTmpKey.get_by_key_name( command.cmd )
                if account_link is None or account_link.is_member_login_reminder( ) is False:
                    return {'success': False, 'status_code': 403, 'body': False}
                if account_link.is_expired( ):
                    return {
                        'success': False,
                        'status_code': 403,
                        'errors': {
                            'linkExpired': 'Link expired %s' % account_link.sys_expired.isoformat( ).split( '.' )[ 0 ]}
                    }
                member = account_link.get_member( )
                if member is None:
                    logging.warning(
                        'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this message.' )
                    return {
                        'success': False,
                        'status_code': 500,
                        'errors': {
                            'system': 'Intralink broken! Damn! Please notify mats.blomdahl@gmail.com about this '
                                      'message.'}
                    }
                if member.sys_deleted:
                    return {
                        'success': False,
                        'status_code': 400,
                        'errors': {'accountDeleted': 'The associated account has been deleted.'}
                    }
                else:
                    account_link.set_expired( )
                    if member.sys_email_verified is False:
                        member.set_email_verified( )
                        member.put( )
                    member_data = command.get_account_data( member )
                    return {
                        'success': True,
                        'data': {'loginUsername': member_data[ 'email' ],
                                 'loginCredentials': member_data[ 'lastPaymentKey' ]}
                    }

        logging.info( 'AccountApiInterface.__load().command.cmd: Unrecognized command \'%s\'' % command.cmd )

        return {
            'success': False,
            'status_code': 403,
            'errors': {'invalidSession': 'The user\'s session is invalid.'}
        }


    # Interface: /account/resend/
    def resend(self, command):
        """Method for sending out reminders with login information.

        Arguments:
            command.payload['loginUsername']: The members email address.

        Returns:
            A confirmation of submission to task queue.
        """

        if not command.is_valid_session( ):
            return {
                'success': False,
                'status_code': 403,
                'errors': {'invalidSession': 'The user\'s session is invalid.'}
            }

        command.set_action( 'user_login_reminder' )
        command.set_description( 'Resend user login details.' )
        command.set_valid_keys( [ ] )

        try:
            email = command.payload[ 'loginUsername' ] #self.request.get('loginUsername', default_value = '')
            logging.info( 'AccountApiInterface.__resend().email: %s' % email )
        except (KeyError):
            logging.info( 'AccountApiInterface.__resend(): No loginUsername supplied.' )
        if email:
            account_link = AccountEmail.get_by_key_name( email )
            if account_link and account_link.memberRef:
                member = command.get_member( account_link.memberRef )
                member.set_tmpkey( 'member_login_reminder' )
                member.put( )

                taskqueue.add(
                    queue_name='mailer' + str( random.randrange( 4 ) ),
                    url='/tasks/send_member_details_reminder/',
                    params={
                        'personal_id': member.personalId,
                        'client_ip': command.client_ip
                    },
                    countdown=5
                )

                return {
                    'success': True,
                    'data': {'emailSent': 'TaskQueued'}
                }

        return {
            'success': False,
            'status_code': 400,
            'errors': {}
        }


