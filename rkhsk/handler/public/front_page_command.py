# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.public.FrontPageCommand
# Author:  Mats Blomdahl
# Version: 2012-03-20

import logging
from rkhsk.handler.generic import Command

from rkhsk.model import RkhskMember

class FrontPageCommand( Command ):
    """Wrapper class for requests to load the client web application."""

    def __init__(self, handler_instance, method, service, session_id, user_id, dev_mode=False):
        """Initialization method for the command wrapper.

        Arguments:
            request: A WebOb-compatible request object.
            method: The HTTP method used (i.e. "GET" or "POST").
            service: The main functionality requested.
            cmd: An optional, service-specific, command.
        """

        if dev_mode:
            logging.info(
                'FrontPageCommand.__init__(account_source=RkhskMember, handler_instance, method=\'%s\', '
                'service=\'%s\', cmd=\'None\', session_id=\'%s\', user_id=\'%s\', dev_mode=%s)' % (
                    str( method ), str( service ), str( session_id ), str( user_id ), str( dev_mode )) )

        super( FrontPageCommand, self ).__init__( account_source=RkhskMember, handler_instance=handler_instance,
                                                  method=method, service=service, cmd=None, session_id=session_id,
                                                  user_id=user_id, dev_mode=dev_mode, user_class='any',
                                                  interface='webapp_interface' )

    def save_session(self):
        """Method for updating the session trace and trigger the evaluation
            process for user interactions.
        """

        self.session.update_session_trace(
            action=self.action,
            interface=self.interface,
            status_code=self.response_data[ 'status_code' ],
            request_data=self.dumps( )
        )


    def dumps(self):
        """Method for dumping the command data.

        Returns:
            A dict with the command attribute configuration.
        """

        instance_data = super( FrontPageCommand, self ).dumps( [
            'client_ip',
            'session_id',
            'method',
            'service',
            'cmd',
            'user_class',
            'interface',
            'action',
            'description',
            'date_format',
            'valid_keys',
            'json_data',
            'param_data',
            'response_data'
        ] )

        return instance_data


