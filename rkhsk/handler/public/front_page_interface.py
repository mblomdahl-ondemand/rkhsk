# -*- coding: utf-8 -*-
#
# Title:   rkhsk.handler.public.FrontPageInterface
# Author:  Mats Blomdahl
# Version: 2012-08-27

import logging

from time import time
from webapp2 import RequestHandler, cached_property
from webapp2_extras import sessions

from google.appengine.ext import ndb
from rkhsk.handler.generic import UniHandler
from rkhsk.handler.public import FrontPageCommand
from rkhsk.model.generic import SessionTrace

class FrontPageInterface( RequestHandler ):
    """The public interface for retrieving the registration page at located at
        http://regga.rkh-sk.se/

    The interface deploys secure cookies using the 'default' store and cookie
        expiration is controlled by the global variable SESSION_DEFAULT_EXPIRATION.
    """

    # session management
    def dispatch(self):
        """Get a session store for this request."""
        self.session_store = sessions.get_store( request=self.request )

        try:
            # Dispatch the request.
            RequestHandler.dispatch( self )
        finally:
            # Save all sessions.
            self.session_store.save_sessions( self.response )

    @cached_property
    def session(self):
        """Returns a session using the default cookie key."""

        _SESSION_DEFAULT_EXPIRATION = self.app.config.get('_SESSION_DEFAULT_EXPIRATION')
        return self.session_store.get_session( name='default', max_age=_SESSION_DEFAULT_EXPIRATION,
                                               backend='securecookie' )


    # /
    # /app_pm
    # /app_pd
    def get(self, service='default'):
        """Request handler for delegating requests functionality to the appropriate
            service and delivering the web application user interface.

        The request handler will also ensure that the client's session cookie is
            set. The session cookie is will be required by any and all subsystems
            within this solution.

        Arguments:
            service: Supported values are '', 'app_pm' and 'app_pd'.
        """

        logging.info( 'FrontPageInterface hit!' )
        if not service:
            service = 'default'

        #baz = Handle(id = 'barbara', foo = 'barabara')
        #baz.put()
        #txt = 'barabarazs2z'
        #baz = Handle( bar=txt )
        #logging.info('this is baz: %s' % str(baz.foo))
        #return

        #self.response.headers.add_header("Access-Control-Allow-Origin", "*")

        starttime = time( )

        _DEVMODE = self.app.config.get( '_DEVMODE' )

        session_id = self.session.get( 'session' )
        if session_id is None:
            session_id = SessionTrace.generate_session_id( )
            self.session[ 'session' ] = session_id

        command = FrontPageCommand(
            handler_instance=self,
            method='GET',
            service=service,
            session_id=session_id,
            user_id=self.session.get( 'user_id' ),
            dev_mode=_DEVMODE
        )

        unihandler = UniHandler( handler_name='RkhskAccountInterface.__unihandler()', command=command,
                                 dev_mode=_DEVMODE )

        unihandler.handle_request( )

        unihandler.serve_response( loadtime={'loadTime': '%s ms' % str( int( (time( ) - starttime) * 1000 ) )} )


    def default(self, command):
        """Method for serving the default web application registration interface.

        Selection of mobile or desktop version of the application is controlled by
            the command's 'client_is_mobile' method.
        """

        if command.client_is_mobile( ):
            if False and command.dev_mode:
                page = 'index-rm-dev.html'
            else:
                page = 'index-rm.html'
        else:
            if False and command.dev_mode:
                page = 'index-rd-dev.html'
            else:
                page = 'index-rd.html'

        page_content = open( page, 'r' )
        response_data = {
            'success': True,
            'headers': {'content_type': 'text/html'},
            'data': page_content.read( )
        }

        page_content.close( )

        """
        if command.client_is_mobile( ):
            if command.dev_mode:
                page = 'app/public_mobile/app.html'
            else:
                pass
                #page = 'index-rm.html'
        else:
            if command.dev_mode:
                page = 'app/public_desktop/app.html'
            else:
                pass
                #page = 'index-rd.html'

        page_content = open( page, 'r' )
        response_data = {
            'success': True,
            'headers': {'content_type': 'text/html'},
            'data': page_content.read( )
        }
        #self.response.write(page_content.read())
        page_content.close( )
        """
        return response_data


    def app_pm(self, command):
        """Method for serving the web application registration interface with the
            application's mobile version enforced.
        """

        if command.dev_mode:
            page = 'app/public_mobile/app.html'
        else:
            pass
            #page = 'index-rm.html'

        page_content = open( page, 'r' )
        response_data = {
            'success': True,
            'headers': {'content_type': 'text/html'},
            'data': page_content.read( ).replace('app.js', 'app_pm.js')
        }
        #self.response.write(page_content.read())
        page_content.close( )

        return response_data


    def app_pd(self, command):
        """Method for serving the web application registration interface with the
            application's desktop version enforced.
        """

        if command.dev_mode:
            page = 'app/public_desktop/app.html'
        else:
            pass
            #page = 'index-rm.html'

        page_content = open( page, 'r' )
        response_data = {
            'success': True,
            'headers': {'content_type': 'text/html'},
            'data': page_content.read( ).replace('app.js', 'app_pd.js')
        }
        #self.response.write(page_content.read())
        page_content.close( )

        return response_data
