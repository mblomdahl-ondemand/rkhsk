Ext.require([
    'Ext.Panel',
    'Ext.form.field.VTypes',
    'Ext.form.Action',
    'Ext.form.Label',
    'Ext.form.Panel',
    'Ext.tab.Panel',
    'Ext.layout.container.Card',
    'Ext.tab.Bar',
    'Ext.tab.Tab',
    'Ext.form.FieldAncestor',
    'Ext.form.Basic',
    'Ext.form.action.Load',
    'Ext.form.action.Submit',
    'Ext.window.MessageBox',
    'Ext.form.field.Text',
    'Ext.form.field.Base',
    'Ext.form.Labelable',
    'Ext.form.field.Field',
    'Ext.layout.component.field.Field',
    'Ext.layout.component.field.Text',
    'Ext.layout.component.field.TextArea',
    'Ext.form.field.TextArea',
    'Ext.layout.container.Anchor',
    'Ext.ProgressBar',
    'Ext.form.FieldSet',
    'Ext.form.field.Checkbox',
    'Ext.form.CheckboxManager',
    'Ext.layout.component.FieldSet',
    'Ext.form.RadioGroup',
    'Ext.form.FieldContainer',
    'Ext.layout.component.field.FieldContainer',
    'Ext.form.field.Radio',
    'Ext.form.RadioManager',
    'Ext.form.field.Picker',
    'Ext.form.field.ComboBox',
    'Ext.form.field.Trigger',
    'Ext.layout.component.field.Trigger',
    'Ext.layout.container.Column',
    'Ext.view.BoundList',
    'Ext.view.View',
    'Ext.view.AbstractView',
    'Ext.selection.DataViewModel',
    'Ext.selection.Model',
    'Ext.layout.component.BoundList',
    'Ext.toolbar.Paging',
    'Ext.toolbar.TextItem',
    'Ext.form.field.Number',
    'Ext.form.field.Spinner',
    'Ext.view.BoundListKeyNav',
    'RKHSK.store.AdmissionSemesterD',
    'RKHSK.store.StreetFloor',
    'RKHSK.controller.RegCtrlD'
]);

Ext.application({
    name       : "RKHSK",
    appFolder  : "app",
    controllers: [
        'RegCtrlD'
    ]
});

/*
 Ext.Loader.setConfig({
 enabled: true
 });

 Ext.application({
 name: 'MyApp',

 launch: function() {
 Ext.QuickTips.init();

 var cmp1 = Ext.create('MyApp.view.MyViewport', {
 renderTo: Ext.getBody()
 });
 cmp1.show();
 }
 });

 <script type="text/javascript" src="app/view/ui/MyViewport.js"></script>
 <script type="text/javascript" src="app/view/MyViewport.js"></script>
 */
