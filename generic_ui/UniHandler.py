#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   UniHandler
# Author:  Mats Blomdahl
# Version: 2012-03-15

import json
import logging
import datetime

class UniHandler(object):
  """A generic redirect handler.
    
  Attributes:
    handler_name: The handler's name or descriptor, primarily used for event
      logging, a string.
    command: A Command instance.
    dev_mode: A boolean flag marking development mode. If True, logging will
      be suppressed.
    response_data: The resulting dataset created by the 'serve_response'
      instance method.
  """
  
  def __init__(self, handler_name, command, dev_mode = False):
    """Constructor for the uni-handler.
      
    Arguments:
      handler_name: The handler's name or descriptor, primarily used for
        event logging, a string.
      command: A Command instance.
      dev_mode: A boolean flag marking development mode, logging will be
        suppressed if True (default = False).
    """
    
    self.handler_name = handler_name
    self.command = command
    self.dev_mode = dev_mode
    self.response_data = None
  
  
  def handle_request(self):
    """Method for handling a user's request.
      
    The method attempts to identify and call a matching handler method within
      the Command instance's WebOb-compatible 'request' attribute.
      
    Returns:
      The identified handler method's response or a 400 error if the 
        identification process failed.
    """
    
    response_data = None
    handler_name = self.handler_name
    command = self.command
    dev_mode = self.dev_mode
    
    func = getattr(command.request, command.service, None)
    if func:
      if dev_mode:
        logging.info('%s.func: \'%s\' found in command.service' % (handler_name, command.service))
      response_data = func(command)
    elif command.json_data:
      for item in command.json_data.keys():
        if item[0] == '_':
          if dev_mode:
            logging.info('%s: Hacking attempt??? Attempted syscall (%r) on /admin/update interface from IP address %s (command.json_data=%r).' % (handler_name, item, command.client_id, str(command.json_data)))
          response_data = {
            'success'    : False,
            'status_code': 400,
            'errors'     : { 'invalidCommand': 'Hacking attempt??? Syscall \'%s\' passed to %s.' % (handler_name, item) }
          }
          break
        
        func = getattr(command.request, item, None)
        if func:
          if dev_mode:
            logging.info('%s.func: \'%s\' found in command.json_data.keys()!' % (handler_name, item))
          response_data = func(command)
          break
        elif dev_mode:
          logging.info('%s: No match for \'%s\' in command.json_data.keys()!' % (handler_name, item))
    
    if response_data is None:
      try:
        json_data_keys = command.json_data.keys()
      except (AttributeError):
        json_data_keys = []
      interface_params = {
        'method'    : method,
        'service'   : service,
        'cmd'       : cmd,
        'request_id': request_id
      }
      response_data = {
        'success'    : False,
        'status_code': 400,
        'errors'     : { 'invalidCommand': 'No handlers matched to interface params %s and JSON data keys %s' % (str(interface_params), str(json_data_keys)) }
      }
    
    if not 'status_code' in response_data and response_data['success'] is True:
      response_data.update({ 'status_code': 200 })
    
    command.set_response(response_data)
    
    self.response_data = response_data
    
    return response_data
  
  
  def serve_response(self, **kwargs):
    """Method for serving the request response.
      
    The method serves the class' 'response_data' attribute to the user 
      enriched with any parameters in 'kwargs'. The response is served using
      the WebOb-compatible response object found in the UniHandler's 'command'
      attribute.
      
    Arguments:
      kwargs: Custom keyword arguments to be merged into a JSON data response,
        the arguments must be values of type 'dict'.
    """
    
    handler_name = self.handler_name
    response_data = self.response_data
    command = self.command
    
    logging.info('%s.stats: type=\'POST\' / user_id=\'%s\' / url=\'/admin/\' / service=\'%s\' / cmd=\'%s\' / request_id=\'%s\' / json_body=\'%s\'' % (handler_name, str(command.user_id), str(service), str(cmd), str(request_id), str(command.json_data)))
    command.response.headers.add_header("Access-Control-Allow-Origin", "*")
    if 'headers' in response_data:
      for header, value in response_data['headers'].iteritems():
        if hasattr(command.response, header):
          logging.info('attr. %s set to %s' % (header, value))
          setattr(command.response, header, value)
        else:
          logging.info('header %s added with value %s' % (header, value))
          command.response.headers.add_header(header, value)
      
    if 'status_code' in response_data:
      command.response.status = response_data['status_code']
    if 'location' in response_data:
      command.response.location = response_data['location']
    if 'body' in response_data:
      if response_data['body']:
        if isinstance(response_data['body'], unicode):
          command.response.unicode_body = response_data['body']
        else:
          command.response.body = response_data['body']
    else:
      command.response.content_type = 'application/json'
      for param in kwargs:
        response_data.update(kwargs[param])
      body = json.dumps(response_data)
      if isinstance(body, unicode):
        command.response.unicode_body = body
      else:
        command.response.body = body
    if 'redirect' in response_data:
      command.response.redirect(response_data['redirect'])
    logging.info('%s.response_data: success=\'%s\' / headers=\'%s\' / body=\'%s\'' % (handler_name, response_data['success'], command.response.headers, response_data))
    
    command.save_session()
  

