#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Title:   Command
# Author:  Mats Blomdahl
# Version: 2012-03-18

import cgi
import json
import logging

from datetime import datetime
from generic_model import SessionTrace

# TODO(mats.blomdahl@gmail.com): After testing is complete, implement property and @property decorators.
class Command(object):
  """Wrapper class for commands called through the system GUI.
    
  The class recieves a command, JSON request body or a combination of both and
    packs it for easy reusability. The class will also store miscellaneous
    auxiliary data associated with the request.
    
  Attributes:
    request: A WebOb-compatible 'request' instance.
    response: A WebOb-compatible 'response' instance.
    
    method: The HTTP method used (i.e. 'GET' or 'POST').
    service: The main functionality requested.
    cmd: An optional, service-specific, command.
    
    client_ip: The client's IP address.
    session_id: The request's session ID (a cookie used for tracking handler
      resource usage, enforcing usage limits and analyzing user interaction
      patterns).
    user_id: The decoded user ID an admin will recieve on login.
    
    dev_mode: A boolean flag marking development mode. If True, logging will be
      suppressed.
    
    json_data: An optional dict, extracted from the WebOb-compatible 'request'
      instance attribute.
    param_data: An optional dict, extracted from the WebOb-compatible 'request'
      instance attribute.
    payload: The actual request data submitted with the request, extracted from
      class attributes 'json_data' and/or 'param_data'.
    valid_keys: An array containing account attributes available for processing
      by this command.
    date_format: A date formating string (default value = 'isoformat') used
      for controlling how retrieved datetime.datetime instances are converted.
    
    interface: An interface identifier (a string in lowercase_underscore_format)
      that might be used by external methods for classifying the request.
    user_class: A user class identifier that might be used by an external
      method recieving the Command wrapper for determining an appropriate
      handling.
    action: An action identifier (in lowercase_underscore_format) that might be
      used by a recieving method for determining an appropriate handling.
    description: A short description of what the command does, primarily used
      for debugging and logging.
  """
  
  def __init__(self, account_source, request, response, method, service, cmd, session_id, user_id, dev_mode = False, **kwargs):
    """Initialization method for the command wrapper.
      
    Arguments:
      request: A WebOb-compatible request object.
      response: A WebOb-compatible response object.
      method: The HTTP method used (i.e. "GET" or "POST").
      service: The main functionality requested.
      cmd: An optional, service-specific, command.
      session_id: The request's session ID (a cookie used for tracking handler
        resource usage, enforcing usage limits and analyzing user interaction
        patterns).
      user_id: The decoded user ID an admin will recieve on login.
      dev_mode: A boolean flag marking development mode, logging will be
        suppressed if True (default = False).
      kwargs: An (optional) set of keyword arguments to be applied as
        attributes of the Command instance.
    """
    
    if dev_mode:
      logging.info('Command.__init__(request, response, %s, %s, %s)' % (str(method), str(service), str(cmd)))
    
    self.account_source = account_source
    self.request = request
    self.response = response
    self.method = method
    self.service = service
    self.cmd = cmd
    
    self.client_ip = request.remote_addr
    self.session_id = session_id
    self.session = SessionTrace(
      client_ip = self.client_ip,
      session_id = session_id,
      user_id = user_id,
      dev_mode = dev_mode
    )
    
    self.user_id = user_id
    
    self.dev_mode = dev_mode
    
    self.user_class = None
    self.interface = None
    self.action = None
    self.description = None
    self.response_data = None
    
    self.set_date_format() # defaults to 'isoformat'
    
    for param in kwargs:
      if dev_mode:
        logging.info('Command.__init__(request, response, method, service, cmd, %s): Custom attribute \'%s\' set to %s.' % (param, param, str(kwargs[param])))
      setattr(self, param, kwargs['param'])
    
    args = request.arguments()
    self.param_data = {}
    for param in args:
      value = request.get(param, default_value = None)
      if value is not None:
        value = cgi.escape(value)
      self.param_data.update({ param: value })
    
    try:
      self.json_data = json.loads(request.body)
      self.payload = self.param_data
      self.payload.update(json_data)
    except (ValueError):
      self.json_data = None
      self.payload = self.param_data
  
  
  def is_valid_session(self):
    """Getter method for checking session validity.
      
    Returns:
      True: If the session is valid (i.e. it exists and is not blocked).
      False: If the session doesn't exist or is blocked.
    """
    
    result = self.session.verify_session(self.action, self.interface)
    if result['clear_to_recieve']:
      if self.dev_mode:
        logging.info('Command.is_valid_session(): %s' % str(result))
      return True
    else:
      if self.dev_mode:
        logging.info('Command.is_valid_session(): %s' % str(result))
      return False
  
  
  def from_verified_admin(self):
    """Getter method for checking authorization status.
      
    Returns:
      True: If the 'user_id' attribute matches an administrators' account key
        name.
      False: If 'user_id' attribute does not belong to an administrator.
    """
    
    if self.session.verify_as_admin():
      if self.dev_mode:
        logging.info('Command.from_verified_admin(): True')
      return True
    else:
      if self.dev_mode:
        logging.info('Command.from_verified_admin(): False')
      return False
  
  
  def set_valid_keys(self, valid_keys):
    """Setter for applying the 'valid_keys' attribute.
      
    The 'valid_keys' attribute control which attributes of an account will be
      available for update as well as the set of attributes can be returned by
      self.get_account_data().
      
    Arguments:
      valid_keys: An array containing the attributes to work with.
    """
    
    if self.dev_mode:
      logging.info('Command.set_valid_keys(%s)' % str(valid_keys))
    self.valid_keys = valid_keys
  
  
  def set_description(self, description):
    """Setter for applying the 'description' attribute.
      
    A short description of what the command does, primarily used for debugging
      and logging.
      
    Arguments:
      description: A short description of what the command does.
      
    """
    
    if self.dev_mode:
      logging.info('Command.set_description(%s)' % description)
    self.description = description
    
  
  
  def set_action(self, action):
    """Setter for applying the 'action' attribute.
      
    The 'action' attribute can be used by the recieving method for determining
      the appropriate handling (when the AccountCommand object is passed as 
      container for the arguments).
      
    Arguments:
      action: An action identifier (in lowercase_underscore_format).
    """
    
    if dev_mode:
      logging.info('Command.set_action(%s)' % action)
    self.action = action
  
  
  def set_response_data(self, response_data):
    """Setter for applying the request handler's response data.
      
    Arguments:
      response_data: The request handler method's response, a dict.
    """
    
    if self.dev_mode:
      logging.info('Command.set_response_data(%s)' % str(response_data))
    self.response_data = response_data
  
  
  def set_date_format(self, date_format = 'isoformat'):
    """Setter for applying the 'date_format' attribute.
      
    The 'date_format' attribute controls to which format datetime.datetime
      instances will be converted on retrieval by self.get_account_data().
      
    Arguments:
      date_format: A date formating string (default value = 'isoformat').
    """
    
    if self.dev_mode:
      logging.info('Command.set_date_format(%s)' % date_format)
    self.date_format = date_format
  
  
  def get_account_data(self, account):
    """Getter method for retrieving account data.
      
    The method returns account data specified by the 'valid_keys' attribute,
      deleted accounts will be ignored.
      
    Arguments:
      account: A instance 'key_name' attribute or a instance of the
        self.account_source class.
      
    Returns:
      A dict containing the account data.
    """
    
    if isinstance(account, self.account_source):
      if account.sys_deleted:
        return False # TODO(mats.blomdahl@gmail.com): Change to raise an appropriate exception.
      account_data = account.dumps(self.valid_keys, self.date_format)
    else:
      account = self.account_source.get_by_key_name(account)
      if not account or account.sys_deleted:
        return False # TODO(mats.blomdahl@gmail.com): Change to raise an appropriate exception.
      account_data = account.dumps(self.valid_keys, self.date_format)
    
    if self.dev_mode:
      logging.info('AccountCommand.get_account_data(%s): %s' % (account.key_name, str(account_data)))
    return account_data
  
  
  def save_session(self):
    """Method for updating the session trace and trigger the evaluation
      process for user interactions.
    """
    
    self.session.update_session_trace(
      action = self.action,
      interface = self.interface,
      status_code = self.response_data['status_code'],
      request_data = self.dumps()
    )
  
  
  def dumps(self, attribs = None):
    """Method for dumping the command data.
      
    Returns:
      A dict with the command attribute configuration.
    """
    
    response = {}
    try:
      for attr in attribs:
        value = getattr(self, attr, None)
        if isinstance(value, datetime.datetime):
          value = value.isoformat().split('.')[0]
        response.update({ attr: value })
    
    except (TypeError):
      response.update({
        'client_ip'    : self.client_ip,
        'user_id'      : self.user_id,
        'session_id'   : self.session_id,
        'method'       : self.method,
        'service'      : self.service,
        'cmd'          : self.cmd,
        'user_class'   : self.user_class,
        'interface'    : self.interface,
        'action'       : self.action,
        'description'  : self.description,
        'date_format'  : self.date_format,
        'valid_keys'   : self.valid_keys,
        'payload'      : self.payload,
        'json_data'    : self.json_data,
        'param_data'   : self.param_data,
        'response_data': self.response_data
      })
    
    if self.dev_mode:
      logging.info('Command.dumps(%s).response: %s' % (str(attribs), str(response)))
    
    return response
  

