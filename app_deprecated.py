olddevfunctions:

# /admin/update/

def extensions(self, request_details, params):
    """
    Method for triggering and updating extensions.
    """

    if verify_admin(request_details['userId']) is False:
        return {'success': False, 'status_code': 403, 'body': False} # forbidden

    details = {
        'action': 'admin_update',
        'description': 'Matchings performed by the extensions subroutine.',
        'validKeys': [
            'email',
            'admittedSemester'
        ]
    }
    details.update(request_details)

    query = db.GqlQuery("SELECT * FROM RkhskMember WHERE email = NULL")
    updates = []
    for member in query:
        match = db.GqlQuery(
            "SELECT * FROM OwaUser WHERE firstName = '%s' AND lastName = '%s' AND category = 'Student'" % (
            member.firstName, member.lastName)).get()
        if match:
            updatedict = {
                'personalId': member.personalId,
                'email': match.email
            }
            if re.match('[a-z]{4}[hv]k\d\d@rkh\.se', match.email) is not None:
                updatedict.update({'admittedSemester': match.email[4:8].upper()})
            updates.append(updatedict)
        else:
            logging.info('nomatch for %s' % member.personalId)
        if len(updates) > 10:
            taskqueue.add(queue_name='registrator', url='/tasks/apply_owa_extensions/',
                          params={'updates': pickle.dumps(updates), 'details': pickle.dumps(details)})
            updates = []

    if any(updates):
        taskqueue.add(queue_name='registrator', url='/tasks/apply_owa_extensions/',
                      params={'updates': pickle.dumps(updates), 'details': pickle.dumps(details)})
        return {'success': True, 'membersExtended': len(updates)}
    else:
        return {'success': True, 'membersExtended': 0}


def owa_contacts(self, request_details, contacts): # DEV
    """
    Method for collecting and storing harvest from Outlook Web Access.
    """

    if verify_admin(request_details['userId']) is False:
        return {'success': False, 'status_code': 403, 'body': False} # forbidden

    if DEVMODE is False:
        logging.warning('Method RkhskAdminUpdate.owa_contacts() called with devmode disabled.')
        return {'success': False, 'status_code': 403, 'body': False} # forbidden

    details = {
        'action': 'admin_update',
        'description': 'Refreshing Outlook Web Access harvest.'
    }
    details.update(request_details)

    updates = []
    for i in range(len(contacts)):
        name = contacts[i]['name']
        splitname = name.split(' ')
        if len(splitname) == 1:
            splitname = (splitname[0], '')
        elif len(splitname) == 2:
            splitname = (splitname[0], splitname[1])
        elif len(splitname) > 2:
            splitname = (splitname[0], string.join(splitname[1:]))
        if len(name) == 0:
            name = contacts[i]['group']
        updates.append({
            'key_name': name,
            'category': contacts[i]['category'],
            'email': contacts[i]['email'],
            'group': contacts[i]['group'],
            'phone': contacts[i]['phone'],
            'office': contacts[i]['office'],
            'company': contacts[i]['company'],
            'firstName': splitname[0],
            'lastName': splitname[1]
        })

    if OwaUser.create_or_update(details, updates):
        return {'success': True, 'owaContactsAddedOrUpdated': len(updates)}
    else:
        return {'success': False, 'status_code': 400, 'body': False} # bad request


        def batch(self, request_details, members): # DEV
            """
            Method for performing batch updates.
            """

            if verify_admin(request_details['userId']) is False:
                return {'success': False, 'status_code': 403, 'body': False} # forbidden

            if DEVMODE is False:
                logging.warning('Method RkhskAdminInterface.batch() called with DEVMODE disabled.')
                return {'success': False, 'status_code': 403, 'body': False} # forbidden

            details = {
                'action': 'admin_update',
                'description': 'Batch update of stored members.',
                'validKeys': [
                    'lastMembershipFor',
                    'firstName',
                    'lastName',
                    'streetCo',
                    'streetAddress',
                    'streetNumber',
                    'streetEntrance',
                    'streetApartment',
                    'streetFloor',
                    'zipCode',
                    'city',
                    'country',
                    'email',
                    'phone',
                    'lastPaymentRecieved',
                    'lastApplicationDate',
                    'lastSscoReg',
                    'lastMecenatReg',
                    'lastPaymentRecieved',
                    'admittedSemester',
                    'lastApplicationDate',
                    'lastApplicationFor',
                    'studentConfirmation',
                    'sys_accounttype',
                    'sys_su'
                ]
            }
            details.update(request_details)

            if RkhskMember.create_or_update(details, members):
                return {'success': True, 'membersUpdatedCount': len(members)}
            else:
                return {'success': False, 'status_code': 400, 'body': False} # bad request




                # TODO(mats.blomdahl@gmail.com): Deprecated.
                # usage: taskqueue.add(queue_name = 'registrator', url = '/tasks/sync_to_mecenat/',
                # params = { 'personal_id': member.personalId, 'activator': details['userId'] })
                def __sync_to_mecenat(self):
                    """
                    Method for auditing the sync-to-Mecenat process and, when appropriate,
                    update associated member records.
                    """

                    pass

                    from google.appengine.api import urlfetch
                    from HTMLParser import HTMLParser
                    import string
                    #import poster

                    pushtasks = None
                    personal_id = self.request.get('personal_id', default_value=None)
                    if personal_id is None:
                        logging.info(
                            'sync_to_mecenat: \'personal_id\' parameter missing. Collecting incomplete tasks...')
                        pushtasks = MecenatPushTask.gql("WHERE sys_pushcomplete = FALSE LIMIT 10")
                        if pushtasks is None:
                            logging.warning(
                                'sync_to_mecenat: No task matching \'sys_pushcomplete = FALSE\' query. Aborting...')
                            return False
                    else:
                        pushtasks = MecenatPushTask.gql(
                            "WHERE personalId = '%s' AND sys_pushcomplete = FALSE" % personal_id)
                        if pushtasks is None:
                            logging.warning(
                                'sync_to_mecenat: No task matching \'personalId=%s\' parameter. Aborting...' %
                                personal_id)
                            return False

                    global my_cookies
                    my_cookies = {}
                    domain = "https://www.mecenat.se"
                    default_deadline = 30
                    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.7 (KHTML, ' \
                                 'like Gecko) Chrome/16.0.912.77 Safari/535.7'
                    accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
                    accept_language = 'en-US,en;q=0.8,sv;q=0.6'
                    accept_charset = 'UTF-8,*;q=0.5'

                    def parse_cookies(cookies, set_cookies):
                        set_cookies = re.findall('[a-z\dA-Z\._]+=[a-z\dA-Z\._]+; path=/; HttpOnly', set_cookies)
                        for cookie in set_cookies:
                            trim_at = cookie.find('; path=')
                            cookie = cookie[0:trim_at]
                            cookie = cookie.split('=')
                            cookies.update({cookie[0]: cookie[1]})
                        logging.info('parse-cookies: %s' % cookies)
                        return cookies


                    def format_cookies(cookies):
                        formated = []
                        for cookie in cookies:
                            formated.append('%s=%s' % (cookie, cookies[cookie]))
                        formated = string.join(formated, '; ')
                        logging.info('format-cookies: %s' % formated)
                        return formated


                    def encode_dict(in_dict):
                        out_dict = {}
                        for k, v in in_dict.iteritems():
                            logging.info('encoded_dict: key=%s' % repr(k))
                            logging.info('encoded_dict: value=%s' % repr(v))
                            if isinstance(v, unicode):
                                v = v.encode('utf8')
                            elif isinstance(v, str):
                                # Must be encoded in UTF-8
                                v.decode('utf8')
                            logging.info('encoded_dict: newsvalue=%s' % repr(v))
                            out_dict[k] = v
                        return out_dict


                    def url_get(path, referer=None, success_status=200, cache_control='max-age=0'):
                        global my_cookies
                        headers = {
                            'Cookie': format_cookies(my_cookies),
                            'User-Agent': user_agent,
                            'Accept': accept,
                            'Accept-Language': accept_language,
                            'Accept-Charset': accept_charset,
                            'Cache-Control': cache_control
                        }
                        if referer is not None:
                            headers['Referer'] = domain + referer
                        response = urlfetch.fetch(
                            url=domain + path,
                            headers=headers,
                            deadline=default_deadline
                        )
                        if response.status_code == success_status:
                            if 'set-cookie' in response.headers:
                                my_cookies = parse_cookies(my_cookies, response.headers['set-cookie'])
                            return response
                        else:
                            logging.warning(
                                'sync_to_mecenat: The page \'%s%s\' could not be fetched (status_code=%s, '
                                'content=%s). Aborting...' % (
                                domain, path, response.status_code, response.content))
                            return False


                    def url_post(path, payload, referer=None, success_status=200, cache_control='max-age=0'):
                        logging.info('posting payload: %s' % payload)
                        global my_cookies
                        headers = {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF8',
                            'Cookie': format_cookies(my_cookies),
                            'User-Agent': user_agent,
                            'Accept': accept,
                            'Accept-Language': accept_language,
                            'Accept-Charset': accept_charset,
                            'Cache-Control': cache_control
                        }
                        if referer is not None:
                            headers['Referer'] = domain + referer
                        response = urlfetch.fetch(
                            url=domain + path,
                            payload=urllib.urlencode(payload),
                            method=urlfetch.POST,
                            headers=headers,
                            deadline=default_deadline,
                            follow_redirects=False
                        )
                        if response.status_code == success_status:
                            if 'set-cookie' in response.headers:
                                my_cookies = parse_cookies(my_cookies, response.headers['set-cookie'])
                            return response
                        else:
                            logging.warning(
                                'sync_to_mecenat: The page \'%s%s\' could not be fetched (status_code=%s, '
                                'content=%s). Aborting...' % (
                                domain, path, response.status_code, response.content))
                            return False


                    class LoginPageParser(HTMLParser):
                        def __init__(self, *args, **kwargs):
                            HTMLParser.__init__(self)
                            self.request = {}

                        def get_request(self):
                            return self.request

                        def update_request(self, field):
                            self.request.update(field)

                        def handle_starttag(self, tag, attrs):
                            if tag == 'input' or tag == 'form':
                                name = None
                                value = None
                                for attr, val in attrs:
                                    if attr == "name" and len(val):
                                        name = val
                                    elif attr == "value":
                                        value = val
                                if name:
                                    if name.find('UsernameStudentAdmin') != -1:
                                        value = "roda"
                                    elif name.find('PasswordStudentAdmin') != -1:
                                        value = "ZzvNnbgh"

                                    self.update_request({name: value})

                                return True

                                logging.info("Encountered a start tag: %s and attr: %s" % (tag, attrs))


                    class RegFormParser(HTMLParser):
                        def __init__(self, member, *args, **kwargs):
                            HTMLParser.__init__(self)
                            self.request = {}
                            self.member = member

                        def get_request(self):
                            return self.request

                        def update_request(self, field):
                            self.request.update(field)

                        def handle_starttag(self, tag, attrs):
                            if tag == 'script':
                                address = None
                                for attr, val in attrs:
                                    if attr == 'src' and val.find('radScriptManager') != -1:
                                        address = val.replace('&amp;', '&')

                                if address:
                                    scriptcontent = url_get(path=address)
                                    if scriptcontent is False:
                                        logging.warning(
                                            'sync_to_mecenat.RegFormParser: Scripttag \'%s\' not read.' % address)
                                    else:
                                        index = scriptcontent.find('hf.value += \';;System.Web.Extensions')
                                        scriptcontent = scriptcontent[index + 13:]
                                        index = scriptcontent.find('\';')
                                        scriptcontent = scriptcontent[:index]
                                        self.scriptkey = scriptcontent

                            elif tag == 'input' or tag == 'form':
                                name = None
                                value = u''
                                for attr, val in attrs:
                                    if attr == "name":# and len(val):
                                        name = unicode(val, STDENC)
                                    elif attr == "value":
                                        value = unicode(val, STDENC)

                                if name:
                                    if name.find('tbSSN') != -1:
                                        value = self.member['personalId']
                                    elif name.find('tbFirstname') != -1:
                                        value = self.member['firstName']
                                    elif name.find('tbLastname') != -1:
                                        value = self.member['lastName']
                                    elif name.find('tbCO') != -1:
                                        value = self.member['streetCo']
                                    elif name.find('tbAddress') != -1:
                                        value = self.member['streetAddress']
                                    elif name.find('tbPostalCode') != -1:
                                        value = self.member['zipCode']
                                    elif name.find('tbCity') != -1:
                                        value = self.member['city']
                                    elif name.find('tbCountry') != -1:
                                        value = 'Sverige'
                                    elif name.find('ddRkhskMembers') != -1:
                                        value = '1'
                                    elif name.find('radScriptManager') != -1:
                                        value = self.scriptkey

                                    #urllib.urlencode({ name: value })

                                    self.update_request({name: value})

                                return


                    # get login page
                    path1 = "/student-admin/SignIn.aspx?ReturnUrl=%2fstudent-admin"
                    result = url_get(path=path1, cache_control='no-cache')
                    if result:
                        logging.info('heartbeat1')
                    else:
                        return True # erase task on failure

                    # parse login page
                    html_parser = LoginPageParser()
                    html_parser.feed(result.content)

                    # attempt login
                    result = url_post(path=path1, payload=html_parser.get_request(),
                                      referer='/student-admin/SignIn.aspx?ReturnUrl=%2fstudent-admin',
                                      success_status=304)
                    if result:
                        logging.info('heartbeat2')
                    else:
                        return True # erase task on failure

                    # continue to start page
                    path2 = result.headers['location']
                    result = url_get(path=path2, referer=path1)
                    if result:
                        logging.info('heartbeat3')
                    else:
                        return True # erase task on failure

                    # go to the *add student*-page
                    path3 = path2 + '/lagg-till-student-kort'
                    result = url_get(path=path3, referer=path2)
                    if result:
                        logging.info('heartbeat4')
                    else:
                        return True # erase task on failure

                    for member in pushtasks:
                        ds_data = member.dumps()
                        logging.info('ds_data=%s' % ds_data)
                        #try:
                        #  urllib.urlencode(ds_data)
                        #except:
                        #  logging.warning('sync_to_mecenat: Datastore read failed. Aborting...')
                        #  return True
                        html_parser = RegFormParser(ds_data)
                        html_parser.feed(result.content)
                        requestdata = encode_dict(html_parser.get_request())
                        for item in requestdata:
                            try:
                                logging.info('sync_to_mecenat: Parsing form name... %s' % repr(item))
                                logging.info('sync_to_mecenat: Parsing form parameter... %s=%s' % (
                                repr(item), repr(requestdata[item])))
                            except:
                                member.sys_pushfailures.append(datetime.datetime.today())
                                member.put()
                                logging.warning(
                                    'sync_to_mecenat: Member %s could not be submitted (status_code=%s, content=%s). Aborting...' % (
                                    member.personalId, result.status_code, result.content))
                                continue

                            try:
                                logging.info('sync_to_mecenat: urlencode name: %s' % urllib.urlencode({'foo': item}))
                                logging.info('sync_to_mecenat: urlencode value: %s' % urllib.urlencode(
                                        {'bar': requestdata[item]}))
                            except:
                                member.sys_pushfailures.append(datetime.datetime.today())
                                member.put()
                                logging.warning(
                                    'sync_to_mecenat: Member %s could not be submitted (status_code=%s, content=%s). Aborting...' % (
                                    member.personalId, result.status_code, result.content))
                                continue

                        for item in requestdata:
                            try:
                                logging.info('parsed item: %s' % item)
                                logging.info('parsed value: %s' % repr(requestdata[item].decode('utf8')))
                            except:
                                member.sys_pushfailures.append(datetime.datetime.today())
                                member.put()
                                logging.warning(
                                    'sync_to_mecenat: Member %s could not be submitted (status_code=%s, content=%s). Aborting...' % (
                                    member.personalId, result.status_code, result.content))
                                continue
                        try:
                            requestdata = urllib.urlencode(requestdata)
                            result = url_post(path=path3, payload=requestdata, referer=path3)
                        except (DownloadError):
                            return False

                        if result:
                            memcache.set('temp', result)
                            logging.info('sync_to_mecenat: Successfully added member %s' % member.personalId)
                            member.sys_pushsuccesses.append(datetime.datetime.today())
                            member.save_success_state()
                        else:
                            logging.warning(
                                'sync_to_mecenat: Member %s could not be submitted (status_code=%s, content=%s). Aborting...' % (
                                member.personalId, result.status_code, result.content))

                    return


# TODO(mats.blomdahl@gmail.com): Document.
# usage: taskqueue.add(queue_name = 'registrator', url = '/tasks/apply_owa_extensions/', params = { 'updates': pickle.dumps(updates), 'details': pickle.dumps(details) })
def __apply_owa_extensions(self):
    """A task that applies additional information to members based on the
      stored Outlook Web Access contacts data.

    This task belong to the queue 'registrator' and distribute messages to
    connected admins through App Engines' channel service.

    To use:
    >>> taskqueue.add(
          queue_name = 'broadcaster'+str(random.randrange(4)),
          url = '/tasks/send_member_activation_notice/',
          params = {
            'updates': pickle.dumps(datetime.datetime.today()),
            'details': pickle.dumps(datetime.datetime.today()),
            'remote_event': <event name>,
            'data'        : json.dumps(data)
          }
        )


    Arguments (i.e. HTTP-POST params):
      broadcaster: The broadcasters' key name/personal id, a string.
      timestamp: A pickled datetime instance (UTC).
      remote_event: The event to be broadcasted, one of: 'admin_connected',
        'admin_disconnected', 'member_created', 'member_updated',
        'mecenat_sync_failed', 'mecenat_sync_completed',
        'mecenat_sync_data_downloaded', 'mecenat_sync_initiated',
        'ssco_sync_failed', 'ssco_sync_completed', 'ssco_sync_data_downloaded'
        and 'ssco_sync_initiated'.
      data: Semi-optional payload for the broadcast, as JSON.
    """

    details = pickle.loads(str(self.request.get('details')))
    updates = pickle.loads(str(self.request.get('updates')))

    if RkhskMember.create_or_update(details, updates):
        logging.info('OWA extension completed.')
    else:
        logging.warning('OWA extension failed.')

    return




    # TODO(mats.blomdahl@gmail.com): Document.
    class DirectMarketingUris(db.Model):
        """
        A unique url for fast and easy direct marketing by email.
        """

        personalId = db.StringProperty(default='')
        firstName = db.StringProperty(default='')
        lastName = db.StringProperty(default='')
        email = db.EmailProperty()
        streetAddress = db.StringProperty(default='')
        streetNumber = db.StringProperty(default='')
        streetEntrance = db.StringProperty(default='')
        streetApartment = db.StringProperty(default='')
        streetFloor = db.StringProperty(default='')
        zipCode = db.StringProperty(default='')
        city = db.StringProperty(default='')
        phone = db.StringProperty(default='')
        admittedSemester = db.StringProperty(default='')

        sys_tmpuri = db.StringProperty()




